/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Julian Selley (j.selley@man.ac.uk)
 * whilst at the University of Manchester as a Research Assistant. 
 *
 * The initial code base is copyright by the University of Manchester. 
 * Modifications to the initial code base are copyright of their 
 * respective authors, or their employers as appropriate. Authorship 
 * of the modifications may be determined from the ChangeLog placed 
 * at the end of this file
 */

package uk.ac.man.bioinf.io; // Package name inserted by JPack

import java.io.IOException;
import java.io.Reader;
import uk.ac.man.bioinf.sequence.alignment.SequenceAlignment;
import uk.ac.man.bioinf.sequence.identifier.Identifier;

/**
 * This is an interface that provides a definition for any sequence
 * parsers. It takes input and returns a sequence alignment
 * object. Parsers may <b>or</b> may not have state attached to
 * them. Furthermore they may be re-entered <b>or</b> alternatively
 * they may not. You have been warned.
 * <p>
 * One thing which cannot be enforced in the interface is that every
 * parser should have a default constructor (ie no arguments to the
 * constructor). This is because the parsers are generated through
 * Class.newInstance().
 *
 *
 * Created: Thu May 25 23:01:02 2000
 *
 * @author Julian
 * @version $Id: AlignmentInputParser.java,v 1.4 2001/04/11 17:04:43 lord Exp $ 
 */

public interface AlignmentInputParser 
{
  public SequenceAlignment parse( Identifier ident, Reader reader, ParserExceptionHandler eh )
    throws IOException;
  
  /**
   * This method returns a sequence alignment from a buffered reader
   * input stream.
   *
   * @param reader the input stream
   * @param eh exception handler
   * @return the sequence alignment
   */
  public SequenceAlignment parse(Reader reader, ParserExceptionHandler eh)
    throws IOException;

  /**
   * Returns the description of this alignment parser, and should be
   * human readable/understandable.
   *
   * @return the description
   */
  public String getDescription();
}// AlignmentInputParser


/*
 * ChangeLog
 * $Log: AlignmentInputParser.java,v $
 * Revision 1.4  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.3  2000/09/11 13:19:26  lord
 * Added identifier support
 *
 * Revision 1.2  2000/08/01 14:57:46  jns
 * o removal of BioInterface
 *
 * Revision 1.1  2000/07/27 22:02:40  jns
 * o mv from AlignmentParser because will now have output parsers as well
 * o rm the extends BioInterface as this is becoming extinct - Phil did one
 * end of this, but not here - so...
 *
 * Revision 1.3  2000/06/16 09:37:10  jns
 * o added a getDescription() method for a human readable form of
 * what the parser is.
 *
 * Revision 1.2  2000/06/08 21:14:08  jns
 * o removed toSequence function as it defined implementation.
 * o consequently added an exception handler to the parse method.
 *
 * Revision 1.1  2000/06/08 12:32:17  jns
 * o I/O stuff written to parse in a sequence file
 * o currently only written the PIRProtein parser
 * 
 */
