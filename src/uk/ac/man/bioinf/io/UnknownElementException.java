/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/*
 * This software was written by Julian Selley (j.selley@man.ac.uk)
 * whilst at the University of Manchester as a Research Assistant.
 *
 * The initial code base is copyright by the University of Manchester.
 * Modifications to the initial code base are copyright of their
 * respective authors, or their employers as appropriate. Authorship
 * of the modifications may be determined from the ChangeLog placed
 * at the end of this file
 */

package uk.ac.man.bioinf.io; // Package name inserted by JPack

import uk.ac.man.bioinf.sequence.Sequence;
import uk.ac.man.bioinf.sequence.identifier.Identifier;


/**
 * An exception to flag an unrecognised element in a string.
 *
 *
 * Created: Wed Jun  7 13:28:11 2000
 *
 * @author J Selley
 * @version $Id: UnknownElementException.java,v 1.4 2001/04/11 17:04:43 lord Exp $
 */

public class UnknownElementException extends ParserException
{
  public UnknownElementException(int i)
  {
    super(i);
  }

  public UnknownElementException(String message, int i)
  {
    super(message, i);
  }

  public UnknownElementException(int i, String seq) 
  {
    super(i, seq);
  }

  public UnknownElementException(String message, int i, String seq) 
  {
    super(message, i, seq);
  }

  public UnknownElementException(String message, int i, String seq, Identifier seqIdent) 
  {
    super(message, i, seq, seqIdent);
  }
} // UnknownElementException



/*
 * ChangeLog
 * $Log: UnknownElementException.java,v $
 * Revision 1.4  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.3  2000/10/13 14:33:23  jns
 * o sorting out a problem with null elements (X) that I had previously
 * had working, but I then messed it up trying to be smart. I have sorted
 * by taking out the actualy formation of the sequence, but supplied the
 * basic elements for creating the sequence.
 *
 * Revision 1.2  2000/07/27 22:09:37  jns
 * o changed to ParserException as this was mv to new file name
 * o added stuff to handle sequences as well as just position and message
 *
 * Revision 1.1  2000/06/08 12:32:17  jns
 * o I/O stuff written to parse in a sequence file
 * o currently only written the PIRProtein parser
 *
 */
