/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.io; // Package name inserted by JPack
import java.io.PrintStream;
import java.io.PrintWriter;


/**
 * NullParserExceptionHandler.java
 *
 * An exception handler for all seasons
 *
 * Created: Thu Feb  8 21:24:57 2001
 *
 * @author Phillip Lord
 * @version $Id: NullParserExceptionHandler.java,v 1.2 2001/04/11 17:04:43 lord Exp $
 */

public class NullParserExceptionHandler implements ParserExceptionHandler
{
  private PrintWriter stream = new PrintWriter( System.out );
  private boolean dumpStack;
  
  public NullParserExceptionHandler()
  {
    this( true );
  }
  
  public NullParserExceptionHandler( boolean dumpStack )
  {
    this.dumpStack = dumpStack;
  }

  public NullParserExceptionHandler( PrintWriter stream )
  {
    this.stream = stream;
    this.dumpStack = true;
  }
  
  public void handleException( ParserException exp )
  {
    if( dumpStack ) exp.printStackTrace( stream );
  }
} // NullParserExceptionHandler



/*
 * ChangeLog
 * $Log: NullParserExceptionHandler.java,v $
 * Revision 1.2  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.1  2001/02/19 17:45:31  lord
 * Initial checkin
 *
 */
