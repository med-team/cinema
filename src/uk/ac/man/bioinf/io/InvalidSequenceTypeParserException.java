/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Julian Selley (j.selley@man.ac.uk)
 * whilst at the University of Manchester as a Research Assistant. 
 *
 * The initial code base is copyright by the University of Manchester. 
 * Modifications to the initial code base are copyright of their 
 * respective authors, or their employers as appropriate. Authorship 
 * of the modifications may be determined from the ChangeLog placed 
 * at the end of this file
 */

package uk.ac.man.bioinf.io; // Package name inserted by JPack

import uk.ac.man.bioinf.sequence.SequenceType;


/**
 * This exception is cast when there is a mismatch of sequence types
 * when dealing with parsers.
 *
 *
 * Created: Tue Aug  8 12:32:18 2000
 *
 * @author Julian Selley
 * @version $Id: InvalidSequenceTypeParserException.java,v 1.2 2001/04/11 17:04:43 lord Exp $
 */

public class InvalidSequenceTypeParserException extends ParserException
{
  private SequenceType expectedSequenceType;
  
  public InvalidSequenceTypeParserException()
  {
    super("A mismatch of sequence types has occurred!", -1);
  }

  public InvalidSequenceTypeParserException(String message) 
  {
    super(message, -1);
  }
  
  public InvalidSequenceTypeParserException(SequenceType st) 
  {
    super("A mismatch of sequence types has occurred! " + 
	  st.getName() + " was the expected type.", -1);
    this.expectedSequenceType = st;
  }
  
  public InvalidSequenceTypeParserException(String message, SequenceType st) 
  {
    super(message, -1);
    this.expectedSequenceType = st;
  }

  /**
   * Returns the expected sequence type.
   *
   * @return 
   */
  public SequenceType getExpcetedSequenceType() 
  {
    return this.expectedSequenceType;
  }
} // InvalidSequenceTypeException



/*
 * ChangeLog
 * $Log: InvalidSequenceTypeParserException.java,v $
 * Revision 1.2  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.1  2001/01/22 17:10:19  lord
 * Created due to name clash of InvalidSequenceTypeException
 *
 * Revision 1.1  2000/08/21 17:21:23  jns
 * o added output parser stuff to package
 *
 */
