/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/*
 * This software was written by Julian Selley (j.selley@man.ac.uk)
 * whilst at the University of Manchester as a Research Assistant.
 *
 * The initial code base is copyright by the University of Manchester.
 * Modifications to the initial code base are copyright of their
 * respective authors, or their employers as appropriate. Authorship
 * of the modifications may be determined from the ChangeLog placed
 * at the end of this file
 */

package uk.ac.man.bioinf.io; // Package name inserted by JPack

import uk.ac.man.bioinf.sequence.Sequence;
import uk.ac.man.bioinf.sequence.identifier.Identifier;


/**
 * A general exception thrown up when parsing a sequence. The concept
 * is to make this more specific by sub-classing this exception.
 *
 *
 * Created: Tue Jun  6 18:09:26 2000
 *
 * @author J Selley
 * @version $Id: ParserException.java,v 1.3 2001/04/11 17:04:43 lord Exp $
 */

public class ParserException extends RuntimeException
{
  private int position;  // the position of the residue causing the exception
  private String sequence;  // the sequence causing the exception
  private Identifier identifier;  // the sequence identifier - primarily the description

  public ParserException(int i)
  {
    super();
    this.position = i;
  }

  public ParserException(String message, int i)
  {
    super(message);
    this.position = i;
  }

  public ParserException(int i, String seq) 
  {
    super();
    this.position = i;
    this.sequence = seq;
  }

  public ParserException(String message, int i, String seq) 
  {
    super(message);
    this.position = i;
    this.sequence = seq;
  }

  public ParserException(String message, int i, String seq, Identifier ident) 
  {
    super(message);
    this.position = i;
    this.sequence = seq;
    this.identifier = ident;
  }
  
  /**
   * Returns the integer position in the sequence that caused the
   * exception.
   *
   * @return the location
   */
  public int getPosition()
  {
    return this.position;
  }

  /**
   * Returns the sequence that caused the exception.
   *
   * @return the sequence
   */
  public String getSequence() 
  {
    return this.sequence;
  }

  public Identifier getIdentifier() 
  {
    return this.identifier;
  }
} // ParserException



/*
 * ChangeLog
 * $Log: ParserException.java,v $
 * Revision 1.3  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.2  2000/10/13 14:33:23  jns
 * o sorting out a problem with null elements (X) that I had previously
 * had working, but I then messed it up trying to be smart. I have sorted
 * by taking out the actualy formation of the sequence, but supplied the
 * basic elements for creating the sequence.
 *
 * Revision 1.1  2000/07/27 22:06:44  jns
 * o mv to ParserException from ParseException because I kept getting confused
 * and this seemed a logical time to make a change.
 * o added stuff to deal with sequence as well, which I thought might be logical
 * as my error handling so far was just saying the position and not the sequence
 * in the alignment.
 *
 * Revision 1.1  2000/06/08 12:32:17  jns
 * o I/O stuff written to parse in a sequence file
 * o currently only written the PIRProtein parser
 *
 */
