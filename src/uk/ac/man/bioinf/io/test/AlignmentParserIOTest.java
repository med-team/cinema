/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Julian Selley (j.selley@man.ac.uk)
 * whilst at the University of Manchester as a Research Assistant. 
 *
 * The initial code base is copyright by the University of Manchester. 
 * Modifications to the initial code base are copyright of their 
 * respective authors, or their employers as appropriate. Authorship 
 * of the modifications may be determined from the ChangeLog placed 
 * at the end of this file
 */

package uk.ac.man.bioinf.io.test; // Package name inserted by JPack

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import uk.ac.man.bioinf.io.AlignmentInputParser;
import uk.ac.man.bioinf.io.AlignmentOutputParser;
import uk.ac.man.bioinf.io.DefaultParserExceptionHandler;
import uk.ac.man.bioinf.io.ParserExceptionHandler;
import uk.ac.man.bioinf.io.parsers.PIRProteinAlignmentParser;
import uk.ac.man.bioinf.sequence.alignment.SequenceAlignment;


/**
 * AlignmentParserIOTest.java
 *
 *
 * Created: Mon Aug 14 16:46:12 2000
 *
 * @author Julian Selley
 * @version $Id: AlignmentParserIOTest.java,v 1.2 2001/04/11 17:04:43 lord Exp $
 */

public class AlignmentParserIOTest 
{
  public static void main(String[] args) 
  {
    if (args.length != 2) {
      System.err.println("Usage: java AlignmentParserIOTest <input file> <output file>");
      System.err.println("         where:");
      System.err.println("           <input file>  :  is the sequence filename in PIR format");
      System.err.println("           <output file> :  is the sequence filename for output of the PIR format sequence(s)");
      System.err.println("  [well I never said it would be a useful test]");
      System.exit(255);
    }
    
    ParserExceptionHandler eh = new DefaultParserExceptionHandler();  // a default exception handler to chuck out any exceptions
    AlignmentInputParser iParser = new PIRProteinAlignmentParser();  // an input PIR protein alignment parser
    AlignmentOutputParser oParser = new PIRProteinAlignmentParser();  // an output PIR protein alignment parser
    FileReader fr;  // the file reader to obtain the input alignment
    Writer fw;  // the file writer to pass out the alignment
    SequenceAlignment alignment = null;  // the sequence alignment for storage and output
    
    try {
      fr = new FileReader(args[0]);
      alignment = iParser.parse(fr, eh);
      System.out.println("There are " + alignment.getNumberSequences() +
			 " sequences in that alignment!");
    } catch (FileNotFoundException e) {
      System.err.println("Ooopppss, that file doesn't exist!");
      System.exit(255);
    } catch (IOException e) {
      System.err.println("Ooopppss, got an IO Exception!");
      e.printStackTrace();
      System.exit(255);
    }
    
    if (alignment.getNumberSequences() > 0) {
      try {
	fw = new FileWriter(args[1]);
	oParser.write(alignment, fw, eh);
	fw.close();
      } catch (IOException e) {
	System.err.println("Ooopppss, got an IO Exception on output!");
	e.printStackTrace();
	System.exit(255);
      }
    } else {
      System.err.println("The sequence alignment has no sequences in it!");
      System.exit(255);
    }
  }
} // AlignmentParserIOTest



/*
 * ChangeLog
 * $Log: AlignmentParserIOTest.java,v $
 * Revision 1.2  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.1  2000/08/21 17:21:23  jns
 * o added output parser stuff to package
 *
 */
