/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/*
 * This software was written by Julian Selley (j.selley@man.ac.uk)
 * whilst at the University of Manchester as a Research Assistant.
 *
 * The initial code base is copyright by the University of Manchester.
 * Modifications to the initial code base are copyright of their
 * respective authors, or their employers as appropriate. Authorship
 * of the modifications may be determined from the ChangeLog placed
 * at the end of this file
 */

package uk.ac.man.bioinf.io.test; // Package name inserted by JPack

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import uk.ac.man.bioinf.io.AlignmentInputParser;
import uk.ac.man.bioinf.io.DefaultParserExceptionHandler;
import uk.ac.man.bioinf.io.ParserExceptionHandler;
import uk.ac.man.bioinf.io.parsers.PIRProteinAlignmentParser;
import uk.ac.man.bioinf.sequence.Sequences;
import uk.ac.man.bioinf.sequence.alignment.SequenceAlignment;


/**
 * AlignmentParserInputTest.java
 *
 *
 * Created: Wed Jun  7 19:18:07 2000
 *
 * @author J Selley
 * @version $Id: AlignmentParserInputTest.java,v 1.2 2001/04/11 17:04:43 lord Exp $
 */

public class AlignmentParserInputTest
{
  public static void main(String[] args)
  {
    if (args.length == 0) {
      System.err.println("No filename supplied!");
      System.exit(255);
    }

    ParserExceptionHandler eh = new DefaultParserExceptionHandler();
    AlignmentInputParser parser = new PIRProteinAlignmentParser();
    FileReader fr;
    SequenceAlignment alignment;

    try {
      fr = new FileReader(args[0]);
      alignment = parser.parse(fr, eh);
      System.out.println("There are " + alignment.getNumberSequences() + " sequences in that alignment!");
      System.out.println("  they were:");
      Sequences.printAlignment(alignment);
    } catch (FileNotFoundException e) {
      System.err.println("Ooopppss, that file doesn't exist!");
      System.exit(255);
    } catch (IOException e) {
      System.err.println("Ooopppss, got an IO Exception!");
      e.printStackTrace();
      System.exit(255);
    }
  }
} // AlignmentParserInputTest



/*
 * ChangeLog
 * $Log: AlignmentParserInputTest.java,v $
 * Revision 1.2  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.1  2000/07/27 22:35:08  jns
 * o mv from AlignmentParserTest to AlignmentParserInputTest as this allows
 * for output stuff to be done now.
 *
 * Revision 1.2  2000/06/08 21:23:30  jns
 * o changed to fit new non-state parser model
 *
 * Revision 1.1  2000/06/08 12:32:17  jns
 * o I/O stuff written to parse in a sequence file
 * o currently only written the PIRProtein parser
 *
 */
