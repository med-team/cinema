/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Julian Selley (j.selley@man.ac.uk)
 * whilst at the University of Manchester as a Research Assistant. 
 *
 * The initial code base is copyright by the University of Manchester. 
 * Modifications to the initial code base are copyright of their 
 * respective authors, or their employers as appropriate. Authorship 
 * of the modifications may be determined from the ChangeLog placed 
 * at the end of this file
 */

package uk.ac.man.bioinf.io; // Package name inserted by JPack

import java.io.Writer;
import uk.ac.man.bioinf.io.ParserExceptionHandler;
import uk.ac.man.bioinf.sequence.alignment.SequenceAlignment;

/**
 * This is an interface that provides a definition for any sequence
 * output parsing. It takes a sequence alignment and returns a
 * writer. Parsers may <b>or</b> may not have state attached to them,
 * and furthermore, they may be re-entered <b>or</b> alternately they
 * may not. You have been warned!
 * <p>
 * One thing that cannot be enforced in the interface is, every parser
 * should have a default constructor (i.e., no arguments to the
 * constructor). This is because the parsers are generated through
 * Class.newInstance().
 *
 *
 * Created: Mon Jul 31 13:04:06 2000
 *
 * @author Julian Selley
 * @version $Id: AlignmentOutputParser.java,v 1.3 2001/04/11 17:04:43 lord Exp $ 
 */

public interface AlignmentOutputParser 
{
  /**
   * Returns a writer for output, when provided with a sequence
   * alignment. Exceptions are handled by the supplied exception
   * handler.
   *
   * @param sa the sequence alignment
   * @param eh the exception handler
   * @return the writer for output
   */
  public Writer write(SequenceAlignment sa, Writer writer, ParserExceptionHandler eh);
  
  /**
   * Returns the description of this alignment output parser, and
   * should be human readable/understandable.
   *
   * @return the description
   */
  public String getDescription();
}// AlignmentOutputParser


/*
 * ChangeLog
 * $Log: AlignmentOutputParser.java,v $
 * Revision 1.3  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.2  2001/01/31 18:02:26  lord
 * Now returns the writer, rather than void. Largely because it can.
 *
 * Revision 1.1  2000/08/21 17:21:23  jns
 * o added output parser stuff to package
 * 
 */

