/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.io.parsers; // Package name inserted by JPack
import uk.ac.man.bioinf.io.ParserExceptionHandler;
import uk.ac.man.bioinf.io.SequenceInputParser;
import uk.ac.man.bioinf.io.UnknownElementException;
import uk.ac.man.bioinf.sequence.Element;
import uk.ac.man.bioinf.sequence.Sequence;
import uk.ac.man.bioinf.sequence.Sequences;
import uk.ac.man.bioinf.sequence.identifier.Identifier;
import uk.ac.man.bioinf.sequence.types.AminoAcid;
import uk.ac.man.bioinf.sequence.types.ProteinSequenceType;


/**
 * AbstractProteinSequenceInputParser.java
 *
 * Provides a root class for all protein sequence parsers. 
 *
 * Created: Wed Feb  7 15:08:25 2001
 *
 * @author Phillip Lord
 * @version $Id: AbstractProteinSequenceInputParser.java,v 1.2 2001/04/11 17:04:43 lord Exp $
 */

public abstract class AbstractProteinSequenceInputParser implements SequenceInputParser
{  
  private static AminoAcid[] aa;

  static
  {
    AminoAcid[] all = AminoAcid.getAll();
    aa = new AminoAcid[26];

    for (int i = 0; i < all.length; i++)
      aa[(int)all[i].toChar() - 65] = all[i];
  }
  
  
  protected Sequence toSequence
    ( Identifier ident, String seq, ParserExceptionHandler eh )
  {
    return Sequences.getElementsAsSequence
      ( toElements( seq, eh ), ProteinSequenceType.getInstance(), ident );
  }
        
  protected Element[] toElements
    ( String seq, ParserExceptionHandler eh )
  {
    Element[] elements = new Element[ seq.length() ];
    
    for( int i = 0; i < seq.length(); i++ ){
      int elem = (int)seq.charAt( i ) - 65;
      if( elem >= elements.length ){
        eh.handleException( createException( i, (char)elem ) );
      }
      
      elements[ i ] = aa[ (int)seq.charAt( i ) - 65 ];
      if( elements[ i ] == null ){
        System.out.println( "Element not recognised is " + (seq.charAt( i )) );
        
        eh.handleException( createException( i, (char)elem ) );
      }
    }
    
    return elements;
  }
  
  protected UnknownElementException createException( int position, char elem )
  {
    return new UnknownElementException
      ( "Element (" + elem + ") at position " + (position + 1) + 
        " was not recognised", (position + 1) );
  }
  
} // AbstractProteinSequenceInputParser



/*
 * ChangeLog
 * $Log: AbstractProteinSequenceInputParser.java,v $
 * Revision 1.2  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.1  2001/02/19 17:45:48  lord
 * Initial checkin
 *
 */
