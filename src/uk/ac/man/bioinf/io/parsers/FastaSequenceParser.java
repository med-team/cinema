/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.io.parsers; // Package name inserted by JPack

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import uk.ac.man.bioinf.io.ParserExceptionHandler;
import uk.ac.man.bioinf.io.SequenceInputParser;
import uk.ac.man.bioinf.sequence.Sequence;
import uk.ac.man.bioinf.sequence.identifier.Identifier;
import uk.ac.man.bioinf.sequence.identifier.SimpleIdentifier;


/**
 * FastaSequenceParser.java
 *
 *
 * Created: Wed Feb  7 15:07:45 2001
 *
 * @author Phillip Lord
 * @version $Id: FastaSequenceParser.java,v 1.2 2001/04/11 17:04:43 lord Exp $
 */

public class FastaSequenceParser extends AbstractProteinSequenceInputParser
{
  
  public Sequence parse( Reader reader, ParserExceptionHandler exh ) throws IOException
  {
    String currentLine;
    BufferedReader read = new BufferedReader( reader );
    
    while( (currentLine = read.readLine()) != null 
           && !currentLine.startsWith( ">" ) );
    
    // need to have a specific Identifier for Fasta which is more descriptive. 
    return parseSequence( new SimpleIdentifier( currentLine ), read, exh );
  }
  
  public Sequence parse( Identifier ident, Reader reader, ParserExceptionHandler exh ) 
    throws IOException
  {
    String currentLine;
    BufferedReader read = new BufferedReader( reader );
    
    while( !(currentLine = read.readLine() ).startsWith( ">" ) );
    
    return parseSequence( ident, read, exh );
  }
  
  private Sequence parseSequence( Identifier ident, BufferedReader reader, ParserExceptionHandler eh )
    throws IOException
  {
    boolean cont = true;
    StringBuffer seq = new StringBuffer();

    String currentLine;
    
    while( (currentLine  = reader.readLine()) != null ){
      
      currentLine = currentLine.trim();
      
      if( currentLine.endsWith( "*" ) ){
        cont = false;
        // lop off the last character, which is the *
        currentLine = currentLine.substring( 0, currentLine.length() - 2 );
      }
     
      seq.append( currentLine );
    }

    return toSequence( ident, seq.toString(), eh );
  }
  
  public String getDescription()
  {
    return "Fasta format parser";
  }
} // FastaSequenceParser



/*
 * ChangeLog
 * $Log: FastaSequenceParser.java,v $
 * Revision 1.2  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.1  2001/02/19 17:45:48  lord
 * Initial checkin
 *
 */
