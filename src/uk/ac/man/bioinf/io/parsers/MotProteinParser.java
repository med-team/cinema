/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.io.parsers; // Package name inserted by JPack
import java.io.Writer;
import java.text.DateFormat;
import java.util.Date;
import uk.ac.man.bioinf.io.AlignmentOutputParser;
import uk.ac.man.bioinf.io.InvalidSequenceTypeParserException;
import uk.ac.man.bioinf.io.ParserExceptionHandler;
import uk.ac.man.bioinf.sequence.InvalidSequenceTypeException;
import uk.ac.man.bioinf.sequence.Sequences;
import uk.ac.man.bioinf.sequence.alignment.GappedSequence;
import uk.ac.man.bioinf.sequence.alignment.SequenceAlignment;
import uk.ac.man.bioinf.sequence.types.ProteinSequenceType;
import java.io.IOException;
import uk.ac.man.bioinf.io.SequenceWriterException;


/**
 * MotProteinParser.java
 *
 * Mot files are used by several of the applications that are used to
 * produce PRINTS. Its a very simple format. One line per sequence
 * (motif files tend to be fairly short alignments). Comments are
 * designated with a #. PRINTS motif's can't contain gaps so by
 * definition here this parser does not like gaps and will not write
 * them out. 
 *
 * Created: Mon Jan 22 16:44:15 2001
 *
 * @author Phillip Lord
 * @version $Id: MotProteinParser.java,v 1.3 2001/04/11 17:04:43 lord Exp $
 */

public class MotProteinParser implements AlignmentOutputParser
{

  public Writer write( SequenceAlignment sa, Writer writer, ParserExceptionHandler eh )
  {
    try{
      
      // is this off the right type. 
      if( sa.getSequenceType() != ProteinSequenceType.getInstance() ){
        throw new InvalidSequenceTypeParserException( ProteinSequenceType.getInstance() );
      }
      
      writer.write
        ( "# Produced by Cinema " + DateFormat.getTimeInstance().format( new Date() ) + "\n");
      
      Sequences.printAlignment( sa );
      
      for( int i = 1; i < sa.getNumberSequences() + 1; i++ ){
        
        GappedSequence seq = sa.getSequenceAt( i );
        Sequences.printSequence( seq );
        
        if( sa.getInset( i ) != 0 ) 
          throw new InvalidSequenceTypeException( "Mot files can not contain any gaps" );
        
        String seqString = Sequences.getGappedSequenceAsString( seq );
        writer.write( seqString );
        writer.write( "\n" );
      }
    }
    catch( InvalidSequenceTypeParserException istpe ){
      eh.handleException( istpe );
    }
    catch( IOException exp ){
      exp.printStackTrace();
      eh.handleException( new SequenceWriterException() );
    }
    return writer;
  }
  
  public String getDescription()
  {
    return "Parser for .mot file format." ;
  }
} // MotProteinParser




/*
 * ChangeLog
 * $Log: MotProteinParser.java,v $
 * Revision 1.3  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.2  2001/01/31 18:02:52  lord
 * Now returns the writer, rather than void. Largely because it can.
 *
 * Revision 1.1  2001/01/23 18:00:08  lord
 * Initial checkin
 *
 */
