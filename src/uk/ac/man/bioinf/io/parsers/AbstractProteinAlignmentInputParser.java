/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Julian Selley (j.selley@man.ac.uk)
 * whilst at the University of Manchester as a Research Assistant. 
 *
 * The initial code base is copyright by the University of Manchester. 
 * Modifications to the initial code base are copyright of their 
 * respective authors, or their employers as appropriate. Authorship 
 * of the modifications may be determined from the ChangeLog placed 
 * at the end of this file
 */

package uk.ac.man.bioinf.io.parsers; // Package name inserted by JPack

import uk.ac.man.bioinf.io.AlignmentInputParser;
import uk.ac.man.bioinf.io.ParserExceptionHandler;
import uk.ac.man.bioinf.io.UnknownElementException;
import uk.ac.man.bioinf.sequence.Element;
import uk.ac.man.bioinf.sequence.Sequences;
import uk.ac.man.bioinf.sequence.alignment.Gap;
import uk.ac.man.bioinf.sequence.alignment.GappedSequence;
import uk.ac.man.bioinf.sequence.identifier.SimpleIdentifier;
import uk.ac.man.bioinf.sequence.types.AminoAcid;
import uk.ac.man.bioinf.sequence.types.ProteinSequenceType;


/**
 * Provides a function to parse amino acids from the input stream.
 *
 *
 * Created: Fri May 26 12:32:09 2000
 *
 * @author J Selley
 * @version $Id: AbstractProteinAlignmentInputParser.java,v 1.6 2001/04/11 17:04:43 lord Exp $
 */

public abstract class AbstractProteinAlignmentInputParser
  implements AlignmentInputParser
{
  private static AminoAcid[] aa;

  static
  {
    AminoAcid[] all = AminoAcid.getAll();
    aa = new AminoAcid[26];

    for (int i = 0; i < all.length; i++)
      aa[(int)all[i].toChar() - 65] = all[i];
  }
  
  protected GappedSequence toSequence
    (String seq, String description, ParserExceptionHandler eh)
  {
    Element[] elements = new Element[seq.length()];

    // cycle through sequence string and generate an set of
    // amino-acids/gaps.
    for (int i = 0; i < seq.length(); i++) {
      if (seq.charAt(i) == Gap.GAP.toChar())
	elements[i] = Gap.GAP;
      else {
	try {
	  elements[i] = aa[((int)seq.charAt(i) - 65)];
	  if (elements[i] == null) {
	    eh.handleException(new UnknownElementException
	      ("Element (" + seq.charAt(i) + ") at position " + (i + 1) +
	       " was not recognised", (i+1), seq, 
	       new SimpleIdentifier(description)));
	    elements[i] = Gap.GAP;
	  }
	} catch (ArrayIndexOutOfBoundsException e) {
	  eh.handleException(new UnknownElementException
	    ("Element (" + seq.charAt(i) + ") at position " + (i + 1) +
	     " was not recognised", (i + 1), seq, 
	     new SimpleIdentifier(description)));
	  elements[i] = Gap.GAP;
	}
      }
    }

    return Sequences.getElementsAsGappedSequence
      (elements, ProteinSequenceType.getInstance(), 
       new SimpleIdentifier(description));
  }
} // AbstractProteinAlignmentInputParser



/*
 * ChangeLog
 * $Log: AbstractProteinAlignmentInputParser.java,v $
 * Revision 1.6  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.5  2000/10/13 14:33:23  jns
 * o sorting out a problem with null elements (X) that I had previously
 * had working, but I then messed it up trying to be smart. I have sorted
 * by taking out the actualy formation of the sequence, but supplied the
 * basic elements for creating the sequence.
 *
 * Revision 1.4  2000/09/11 13:19:07  lord
 * Added identifier support
 *
 * Revision 1.3  2000/08/01 14:58:15  jns
 * o removal of BioObject
 *
 * Revision 1.2  2000/07/27 22:34:00  jns
 * o uncalculated compile problem now sorted - but not sure it is an ideal
 * solution.
 *
 * Revision 1.1  2000/07/27 22:11:33  jns
 * o mv because now will have output parsers as well as input stuff
 * o also catered for sequence causeing exceptions - enable tighter exception
 * handling
 *
 * Revision 1.5  2000/07/18 11:10:23  lord
 * Import rationalisation.
 * Changes due to removal of biointerface
 *
 * Revision 1.4  2000/06/16 11:36:29  jns
 * o adding chomp of leading gaps into code
 *
 * Revision 1.3  2000/06/08 21:22:20  jns
 * o forgot to remove method parse
 *
 * Revision 1.2  2000/06/08 21:18:01  jns
 * o made the toSequence method, protected rather than public
 *
 * Revision 1.1  2000/06/08 12:32:17  jns
 * o I/O stuff written to parse in a sequence file
 * o currently only written the PIRProtein parser
 *
 */
