/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/*
 * This software was written by Julian Selley (j.selley@man.ac.uk)
 * whilst at the University of Manchester as a Research Assistant.
 *
 * The initial code base is copyright by the University of Manchester.
 * Modifications to the initial code base are copyright of their
 * respective authors, or their employers as appropriate. Authorship
 * of the modifications may be determined from the ChangeLog placed
 * at the end of this file
 */

package uk.ac.man.bioinf.io; // Package name inserted by JPack


/**
 * This parser exception handler does very little.
 *
 *
 * Created: Sat Jun  3 18:14:01 2000
 *
 * @author Julian Selley
 * @version $Id: DefaultParserExceptionHandler.java,v 1.3 2001/04/11 17:04:43 lord Exp $
 */

public class DefaultParserExceptionHandler 
  implements ParserExceptionHandler
{
  public DefaultParserExceptionHandler() {}

  public void handleException(ParserException e)
  {
    if (e.getMessage() != null)
      System.err.println(e.getMessage());
    throw e;
  }
} // DefaultParserExceptionHandler



/*
 * ChangeLog
 * $Log: DefaultParserExceptionHandler.java,v $
 * Revision 1.3  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.2  2000/07/27 22:04:37  jns
 * o rm the extends BioObject as this is being retired for a newer and better
 * system, as yet unrevieled
 * o added to deal with ParserException rather than ParseException - I felt
 * this was a better name as I invariably forgot when we were dealing with
 * ParseExceptions and when we were dealing with ParserExceptionHandlers.
 *
 * Revision 1.1  2000/06/08 12:32:17  jns
 * o I/O stuff written to parse in a sequence file
 * o currently only written the PIRProtein parser
 *
 */
