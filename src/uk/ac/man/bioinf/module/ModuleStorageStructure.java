/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/*
 * This software was written by Julian Selley (j.selley@man.ac.uk)
 * whilst at the University of Manchester as a Research Assistant.
 *
 * The initial code base is copyright by the University of Manchester.
 * Modifications to the initial code base are copyright of their
 * respective authors, or their employers as appropriate. Authorship
 * of the modifications may be determined from the ChangeLog placed
 * at the end of this file
 */

package uk.ac.man.bioinf.module; // Package name inserted by JPack

import java.util.Arrays;


/**
 * ModuleStorageStructure.java
 *
 * Stores the module (if it exists), the context, the module
 * configuration object and the required modules for one module
 * identifier. This class is currently exclusively used in the
 * DefaultModuleFactoryInstance.
 *
 * Created: Wed May 10 17:02:26 2000
 *
 * @author J Selley
 * @version $Id: ModuleStorageStructure.java,v 1.6 2001/04/11 17:04:43 lord Exp $
 */

public class ModuleStorageStructure 
{
  private Module module;
  private Module[] requiredModules;
  private ModuleContext context;
  private Object config;
  private ModuleIdentifier concrete;

  public ModuleStorageStructure() {}

  public ModuleStorageStructure(ModuleContext context)
  {
    this.context = context;
  }

  public ModuleStorageStructure(ModuleContext context, Object config)
  {
    this.context = context;
    this.config = config;
  }

  public void setModule(Module module) throws ModuleException
  {
    if (this.module != null)
      throw new
	ModuleException("Module already defined in ModuleStorageStructure (" +
			this + ")", ModuleExceptionType.MODULE_EXISTS);
    else
      this.module = module;
  }

  public Module getModule()
  {
    return this.module;
  }

  public void addRequiredModules(Module[] reqMods)
  {
    Module[] newModules;
    int reqModsLength = (this.requiredModules != null) ? this.requiredModules.length : 0;
    int givenModsLength = reqMods.length;

    // generate correct sized array of new modules
    newModules = new Module[reqModsLength + givenModsLength];

    // put old required modules into new modules
    for (int i = 0; i < reqModsLength; i++) {
      newModules[i] = this.requiredModules[i];
    }

    // put given req. mods into the new modules
    for (int i = reqModsLength; i < reqModsLength + givenModsLength; i++) {
      newModules[i] = reqMods[i - reqModsLength];
    }

    // set new mods to req mods of this object
    this.requiredModules = newModules;
  }
  
  public void addRequiredModule(Module reqMod)
  {
    // if there are previous required modules
    if (this.requiredModules != null) {
      // generate new modules array
      Module[] newModules = new Module[this.requiredModules.length + 1];
      
      // put previous required modules into new modules array
      for (int i = 0; i < this.requiredModules.length; i++) {
	newModules[i] = this.requiredModules[i];
      }
      // add the new req. mod. as the last module
      newModules[this.requiredModules.length] = reqMod;

      // set the new mods to the req mods
      this.requiredModules = newModules;

    } else {
      this.requiredModules = new Module[1];
      this.requiredModules[0] = reqMod;
    }
  }

  public ModuleList getRequiredModules()
  {
    for( int i = 0; i < requiredModules.length; i++ ){
      if( !requiredModules[ i ].isStarted() ){
	requiredModules[ i ].runStart();
      }
    }

    return new ModuleList( Arrays.asList( requiredModules ) );
  }

  public void setContext(ModuleContext context)
  {
    this.context = context;
  }
  
  public ModuleContext getContext()
  {
    return this.context;
  }

  public void setConfig(Object config)
  {
      this.config = config;
  }

  public Object getConfig()
  {
    return config;
  }

  public void setConcreteIdentifier( ModuleIdentifier ident )
  {
    this.concrete = ident;
  }
  
  public ModuleIdentifier getConcreteIdentifier()
  {
    return concrete;
  }
} // ModuleStorageStructure



/*
 * ChangeLog
 * $Log: ModuleStorageStructure.java,v $
 * Revision 1.6  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.5  2001/01/31 18:03:53  lord
 * Changes due to removal of InterfaceIdentifier
 *
 * Revision 1.4  2000/06/16 09:40:07  jns
 * o removed existence of BioInterface stuff, as it isn't really relevent here.
 * Plus, in the pipe work is a reworking of what the BioInterface will
 * represent and do.
 *
 * Revision 1.3  2000/05/30 16:27:56  lord
 * Changes to ensure that modules are started.
 *
 * Revision 1.2  2000/05/24 15:38:37  jns
 * o added ModuleList and MIList stuff and altered java to return module lists
 * adn MILists rather than arrays.
 *
 * Revision 1.1  2000/05/24 13:09:42  jns
 * o initial coding of module package
 *
 */
