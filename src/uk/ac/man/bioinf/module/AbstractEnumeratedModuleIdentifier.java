/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.module; // Package name inserted by JPack
import uk.ac.man.bioinf.util.AbstractEnumeration;
import java.lang.reflect.Field;
import uk.ac.man.bioinf.debug.Debug;
import java.lang.reflect.Modifier;


/**
 * AbstractEnumeratedModuleIdentifier.java
 *
 *
 * Created: Sun May  7 17:42:13 2000
 *
 * @author Phillip Lord
 * @version $Id: AbstractEnumeratedModuleIdentifier.java,v 1.4 2001/04/11 17:04:43 lord Exp $
 */

public abstract class AbstractEnumeratedModuleIdentifier extends AbstractEnumeration
  implements ModuleIdentifier
{
  private String moduleName, className;
  private boolean isInterface;
  
  public AbstractEnumeratedModuleIdentifier( String className, String toString )
  {
    this( className, toString, false );
  }
  
  public AbstractEnumeratedModuleIdentifier( String className, String toString, boolean isInterface )
  {
    super( toString );
    this.className = className;
    this.isInterface = isInterface;
  }
  
  public static ModuleIdentifier[] getAllIdentifiers( Class cla )
  {
    // java casting,
    // I hate java casting,
    // tra-la, tra-la, la la ley
    AbstractEnumeration[] enumArray = getAllElements( cla );
    ModuleIdentifier[] moduleArray = new ModuleIdentifier[ enumArray.length ];
    
    System.arraycopy( enumArray, 0, moduleArray, 0, enumArray.length );
    return moduleArray;
  }
  
  /**
   * This method uses reflection to make sure that the module name
   * returned is the same as its Field. This means that I dont have to
   * ensure that I get problems with name typo.
   */
  private void initModuleNames()
  {
    Class cla = getClass();
    Field[] fields = cla.getDeclaredFields();
    for( int i = 0; i < fields.length; i++ ){
      try{
	// only bother for static fields. We need to check this
	// otherwise the get( null ) call with raise a NPE also
	if( Modifier.isStatic( fields[ i ].getModifiers() ) ){
	  if( fields[ i ].get( null ) == this ){
	    moduleName = fields[ i ].getName();
	  }
	}
      }
      catch( IllegalAccessException iae ){
	// This can not happen because the class is this class, and so
	// we can access everything
	if( Debug.debug )
	  Debug.both( this, "Design mistake in software!", iae );
      }
    }
  }
    
  public String getModuleName()
  {
    if( moduleName == null ) 
      initModuleNames();
    return moduleName;
  }
  
  public String getClassName()
  {
    return className;
  }
  
  public boolean isInterface()
  {
    return isInterface;
  }
  
  public String toString()
  {
    return "Enumerated Module Identifier:- " + super.toString();
  }
} // AbstractEnumeratedModuleIdentifier



/*
 * ChangeLog
 * $Log: AbstractEnumeratedModuleIdentifier.java,v $
 * Revision 1.4  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.3  2001/01/31 18:03:53  lord
 * Changes due to removal of InterfaceIdentifier
 *
 * Revision 1.2  2000/08/03 16:40:37  lord
 * Got Interface Modules working correctly which required quite a few changes
 *
 * Revision 1.1  2000/07/25 12:00:36  lord
 * Renamed from AbstractEnumeratedModuleIndentifier
 *
 * Revision 1.4  2000/06/27 16:07:36  lord
 * Removed debug System.out
 *
 * Revision 1.3  2000/06/16 09:40:07  jns
 * o removed existence of BioInterface stuff, as it isn't really relevent here.
 * Plus, in the pipe work is a reworking of what the BioInterface will
 * represent and do.
 *
 * Revision 1.2  2000/05/30 16:23:35  lord
 * Method name changes
 *
 * Revision 1.1  2000/05/08 18:06:27  lord
 * Initial checkin
 *
 */
