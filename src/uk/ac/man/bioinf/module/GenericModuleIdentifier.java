/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.module; // Package name inserted by JPack


/**
 * GenericModuleIdentifier.java
 *
 *
 * Created: Mon May 15 16:02:45 2000
 *
 * @author Phillip Lord
 * @version $Id: GenericModuleIdentifier.java,v 1.6 2001/04/11 17:04:43 lord Exp $
 */

public class GenericModuleIdentifier implements ModuleIdentifier
{

  private String className, moduleName;
  private boolean isInterface;

  public GenericModuleIdentifier( String className, String moduleName, boolean isInterface )
  {
    this.className  = className;
    this.moduleName = moduleName;
  }

  public boolean isInterface()
  {
    return isInterface;
  }

  public String getClassName()
  {
    return className;
  }

  public String getModuleName()
  {
    return moduleName;
  }
} // GenericModuleIdentifier



/*
 * ChangeLog
 * $Log: GenericModuleIdentifier.java,v $
 * Revision 1.6  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.5  2001/01/31 18:03:53  lord
 * Changes due to removal of InterfaceIdentifier
 *
 * Revision 1.4  2000/08/01 17:14:28  lord
 * Now have different Identifier for non concrete instances
 *
 * Revision 1.3  2000/06/16 09:40:07  jns
 * o removed existence of BioInterface stuff, as it isn't really relevent here.
 * Plus, in the pipe work is a reworking of what the BioInterface will
 * represent and do.
 *
 * Revision 1.2  2000/05/24 15:38:37  jns
 * o added ModuleList and MIList stuff and altered java to return module lists
 * adn MILists rather than arrays.
 *
 * Revision 1.1  2000/05/15 16:24:15  lord
 * Initial checkin
 *
 */
