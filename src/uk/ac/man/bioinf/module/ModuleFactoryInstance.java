/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/*
 * This software was written by Julian Selley (j.selley@man.ac.uk)
 * whilst at the University of Manchester as a Research Assistant.
 *
 * The initial code base is copyright by the University of Manchester.
 * Modifications to the initial code base are copyright of their
 * respective authors, or their employers as appropriate. Authorship
 * of the modifications may be determined from the ChangeLog placed
 * at the end of this file
 */

package uk.ac.man.bioinf.module; // Package name inserted by JPack



/**
 * ModuleFactoryInstance.java
 *
 * This interface is designed to provide a means of storing loaded
 * modules and their associated identifiers. It also allows
 * configuration of the module.
 *
 * This is instance is called from the ModuleFactory which simply
 * relays the requests here.
 *
 * Created: Tue May  9 13:53:20 2000
 *
 * @author J Selley
 * @version $Id: ModuleFactoryInstance.java,v 1.10 2001/04/11 17:04:43 lord Exp $
 */

public interface ModuleFactoryInstance 
{
  /**
   * Returns whether a module is available for use. This allows the
   * use of optional modules.
   *
   * @param identifier the module identifier
   * @return the availability of the target module
   */
  public boolean isModuleAvailable(ModuleIdentifier identifier);

  /**
   * Returns the module, given the module identifier.
   *
   * @param identifier the module identifier
   * @return the module
   */
  public Module getModule(ModuleIdentifier identifier) throws ModuleException;

  /**
   * Returns the modules required by the given module.
   *
   * @param identifier the module identifier of the module of interest
   * @return the required modules
   */
  public ModuleList getRequiredModules(ModuleIdentifier identifier);

  /**
   * Sets the config to a particular module described by an identifier.
   *
   * @param identifier the identifier
   * @param config the config
   */
  public void setConfig(ModuleIdentifier identifier, Object config);

  /**
   * Returns the config object a given module
   */
  public Object getConfig( ModuleIdentifier identifier );
  
  /**
   * Set the concrete identifier for a given abstract identifier.
   */
  public void setConcreteIdentifier( ModuleIdentifier abstractIdentifer, ModuleIdentifier concreteIdentifier );
  
  /**
   * Adds an identifier to the factory storage mechanism.
   *
   * @param identifier the identifier
   */
  public void addIdentifier(ModuleIdentifier identifier);

  /**
   * Adds identifiers to the factory storage mechanism.
   *
   * @param identifiers an array of identifiers
   */
  public void addIdentifier(ModuleIdentifier[] identifiers);
  
  /**
   * Resolve a module name into a ModuleIdentifier
   * @param moduleName the name of the module
   */
  public ModuleIdentifier resolveModuleName( String moduleName );
  
  /**
   * Resolves a classname of a module into the module identifier.
   *
   * @param classname the classname of the module
   * @return the module identifier
   */
  public ModuleIdentifier resolveClassName(String className);
  
  /**
   * Loads the module associated with a particular identifier. This
   * will include a call to the load method of the module. It should
   * also subsequently check that all requried modules are loaded as
   * well.
   *
   * @param identifier the identifier
   */
  public Module load(ModuleIdentifier identifier) throws ModuleException;

  /**
   * Destroy all of the module instances, and remove all the internal
   * data structures of this Factory. 
   */
  public void destroy();
  
  
}// ModuleFactoryInstance


/*
 * ChangeLog
 * $Log: ModuleFactoryInstance.java,v $
 * Revision 1.10  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.9  2001/01/31 18:03:53  lord
 * Changes due to removal of InterfaceIdentifier
 *
 * Revision 1.8  2001/01/26 17:13:45  lord
 * Can now get config as well as set it.
 *
 * Revision 1.7  2000/09/15 16:31:23  lord
 * Modifications to remove the ModuleFactory static class. This turned
 * out to be rather limiting and also entirely unnecessary so I removed
 * it.
 *
 * Revision 1.6  2000/06/16 09:40:07  jns
 * o removed existence of BioInterface stuff, as it isn't really relevent here.
 * Plus, in the pipe work is a reworking of what the BioInterface will
 * represent and do.
 *
 * Revision 1.5  2000/05/30 16:27:34  lord
 * resolveModuleName method
 * resolveClassName method
 *
 * Revision 1.4  2000/05/24 21:21:15  jns
 * o added addIdentifier(MI[] ) (I think)
 * o added resolveClassName function
 *
 * Revision 1.3  2000/05/24 19:21:03  lord
 * Changed return type of load method
 *
 * Revision 1.2  2000/05/24 15:38:37  jns
 * o added ModuleList and MIList stuff and altered java to return module lists
 * adn MILists rather than arrays.
 *
 * Revision 1.1  2000/05/24 13:09:42  jns
 * o initial coding of module package
 *
 */

