/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/*
 * This software was written by Julian Selley (j.selley@man.ac.uk)
 * whilst at the University of Manchester as a Research Assistant.
 *
 * The initial code base is copyright by the University of Manchester.
 * Modifications to the initial code base are copyright of their
 * respective authors, or their employers as appropriate. Authorship
 * of the modifications may be determined from the ChangeLog placed
 * at the end of this file
 */

package uk.ac.man.bioinf.module; // Package name inserted by JPack

import java.util.List;


/**
 * To provide a list of modules in a type safe manor. This is
 * basically a thin wrapper to list, specially targeted for modules.
 *
 *
 * Created: Wed May 24 15:06:49 2000
 *
 * @author J Selley
 * @version $Id: ModuleList.java,v 1.2 2001/04/11 17:04:43 lord Exp $
 */

public class ModuleList
{
  private List list;

  public ModuleList(List list)
  {
    this.list = list;
  }

  /**
   * Adds a module to the list.
   *
   * @param m the module
   * @return the success of the addition
   */
  public boolean add(Module m)
  {
    return this.list.add(m);
  }

  /**
   * Clear the list.
   */
  public void clear()
  {
    this.list.clear();
  }

  /**
   * Returns whether a module is contained in this list.
   *
   * @param m the query module
   * @return T/F exists
   */
  public boolean contains(Module m)
  {
    return this.list.contains(m);
  }

  /**
   * Returns the module at a given index.
   *
   * @param i the index
   * @return the module
   */
  public Module get(int i)
  {
    return (Module)this.list.get(i);
  }

  /**
   * Returns the index of a module.
   *
   * @param m the module
   * @return the index
   */
  public int indexOf(Module m)
  {
    return this.list.indexOf(m);
  }

  /**
   * Returns whether the list is empty.
   *
   * @return T/F is the list empty
   */
  public boolean isEmpty()
  {
    return this.list.isEmpty();
  }

  /**
   * Removes a module at a specific index.
   *
   * @param i the index of the module
   * @return the module removed
   */
  public Module remove(int i)
  {
    return (Module)this.list.remove(i);
  }

  /**
   * Removes a module from the list.
   *
   * @param m the module to be removed
   * @return the success
   */
  public boolean remove(Module m)
  {
    return this.list.remove(m);
  }

  /**
   * Returns the number of modules.
   *
   * @return the size of the list
   */
  public int size()
  {
    return this.list.size();
  }

  /**
   * Returns an array of modules.
   *
   * @return the modules
   */
  public Module[] toArray()
  {
    Object[] objs = this.list.toArray();
    Module[] rtn = new Module[objs.length];

    for (int i = 0; i < rtn.length; i++) {
      rtn[i] = (Module)objs[i];
    }

    return rtn;
  }
} // ModuleList



/*
 * ChangeLog
 * $Log: ModuleList.java,v $
 * Revision 1.2  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.1  2000/05/24 15:38:37  jns
 * o added ModuleList and MIList stuff and altered java to return module lists
 * adn MILists rather than arrays.
 *
 */
