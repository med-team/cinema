/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/*
 * This software was written by Julian Selley (j.selley@man.ac.uk)
 * whilst at the University of Manchester as a Research Assistant.
 *
 * The initial code base is copyright by the University of Manchester.
 * Modifications to the initial code base are copyright of their
 * respective authors, or their employers as appropriate. Authorship
 * of the modifications may be determined from the ChangeLog placed
 * at the end of this file
 */

package uk.ac.man.bioinf.module; // Package name inserted by JPack


/**
 * ModuleException.java
 *
 * An exception thrown if a module does not load sucessfully.
 *
 * Created: Tue May  9 18:45:35 2000
 *
 * @author J Selley
 * @version $Id: ModuleException.java,v 1.4 2001/04/11 17:04:43 lord Exp $
 */

public class ModuleException extends Exception
{
  private ModuleExceptionType id;
  private Throwable throwable;
  
  public ModuleException(String message)
  {
    super(message);
  }

  public ModuleException(ModuleExceptionType id)
  {
    super(id.toString());
    this.id = id;
  }
  
  public ModuleException( Throwable throwable )
  {
    super( throwable.toString() );
    this.throwable = throwable;
  }
  
  public ModuleException(String message, ModuleExceptionType id)
  {
    super(message);
    this.id = id;
  }
  
  public ModuleException( String message, ModuleExceptionType id, Throwable throwable )
  {
    this( message, id );
    this.throwable = throwable;
  }
  
  public Throwable getThrowable()
  {
    return throwable;
  }

  public ModuleExceptionType getID()
  {
    return this.id;
  }
} // ModuleException



/*
 * ChangeLog
 * $Log: ModuleException.java,v $
 * Revision 1.4  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.3  2000/12/13 16:33:02  lord
 * New constructor
 *
 * Revision 1.2  2000/08/03 16:40:37  lord
 * Got Interface Modules working correctly which required quite a few changes
 *
 * Revision 1.1  2000/05/24 13:08:32  jns
 * o initial coding of module package
 *
 */
