/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/*
 * This software was written by Julian Selley (j.selley@man.ac.uk)
 * whilst at the University of Manchester as a Research Assistant.
 *
 * The initial code base is copyright by the University of Manchester.
 * Modifications to the initial code base are copyright of their
 * respective authors, or their employers as appropriate. Authorship
 * of the modifications may be determined from the ChangeLog placed
 * at the end of this file
 */

package uk.ac.man.bioinf.module; // Package name inserted by JPack

import java.util.List;


/**
 * To provide a list of module identifiers in a type safe manor. This
 * is basically a thin wrapper to list, specially targeted for
 * module identifiers.
 *
 *
 * Created: Wed May 24 15:06:49 2000
 *
 * @author J Selley
 * @version $Id: ModuleIdentifierList.java,v 1.3 2001/04/11 17:04:43 lord Exp $
 */

public class ModuleIdentifierList
{
  private List list;

  public ModuleIdentifierList(List list)
  {
    this.list = list;
  }

  /**
   * Adds a module identifier to the list.
   *
   * @param m the module identifier
   * @return the success of the addition
   */
  public boolean add(ModuleIdentifier m)
  {
    return this.list.add(m);
  }

  /**
   * Clear the list.
   */
  public void clear()
  {
    this.list.clear();
  }

  /**
   * Returns whether a module identifier is contained in this list.
   *
   * @param m the query module identifier
   * @return T/F exists
   */
  public boolean contains(ModuleIdentifier m)
  {
    return this.list.contains(m);
  }

  /**
   * Returns the module identifier at a given index.
   *
   * @param i the index
   * @return the module identifier
   */
  public ModuleIdentifier get(int i)
  {
    return (ModuleIdentifier)this.list.get(i);
  }

  /**
   * Returns the index of a module identifier.
   *
   * @param m the module identifier
   * @return the index
   */
  public int indexOf(ModuleIdentifier m)
  {
    return this.list.indexOf(m);
  }

  /**
   * Returns whether the list is empty.
   *
   * @return T/F is the list empty
   */
  public boolean isEmpty()
  {
    return this.list.isEmpty();
  }

  /**
   * Removes a module identifier at a specific index.
   *
   * @param i the index of the module identifier
   * @return the module identifier removed
   */
  public ModuleIdentifier remove(int i)
  {
    return (ModuleIdentifier)this.list.remove(i);
  }

  /**
   * Removes a module identifier from the list.
   *
   * @param m the module identifier to be removed
   * @return the success
   */
  public boolean remove(ModuleIdentifier m)
  {
    return this.list.remove(m);
  }

  /**
   * Returns the number of module identifiers.
   *
   * @return the size of the list
   */
  public int size()
  {
    return this.list.size();
  }

  /**
   * Returns an array of module identifiers.
   *
   * @return the module identifiers
   */
  public ModuleIdentifier[] toArray()
  {
    Object[] objs = this.list.toArray();
    ModuleIdentifier[] rtn = new ModuleIdentifier[objs.length];

    System.arraycopy( objs, 0, rtn, 0, objs.length );
    
    return rtn;
  }
} // ModuleIdentifierList



/*
 * ChangeLog
 * $Log: ModuleIdentifierList.java,v $
 * Revision 1.3  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.2  2000/05/24 19:21:34  lord
 * Used system.arraycopy rather than manual method
 *
 * Revision 1.1  2000/05/24 15:38:37  jns
 * o added ModuleList and MIList stuff and altered java to return module lists
 * adn MILists rather than arrays.
 *
 */

