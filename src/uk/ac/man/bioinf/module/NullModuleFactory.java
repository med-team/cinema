/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.module; // Package name inserted by JPack
import uk.ac.man.bioinf.module.Module;
import uk.ac.man.bioinf.module.ModuleFactoryInstance;
import uk.ac.man.bioinf.module.ModuleIdentifier;

/**
 * NullModuleFactory.java
 *
 * This class does precisely nothing, but in a very special way.....
 *
 * Created: Mon May 15 15:03:40 2000
 *
 * @author Phillip Lord
 * @version $Id: NullModuleFactory.java,v 1.10 2001/04/11 17:04:43 lord Exp $ 
 */

public class NullModuleFactory implements ModuleFactoryInstance
{
  public boolean isModuleAvailable(ModuleIdentifier identifier)
  {
    return true;
  }

  public Module getModule(ModuleIdentifier identifier)
  {
    return null;
  }

  public ModuleList getRequiredModules(ModuleIdentifier identifier)
  {
    return null;
  }

  public void addIdentifier(ModuleIdentifier identifier)
  {
  }

  public void addIdentifier(ModuleIdentifier[] identifiers)
  {
  }

  public void addConfig(ModuleIdentifier identifier, Object config)
  {
  }

  public Module load(ModuleIdentifier identifier)
  {
    return null;
  }
  
  public void setConfig( ModuleIdentifier mod, Object config )
  {
  }
  
  public Object getConfig( ModuleIdentifier mod )
  {
    return null;
  }
  
  public ModuleIdentifier resolveModuleName( String moduleName )
  {
    return null;
  }
  
  public ModuleIdentifier resolveClassName( String className )
  {
    return null;
  }

  public void destroy()
  {
  }
  
  public void setConcreteIdentifier
    ( ModuleIdentifier abstractIdentifier, ModuleIdentifier concreteIdentifier )
  {
  }
  
}// NullModuleFactory


/*
 * ChangeLog
 * $Log: NullModuleFactory.java,v $
 * Revision 1.10  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.9  2001/01/31 18:03:53  lord
 * Changes due to removal of InterfaceIdentifier
 *
 * Revision 1.8  2001/01/26 17:13:55  lord
 * Added get config method
 *
 * Revision 1.7  2000/09/15 16:31:23  lord
 * Modifications to remove the ModuleFactory static class. This turned
 * out to be rather limiting and also entirely unnecessary so I removed
 * it.
 *
 * Revision 1.6  2000/07/18 11:12:14  lord
 * Import rationalisation
 *
 * Revision 1.5  2000/06/16 09:40:07  jns
 * o removed existence of BioInterface stuff, as it isn't really relevent here.
 * Plus, in the pipe work is a reworking of what the BioInterface will
 * represent and do.
 *
 * Revision 1.4  2000/05/30 16:28:12  lord
 * resolveModuleName method
 * resolveClassName method
 *
 * Revision 1.3  2000/05/24 19:22:09  lord
 * Added addConfig method.
 * Changed return type of load
 *
 * Revision 1.2  2000/05/24 15:38:37  jns
 * o added ModuleList and MIList stuff and altered java to return module lists
 * adn MILists rather than arrays.
 *
 * Revision 1.1  2000/05/15 14:11:36  lord
 * Initial Checkin
 * 
 */
