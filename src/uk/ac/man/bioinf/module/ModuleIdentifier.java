/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Julian Selley (j.selley@man.ac.uk)
 * whilst at the University of Manchester as a Research Assistant. 
 *
 * The initial code base is copyright by the University of Manchester. 
 * Modifications to the initial code base are copyright of their 
 * respective authors, or their employers as appropriate. Authorship 
 * of the modifications may be determined from the ChangeLog placed 
 * at the end of this file
 */

package uk.ac.man.bioinf.module; // Package name inserted by JPack



/**
 * ModuleIdentifier.java
 *
 * A thin tag for a module, rather than banding round modules.
 *
 * Created: Wed Apr 26 17:05:48 2000
 *
 * @author J Selley
 * @version $Id: ModuleIdentifier.java,v 1.3 2001/04/11 17:04:43 lord Exp $ 
 */

public interface ModuleIdentifier 
{
  /**
   * Returns whether this module is an interface.
   *
   * @return whether this module is an interface.
   */
  public boolean isInterface();

  /**
   * Returns the string of the class name.
   *
   * @return the class name
   */
  public String getClassName();

  /**
   * Returns the name of the module being defined.
   *
   * @return the name
   */
  public String getModuleName();
}// ModuleIdentifier


/*
 * ChangeLog
 * $Log: ModuleIdentifier.java,v $
 * Revision 1.3  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.2  2000/06/16 09:40:07  jns
 * o removed existence of BioInterface stuff, as it isn't really relevent here.
 * Plus, in the pipe work is a reworking of what the BioInterface will
 * represent and do.
 *
 * Revision 1.1  2000/05/24 13:08:32  jns
 * o initial coding of module package
 * 
 */
