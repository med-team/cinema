/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/*
 * This software was written by Julian Selley (j.selley@man.ac.uk)
 * whilst at the University of Manchester as a Research Assistant.
 *
 * The initial code base is copyright by the University of Manchester.
 * Modifications to the initial code base are copyright of their
 * respective authors, or their employers as appropriate. Authorship
 * of the modifications may be determined from the ChangeLog placed
 * at the end of this file
 */

package uk.ac.man.bioinf.module; // Package name inserted by JPack

import uk.ac.man.bioinf.util.AbstractEnumeration;


/**
 * This class provides an identifier for the module exception that has
 * been thrown, rather than string comparisons needing to be carried
 * out.
 *
 *
 * Created: Mon May 22 11:26:22 2000
 *
 * @author J Selley
 * @version $Id: ModuleExceptionType.java,v 1.4 2001/04/11 17:04:43 lord Exp $
 */

public final class ModuleExceptionType extends AbstractEnumeration
{
  private ModuleExceptionType(String message)
  {
    super(message);
  }
  
  public static final ModuleExceptionType MODULE_DOES_NOT_EXIST
    = new ModuleExceptionType( "Module does not exist ");
  
  /** 
   * Indicates a failed attempt to resolve a module name
   */
  public static final ModuleExceptionType MODULE_RESOLVE_FAIL =
    new ModuleExceptionType( "Module failed to resolve module name " );

  /**
   * The module has previously been defined and created.
   */
  public static final ModuleExceptionType MODULE_EXISTS =
    new ModuleExceptionType("Module already defined in storage structure");

  /**
   * The module class was not found in the location stated.
   */
  public static final ModuleExceptionType MODULE_CLASS_NOT_FOUND =
    new ModuleExceptionType("Module class not found");

  /**
   * Creating and instantiating the module generated a problem.
   */
  public static final ModuleExceptionType MODULE_INIT_PROBLEM =
    new ModuleExceptionType("Module instantiation caused a problem");
} // ModuleExceptionType



/*
 * ChangeLog
 * $Log: ModuleExceptionType.java,v $
 * Revision 1.4  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.3  2001/01/31 18:03:53  lord
 * Changes due to removal of InterfaceIdentifier
 *
 * Revision 1.2  2001/01/26 17:13:31  lord
 * New type
 *
 * Revision 1.1  2000/05/24 13:08:32  jns
 * o initial coding of module package
 *
 */
