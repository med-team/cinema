/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/*
 * This software was written by Julian Selley (j.selley@man.ac.uk)
 * whilst at the University of Manchester as a Research Assistant.
 *
 * The initial code base is copyright by the University of Manchester.
 * Modifications to the initial code base are copyright of their
 * respective authors, or their employers as appropriate. Authorship
 * of the modifications may be determined from the ChangeLog placed
 * at the end of this file
 */

package uk.ac.man.bioinf.module; // Package name inserted by JPack



/**
 * ModuleContext.java
 *
 * Gives context to a module (e.g: configuration information).
 *
 * Created: Tue Apr 18 19:17:55 2000
 *
 * @author J Selley
 * @version $Id: ModuleContext.java,v 1.6 2001/04/11 17:04:43 lord Exp $
 */

public abstract class ModuleContext 
{
  private ModuleFactoryInstance moduleFactory;
  
  public void setModuleFactory( ModuleFactoryInstance moduleFactory )
  {
    this.moduleFactory = moduleFactory;
  }
  
  public ModuleFactoryInstance getModuleFactory()
  {
    return moduleFactory;
  }
  
  /**
   * Returns whether a module is available for use, given the
   * identifier. Allows for optional modules.
   *
   * @param identifier the module identifier for the optional module
   * @return the availability of the module
   */
  public boolean isModuleAvailable(ModuleIdentifier identifier)
  {
    return moduleFactory.isModuleAvailable(identifier);
  }

  /**
   * Returns a module by interfacing with the ModuleHandlerFactory. It
   * is designed to allow use of optional modules.
   *
   * @param identifier the module identifier
   * @return the module
   */
  public Module getModule(ModuleIdentifier identifier) throws ModuleException
  {
    return moduleFactory.getModule(identifier);
  }

  /**
   * This method resolves a module name into a ModuleIdentifier. Generally
   * speaking this method is not the preferred mechanisms for getting a
   * ModuleIdentifier. If a ModuleIdentifier extends
   * AbstractEnumeratedModuleIdentifier, then they can be directly accessed in a
   * way which is compile time type safe.
   * @param moduleName
   * @return
   */
  public ModuleIdentifier resolveModuleName( String moduleName )
  {
    return moduleFactory.resolveModuleName( moduleName );
  }
  
  /**
   * Returns the associated module for this context.
   *
   * @return the module
   */
  public Module getModule() throws ModuleException
  {
    return moduleFactory.getModule(getIdentifier());
  }

  private ModuleList requiredModules;
  
  public ModuleList getRequiredModules()
  {
    if( requiredModules == null ){
      requiredModules = moduleFactory.getRequiredModules( getIdentifier() );
    }
    return requiredModules;
  }
  
  /**
   * Returns the modules identifier.
   *
   * @return the module identifier
   */
  public abstract ModuleIdentifier getIdentifier();

  /**
   * Returns an object representing the configuration of the module.
   *
   * @return the configuration
   */
  public abstract Object getConfig();
}// ModuleContext


/*
 * ChangeLog
 * $Log: ModuleContext.java,v $
 * Revision 1.6  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.5  2001/01/31 18:03:53  lord
 * Changes due to removal of InterfaceIdentifier
 *
 * Revision 1.4  2000/09/15 16:31:23  lord
 * Modifications to remove the ModuleFactory static class. This turned
 * out to be rather limiting and also entirely unnecessary so I removed
 * it.
 *
 * Revision 1.3  2000/06/16 09:40:07  jns
 * o removed existence of BioInterface stuff, as it isn't really relevent here.
 * Plus, in the pipe work is a reworking of what the BioInterface will
 * represent and do.
 *
 * Revision 1.2  2000/05/30 16:26:42  lord
 * resolveModuleName method
 *
 * Revision 1.1  2000/05/24 13:08:32  jns
 * o initial coding of module package
 *
 */
