/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package uk.ac.man.bioinf.util;
import java.util.ResourceBundle;
import java.util.Locale;
import java.net.URL;
import java.io.File;
import java.io.IOException;
/**
 * Looks specifically for resources in the package 'resource' that should be at the route of the Application's class path.
 * Specifically:
 * <ul>
 * <li> images should go in <code>resources.images</code>
 * <li> help documentation should go in <code>resources.helpfiles</code>
 * <li> locale data (messages, tooltips and paths to other resources such as images) 
 * should go in the appropriately named properties file in <code>resources.locale</code>.
 * </ul>
 * @author C.Miller
 * @version 1.0
 */
public class PackageResourceAdapter extends ResourceAdapter{

   // constructor
   public PackageResourceAdapter(String propertyFile) throws IOException {
      super();
      props = "resources.locale." + propertyFile;
      init();
   }

   public URL getHelpURL(String helpURLName) {
      String path = "/resources/helpfiles/" + bundle.getString("helpURL." + helpURLName);
      URL url = getClass().getResource(path);
      return url;
   }

   public URL getImageURL(String imageName) {
      String imageFilename = "/resources/images/" + bundle.getString("image." + imageName);
      URL url = getClass().getResource(imageFilename);
      return url;
   }
}
