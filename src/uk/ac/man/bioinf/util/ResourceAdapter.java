/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package uk.ac.man.bioinf.util;
import java.util.ResourceBundle;
import java.util.Locale;
import java.net.URL;
import java.io.File;
import java.io.IOException;
/**
 * Generic class for locating resources within an application/applet's package
 * hierarchy. Resources are defined in a locale specific property file according to 
 * the conventions defined in ResourceBundle.
 * For URLs, looks up the appropriate path in the specified property file, 
 * and then uses that to retrieve a pointer to it.
 * @author C.Miller
 * @version 1.0
 */
public class ResourceAdapter {

   protected Locale defaultLocale = Locale.getDefault();
   protected ResourceBundle bundle;
   protected String props;
   // constructor

   public ResourceAdapter() {
   }

   public ResourceAdapter(String propertyFile) throws IOException {
      props = propertyFile;
      init();
   }

        
   protected void init() throws IOException {
      bundle = ResourceBundle.getBundle(props,defaultLocale);
      if(bundle == null) throw new IOException("Can't find properties file for resource bundle.");
   }

   /**
    * property is message.<messageName>
    */
   public String getMessageString(String messageName) {
      String message = bundle.getString("message." + messageName);
      return message;
   }
   
   /**
    * property is tooltip.<tooltipName>
    */
   public String getToolTip(String tooltipName) {
      String message = bundle.getString("tooltip." + tooltipName);
      return message;
   }

   /**
    * property is helpURL.<helpURLName>
    */
   public URL getHelpURL(String helpURLName) {
      String message = bundle.getString("helpURL." + helpURLName);
      URL url = getClass().getResource(message);
      return url;
   }

   /**
    * property is image.<imageName>  value should be path to the image
    */
   public URL getImageURL(String imageName) {
      String imageFilename = bundle.getString("image." + imageName);
      URL url = getClass().getResource(imageFilename);
      return url;
   }
 
   /**
    * property is file.<fileDescriptor>  value should be path to the image
    */
   public File getFile(String fileDescriptor) {
      String fileDesc = bundle.getString("file." + fileDescriptor);
      File f = new File(fileDesc);
      return f;
   }

   /**
    * property is url.<URLDescriptor>  value should be a URL
    */
   public URL getURL(String URLDescriptor) {
      String fileDesc = bundle.getString("url." + URLDescriptor);
      URL url = getClass().getResource(fileDesc);
      return url;
   }

   /**
    * property is string.<descriptor>  value should be a String
    */
   public String getTextString(String descriptor) {
      String fileDesc = bundle.getString("string." + descriptor);
      return fileDesc;
   }
}

