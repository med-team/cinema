/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.util; // Package name inserted by JPack
import java.util.Iterator;
import java.util.Map;


/**
 * CollectionPrinter.java
 *
 *
 * Created: Thu Jul 20 19:47:52 2000
 *
 * @author Phillip Lord
 * @version $Id: CollectionPrinter.java,v 1.4 2001/04/11 17:04:43 lord Exp $
 */

public class CollectionPrinter 
{
  public static void print( Map map )
  {
    Iterator keyIter = map.keySet().iterator();
    
    while( keyIter.hasNext() ){
      Object key = keyIter.next();
      
      System.out.println( "Key: " + key + " Value: " + map.get( key ) );
    }
  }

  public static void print( Object[] obj )
  {
    System.out.println( "Printing out array " + obj );
    
    for( int i = 0; i < obj.length; i++ ){
      System.out.println( i + ": " + obj[ i ] );
    }
    
  }
} // CollectionPrinter



/*
 * ChangeLog
 * $Log: CollectionPrinter.java,v $
 * Revision 1.4  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.3  2000/12/18 12:15:05  jns
 * o getting rid of system.out.println to avoid noisy output out of debug
 * mode
 *
 * Revision 1.2  2000/11/27 18:17:20  lord
 * More methods added
 *
 * Revision 1.1  2000/07/26 13:29:27  lord
 * Initial checkin
 *
 */
