/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.util; // Package name inserted by JPack
import java.util.NoSuchElementException;


/**
 * ArrayManipulation.java
 * 
 * Just some utility functions for manipulating arrays. 
 *
 * Created: Thu Nov 23 14:03:06 2000
 *
 * @author Phillip Lord
 * @version $Id: ArrayManipulation.java,v 1.2 2001/04/11 17:04:43 lord Exp $
 */

public class ArrayManipulation 
{
  public static int linearSearch( Object[] array, Object key )
  {
    for( int i = 0; i < array.length; i++ ){
      if( array[ i ].equals( key ) ) return i;
    }
    
    return -1;
  }
  
  public static void removeFromArray( Object[] src, Object[] dest, Object remove )
  {
    int index = linearSearch( src, remove );

    if( index == -1 ) throw new NoSuchElementException
		       ( "The source array does not contain this object" );
    
    // copy before the index
    System.arraycopy( src, 0, dest, 0, index );
    
    // copy after the index
    System.arraycopy( src, index + 1, dest, index, dest.length - index );
  }

  public static void main( String[] args )
  {
    Object[] src =
      {
	new Integer( 0 ),
	new Integer( 1 ), 
	new Integer( 2 ),
	new Integer( 3 ), 
	new Integer( 4 ), 
	new Integer( 5 ), 
	new Integer( 6 ), 
	new Integer( 7 ),
	new Integer( 8 ), 
	new Integer( 9 ) 
      };

    CollectionPrinter.print( src );
    
    Object[] dest = new Object[ 9 ];
    removeFromArray( src, dest,  new Integer( 4 ) );
    CollectionPrinter.print( dest );
  } //end main method 
  
} // ArrayManipulation



/*
 * ChangeLog
 * $Log: ArrayManipulation.java,v $
 * Revision 1.2  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.1  2000/12/05 15:30:52  lord
 * Initial checkin
 *
 */

