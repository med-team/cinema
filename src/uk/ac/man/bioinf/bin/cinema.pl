#!/usr/bin/perl


### This is a launch script for Cinema


###
### This is a direct port of the Cinema sh script which is to be
### considered the definative version in terms of functionality.
###
### This is particularly true in the case of this script as I am a
### registered member of the perl post-traumatic stress disorder
### self-help society.
###
### Copyright (c) 2001 Phillip Lord (p.lord@hgmp.mrc.ac.uk)
###



use Getopt::Std;

my $CINEMA_LIB="../lib";
my $USER_EXTENSIONS="~/.cinema/ext";
my $VERBOSE=0;
my $JVM_OPTIONS;

getopts( 'L:U:D:V' );


if( $opt_L ){
  $CINEMA_LIB = $opt_L;
}

if( $opt_U ){
  $USER_EXTENSIONS=$opt_U;
}

if( $opt_V ){
  $VERBOSE=$opt_V;
}

if( $opt_D ){
  ## I don't think that this will work correctly. Its only going to
  ## set an single -D option which is a bit of a pain.
  $JVM_OPTIONS="$JVM_OPTIONS -D$opt_D";
}

my $jarfile;
my $user_jars = "";
my $cinema_jars= "";
my $ext_jars = "";


foreach $jarfile ( glob( "$USER_EXTENSIONS/*jar" ) ){
  $user_jars.=":$jarfile";
}

foreach $jarfile ( glob( "$CINEMA_LIB/*jar" ) ){
  $cinema_jars.=":$jarfile";
}

foreach $jarfile ( glob( "$CINEMA_LIB/ext/*jar" ) ){
  $ext_jars.=":$jarfile";
}

my $classpath="$user_jars$cinema_jars$ext_jars";

if( $VERBOSE ){
  print "java -classpath $classpath $JVM_OPTIONS uk.ac.man.bioinf.apps.cinema.Cinema @ARGV";
}

print `java -classpath $classpath $JVM_OPTIONS uk.ac.man.bioinf.apps.cinema.Cinema @ARGV`;






