/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/*
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.debug; // Package name inserted by JPack


/**
 * AbstractDebug.java
 *
 *
 * Created: Wed Mar  1 22:22:22 2000
 *
 * @author Phillip Lord
 * @version
 */

public abstract class AbstractDebug implements DebugInterface 
{
  public abstract void message( Class cla, String message );
  public abstract void throwable( Class cla, Throwable th );
  

  
  public void message( Object obj, String message )
  {
    message( obj.getClass(), message );
  }

  public void message( Object obj, Object message )
  {
    message( obj, message.toString() );
  }
  
  public void message( Class cla, Object message )
  {
    message( cla, message.toString() );
  }
  
  public void throwable( Object obj, Throwable th )
  {
    throwable( obj.getClass(), th );
  }
  
  public void both( Object obj, String message, Throwable th )
  {
    message( obj, message );
    throwable( obj, th );
  }

  public void both( Class cla, String message, Throwable th )
  {
    message( cla, message );
    throwable( cla, th );
  }
  
  public void both( Object obj, Object message, Throwable th )
  {
    message( obj, message );
    throwable( obj, th );
  }
  
  public void both( Class cla, Object message, Throwable th )
  {
    message( cla, message );
    throwable( cla, th );
  }
}// AbstractDebug
