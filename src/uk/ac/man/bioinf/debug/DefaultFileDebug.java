/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.debug; // Package name inserted by JPack
import java.io.Writer;
import java.io.File;
import java.io.PrintWriter;
import java.io.FileWriter;
import java.io.IOException;


/**
 * DefaultFileDebug.java
 *
 *
 * Created: Sun May  7 23:46:57 2000
 *
 * @author Phillip Lord
 * @version $Id: DefaultFileDebug.java,v 1.2 2001/04/11 17:04:42 lord Exp $
 */

public class DefaultFileDebug extends AbstractDebug
{
  private File file;
  private PrintWriter writer;
  
  public DefaultFileDebug( File file ) throws IOException
  {
    this.file = file;
    writer = new PrintWriter( new FileWriter( file ) );
  }

  public void message( Class cla, String message )
  {
    writer.println( "Debug Message: " + message );
    writer.flush();
  }
  
  public void throwable( Class cla, Throwable th )
  {
    writer.println( "Debug Throwable: " + th );
    writer.println( "Debug Stack: " );
    
    th.printStackTrace( writer );
    writer.flush();
  }
} // DefaultFileDebug



/*
 * ChangeLog
 * $Log: DefaultFileDebug.java,v $
 * Revision 1.2  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.1  2000/05/08 16:22:57  lord
 * Initial checkin
 *
 */
