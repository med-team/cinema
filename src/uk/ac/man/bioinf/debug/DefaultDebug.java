/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.debug; // Package name inserted by JPack


/**
 * DefaultDebug.java
 * 
 * Simple Debug interface which prints things to screen
 *
 * Created: Thu Mar  9 17:57:34 2000
 *
 * @author Phillip Lord
 * @version $Id: DefaultDebug.java,v 1.4 2001/04/11 17:04:42 lord Exp $
 */

public class DefaultDebug extends AbstractDebug
{

  public void message( Class cla, String message )
  {
    System.out.println( "Debug Message: " + message );
  }
  
  public void throwable( Class cla, Throwable th )
  {
    System.out.println( "Debug Throwable: " + th );
    System.out.print( "Debug Stack: " );
    
    th.printStackTrace();
  }
} // DefaultDebug



/*
 * ChangeLog
 * $Log: DefaultDebug.java,v $
 * Revision 1.4  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.3  2000/05/08 16:26:42  lord
 * Now dumps stack also
 *
 * Revision 1.2  2000/04/20 14:04:38  lord
 * Cosmetic changes to output
 *
 * Revision 1.1  2000/03/09 18:02:47  lord
 * Implemented default class
 *
 */
