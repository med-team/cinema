/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.analysis.consensus; // Package name inserted by JPack
import uk.ac.man.bioinf.apps.invoker.AnonInvoker;
import uk.ac.man.bioinf.apps.invoker.InvokerInternalQueue;


/**
 * ThreadedConsensusSequence.java
 *
 * This class calls the calculation out of the event thread, so that
 * background analysis is made more easily. To use this class you have
 * to provide it with a thread that it can use for the
 * calculation. You can do this either on a per instance basis, or by
 * setting a static default. You have to do one of the other though!
 *
 * Created: Tue Nov  7 18:36:22 2000
 *
 * @author Phillip Lord
 * @version $Id: ThreadedConsensusSequence.java,v 1.3 2001/04/11 17:04:41 lord Exp $
 */

public class ThreadedConsensusSequence extends DefaultConsensusSequence
{

  private InvokerInternalQueue queue;
  
  public ThreadedConsensusSequence( ConsensusCalculator calculator )
  {
    this( calculator, getDefaultInvokerInternalQueue() );
    if( queue == null ) 
      throw new NullPointerException( "You have to either set a default queue, or provide on in the constructor" );
  }
  
  public ThreadedConsensusSequence( ConsensusCalculator calculator, InvokerInternalQueue queue )
  {
    super( calculator );
    this.queue = queue;
  }
  
  protected void validate()
  {
    // we want to do the calculation out of the event thread, and then
    // fire change events back in it...
    if( queue != null ){
      queue.makeEmpty();
      queue.enqueue
	( new AnonInvoker(){
	    public Object slow()
	    {
	      ThreadedConsensusSequence.this.calcConsensus();
	      return null;
	    }
	    
	    public void doRun()
	    {
	      ThreadedConsensusSequence.this. 
		fireChangeEvent();
	    }
	  });
    }
    else{
      // the first time around we want to super class validate.
      super.validate();
    }
  }
  
  public void destroy()
  {
    super.destroy();
    // the queue has to be shut down manually because it is a system
    // event listener and this will prevent it from GC otherwise. 
    queue.destroy();
  }
      
  private static InvokerInternalQueue qu;
  
  public static void setDefaultInvokerInternalQueue( InvokerInternalQueue queue )
  {
    qu = queue;
  }
  
  public static InvokerInternalQueue getDefaultInvokerInternalQueue()
  {
    return qu;
  }
} // ThreadedConsensusSequence



/*
 * ChangeLog
 * $Log: ThreadedConsensusSequence.java,v $
 * Revision 1.3  2001/04/11 17:04:41  lord
 * Added License agreements to all code
 *
 * Revision 1.2  2000/12/05 15:09:23  lord
 * Added proper destroy method.
 *
 * Revision 1.1  2000/11/08 18:27:38  lord
 * Initial checkin
 *
 */
