/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.analysis.consensus; // Package name inserted by JPack
import uk.ac.man.bioinf.sequence.alignment.SequenceAlignment;
import uk.ac.man.bioinf.sequence.identifier.AbstractIdentifier;
import uk.ac.man.bioinf.sequence.identifier.NoSource;
import uk.ac.man.bioinf.sequence.identifier.Source;


/**
 * ConsensusIdentifier.java
 *
 *
 * Created: Mon Oct 16 15:40:30 2000
 *
 * @author Phillip Lord
 * @version $Id: ConsensusIdentifier.java,v 1.3 2001/04/11 17:04:41 lord Exp $
 */

public class ConsensusIdentifier extends AbstractIdentifier
{
  private String title;
  
  public ConsensusIdentifier( SequenceAlignment alignment )
  {
    this( new NoSource(), alignment );
  }
  
  public ConsensusIdentifier( Source source, SequenceAlignment alignment )
  {
    super( source );
    this.title = "Consensus: " + alignment.getIdentifier().getTitle();
  }

  public String getTitle()
  {
    return title;
  }
} // ConsensusIdentifier



/*
 * ChangeLog
 * $Log: ConsensusIdentifier.java,v $
 * Revision 1.3  2001/04/11 17:04:41  lord
 * Added License agreements to all code
 *
 * Revision 1.2  2000/11/10 15:11:10  lord
 * Added imports
 *
 * Revision 1.1  2000/10/19 17:34:32  lord
 * Consensus sequences now have an sane "getTitle()" identifier method
 *
 */
