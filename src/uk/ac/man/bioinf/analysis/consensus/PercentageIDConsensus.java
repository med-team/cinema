/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.analysis.consensus; // Package name inserted by JPack
import uk.ac.man.bioinf.analysis.consensus.ConsensusIdentifier;
import uk.ac.man.bioinf.analysis.misc.PercentageIDCalculator;
import uk.ac.man.bioinf.sequence.Sequences;
import uk.ac.man.bioinf.sequence.alignment.GappedSequence;
import uk.ac.man.bioinf.sequence.alignment.SequenceAlignment;


/**
 * PercentageIDConsensus.java
 *
 * Calculates a consensus sequence based simply on which element is
 * most frequent at that row. If there are several possibilities it
 * just takes which ever one came first. 
 *
 * Created: Thu Jun 15 18:06:30 2000
 *
 * @author Phillip Lord
 * @version $Id: PercentageIDConsensus.java,v 1.10 2001/04/11 17:04:41 lord Exp $
 */

public class PercentageIDConsensus implements ConsensusCalculator
{
  public static final String NAME = "Percentage ID Consensus Calculator";
  
  private PercentageIDCalculator calc;
  private GappedSequence sequence;
  private int checksum = -1;

  public PercentageIDConsensus( PercentageIDCalculator calc )
  {
    this.calc = calc;
  }
  
  private void fetchConsensus()
  {
    sequence =  Sequences.
      getElementsAsGappedSequence( calc.getConsensus(), 
				   calc.getAlignment().getSequenceType(), 
				   new ConsensusIdentifier( calc.getAlignment() ) ); 
    checksum = calc.getCheckSum();
  }
    
  public GappedSequence getCurrentConsensus()
  {
    if( checksum != calc.getCheckSum() ){
      fetchConsensus();
    }
    return sequence;
  }
  
  public String getCalculatorName()
  {
    return NAME;
  }
  
  public SequenceAlignment getSequenceAlignment()
  {
    return calc.getAlignment();
  }
} // PercentageIDConsensus



/*
 * ChangeLog
 * $Log: PercentageIDConsensus.java,v $
 * Revision 1.10  2001/04/11 17:04:41  lord
 * Added License agreements to all code
 *
 * Revision 1.9  2000/12/05 15:54:37  lord
 * Import rationalisation
 *
 * Revision 1.8  2000/11/13 16:52:11  jns
 * o bug fixing because a class has moved else where in the tree
 *
 * Revision 1.7  2000/11/08 18:26:57  lord
 * Calculator now has state
 *
 * Revision 1.6  2000/10/19 17:34:32  lord
 * Consensus sequences now have an sane "getTitle()" identifier method
 *
 * Revision 1.5  2000/08/01 16:07:06  lord
 * PercentageIDCalculator is now tied to a specific SequenceAlignment
 * instance.
 * The name of this Calculator is now available as a public static final string
 *
 * Revision 1.4  2000/07/26 13:24:56  lord
 * Last comment is wrong. I have changed this in response to changes
 * in PercentageIDCalculator which is now tied to a specific alignment
 *
 * Revision 1.3  2000/07/26 13:23:26  lord
 * Have now tied Object instance to specific SequenceAlignment
 *
 * Revision 1.2  2000/07/18 10:35:37  lord
 * Import rationalisation
 *
 * Revision 1.1  2000/06/27 15:53:56  lord
 * Initial checkin
 *
 */
