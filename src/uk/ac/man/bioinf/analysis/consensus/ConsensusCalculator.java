/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.analysis.consensus; // Package name inserted by JPack
import uk.ac.man.bioinf.sequence.alignment.GappedSequence;
import uk.ac.man.bioinf.sequence.alignment.SequenceAlignment;

/**
 * ConsensusCalculator.java
 *
 *
 * Created: Thu Jun 15 15:11:33 2000
 *
 * @author Phillip Lord
 * @version $Id: ConsensusCalculator.java,v 1.6 2001/04/11 17:04:41 lord Exp $ 
 */

public interface ConsensusCalculator 
{
  public SequenceAlignment getSequenceAlignment();
  
  public GappedSequence getCurrentConsensus();
  
  public String getCalculatorName();
  
}// ConsensusCalculator


/*
 * ChangeLog
 * $Log: ConsensusCalculator.java,v $
 * Revision 1.6  2001/04/11 17:04:41  lord
 * Added License agreements to all code
 *
 * Revision 1.5  2000/12/05 15:08:52  lord
 * Import rationalisation
 *
 * Revision 1.4  2000/11/08 18:24:47  lord
 * The consensus calculator interface has been changed to reflect the
 * fact that instances are expected to have state, that is to be tied to
 * a specific SequenceAlignment
 *
 * Revision 1.3  2000/10/19 17:33:49  lord
 * Import rationalisation
 *
 * Revision 1.2  2000/08/01 16:05:50  lord
 * Instance now tied to a specific SequenceAlignment
 *
 * Revision 1.1  2000/06/27 15:53:56  lord
 * Initial checkin
 * 
 */

