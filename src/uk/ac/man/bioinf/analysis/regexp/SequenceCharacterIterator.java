/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.analysis.regexp; // Package name inserted by JPack
import org.apache.regexp.CharacterIterator;
import uk.ac.man.bioinf.sequence.Sequence;
import uk.ac.man.bioinf.sequence.Sequences;


/**
 * SequenceCharacterIterator.java
 *
 * This class provides the link between the sequence package and
 * jakarta regexp package. This class can be used as a wrapper for
 * Sequences. 
 * @see GappedSequenceCharacterIterator
 *
 * Created: Mon Feb  5 13:53:45 2001
 *
 * @author Phillip Lord
 * @version $Id: SequenceCharacterIterator.java,v 1.2 2001/04/11 17:04:41 lord Exp $
 */

public class SequenceCharacterIterator implements CharacterIterator
{
  private Sequence seq;
  
  public SequenceCharacterIterator( Sequence seq )
  {
    this.seq = seq;
  }
  
  public char charAt( int at )
  {
    return seq.getElementAtAsChar( at + 1 );
  }
  
  public boolean isEnd( int position )
  {
    return ((position + 1) >= seq.getLength() );
  }
  
  public String substring( int index )
  {
    return Sequences.getSubSequenceAsString( seq, (index + 1), seq.getLength() );
  }
  
  public String substring( int index, int length )
  {
    return Sequences.getSubSequenceAsString( seq, (index + 1), length );
  }
} // SequenceCharacterIterator



/*
 * ChangeLog
 * $Log: SequenceCharacterIterator.java,v $
 * Revision 1.2  2001/04/11 17:04:41  lord
 * Added License agreements to all code
 *
 * Revision 1.1  2001/02/19 16:47:55  lord
 * Initial checkin
 *
 */
