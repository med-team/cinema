/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.sequence.complex; // Package name inserted by JPack
import java.util.HashMap;


/**
 * ModificationFactory.java
 *
 *
 * Created: Mon Feb 28 23:07:50 2000
 *
 * @author Phillip Lord
 * @version $Id: ModificationFactory.java,v 1.4 2001/04/11 17:04:43 lord Exp $
 */

public class ModificationFactory 
{
  private static HashMap modifications = new HashMap();
  
  static void addNewSingletonModification( String name, SingletonModification modification )
  {
    if( modifications.containsKey( name ) )
      throw new IllegalDuplicateModificationException
	( "Attempt to install singleton modification " + modification.getName() +
	  " conflicts with modification " + modifications.get( name ) );
    
    modifications.put( name, modification );
  }

  static void addNewGenericModification( String name, GenericModification modification )
  {
    if( modifications.containsKey( name ) )
      throw new IllegalDuplicateModificationException
	( "Attempt to generic singleton modification " + modification.getName() +
	  " conflicts with modification " + modifications.get( name ) );
    
    modifications.put( name, modification );
  }

  public static Modification getModification( String name )
  {
    Object retn = modifications.get( name );
    if( retn == null ) throw new NoSuchModificationException
      ( "Attempt to recover modification of name " + name + " which does not exist. " +
	"Use the ModificationFactory.createModification instead" );
    
    return (Modification)retn;
  }
  
  public static Modification createModification( String name, char toChar )
  {
    // create a new modification
    GenericModification mod = new GenericModification( name, toChar );
    // and add it to the cache, also duplicate check
    addNewGenericModification( name, mod );
    return mod;
  }
} // ModificationFactory



/*
 * ChangeLog
 * $Log: ModificationFactory.java,v $
 * Revision 1.4  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.3  2000/11/27 18:14:02  lord
 * Realised that I had not implemented the ComplexSequence. Have now done
 * this and have made quite a few changes to this package as a result
 *
 * Revision 1.2  2000/03/01 14:27:36  lord
 * cosmetic changes to string output
 *
 * Revision 1.1  2000/02/29 11:24:58  lord
 * Initial Checkin
 *
 */
