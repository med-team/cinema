/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.sequence.complex; // Package name inserted by JPack

import uk.ac.man.bioinf.sequence.Residue;


/**
 * DefaultComplexElement.java
 *
 *
 * Created: Mon Feb 28 20:42:48 2000
 *
 * @author Phillip Lord
 * @version $Id: DefaultComplexElement.java,v 1.6 2001/04/11 17:04:43 lord Exp $
 */

public class DefaultComplexElement implements ComplexElement
{
  private Residue residue;
  private ModificationSet modifications;
  
  DefaultComplexElement()
  {
  }
  
  // element implementation
  public char toChar()
  {
    return residue.toChar();
  }

  // These methods are all package access because a ComplexElement
  // should really be immutable once it has been created. The problem
  // is with this is that it means we have to create a new
  // ComplexElement for each look up which is a waste of time. This
  // way we only create a new ComplexElement when we need a new one...
  void setResidue( Residue residue )
  {
    this.residue = residue;
  }
  
  void setModifications( Modification[] mods, Modification mod )
  {
    modifications = ModificationSetFactory.getModificationSet( mods, mod );
  }
  
  void setModifications( Modification[] mod )
  {
    modifications = ModificationSetFactory.getModificationSet( mod );
  }

  void setModifications( Modification mod )
  {
    modifications = ModificationSetFactory.getModificationSet( mod );
  }

  void setModifications()
  {
    modifications = ModificationSetFactory.getModificationSet();
  }

  // I originally put this method in, and leave it here to remind
  // myself not to do it. It would almost certainly be a mistake as
  // alterations to this, would also percolate through to the object
  // passed as a parameter. Which is almost certainly guarenteed to be
  // a mistake. 
  /*
  void setModifications( DefaultComplexElement elem )
  {
    this.modifications = elem.modifications;
  }
  */

  public Residue getResidue()
  {
    return residue;
  }
  
  public Modification[] getModifications()
  {
    return modifications.getModifications();
  }
  
  public boolean isModified( Modification mod )
  {
    return modifications.containsModification( mod );
  }

  public boolean equals( Object obj )
  {
    // if these are not of the same type they are not the
    // same. Obviously. 
    if( !( obj instanceof DefaultComplexElement ) ){
      return false;
    }
    
    // cast it down. Reasons to hate java?
    DefaultComplexElement elem = (DefaultComplexElement)obj;
    
    // if they contain a different Residue then return false. Residues
    // are considered equal if they are == therefore this is okay!
    if( elem.residue != this.residue ) return false;
    
    // if its got this far then are all the modifications equal
    return elem.modifications.equals( this.modifications );
  }

  public int hashCode()
  {
    // we dont really care if this overflows, as a wrapped around
    // hashcode is just as good a hash code.
    return residue.hashCode() + modifications.hashCode();
  }
} // DefaultComplexElement



/*
 * ChangeLog
 * $Log: DefaultComplexElement.java,v $
 * Revision 1.6  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.5  2000/12/05 15:54:37  lord
 * Import rationalisation
 *
 * Revision 1.4  2000/11/27 18:14:02  lord
 * Realised that I had not implemented the ComplexSequence. Have now done
 * this and have made quite a few changes to this package as a result
 *
 * Revision 1.3  2000/08/01 12:47:05  jns
 * o removed references to BioInterface and BioObject.
 *
 * Revision 1.2  2000/03/01 14:25:50  lord
 * Now uses factory based Modification Sets
 *
 * Revision 1.1  2000/02/29 11:24:57  lord
 * Initial Checkin
 *
 */

