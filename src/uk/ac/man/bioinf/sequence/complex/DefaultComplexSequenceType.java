/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.sequence.complex; // Package name inserted by JPack
import uk.ac.man.bioinf.sequence.Element;
import uk.ac.man.bioinf.sequence.SequenceType;


/**
 * DefaultComplexSequenceType.java
 *
 *
 * Created: Mon Nov 27 16:05:27 2000
 *
 * @author Phillip Lord
 * @version $Id: DefaultComplexSequenceType.java,v 1.2 2001/04/11 17:04:43 lord Exp $
 */

public class DefaultComplexSequenceType implements ComplexSequenceType
{
  private Modification[] mods;
  private SequenceType type;
  private String name;
  
  public DefaultComplexSequenceType( SequenceType type, Modification[] mods, String name )
  {
    this.type = type;
    this.mods = mods;
    this.name = name;
  }
  
  public String getName()
  {
    return name;
  }
  
  public Modification[] getModifications()
  {
    return ((Modification[])mods.clone());
  }
  
  public boolean isModification( Modification mod )
  {
    // if there were lots of Modifications we could probably speed
    // this up with a hash look up. 
    for( int i = 0; i < mods.length; i++ ){
      if( mods[ i ].equals( mod ) ){
	return true;
      }
    }
    
    return false;
  }
  
  public boolean isModification( Modification[] mods )
  {
    for( int i = 0; i < mods.length; i++ ){
      if( !isModification( mods[ i ] ) ){
	return false;
      }
    }
    return true;
  }

  public boolean isAllowableElement( ComplexElement element )
  {
    return
      ( type.isElement( element.getResidue() ) &&
	isModification( element.getModifications() ));
  }
  
  // Code for delegation of uk.ac.man.bioinf.sequence.SequenceType methods to type
  public boolean isElement(Element[] param1) {
    return type.isElement(param1);
  }
  
  public boolean isElement(char[] param1) {
    return type.isElement(param1);
  }
  
  public boolean isElement(Element param1) {
    return type.isElement(param1);
  }
  
  public boolean isElement(char param1) {
    return type.isElement(param1);
  }
  
  public Element[] getElements() {
    return type.getElements();
  }
  
  public Element getElementForInt(int param1) {
    return type.getElementForInt(param1);
  }
  
  public int getIntForElement(Element param1) {
    return type.getIntForElement(param1);
  }
  
  public int size() {
    return type.size();
  }
} // DefaultComplexSequenceType



/*
 * ChangeLog
 * $Log: DefaultComplexSequenceType.java,v $
 * Revision 1.2  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.1  2000/11/27 18:14:02  lord
 * Realised that I had not implemented the ComplexSequence. Have now done
 * this and have made quite a few changes to this package as a result
 *
 */
