/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.sequence.complex; // Package name inserted by JPack
import uk.ac.man.bioinf.sequence.NoSuchSequenceElementException;
import uk.ac.man.bioinf.sequence.Sequence;
import uk.ac.man.bioinf.sequence.event.SequenceVetoException;

/**
 * ComplexSequence.java
 *
 *
 * Created: Wed Nov 22 17:56:08 2000
 *
 * @author Phillip Lord
 * @version $Id: ComplexSequence.java,v 1.2 2001/04/11 17:04:43 lord Exp $ 
 */

public interface ComplexSequence extends Sequence
{
  public ComplexElement addModificationAt( Modification mod, int index )
    throws NoSuchSequenceElementException, SequenceVetoException;
  
  public ComplexElement removeModificationAt( Modification mod, int index )
    throws NoSuchSequenceElementException, SequenceVetoException, NoSuchModificationException;
  
  public ComplexElement getComplexElementAt( int index );
   
  /**
   * Representing a ComplexElement as a char is not a simple task. An
   * Element can have any number of Modifications each of which comes
   * with its own char representation. The following approach is
   * used. Each Element is represented first by its own char
   * representation, and then by its all of its modifications, and
   * then finally by an underscore _, which acts as a standard
   * terminator. Counting the number of underscores should show how
   * many Elements there are. 
   * This is not meant to be a particular wonderfully parse-able
   * format. 
   *
   * @param index
   * @return a <code>char[]</code> value
   */
  public char[] getComplexElementAtAsChar( int index );

  public char[] getComplexSequenceAsChars();
  
}// ComplexSequence


/*
 * ChangeLog
 * $Log: ComplexSequence.java,v $
 * Revision 1.2  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.1  2000/11/27 18:14:02  lord
 * Realised that I had not implemented the ComplexSequence. Have now done
 * this and have made quite a few changes to this package as a result
 * 
 */

