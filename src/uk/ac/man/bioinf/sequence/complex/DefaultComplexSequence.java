/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.sequence.complex; // Package name inserted by JPack
import java.util.ArrayList;
import java.util.Arrays;
import uk.ac.man.bioinf.sequence.Element;
import uk.ac.man.bioinf.sequence.Residue;
import uk.ac.man.bioinf.sequence.Sequences;
import uk.ac.man.bioinf.sequence.identifier.Identifier;


/**
 * DefaultComplexSequence.java
 *
 *
 * Created: Fri Nov 24 14:08:23 2000
 *
 * @author Phillip Lord
 * @version $Id: DefaultComplexSequence.java,v 1.3 2001/04/11 17:04:43 lord Exp $
 */

public class DefaultComplexSequence extends AbstractComplexSequence
{
  private ArrayList elements;
  private Identifier ident;
  
  public DefaultComplexSequence( Residue[] elements, ComplexSequenceType type, Identifier identifier )
  {
    super( type  );
    
    checkSequenceType( elements );
    
    this.ident = ident;

    ComplexElement[] complex = Sequences.getResiduesAsComplexElements( elements );
    this.elements = new ArrayList( Arrays.asList( complex ) );
  }
				 
  public DefaultComplexSequence( ComplexElement[] elements, ComplexSequenceType type, Identifier identifier )
  {
    super( type );
    
    checkComplexSequenceType( elements );
    this.ident = ident;
    
    this.elements = new ArrayList( Arrays.asList( elements ) );
  }
  
  public Identifier getIdentifier()
  {
    return ident;
  }
  
  public Element getElementAt( int index )
  {
    return getComplexElementAt( index ).getResidue();
  }

  public ComplexElement getComplexElementAt( int index )
  {
    return (ComplexElement)elements.get( index - 1 );
  }
  
  public int getLength()
  {
    return elements.size();
  }
  
  public ComplexElement[] getSequenceAsComplexElements()
  {
    Object[] elemObjs = elements.toArray();
    ComplexElement[] elems = new ComplexElement[ elemObjs.length ];
    
    System.arraycopy( elemObjs, 0, elems, 0, elemObjs.length );
    return elems;
  }
  
  public Element[] getSequenceAsElements()
  {
    // get the elements, and make the destination array
    Object[] elemObjs = elements.toArray();
    Element[] elems = new Element[ elemObjs.length ];
    
    // do the copy to array of appropriate type and return it
    System.arraycopy( elemObjs, 0, elems, 0, elemObjs.length );
    return elems;
  }
  
  protected ComplexElement setElementAtQuietly( ComplexElement elem, int index )
  {
    checkComplexSequenceType( elem );
    elements.set( index - 1, elem );
    return elem;
  }
  
}// DefaultComplexSequence

/*
 * ChangeLog
 * $Log: DefaultComplexSequence.java,v $
 * Revision 1.3  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.2  2000/12/13 16:35:25  lord
 * Some implementation moved to Sequences static class
 *
 * Revision 1.1  2000/11/27 18:14:02  lord
 * Realised that I had not implemented the ComplexSequence. Have now done
 * this and have made quite a few changes to this package as a result
 * 
 */

  
  

