/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.sequence.complex; // Package name inserted by JPack
import uk.ac.man.bioinf.sequence.Residue;
import java.util.Map;
import java.util.HashMap;


/**
 * HardComplexElementFactory.java
 *
 *
 * Created: Tue Feb 29 00:21:47 2000
 *
 * @author Phillip Lord
 * @version $Id: HardComplexElementFactory.java,v 1.3 2001/04/11 17:04:43 lord Exp $
 */

class HardComplexElementFactory 
{
  // its in here that we keep a modification which is used each time
  // we do a get call. This way we dont have to instantiate a new
  // object for every get, only those where a previously cached object
  // is not found
  private DefaultComplexElement cachedElement;
  private Map complexElements = new HashMap();
  
  public ComplexElement getComplexElement( Residue res, Modification mod )
  {
    // first we need a DefaultElement to be dealing withh
    if( cachedElement == null ){
      cachedElement = new DefaultComplexElement();
    }
    
    // now set this modification so that it looks like the one that we
    // want. 
    cachedElement.setResidue( res );
    cachedElement.setModifications( mod );
  
    return getCachedComplexElement();
  }
  
  public ComplexElement getComplexElement( ComplexElement elem, Modification mod )
  {
    // do we need a new element
    if( cachedElement == null ){
      cachedElement = new DefaultComplexElement();
    }
    
    // we now want to set the Modification up to duplicate the old one
    // (PENDING) I think that there is a problem here. Really this
    // method should work whether for all objects of type
    // ComplexElement. The problem is at the moment I havent
    // adequately defined the notion of equality of two
    // ComplexElements. Its not clear for instance whether two
    // ComplexElements of different classes, but which contain
    // indentical Residues and Modifications, should be equal. 

    cachedElement.setResidue( elem.getResidue() );
    cachedElement.setModifications( elem.getModifications(), mod );

    // now return the cached copy if it exists, or the one we have
    // just created.
    return getCachedComplexElement();
  }
  
  private ComplexElement getCachedComplexElement()
  {
    // See whether we already have this complex element???
    ComplexElement retn = (ComplexElement)complexElements.get( cachedElement );
    
    // We do already have this element, therefore return the one we
    // already have, keeping the old one for later
    if( retn != null ){
      return retn;
    }
    
    // we dont already have this element, so store the one we have
    // created and return that, make sure that we null the internal we
    // are storing in this class so that we dont use it next time. We
    // store it as the valued keyed on itself, so that we can retrieve
    // this ComplexElement. The point of all this is that
    // ComplexElements which are .equals() to each other should also
    // therefore be == to each other outside of this package.
    complexElements.put( cachedElement, cachedElement );
    retn = cachedElement;
    cachedElement = null;
    return retn;
  }
} // HardComplexElementFactory



/*
 * ChangeLog
 * $Log: HardComplexElementFactory.java,v $
 * Revision 1.3  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.2  2000/05/30 16:04:18  lord
 * Have rationalised and sorted all the import statements
 *
 * Revision 1.1  2000/03/01 14:33:08  lord
 * Initial checkin
 *
 * Revision 1.1  2000/02/29 11:24:57  lord
 * Initial Checkin
 *
 */









