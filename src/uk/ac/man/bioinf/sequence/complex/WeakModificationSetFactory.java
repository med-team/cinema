/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.sequence.complex; // Package name inserted by JPack
import java.lang.ref.WeakReference;
import java.util.WeakHashMap;


/**
 * WeakModificationSetFactory.java
 *
 *
 * Created: Wed Mar  1 15:09:49 2000
 *
 * @author Phillip Lord
 * @version $Id: WeakModificationSetFactory.java,v 1.3 2001/04/11 17:04:43 lord Exp $
 */

public class WeakModificationSetFactory implements ModificationSetFactoryInterface
{

  private ModificationSet cachedSet;
  private WeakHashMap modificationSet = new WeakHashMap();

  public ModificationSet getModificationSet()
  {
    if( cachedSet == null ){
      cachedSet = new ModificationSet();
    }
    
    cachedSet.setModification();
    return getCachedModificationSet();
  }
  
  public ModificationSet getModificationSet(Modification modification ) 
  {
    if( cachedSet == null ){
      cachedSet = new ModificationSet();
    }
    
    cachedSet.setModification( modification );
    return getCachedModificationSet();
  }
  
  public ModificationSet getModificationSet(Modification[] modifications ) 
  {
    if( cachedSet == null ){
      cachedSet = new ModificationSet();
    }
    
    cachedSet.setModification( modifications );
    return getCachedModificationSet();
  }
  
  public ModificationSet getModificationSet(Modification[] modifications, Modification modification ) 
  {
    // make an array which is big enough.
    int size = modifications.length;
    Modification[] longModifications = new Modification[ size + 1 ];
    // copy the old array across
    System.arraycopy( modifications, 0, longModifications, 0, size );
    // and stuff the new one in at then end
    longModifications[ size ] = modification;
    
    return getModificationSet( longModifications );
  }
  
  private ModificationSet getCachedModificationSet()
  {
    // get back the weak reference if it is there
    Object retnRef = modificationSet.get( cachedSet ) ;
    
		      
    if( retnRef != null ){
      // we have a weak reference, get what it refers to 
      Object retnObj = ((WeakReference)retnRef).get();
      
      // check for null again because its possible that the weakly
      // refered object has been gc'd between the get call and now
      if( retnObj != null ){
	return (ModificationSet)retnObj;
      }
    }
    
    // we dont already have this element so we store the one we have
    // created, null the cachedReference so that we dont use it again,
    // and then return it
    modificationSet.put( cachedSet, new WeakReference( cachedSet ) );
    ModificationSet retn = cachedSet;
    cachedSet = null;
    return retn;    
  }
  
} // WeakModificationSetFactory



/*
 * ChangeLog
 * $Log: WeakModificationSetFactory.java,v $
 * Revision 1.3  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.2  2000/11/27 18:14:02  lord
 * Realised that I had not implemented the ComplexSequence. Have now done
 * this and have made quite a few changes to this package as a result
 *
 * Revision 1.1  2000/03/01 20:17:55  lord
 * Lots of changes
 *
 */
