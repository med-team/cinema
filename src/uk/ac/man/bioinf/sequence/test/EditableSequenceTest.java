/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.sequence.test; // Package name inserted by JPack
import java.util.Random;
import uk.ac.man.bioinf.sequence.DefaultEditableSequence;
import uk.ac.man.bioinf.sequence.EditableSequence;
import uk.ac.man.bioinf.sequence.Element;
import uk.ac.man.bioinf.sequence.Sequence;
import uk.ac.man.bioinf.sequence.event.SequenceEvent;
import uk.ac.man.bioinf.sequence.event.SequenceListener;
import uk.ac.man.bioinf.sequence.event.SequenceVetoException;
import uk.ac.man.bioinf.sequence.event.VetoableSequenceEvent;
import uk.ac.man.bioinf.sequence.event.VetoableSequenceListener;
import uk.ac.man.bioinf.sequence.identifier.NoIdentifier;
import uk.ac.man.bioinf.sequence.types.AminoAcid;
import uk.ac.man.bioinf.sequence.types.ProteinSequenceType;


/**
 * EditableSequenceTest.java
 *
 *
 * Created: Fri Mar  3 16:08:35 2000
 *
 * @author Phillip Lord
 * @version $Id: EditableSequenceTest.java,v 1.5 2001/04/11 17:04:43 lord Exp $
 */

public class EditableSequenceTest 
{
  public static void main( String[] args )
  {
    System.out.println( "This class tests out the DefaultEditableSequenceClass. Its is designed to test " );
    System.out.println( "that the class allows correct editing of sequences and further signals the correct " );
    System.out.println( "events" );
    
    System.out.println( "First make a series of elements up to test the sequence out " );
    
    AminoAcid[] elems = AminoAcid.getAll();
    
    DefaultEditableSequence seq = new DefaultEditableSequence( elems, ProteinSequenceType.getInstance(),
							       new NoIdentifier() );
    
    printSeq( seq );
    System.out.println( "*************" );
    System.out.println();
    System.out.println();
    

    System.out.println( "Testing out alteration, addition and removal of elements " );
    alterSeq( seq );
    System.out.println( "Editability testing complete" );
    System.out.println( "****************************" );
    System.out.println();
    System.out.println();
    
    System.out.println( "Testing out listening abilities" );
    System.out.println( "Adding sequence listener to sequence " );
    SequenceListener seqList = new SequenceListener(){
	public void changeOccurred( SequenceEvent event )
	{
	  System.out.println( event );
	}
      };
    
    seq.addSequenceListener( seqList );

    System.out.println( "Repeating addn removal tests. The listner should print out the event for each of the change events" );
    alterSeq( seq );
    System.out.println( "Removing listener " );
    seq.removeSequenceListener( seqList );
    System.out.println( "Listener tests complete" );
    System.out.println( "***********************" );
    System.out.println();
    System.out.println();
    

    System.out.println( "Now testing with a compliant veto listener" );
    VetoableSequenceListener compList = new VetoableSequenceListener(){
	public void changeOccurred( SequenceEvent event )
	{
	  System.out.println( event );
	}
	public void vetoableChangeOccurred( VetoableSequenceEvent event )
	{
	  System.out.println( event );
	}
      };
    seq.addVetoableSequenceListener( compList );
    alterSeq( seq );
    seq.removeSequenceListener( compList );
    
    System.out.println( "Compliant listener tests complete" );
    System.out.println( "*********************************" );
    System.out.println();
    System.out.println();
    
    System.out.println( "Testing with a non compliant listener" );
    System.out.println( "These should give rise to exceptions which are caught" );
    VetoableSequenceListener nonComp = new VetoableSequenceListener(){
	public void changeOccurred( SequenceEvent event )
	{
	  System.out.println( event );
	}
	
	public void vetoableChangeOccurred( VetoableSequenceEvent event ) throws SequenceVetoException
	{
	  System.out.println( event );
	  System.out.println( "Now throwing veto to this event " );
	  throw new SequenceVetoException( "Blarrrgh", event );
	}
      };
    seq.addVetoableSequenceListener( nonComp );
    alterSeq( seq );
    System.out.println( "Non Compliant Listener test complete" );
    System.out.println( "************************************" );
    
    System.out.println();
    System.out.println();
    
    System.out.println( "Test class complete" );
  } //end main method 

  public static void printSeq( Sequence seq )
  {
    System.out.println( "The elements making up this sequence are :- " );
    System.out.println( String.valueOf( seq.getSequenceAsChars() ) );
  }
  
  private static void alterSeq( EditableSequence seq )
  {
    alterElement( seq );
    insertAndRemoveElement( seq );
  }
  

  private static void alterElement( EditableSequence seq )
  {
    try{
      Random rand = new Random();
      
      int index = rand.nextInt( seq.getLength() - 1 ) + 1;
      
      System.out.println( "Changing the element at " +  index + " to glycine" );
      Element old = seq.setElementAt( AminoAcid.GLYCINE, index );
      
      System.out.println( "Sequence is now " );
      printSeq( seq );
      
      System.out.println( "Changing sequence back " );
      seq.setElementAt( old, index );
      printSeq( seq );
    }
    catch( SequenceVetoException sve ){
      System.out.println( "There has been a veto" );
      System.out.println( sve );
    }
  }
  
  private static void insertAndRemoveElement( EditableSequence seq )
  {
    try{
      Random rand = new Random();
      
      int index = rand.nextInt( seq.getLength() - 1 ) + 1;
      
      System.out.println( "Inserting a glycine at " + index );
      seq.insertElementAt( AminoAcid.GLYCINE, index );
      
      System.out.println( "Sequence is now:- " );
      printSeq( seq );
      
      System.out.println( "Now removing this element " );
      
      System.out.println( "Removal: " + seq.deleteElementAt( index ) );
    }
    catch( SequenceVetoException sve ){
    }
  }
} // EditableSequenceTest
  


/*
 * ChangeLog
 * $Log: EditableSequenceTest.java,v $
 * Revision 1.5  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.4  2000/09/15 17:04:57  lord
 * Updated due to change in SequenceIdentifier name
 *
 * Revision 1.3  2000/07/18 12:38:54  lord
 * Import rationalisation
 * Changes due to BioInterface removal
 *
 * Revision 1.2  2000/04/18 17:52:27  jns
 * o changes madde because of files moved to types package.
 *
 * Revision 1.1  2000/03/08 17:26:36  lord
 * To many changes to document
 *
 */












