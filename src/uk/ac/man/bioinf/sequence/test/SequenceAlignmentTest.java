/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Julian Selley (j.selley@man.ac.uk)
 * whilst at the University of Manchester as a Research Assistant. 
 *
 * The initial code base is copyright by the University of Manchester. 
 * Modifications to the initial code base are copyright of their 
 * respective authors, or their employers as appropriate. Authorship 
 * of the modifications may be determined from the ChangeLog placed 
 * at the end of this file
 */

package uk.ac.man.bioinf.sequence.test; // Package name inserted by JPack
import uk.ac.man.bioinf.sequence.alignment.DefaultGappedSequence;
import uk.ac.man.bioinf.sequence.alignment.DefaultSequenceAlignment;
import uk.ac.man.bioinf.sequence.event.SequenceListener;
import uk.ac.man.bioinf.sequence.event.VetoableSequenceListener;
import uk.ac.man.bioinf.sequence.alignment.event.AlignmentListener;
import uk.ac.man.bioinf.sequence.alignment.event.VetoableAlignmentListener;
import uk.ac.man.bioinf.sequence.event.SequenceEvent;
import uk.ac.man.bioinf.sequence.event.VetoableSequenceEvent;
import uk.ac.man.bioinf.sequence.event.SequenceVetoException;
import uk.ac.man.bioinf.sequence.alignment.event.AlignmentEvent;
import uk.ac.man.bioinf.sequence.alignment.event.VetoableAlignmentEvent;
import uk.ac.man.bioinf.sequence.alignment.event.AlignmentVetoException;
import uk.ac.man.bioinf.sequence.Element;
import uk.ac.man.bioinf.sequence.Sequences;
import uk.ac.man.bioinf.sequence.types.ProteinSequenceType;
import uk.ac.man.bioinf.sequence.types.AminoAcid;


/**
 * SequenceAlignmentTest.java
 *
 *
 * Created: Fri Mar 10 12:39:40 2000
 *
 * @author J Selley
 * @version $Id: SequenceAlignmentTest.java,v 1.5 2001/04/11 17:04:43 lord Exp $
 */

public class SequenceAlignmentTest implements SequenceListener,
					      VetoableSequenceListener,
					      AlignmentListener,
					      VetoableAlignmentListener
{
  private SequenceAlignmentTest()
  {
    // print header...
    System.out.println("-= SequenceAlignmentTest =-");
    System.out.println("\nTesting the sequence alignment class, and the events\n");
    
    // generate sequences of elements
    Element[][] elems = new Element[3][];
    elems[0] = new Element[] {
      AminoAcid.ALANINE,
      AminoAcid.ARGININE,
      AminoAcid.ASPARAGINE,
      AminoAcid.ASPARTICACID,
      AminoAcid.CYSTEINE,
      AminoAcid.GLUTAMICACID,
      AminoAcid.GLUTAMINE,
      AminoAcid.GLYCINE,
      AminoAcid.HISTIDINE,
      AminoAcid.ISOLEUCINE,
      AminoAcid.LEUCINE,
      AminoAcid.LYSINE,
      AminoAcid.METHIONINE,
      AminoAcid.PHENYLALANINE,
      AminoAcid.SERINE,
      AminoAcid.THREONINE,
      AminoAcid.TYROSINE,
      AminoAcid.TRYPTOPHAN,
      AminoAcid.VALINE
    };
    elems[1] = AminoAcid.getAll();
    elems[2] = new Element[] {
      AminoAcid.VALINE,
      AminoAcid.PROLINE,
      AminoAcid.METHIONINE,
      AminoAcid.ALANINE
    };
    System.out.println("\ngenerated sequences of elements. The lengths are:");
    for (int i = 0; i < elems.length; i++) {
      System.out.print("  " + Integer.toString(elems[i].length));
    }
    System.out.print("\n");
    
    // generate sequences from elems array
    DefaultGappedSequence seqs[] = new DefaultGappedSequence[elems.length];
    for (int i = 0; i < seqs.length; i++) {
      seqs[i] = new DefaultGappedSequence
	(elems[i], ProteinSequenceType.getInstance());
    }
    System.out.println("\ngenerated sequences:");
    for (int i = 0; i < seqs.length; i++) {
      System.out.println("  " + String.valueOf(seqs[i].getSequenceAsChars()));
    }
    
    // make sequences all same length
    int max_seq_length = 0;
    for (int i = 0; i < seqs.length; i++) {
      if (max_seq_length < seqs[i].getGappedLength())
	max_seq_length = seqs[i].getGappedLength();
    }
    for (int i = 0; i < seqs.length; i++) {
      if ((max_seq_length - seqs[i].getGappedLength()) > 0) {
	try {
	  seqs[i].insertGapAt((seqs[i].getGappedLength() + 1), 
			       (max_seq_length - seqs[i].getGappedLength()));
	} catch (SequenceVetoException e) {
	  System.out.println("    caught sequence veto exception with " +
			     "sequence " + Integer.toString(i) + ".");
	  e.printStackTrace();
	}
      }
    }
    System.out.println("\nsequences lengthened and now read:");
    for (int i = 0; i < seqs.length; i++) {
      System.out.println("  " + String.valueOf(seqs[i].getGappedSequenceAsChars()));
    }
    
    // generate msa
    DefaultSequenceAlignment msa = new DefaultSequenceAlignment
      (seqs, ProteinSequenceType.getInstance());
    System.out.println("\ngenerated a sequence alignment (type: " +
		       msa.getSequenceType() + ") of the " + 
		       Integer.toString(msa.getNumberSequences()) + 
		       " sequences, ");
    System.out.println("which makes the msa " + 
		       Integer.toString(msa.getLength()) + 
		       " elements long...");
    System.out.println("sequence at index 3: " + 
		       Sequences.getGappedSequenceAsString
		       ((msa.getSequenceAt(3))));
    System.out.println("  reverse using getSequenceIndex: " +
		       Integer.toString(msa.getSequenceIndex
					(msa.getSequenceAt(3))));
    
    
    // generating a sub-alignment
    System.out.println("\ngetting a sub-alignment [3,10,1,3]...");
    DefaultSequenceAlignment submsa = (DefaultSequenceAlignment)
      msa.getSubAlignment(3, 10, 1, 3);
    for (int i = 0; i < submsa.getNumberSequences(); i++) {
      System.out.println("  " + Sequences.getGappedSequenceAsString
			 (submsa.getSequenceAt(i + 1)));
    }

    Sequences.printAlignment( submsa );

    // become a listener of the alignment
    msa.addAlignmentListener(this);
    System.out.println("\nadded myself as a listener of the MSA...");

    System.out.println("\nsetting inset on sequence 1 of 3...");
    try {
      msa.setInset(1, 3);
    } catch (AlignmentVetoException e) {
      System.out.print("  EXCEPTION: Alignment veto exception was thrown and caught\n" +
			 "    ");
      e.printStackTrace();
      System.exit(1);
    }

    // print out msa, with '_' representing inset's
    System.out.println("msa now:");
    for (int i = 0; i < msa.getNumberSequences(); i++) {
      System.out.print("  ");
      for (int j = 0; j < msa.getInset(i + 1); j++)
	System.out.print("_");
      System.out.println(Sequences.getGappedSequenceAsString(msa.getSequenceAt(i + 1)));
    }
    
    
    System.out.println("\n\n-= SequenceAlignmentTest class complete =-");
  }

  public static void main(String[] args) 
  {
    SequenceAlignmentTest sat = new SequenceAlignmentTest();
  }

  public void changeOccurred(SequenceEvent event) 
  {
    System.out.println("\n-< a sequence event was fired to indicate a " + 
		       "change >-");
  }
  
  public void vetoableChangeOccurred(VetoableSequenceEvent event) 
    throws SequenceVetoException 
  {
    System.out.println("\n-< a vetoable sequence event was fired to indicate " + 
		       "a change >-");
  }
  
  public void changeOccurred(AlignmentEvent event) 
  {
    System.out.println("\n-< an alignment event was fired to indicate a " + 
		       "change to the alignment >-");
  }
  
  public void vetoableChangeOccurred(VetoableAlignmentEvent event)
    throws AlignmentVetoException 
  {
    System.out.println("\n-< a vetoable alignment event was fired to indicate " +
		       "a change to the alignment >-");
  }
} // SequenceAlignmentTest



/*
 * ChangeLog
 * $Log: SequenceAlignmentTest.java,v $
 * Revision 1.5  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.4  2000/05/30 16:04:18  lord
 * Have rationalised and sorted all the import statements
 *
 * Revision 1.3  2000/04/18 17:52:28  jns
 * o changes madde because of files moved to types package.
 *
 * Revision 1.2  2000/03/21 13:14:51  jns
 * o added listener stuff, plus a check on the get/set Inset methods.
 *
 * Revision 1.1  2000/03/16 17:32:56  jns
 * initial code
 *
 */
