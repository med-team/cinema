/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.sequence.test; // Package name inserted by JPack
import uk.ac.man.bioinf.sequence.InvalidSequenceTypeException;
import uk.ac.man.bioinf.sequence.Residue;
import uk.ac.man.bioinf.sequence.Sequences;
import uk.ac.man.bioinf.sequence.complex.ComplexSequence;
import uk.ac.man.bioinf.sequence.complex.DefaultComplexSequence;
import uk.ac.man.bioinf.sequence.complex.DefaultComplexSequenceType;
import uk.ac.man.bioinf.sequence.complex.Methylation;
import uk.ac.man.bioinf.sequence.complex.Modification;
import uk.ac.man.bioinf.sequence.complex.ModificationFactory;
import uk.ac.man.bioinf.sequence.identifier.NoIdentifier;
import uk.ac.man.bioinf.sequence.types.RNANucleotide;
import uk.ac.man.bioinf.sequence.types.RNASequenceType;


/**
 * ComplexSequenceTest.java
 *
 *
 * Created: Mon Nov 27 14:09:29 2000
 *
 * @author Phillip Lord
 * @version $Id: ComplexSequenceTest.java,v 1.2 2001/04/11 17:04:43 lord Exp $
 */

public class ComplexSequenceTest 
{
  public static void main( String[] args ) throws Throwable
  {
    System.out.println( "First testing construction of the an RNA sequence " );
    Modification[] mods = 
      {
	Methylation.METHYLATION 
      };

    ComplexSequence seq = new DefaultComplexSequence( createRNAResidues(), new DefaultComplexSequenceType
						      ( RNASequenceType.getInstance(),
							mods, 
							"Methylated RNA "),
						      new NoIdentifier() );
    
    System.out.println( "Have sequence of " + seq.getLength() + " length" );
    Sequences.printSequence( seq );


    System.out.println( "Now trying to methylate some residues" );
    
    seq.addModificationAt( Methylation.METHYLATION, 4);
    seq.addModificationAt( Methylation.METHYLATION, 5);
    seq.addModificationAt( Methylation.METHYLATION, 8);
    
    Sequences.printSequence( seq );

    System.out.println( "Now attempting to phosphorylate an element (should fail ) " );
    Modification phospho = ModificationFactory.createModification( "Phospho", 'p' );
    
    try{
      seq.addModificationAt( phospho, 6 );
    }
    catch( InvalidSequenceTypeException exp ) {
      System.out.println( "Exception caught correctly " );
    }
    
    System.out.println( "Starting again with phospho included in type" );
    Modification[] mods2 = 
      {
	Methylation.METHYLATION,
	phospho
      };

    
    seq = new DefaultComplexSequence( createRNAResidues(), new DefaultComplexSequenceType
				      ( RNASequenceType.getInstance(),
					mods2, 
					"Methylated RNA "),
				      new NoIdentifier() );
    
    
    Sequences.printSequence( seq );
    
    seq.addModificationAt( Methylation.METHYLATION, 4);
    seq.addModificationAt( Methylation.METHYLATION, 5);
    seq.addModificationAt( Methylation.METHYLATION, 8);

    Sequences.printSequence( seq );
    
    seq.addModificationAt( phospho, 4);
    seq.addModificationAt( phospho, 10);
    seq.addModificationAt( phospho, 8);
    
    Sequences.printSequence( seq );
    
    
  } //end main method 
  
  public static Residue[] createRNAResidues()
  {
    Residue[] res = 
      {

	RNANucleotide.ADENOSINE,
	RNANucleotide.URACIL,
	RNANucleotide.CYTOSINE,
	RNANucleotide.GUANINE,
	RNANucleotide.ADENOSINE,
	RNANucleotide.URACIL,
	RNANucleotide.CYTOSINE,
	RNANucleotide.GUANINE,
	RNANucleotide.ADENOSINE,
	RNANucleotide.URACIL,
	RNANucleotide.CYTOSINE,
	RNANucleotide.GUANINE,
	RNANucleotide.ADENOSINE,
	RNANucleotide.URACIL,
	RNANucleotide.CYTOSINE,
	RNANucleotide.GUANINE,
	RNANucleotide.ADENOSINE,
	RNANucleotide.URACIL,
	RNANucleotide.CYTOSINE,
	RNANucleotide.GUANINE,
	RNANucleotide.ADENOSINE,
	RNANucleotide.URACIL,
	RNANucleotide.CYTOSINE,
	RNANucleotide.GUANINE,
	RNANucleotide.ADENOSINE,
	RNANucleotide.URACIL,
	RNANucleotide.CYTOSINE,
	RNANucleotide.GUANINE,
	RNANucleotide.ADENOSINE,
	RNANucleotide.URACIL,
	RNANucleotide.CYTOSINE,
	RNANucleotide.GUANINE,
	RNANucleotide.ADENOSINE,
	RNANucleotide.URACIL,
	RNANucleotide.CYTOSINE,
	RNANucleotide.GUANINE,
	RNANucleotide.ADENOSINE,
	RNANucleotide.URACIL,
	RNANucleotide.CYTOSINE,
	RNANucleotide.GUANINE,
	RNANucleotide.URACIL
      };
    return res;
  }
} // ComplexSequenceTest



/*
 * ChangeLog
 * $Log: ComplexSequenceTest.java,v $
 * Revision 1.2  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.1  2000/11/27 18:14:59  lord
 * *** empty log message ***
 *
 */

