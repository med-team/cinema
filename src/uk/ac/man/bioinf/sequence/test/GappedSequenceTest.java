/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.sequence.test; // Package name inserted by JPack
import java.util.Random;
import uk.ac.man.bioinf.apps.cinema.core.test.CinemaTestLaunch;
import uk.ac.man.bioinf.sequence.alignment.DefaultGappedSequence;
import uk.ac.man.bioinf.sequence.alignment.GappedSequence;
import uk.ac.man.bioinf.sequence.types.AminoAcid;
import uk.ac.man.bioinf.sequence.types.ProteinSequenceType;


/**
 * GappedSequenceTest.java
 *
 *
 * Created: Wed Jun  7 16:18:11 2000
 *
 * @author Phillip Lord
 * @version $Id: GappedSequenceTest.java,v 1.2 2001/04/11 17:04:43 lord Exp $
 */

public class GappedSequenceTest 
{

  public static void main( String[] args ) throws Throwable
  {
    System.out.println( "This is a test to make sure that the DefaultGappedSequence class is working" );
    System.out.println( "Events are not tested in this class" );
    
    GappedSequence seq;
    
    if( false ){
      AminoAcid[] elems = AminoAcid.getAll();
      
      seq = new DefaultGappedSequence( elems, ProteinSequenceType.getInstance() );
    }
    else{
      seq = CinemaTestLaunch.generateMsa().getSequenceAt( 2 );
    }
    
    reportLength( seq );
    
    System.out.println( "Inserting several random gaps" );
    
    for( int i = 0; i < 10; i++ ){
      Random rand = new Random();
      
      int index = rand.nextInt( seq.getLength() - 1 ) + 1;
      
      System.out.println( "Inserting a gap at " + index );
      seq.insertGapAt( index );
    }
    
    reportLength( seq );
    
    try{
      System.out.println( "Attempting to get element before start " );
      seq.getElementAt( 0 );
    }
    catch( Exception exp ){
      System.out.println( "Caught " + exp );
    }
    
    try{
      System.out.println( "Attempting to get element after end " );
      seq.getElementAt( 30 );
    }
    catch( Exception exp ){
      System.out.println( "Caught " + exp );
    }
    
    try{
      System.out.println( "Attempt to get gapped element before start " );
      seq.getGappedElementAt( 0 );
    }
    catch( Exception exp ){
      System.out.println( "Caught " + exp );
    }
    
    try{
      System.out.println( "Attempt to get gapped elemnt after end " );
      seq.getGappedElementAt( 590 );
    }
    catch( Exception exp ){
      System.out.println( "Caught " + exp );
    }
  } //end main method 
  
  private static void reportLength( GappedSequence seq )
  {
    System.out.println( "Sequence is " + seq.getLength() + " whilst gapped length is " + seq.getGappedLength() );
  }
  
} // GappedSequenceTest




/*
 * ChangeLog
 * $Log: GappedSequenceTest.java,v $
 * Revision 1.2  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.1  2000/06/13 11:02:29  lord
 * Boring changes
 *
 */
