/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.sequence.test; // Package name inserted by JPack

import uk.ac.man.bioinf.sequence.*;
import uk.ac.man.bioinf.sequence.Sequences;
import uk.ac.man.bioinf.sequence.alignment.Gap;
import uk.ac.man.bioinf.sequence.types.AminoAcid;
import uk.ac.man.bioinf.sequence.types.ProteinSequenceType;
import uk.ac.man.bioinf.sequence.Element;

/**
 * SequencesTest.java
 *
 *
 * Created: Tue Mar 14 14:19:44 2000
 *
 * @author Phillip Lord
 * @version $Id: SequencesTest.java,v 1.4 2001/04/11 17:04:43 lord Exp $
 */

public class SequencesTest 
{
  
  public static void main( String[] args )
  {
      // generate sequences of elements
    
    Element[] elems = {
      Gap.GAP,
      AminoAcid.ALANINE,
      AminoAcid.ARGININE,
      AminoAcid.ASPARAGINE,
      AminoAcid.ASPARTICACID,
      AminoAcid.CYSTEINE,
      Gap.GAP, 
      AminoAcid.GLUTAMICACID,
      AminoAcid.GLUTAMINE,
      AminoAcid.GLYCINE,
      AminoAcid.HISTIDINE,
      AminoAcid.ISOLEUCINE,
      AminoAcid.LEUCINE,
      AminoAcid.LYSINE,
      AminoAcid.METHIONINE,
      Gap.GAP,
      AminoAcid.PHENYLALANINE,
      AminoAcid.SERINE,
      AminoAcid.THREONINE,
      AminoAcid.TYROSINE,
      AminoAcid.TRYPTOPHAN,
      AminoAcid.VALINE,
      Gap.GAP
    };
    
    Sequences.printSequence( Sequences.getElementsAsGappedSequence( elems, ProteinSequenceType.getInstance() ) );
  } //end main method 
  
  
} // SequencesTest



/*
 * ChangeLog
 * $Log: SequencesTest.java,v $
 * Revision 1.4  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.3  2000/06/23 12:57:59  jns
 * o added in the Sequences import - it wasn't compiling.
 *
 * Revision 1.2  2000/04/18 17:52:28  jns
 * o changes madde because of files moved to types package.
 *
 * Revision 1.1  2000/03/16 16:17:40  lord
 * Initial checkin
 *
*/
