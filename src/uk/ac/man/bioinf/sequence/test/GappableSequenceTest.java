/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.sequence.test; // Package name inserted by JPack
import uk.ac.man.bioinf.sequence.alignment.DefaultGappedSequence;
import uk.ac.man.bioinf.sequence.event.SequenceListener;
import uk.ac.man.bioinf.sequence.event.SequenceEvent;
import uk.ac.man.bioinf.sequence.event.VetoableSequenceListener;
import uk.ac.man.bioinf.sequence.event.VetoableSequenceEvent;
import uk.ac.man.bioinf.sequence.event.SequenceVetoException;
import uk.ac.man.bioinf.sequence.Sequence;
import uk.ac.man.bioinf.sequence.alignment.GappedSequence;
import java.util.Random;
import uk.ac.man.bioinf.sequence.types.ProteinSequenceType;
import uk.ac.man.bioinf.sequence.types.AminoAcid;


/**
 * GappableSequenceTest.java
 *
 *
 * Created: Tue Mar  7 13:45:10 2000
 *
 * @author Phillip Lord
 * @version $Id: GappableSequenceTest.java,v 1.6 2001/04/11 17:04:43 lord Exp $
 */

public class GappableSequenceTest 
{
  public static void main( String[] args )
  {
    System.out.println( "This class tests out the DefaultGappedSequenceClass. Its is designed to test " );
    System.out.println( "that the class allows correct editing of sequences and further signals the correct " );
    System.out.println( "events" );
    
    System.out.println( "First make a series of elements up to test the sequence out " );
    
    Thread.dumpStack();
    
    AminoAcid[] elems = AminoAcid.getAll();
    
    DefaultGappedSequence seq = new DefaultGappedSequence( elems, ProteinSequenceType.getInstance() );
    
    System.out.println( "The value of the element at 20 is" + seq.getGappedElementAt( 20 ).toChar());
    
    printSeq( seq );
    System.out.println( "*************" );
    System.out.println();
    System.out.println();
    

    System.out.println( "Testing out alteration, addition and removal of elements " );
    alterSeq( seq );
    
    System.out.println( "Editability testing complete" );
    System.out.println( "****************************" );
    System.out.println();
    System.out.println();
    
    System.out.println( "Testing out listening abilities" );
    System.out.println( "Adding sequence listener to sequence " );
    SequenceListener seqList = new SequenceListener(){
	public void changeOccurred( SequenceEvent event )
	{
	  System.out.println( event );
	}
      };
    
    seq.addSequenceListener( seqList );

    System.out.println( "Repeating addn removal tests. The listner should print out the event for each of the change events" );
    alterSeq( seq );
    System.out.println( "Removing listener " );
    seq.removeSequenceListener( seqList );
    System.out.println( "Listener tests complete" );
    System.out.println( "***********************" );
    System.out.println();
    System.out.println();
    

    System.out.println( "Now testing with a compliant veto listener" );
    VetoableSequenceListener compList = new VetoableSequenceListener(){
	public void changeOccurred( SequenceEvent event )
	{
	  System.out.println( event );
	}
	public void vetoableChangeOccurred( VetoableSequenceEvent event )
	{
	  System.out.println( event );
	}
      };
    seq.addVetoableSequenceListener( compList );
    alterSeq( seq );
    seq.removeSequenceListener( compList );
    
    System.out.println( "Compliant listener tests complete" );
    System.out.println( "*********************************" );
    System.out.println();
    System.out.println();
    
    System.out.println( "Testing with a non compliant listener" );
    System.out.println( "These should give rise to exceptions which are caught" );
    VetoableSequenceListener nonComp = new VetoableSequenceListener(){
	public void changeOccurred( SequenceEvent event )
	{
	  System.out.println( event );
	}
	
	public void vetoableChangeOccurred( VetoableSequenceEvent event ) throws SequenceVetoException
	{
	  System.out.println( event );
	  System.out.println( "Now throwing veto to this event " );
	  throw new SequenceVetoException( "Blarrrgh", event );
	}
      };
    seq.addVetoableSequenceListener( nonComp );
    alterSeq( seq );
    System.out.println( "Non Compliant Listener test complete" );
    System.out.println( "************************************" );
    
    System.out.println();
    System.out.println();
    
    System.out.println( "Test class complete" );
  } //end main method 

  public static void printSeq( GappedSequence seq )
  {
    System.out.println( "The elements making up this sequence are :- " );
    System.out.println( String.valueOf( seq.getSequenceAsChars() ) );
    System.out.println( "The gapped elements making up this sequence are :- " );
    System.out.println( String.valueOf( seq.getGappedSequenceAsChars() ) );
  }
  
  private static void alterSeq( GappedSequence seq )
  {
    insertAndRemoveElement( seq );
  }
  
  private static void insertAndRemoveElement( GappedSequence seq )
  {
    try{
      Random rand = new Random();
      
      int index = rand.nextInt( seq.getLength() - 1 ) + 1;
      
      System.out.println( "Inserting a gap at " + index );
      seq.insertGapAt( index );
      
      System.out.println( "Sequence is now:- " );
      printSeq( seq );
      
      System.out.println( "Now removing this gap" );
      
      seq.deleteGapAt( index );
      
      System.out.println( "After removal" );
      printSeq( seq );
      
    }
    catch( SequenceVetoException sve ){
    }
  }
} // GappedSequenceTest



/*
 * ChangeLog
 * $Log: GappableSequenceTest.java,v $
 * Revision 1.6  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.5  2000/07/18 12:38:54  lord
 * Import rationalisation
 * Changes due to BioInterface removal
 *
 * Revision 1.4  2000/06/13 11:02:29  lord
 * Boring changes
 *
 * Revision 1.3  2000/04/18 17:52:27  jns
 * o changes madde because of files moved to types package.
 *
 * Revision 1.2  2000/03/16 16:20:34  lord
 * Boring changes
 *
 * Revision 1.1  2000/03/08 17:26:36  lord
 * To many changes to document
 *
 */
