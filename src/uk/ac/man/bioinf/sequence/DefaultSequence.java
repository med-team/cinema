/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.sequence; // Package name inserted by JPack
import uk.ac.man.bioinf.sequence.identifier.NoIdentifier;
import uk.ac.man.bioinf.sequence.identifier.Identifier;


/**
 * DefaultSequence.java
 *
 *
 * Created: Thu Mar  2 15:32:25 2000
 *
 * @author Phillip Lord
 * @version $Id: DefaultSequence.java,v 1.8 2001/04/11 17:04:43 lord Exp $
 */

public class DefaultSequence extends AbstractSequence
{
  private Element[] elements;
  private Identifier ident;
  
  
  public DefaultSequence( Element[] elements, SequenceType type, Identifier ident )
  {
    super( type );
    checkSequenceType( elements );
    // decided not to make a clone at this point although this would
    // probably have been the more secure thing to do, because its a
    // bit of a waste
    this.elements = elements;
    this.ident = ident;
  }
  
  public DefaultSequence( Element[] elements, SequenceType type )
  {
    this( elements, type, new NoIdentifier() );
  }
  
  public DefaultSequence( Sequence seq, SequenceType type )
  {
    this( seq.getSequenceAsElements(), type );
  }
  
  public DefaultSequence( Sequence seq )
  {
    this( seq.getSequenceAsElements(), seq.getSequenceType() );
  }
  
  public Identifier getIdentifier()
  {
    return ident;
  }
  
  public int getLength()
  {
    return elements.length;
  }
  
  public Element getElementAt( int index )
  {
    try{
      return elements[ index - 1 ];
    }
    catch( ArrayIndexOutOfBoundsException ofbe ){
      throw getSequenceIndexException( index );
    }
  }
  
  public Element[] getSequenceAsElements()
  {
    // return a clone here so that changes to this dont percolate through
    return (Element[])elements.clone();
  }
} // DefaultSequence



/*
 * ChangeLog
 * $Log: DefaultSequence.java,v $
 * Revision 1.8  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.7  2000/09/11 13:09:48  lord
 * SequenceIdentifier renamed to Identifier
 *
 * Revision 1.6  2000/07/18 11:12:58  lord
 * Import rationalisation
 * Changes due to BioInterface removal
 *
 * Revision 1.5  2000/05/18 17:05:19  lord
 * Added support for passing in BioObject in cons
 *
 * Revision 1.4  2000/03/23 19:51:36  lord
 * Improved range checking for efficiency of access to elements
 *
 * Revision 1.3  2000/03/16 16:13:48  lord
 * Inserted range check in getElementAt
 *
 * Revision 1.2  2000/03/08 17:26:35  lord
 * To many changes to document
 *
 * Revision 1.1  2000/03/02 17:47:43  lord
 * Trivial non functional implementation that I needed to complete
 * AbstractSequence
 *
 */
