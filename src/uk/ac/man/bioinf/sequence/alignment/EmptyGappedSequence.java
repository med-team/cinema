/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.sequence.alignment; // Package name inserted by JPack
import uk.ac.man.bioinf.sequence.Element;
import uk.ac.man.bioinf.sequence.NoSuchSequenceElementException;
import uk.ac.man.bioinf.sequence.Sequence;
import uk.ac.man.bioinf.sequence.SequenceType;
import uk.ac.man.bioinf.sequence.alignment.GappedSequence;
import uk.ac.man.bioinf.sequence.alignment.NoGapAtThisPositionException;
import uk.ac.man.bioinf.sequence.event.SequenceListener;
import uk.ac.man.bioinf.sequence.event.SequenceVetoException;
import uk.ac.man.bioinf.sequence.event.VetoableSequenceListener;
import uk.ac.man.bioinf.sequence.identifier.Identifier;
import uk.ac.man.bioinf.sequence.identifier.NoIdentifier;
import uk.ac.man.bioinf.sequence.types.EmptySequenceType;


/**
 * EmptyGappedSequence.java
 *
 *
 * Created: Tue Nov  7 18:15:06 2000
 *
 * @author Phillip Lord
 * @version $Id: EmptyGappedSequence.java,v 1.3 2001/04/11 17:04:43 lord Exp $
 */

public class EmptyGappedSequence  implements GappedSequence 
{
  private static final EmptyGappedSequence instance = new EmptyGappedSequence();
  
  public static EmptyGappedSequence getInstance()
  {
    return instance;
  }
  
  private EmptyGappedSequence()
  {
  }
  
  private NoSuchSequenceElementException createException()
  {
    return new NoSuchSequenceElementException
      ( "Empty Gapped Sequence does not do much" );
  }
  
  // implementation of uk.ac.man.bioinf.sequence.Sequence interface
  public Sequence getSubSequence(int param1, int param2) 
  {
    if( param1 != 0 || param2 != 0 )
      throw createException();
    
    return this;
  }

  public SequenceType getSequenceType() {
    return EmptySequenceType.getInstance();
  }
  
  private static Element[] noElements = new Element[ 0 ];
  public Element[] getSequenceAsElements() {
    return noElements;
  }

  private static char[] noChars = new char[ 0 ];
  
  public char[] getSequenceAsChars() {
    return noChars;
  }
  
  public int getLength() {
    return 0;
  }

  public char getElementAtAsChar(int param1) {
    throw createException();
  }
  
  public Element getElementAt(int param1) {
    throw createException();
  }

  public void insertGapAt(int param1, int param2) throws NoSuchSequenceElementException, SequenceVetoException {
    throw createException();
  }

  
  public void insertGapAt(int param1) throws NoSuchSequenceElementException, SequenceVetoException {
    throw createException();
  }

  public int getUngappedPositionOf(int param1) {
    throw createException();
  }
  
  public GappedSequence getGappedSubSequence(int param1, int param2) {
    if( param1 != 0 || param2 != 0 )
      throw createException();
    return this;
  }

  public Element[] getGappedSequenceAsElements() {
    return noElements;
  }

  public char[] getGappedSequenceAsChars() {
    return noChars;
  }


  public int getGappedPositionOf(int param1) {
    throw createException();
  }

  public int getGappedLength() {
    return 0;
  }
  
  public char getGappedElementAtAsChar(int param1) {
    throw createException();
  }

  public Element getGappedElementAt(int param1) {
    throw createException();
  }

  public void deleteGapAt(int param1, int param2) 
    throws NoGapAtThisPositionException, NoSuchSequenceElementException, SequenceVetoException {
    throw createException();
  }
  
  public void deleteGapAt(int param1) 
    throws NoGapAtThisPositionException, NoSuchSequenceElementException, SequenceVetoException {
    throw createException();
  }
  // implementation of uk.ac.man.bioinf.sequence.identifier.Identifiable interface
   // this is immutable so we can use a shared instance. 
  private static Identifier ident = new NoIdentifier();
  
  public Identifier getIdentifier()
  {
    return ident;
  }

  public void removeVetoableSequenceListener(VetoableSequenceListener param1) {
    // do nothing here!
  }
  
  public void removeSequenceListener(SequenceListener param1) {
    // do nothing
  }
  
  public void addVetoableSequenceListener(VetoableSequenceListener param1) {
    // do nothing
  }

  public void addSequenceListener(SequenceListener param1) {
    // do nothing
  }
} // EmptyGappedSequence



/*
 * ChangeLog
 * $Log: EmptyGappedSequence.java,v $
 * Revision 1.3  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.2  2000/12/05 15:54:37  lord
 * Import rationalisation
 *
 * Revision 1.1  2000/11/08 18:20:36  lord
 * Initial checkin
 *
 */
