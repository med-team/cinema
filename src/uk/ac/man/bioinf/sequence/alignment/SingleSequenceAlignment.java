/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.sequence.alignment; // Package name inserted by JPack
import uk.ac.man.bioinf.sequence.Sequence;
import uk.ac.man.bioinf.sequence.SequenceType;
import uk.ac.man.bioinf.sequence.alignment.GappedSequence;
import uk.ac.man.bioinf.sequence.alignment.GappedSequence;
import uk.ac.man.bioinf.sequence.alignment.NoSuchSequenceException;
import uk.ac.man.bioinf.sequence.alignment.SequenceAlignment;
import uk.ac.man.bioinf.sequence.alignment.event.AlignmentEventProvider;
import uk.ac.man.bioinf.sequence.alignment.event.AlignmentListener;
import uk.ac.man.bioinf.sequence.alignment.event.VetoableAlignmentListener;
import uk.ac.man.bioinf.sequence.event.SequenceEvent;
import uk.ac.man.bioinf.sequence.event.SequenceEventProvider;
import uk.ac.man.bioinf.sequence.event.SequenceListener;
import uk.ac.man.bioinf.sequence.event.SequenceVetoException;
import uk.ac.man.bioinf.sequence.event.VetoableSequenceEvent;
import uk.ac.man.bioinf.sequence.event.VetoableSequenceListener;


/**
 * SingleSequenceAlignment.java
 *
 * This class is for those stick occasions when you want a
 * SequenceAlignment but all you have is a Sequence. I don't know how
 * to make an array all within a single line so I have had to make a
 * factory method here. 
 *
 * Created: Tue Aug  1 16:16:33 2000
 *
 * @author Phillip Lord
 * @version $Id: SingleSequenceAlignment.java,v 1.4 2001/04/11 17:04:43 lord Exp $
 */

public class SingleSequenceAlignment extends DefaultSequenceAlignment
{
  public SingleSequenceAlignment( GappedSequence seq )
  {
    super( new GappedSequence[]{ seq },seq.getSequenceType() );
  }
  
  public int getNumberOfSequences()
  {
    return 1;
  }
  
  public GappedSequence getSingleSequence()
  {
    return getSequenceAt( 1 );
  }
} // SingleSequenceAlignment



/*
 * ChangeLog
 * $Log: SingleSequenceAlignment.java,v $
 * Revision 1.4  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.3  2000/12/05 15:54:37  lord
 * Import rationalisation
 *
 * Revision 1.2  2000/08/01 17:39:13  lord
 * Worked out how to create an array in one line
 *
 * Revision 1.1  2000/08/01 17:17:23  lord
 * Intial checkin
 *
 */
