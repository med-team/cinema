/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.sequence.alignment; // Package name inserted by JPack

import uk.ac.man.bioinf.sequence.Element;


/**
 * Gap.java
 *
 *
 * Created: Wed Mar  1 19:35:57 2000
 *
 * @author Phillip Lord
 * @version $Id: Gap.java,v 1.5 2001/04/11 17:04:43 lord Exp $
 */

public class Gap implements Element
{
  private Gap()
  {}
  
  public char toChar()
  {
    return '-';
  }
  
  public String toString()
  {
    return "Gap Element";
  }
  
  
  public static final Gap gap = new Gap();
  public static final Gap GAP = gap;
} // Gap



/*
 * ChangeLog
 * $Log: Gap.java,v $
 * Revision 1.5  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.4  2000/12/05 15:54:37  lord
 * Import rationalisation
 *
 * Revision 1.3  2000/08/21 16:12:17  jns
 * o removing toURL and toResourceString, which were part of the BioInterface
 * stuff that was removed about a month ago.
 *
 * Revision 1.2  2000/03/08 17:24:33  lord
 * *** empty log message ***
 *
 * Revision 1.1  2000/03/01 20:17:14  lord
 * Initial checkin
 *
 */
