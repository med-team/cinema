/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Julian Selley (j.selley@man.ac.uk)
 * whilst at the University of Manchester as a Research Assistant. 
 *
 * The initial code base is copyright by the University of Manchester. 
 * Modifications to the initial code base are copyright of their 
 * respective authors, or their employers as appropriate. Authorship 
 * of the modifications may be determined from the ChangeLog placed 
 * at the end of this file
 */

package uk.ac.man.bioinf.sequence.alignment.event; // Package name inserted by JPack

import java.util.Vector;
import java.util.Enumeration;


/**
 * AlignmentListenerSupport.java
 *
 * This class provides support and handles the listeners to a multiple
 * seqyence alignment.
 * @see AbstractMultipleSequenceAlignment
 *
 * Created: Thu Feb 17 14:57:37 2000
 *
 * @author J Selley
 * @version $Id: AlignmentListenerSupport.java,v 1.4 2001/04/11 17:04:43 lord Exp $
 */

public class AlignmentListenerSupport 
{
  private Vector listeners, vetoableListeners;

  public AlignmentListenerSupport()
  {
    listeners = new Vector();
    vetoableListeners = new Vector();
  }
  
  /**
   * This function fires the MSA event to all the interested listeners, and also
   * signals the vetoable listeners, which are stored separately. This is
   * because the VetoableMultipleSequenceListener interface extends the 
   * MultipleSequenceListener interface.
   * @see VetoableAlignmentListener
   * @see AlignmentListener
   *
   * @param event the multiple sequence event
   */
  public void fireAlignmentEvent(AlignmentEvent event)
  {
    // enumerate through the listeners and exec the changeOccured function
    for (Enumeration i = listeners.elements(); i.hasMoreElements(); ) {
      ((AlignmentListener)i.nextElement()).changeOccurred(event);
    }
    // also enumerate through the vetoable listeners
    // NB: VetoableMultipleSequenceListener extends MultipleSequenceListener
    for (Enumeration i = vetoableListeners.elements(); i.hasMoreElements(); ) {
      ((VetoableAlignmentListener)i.nextElement()).changeOccurred(event);
    }
  }

  /**
   * Notifies the vetoable listeners of a MSA vetoable event.
   *
   * @param event the vetoable multiple sequence event
   */
  public void fireVetoableAlignmentEvent(VetoableAlignmentEvent event)
    throws AlignmentVetoException
  {
    // enumerates through the listeners, notifying them of the change
    for (Enumeration i = vetoableListeners.elements(); i.hasMoreElements(); ) {
      ((VetoableAlignmentListener)i.nextElement()).vetoableChangeOccurred(event);
    }
  }

  /**
   * Adds a alignment listener to a Vector of listeners.
   *
   * @param listener the listener to be added
   */
  public void addAlignmentListener(AlignmentListener listener)
  {
    if (listeners.contains(listener)) return;
    else listeners.addElement(listener);
  }

  /**
   * Removes a listener from the Vector.
   *
   * @param listener the listener to be removed
   */
  public void removeAlignmentListener(AlignmentListener listener)
  {
    listeners.removeElement(listener);
  }

  /**
   * Adds a vetoable alignment listener to the Vector of vetoable
   * listeners.
   *
   * @param listener the vetoable listener to be added.
   */
  public void addVetoableAlignmentListener
    (VetoableAlignmentListener listener)
  {
    if (vetoableListeners.contains(listener)) return;
    else vetoableListeners.addElement(listener);
  }

  /**
   * Removes a vetoable alignment listener.
   *
   * @param listener the listener to be removed
   */
  public void removeVetoableAlignmentListener
    (VetoableAlignmentListener listener)
  {
    vetoableListeners.removeElement(listener);
  }
} // AlignmentListenerSupport



/*
 * ChangeLog
 * $Log: AlignmentListenerSupport.java,v $
 * Revision 1.4  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.3  2000/06/13 11:06:20  lord
 * Fixed bug in vetoable fire method
 *
 * Revision 1.2  2000/03/10 17:53:35  lord
 * Small bug fixes in firing methods
 *
 * Revision 1.1  2000/03/02 19:23:04  jns
 * initial code.
 *
 *
 */
