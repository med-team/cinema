/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.sequence; // Package name inserted by JPack
import uk.ac.man.bioinf.sequence.event.VetoableSequenceEvent;
import uk.ac.man.bioinf.sequence.event.SequenceEventType;
import uk.ac.man.bioinf.sequence.event.SequenceVetoException;


/**
 * AbstractEditableSequence.java
 *
 * This class provides trivial implementations of all of those methods
 * which can be defined in terms of the others of the EditableSequence
 * interface, and also ensures that the event handling occurs correctly
 *
 * Created: Fri Mar  3 12:11:49 2000
 *
 * @author Phillip Lord
 * @version $Id: AbstractEditableSequence.java,v 1.2 2001/04/11 17:04:43 lord Exp $
 */

public abstract class AbstractEditableSequence extends AbstractMutableSequence
  implements EditableSequence
{
  public AbstractEditableSequence( SequenceType type )
  {
    super( type );
  }
  
  protected abstract Element setElementAtQuietly( Element elem, int index );
  
  public Element setElementAt( Element elem, int index ) throws SequenceVetoException
  {
    VetoableSequenceEvent event = new VetoableSequenceEvent( this, index, SequenceEventType.SET );
    fireVetoableSequenceEvent( event );

    Element retn = setElementAtQuietly( elem, index );

    fireSequenceEvent( event );
    return retn;
  }
    
  /**
   * Provides a simple implementation of this method by iterating
   * through the element array, and calling the super class
   * method. Subclasses may override this to provide a more efficient implementation
   * @param elem the elements to set
   * @param index the index at which to start
   */
  public Element[] setElementAt( Element[] elem, int index ) throws SequenceVetoException
  {
    VetoableSequenceEvent event = new VetoableSequenceEvent( this, index, elem.length, SequenceEventType.SET );
    fireVetoableSequenceEvent( event );
    
    Element[] elems = new Element[ elem.length ];
    for( int i = 0; i < elem.length; i++ ){
      elems[ i ] = setElementAtQuietly( elem[ i ], index + i );
    } //end for( i < length )

    fireSequenceEvent( event );
    return elems;
  }

  protected abstract void insertElementAtQuietly( Element elem, int index );
  
  public void insertElementAt( Element elem, int index ) throws SequenceVetoException
  {
    VetoableSequenceEvent event =  new VetoableSequenceEvent
      ( this, index, SequenceEventType.INSERT );
    
    fireVetoableSequenceEvent( event );
    insertElementAtQuietly( elem, index );
    fireSequenceEvent( event );
  }

  
  /**
   * Provides a trivial implementation of this method by calling
   * multiple single insert methods. Subclasses will almost definately
   * find it more efficient to over ride this method!
   * @param elem
   * @param index
   * @exception SequenceVetoException
   */
  public void insertElementAt( Element[] elem, int index ) throws SequenceVetoException
  {
    VetoableSequenceEvent event =  new VetoableSequenceEvent
      ( this, index, elem.length, SequenceEventType.INSERT );
    fireVetoableSequenceEvent( event );

    for( int i = 0; i < elem.length; i++ ){
      insertElementAtQuietly( elem[ i ], index + i );
    } //end for( i < length )
    
    fireSequenceEvent( event );
  }
  
  /**
   * Do the actual deletion but dont do any event signalling. This
   * enables all the deletions to use this method without causing
   * multiple events for deletions longer than one in length
   * @param index the element to delete
   * @return the element just deleted
   */
  protected abstract Element deleteElementAtQuietly( int index );
  
  public Element deleteElementAt( int index ) throws SequenceVetoException
  {
    VetoableSequenceEvent event =  new VetoableSequenceEvent( this, index, SequenceEventType.DELETE );
    fireVetoableSequenceEvent( event );
    Element retn = deleteElementAtQuietly( index );
    fireSequenceEvent( event );
    
    return retn;
  }

  public Element[] deleteElementAt( int index, int length ) throws SequenceVetoException
  {
    // inform any listeners what is about to happen
    VetoableSequenceEvent event = new VetoableSequenceEvent( this, index, length, SequenceEventType.DELETE );
    fireVetoableSequenceEvent( event );
    
    // do the actual deletion via the quiet method
    Element[] elem = new Element[ length ];
    for( int i = 0; i < elem.length; i++ ){
      elem[ i ] = deleteElementAtQuietly( index + i );
    } //end for( i < elem.length )
    
    // now inform any listeners of what has just happened
    fireSequenceEvent( event );
    
    return elem;
  }
} // AbstractEditableSequence



/*
 * ChangeLog
 * $Log: AbstractEditableSequence.java,v $
 * Revision 1.2  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.1  2000/03/08 17:26:35  lord
 * To many changes to document
 *
 */
