/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.sequence.types; // Package name inserted by JPack

import uk.ac.man.bioinf.sequence.Element;


/**
 * AminoAcid.java
 *
 * A class for the representation of an amino-acid. This class is an
 * enumeration of types.
 *
 * Created: Wed Feb 16 20:30:57 2000
 *
 * @author Phillip Lord
 * @version $Id: AminoAcid.java,v 1.6 2001/04/11 17:04:43 lord Exp $
 */

public final class AminoAcid extends AbstractElement
  implements ProteinResidue
{
  // this is for the Element, and AminoAcid implementation.
  private String threeLetter;
  private String full;
  
  private AminoAcid( char singleChar, String threeLetter, String full,  String toString )
  {
    super( toString, singleChar );
    this.threeLetter = threeLetter;
    this.full = full;
  }

  public String getThreeLetter()
  {
    return threeLetter;
  }
   
  public String getFullName()
  {
    return full;
  }

  public static AminoAcid[] getAll()
  {
    Element[] elems = getAllSequenceElements( AminoAcid.class );
    
    AminoAcid[] aa = new AminoAcid[ elems.length ];
    
    System.arraycopy( elems, 0, aa, 0, elems.length );
    
    return aa;
  }
  

  // static types.  
  public static final AminoAcid GLYCINE = 
    new AminoAcid( 'G', "Gly", "Glycine", "AminoAcid Glycine" );
  public static final AminoAcid ALANINE = 
    new AminoAcid( 'A', "Ala", "Alanine", "AminoAcid Alanine" );
  public static final AminoAcid VALINE  = 
    new AminoAcid( 'V', "Val", "Valine", "AminoAcid Valine" );
  public static final AminoAcid LEUCINE = 
    new AminoAcid( 'L', "Leu", "Leucine", "AminoAcid Leucine" );
  public static final AminoAcid ISOLEUCINE = 
    new AminoAcid( 'I', "Ile", "Isoleucine", "AminoAcid IsoLeucine" );
  public static final AminoAcid SERINE = 
    new AminoAcid( 'S', "Ser", "Serine", "AminoAcid Serine" );
  public static final AminoAcid CYSTEINE =
    new AminoAcid( 'C', "Cys", "Cysteine", "AminoAcid Cysteine" );
  public static final AminoAcid THREONINE = 
    new AminoAcid( 'T', "Thr", "Threonine", "AminoAcid Threonine" );
  public static final AminoAcid METHIONINE = 
    new AminoAcid( 'M', "Met", "Methonine", "AminoAcid Methonine" );
  public static final AminoAcid PHENYLALANINE = 
    new AminoAcid( 'F', "Phe", "Phenylalanine", "AminoAcid Phenylalanine" );
  public static final AminoAcid TYROSINE =
    new AminoAcid( 'Y', "Tyr", "Tyrosine", "AminoAcid Tyrosine" );
  public static final AminoAcid TRYPTOPHAN =
    new AminoAcid( 'W', "Trp", "Tryptophan", "AminoAcid Tryptophan" );
  public static final AminoAcid PROLINE =
    new AminoAcid( 'P', "Pro", "Proline", "AminoAcid Proline" );
  public static final AminoAcid HISTIDINE =
    new AminoAcid( 'H', "His", "Histidine", "AminoAcid Histidine" );
  public static final AminoAcid LYSINE =
    new AminoAcid( 'K', "Lys", "Lysine", "AminoAcid Lysine" );
  public static final AminoAcid ARGININE =
    new AminoAcid( 'R', "Arg", "Arginine", "AminoAcid Arginine" );
  public static final AminoAcid ASPARTICACID =
    new AminoAcid( 'D', "Asp", "AsparticAcid", "AminoAcid AsparticAcid" );
  public static final AminoAcid GLUTAMICACID =
    new AminoAcid( 'E', "Glu", "GlutamicAcid", "AminoAcid GlutamicAcid" );
  public static final AminoAcid ASPARAGINE =
    new AminoAcid( 'N', "Asn", "Asparagine", "AminoAcid Asparagine" );
  public static final AminoAcid GLUTAMINE =
    new AminoAcid( 'Q', "Gln", "Glutamine", "AminoAcid Glutamine" );
  public static final AminoAcid ASNORASP = 
    new AminoAcid( 'B', "Asx", "Asparagine or AsparticAcid", "AminoAcid Asn or Asp" );
  public static final AminoAcid GLUORGLN = 
    new AminoAcid( 'Z', "Glx", "GlutamicAcid or Glutamine", "Amino Acid Glu or Gln" );
  public static final AminoAcid ANY = 
    new AminoAcid( 'X', "Any", "Any", "AminoAcid any" );
} // AminoAcid



/*
 * ChangeLog
 * $Log: AminoAcid.java,v $
 * Revision 1.6  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.5  2001/04/06 13:17:33  lord
 * Added ambiguity types
 *
 * Revision 1.4  2001/03/15 13:20:25  lord
 * Fixed bug in Threonine toString() parameter
 *
 * Revision 1.3  2000/07/18 12:39:06  lord
 * Import rationalisation
 *
 * Revision 1.2  2000/05/30 16:04:18  lord
 * Have rationalised and sorted all the import statements
 *
 * Revision 1.1  2000/04/18 17:54:48  jns
 * o files previously in sequence package, moved here.
 *
 * Revision 1.4  2000/03/08 17:26:35  lord
 * To many changes to document
 *
 * Revision 1.3  2000/02/28 16:32:39  lord
 * Numerous changes
 *
 * Revision 1.2  2000/02/28 16:20:07  lord
 * FIxed typo
 *
 * Revision 1.1  2000/02/28 14:07:16  jns
 * Initial code, although will not currently compile due to certain files
 * not being present, such as AbstractSequenceType, and AbstractEnumeration.
 *
 *
 */
