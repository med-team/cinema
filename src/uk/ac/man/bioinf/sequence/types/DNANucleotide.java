/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.sequence.types; // Package name inserted by JPack
import uk.ac.man.bioinf.sequence.Element;


/**
 * DNANucleotide.java
 *
 *
 * Created: Sun Sep 24 17:44:04 2000
 *
 * @author Phillip Lord
 * @version $Id: DNANucleotide.java,v 1.3 2001/04/11 17:04:43 lord Exp $
 */

public class DNANucleotide extends AbstractElement
  implements DNAResidue
{
    //Store the full name
  private String full;
  private DNANucleotide ( char singleChar, String full, String toString )
  {
    super( toString, singleChar );
    this.full = full;
  }
  
  public String getFullName()
  {
    return full;
  }
  
  public static DNANucleotide[] getAll()
  {
    Element[] elems = getAllSequenceElements( DNANucleotide.class );
    DNANucleotide[] dna = new DNANucleotide[ elems.length ];
    
    System.arraycopy( elems, 0, dna, 0, dna.length );
    return dna;
  }
  
  public static final DNANucleotide ADENOSINE =
    new DNANucleotide('A', "Adenosine", "DNANucleotide Adenosine");
  public static final DNANucleotide GUANINE =
    new DNANucleotide('G', "Guanine", "DNANucleotide Guanine");
  public static final DNANucleotide CYTOSINE =
    new DNANucleotide('C', "Cytosine", "DNANucleotide Cytosine");
  public static final DNANucleotide THIAMINE =
    new DNANucleotide('T', "Thiamine", "DNANucleotide Thiamine");     


  
} // DNANucleotide



/*
 * ChangeLog
 * $Log: DNANucleotide.java,v $
 * Revision 1.3  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.2  2000/11/27 18:16:19  lord
 * Amusingly I had Uracil as one of the DNA
 *
 * Revision 1.1  2000/09/25 16:37:38  lord
 * Initial checkin
 *
 */
