/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.sequence.types; // Package name inserted by JPack

import uk.ac.man.bioinf.util.AbstractEnumeration;
import uk.ac.man.bioinf.sequence.Element;


/**
 * AbstractElement.java
 *
 *
 * Created: Mon Feb 28 16:10:45 2000
 *
 * @author Phillip Lord
 * @version $Id: AbstractElement.java,v 1.4 2001/04/11 17:04:43 lord Exp $
 */

public abstract class AbstractElement extends AbstractEnumeration
{
  private char toChar;
  
  public AbstractElement( String toString, char toChar )
  {
    super( toString );
    this.toChar = toChar;
  }
  
  public char toChar() 
  {
    return toChar;
  }
  
  /**
   * Returns all the elements. This method basically switches the type
   * of the 
   * @param cla the class of the elements
   * @return the Elements of the class
   */
  public static Element[] getAllSequenceElements( Class cla )
  {
    AbstractEnumeration[] enum = AbstractEnumeration.getAllElements( cla );
    
    Element[] elems = new Element[ enum.length ];
    
    System.arraycopy( enum, 0, elems, 0, elems.length );
    
    return elems;
  }
} // AbstractElement



/*
 * ChangeLog
 * $Log: AbstractElement.java,v $
 * Revision 1.4  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.3  2000/12/05 15:54:37  lord
 * Import rationalisation
 *
 * Revision 1.2  2000/08/01 12:47:05  jns
 * o removed references to BioInterface and BioObject.
 *
 * Revision 1.1  2000/04/18 17:54:48  jns
 * o files previously in sequence package, moved here.
 *
 * Revision 1.2  2000/03/08 17:26:35  lord
 * To many changes to document
 *
 * Revision 1.1  2000/02/28 16:32:39  lord
 * Numerous changes
 *
 */

