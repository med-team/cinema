/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.sequence.types; // Package name inserted by JPack
import uk.ac.man.bioinf.sequence.Element;
import uk.ac.man.bioinf.sequence.types.AbstractElement;


/**
 * AbstractEnumeratedSequenceType.java
 *
 *
 * Created: Tue Jun 13 13:20:11 2000
 *
 * @author Phillip Lord
 * @version $Id: AbstractEnumeratedSequenceType.java,v 1.2 2001/04/11 17:04:43 lord Exp $
 */

public class AbstractEnumeratedSequenceType extends AbstractSequenceType
{
  
  public AbstractEnumeratedSequenceType( Element[] elements, String name )
  {
    super( elements, name );
  }
  
  public AbstractEnumeratedSequenceType( Element[][] elements, String name )
  {
    super( elements, name );
  }
  
  public int getIntForElement( Element element )
  {
    if( !isElement( element ) ) 
      // (PENDING:- PL) This should be a more specific exception 
      throw new IllegalArgumentException( "Element is not a member of the sequence type " + this );
    
    // we know that this is going to be a AbstractElement because the
    // constructor requires it
    return ((AbstractElement)element).ord;
  }
 
  public Element getElementForInt( int index )
  {
    return elements[ index ];
  }
} // AbstractEnumeratedSequenceType



/*
 * ChangeLog
 * $Log: AbstractEnumeratedSequenceType.java,v $
 * Revision 1.2  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.1  2000/06/27 16:12:15  lord
 * Initial checkin
 *
 */
