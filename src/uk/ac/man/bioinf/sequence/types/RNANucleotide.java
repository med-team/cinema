/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Daniel Jameson (daniel.p.jameson@stud.man.ac.uk)
 * whilst at the University of Manchester as a Masters Student. 
 *
 * The initial code base is copyright by the University of Manchester. 
 * Modifications to the initial code base are copyright of their 
 * respective authors, or their employers as appropriate. Authorship 
 * of the modifications may be determined from the ChangeLog placed 
 * at the end of this file
 */

package uk.ac.man.bioinf.sequence.types; // Package name inserted by JPack

import uk.ac.man.bioinf.sequence.Element;



/**
 * RNANucleotide.java
 *
 * A class for the representation of RNA Nucleotides.  This class
 * is an enumeration of types.
 *
 * Created: Mon Feb 28 15:42:47 2000
 *
 * @author Daniel Jameson
 * @version $Id: RNANucleotide.java,v 1.4 2001/04/11 17:04:43 lord Exp $
 */

public final class RNANucleotide extends AbstractElement
  implements RNAResidue
{
  //Store the full name
  private String full;
  private RNANucleotide ( char singleChar, String full, String toString )
  {
    super( toString, singleChar );
    this.full = full;
  }
  
  public String getFullName()
  {
    return full;
  }
  
  public static RNANucleotide[] getAll()
  {
    Element[] elems = getAllSequenceElements( RNANucleotide.class );
    RNANucleotide[] rna = new RNANucleotide[ elems.length ];
    
    System.arraycopy( elems, 0, rna, 0, rna.length );
    return rna;
  }
  
  public static final RNANucleotide ADENOSINE =
    new RNANucleotide('A', "Adenosine", "RNANucleotide Adenosine");
  public static final RNANucleotide GUANINE =
    new RNANucleotide('G', "Guanine", "RNANucleotide Guanine");
  public static final RNANucleotide CYTOSINE =
    new RNANucleotide('C', "Cytosine", "RNANucleotide Cytosine");
  public static final RNANucleotide URACIL =
    new RNANucleotide('U', "Uracil", "RNANucleotide Uracil");     
} // RNANucleotide



/*
 * ChangeLog
 * $Log: RNANucleotide.java,v $
 * Revision 1.4  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.3  2000/05/30 16:04:18  lord
 * Have rationalised and sorted all the import statements
 *
 * Revision 1.2  2000/05/10 15:00:26  lord
 * Get all elements support added
 *
 * Revision 1.1  2000/04/18 17:54:48  jns
 * o files previously in sequence package, moved here.
 *
 * Revision 1.2  2000/02/28 16:32:39  lord
 * Numerous changes
 *
 * Revision 1.1  2000/02/28 16:06:36  jameson
 * Initial checkin.
 *
 */

