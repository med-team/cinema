/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Daniel Jameson (daniel.p.jameson@stud.man.ac.uk)
 * whilst at the University of Manchester as a Masters Student. 
 *
 * The initial code base is copyright by the University of Manchester. 
 * Modifications to the initial code base are copyright of their 
 * respective authors, or their employers as appropriate. Authorship 
 * of the modifications may be determined from the ChangeLog placed 
 * at the end of this file
 */

package uk.ac.man.bioinf.sequence.types; // Package name inserted by JPack

/**
 * RNAResidue.java
 *
 * An interface to represent an RNA Residue
 *
 * Created: Mon Feb 28 13:02:16 2000
 *
 * @author Daniel Jameson
 * @version $Id: RNAResidue.java,v 1.2 2001/04/11 17:04:43 lord Exp $ 
 */

public interface RNAResidue extends NucleotideResidue
{
}// RNAResidue


/*
 * ChangeLog
 * $Log: RNAResidue.java,v $
 * Revision 1.2  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.1  2000/04/18 17:54:48  jns
 * o files previously in sequence package, moved here.
 *
 * Revision 1.2  2000/02/28 16:32:39  lord
 * Numerous changes
 *
 * Revision 1.1  2000/02/28 15:40:46  jameson
 * Initial checkin.
 * 
 */

