/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Julian Selley (j.selley@man.ac.uk)
 * whilst at the University of Manchester as a Research Assistant. 
 *
 * The initial code base is copyright by the University of Manchester. 
 * Modifications to the initial code base are copyright of their 
 * respective authors, or their employers as appropriate. Authorship 
 * of the modifications may be determined from the ChangeLog placed 
 * at the end of this file
 */

package uk.ac.man.bioinf.sequence.types; // Package name inserted by JPack


/**
 * DNASequenceType.java
 *
 * This class represents the DNA sequence type, allowing only a set
 * of particular bases.
 *
 * Created: Tue Aug 22 18:41:00 2000
 *
 * @author Julian Selley
 * @version $Id: DNASequenceType.java,v 1.3 2001/04/11 17:04:43 lord Exp $
 */

public class DNASequenceType extends AbstractEnumeratedSequenceType
{
  private DNASequenceType()
  {
    super(AbstractElement.getAllSequenceElements(DNANucleotide.class), 
	  "DNASequenceType");
  }
  
  public static final DNASequenceType instance = new DNASequenceType();
  
  public static DNASequenceType getInstance() 
  {
    return instance;
  }
} // DNASequenceType



/*
 * ChangeLog
 * $Log: DNASequenceType.java,v $
 * Revision 1.3  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.2  2000/11/15 14:35:15  lord
 * Updated due to removal of DNABase class
 *
 * Revision 1.1  2000/09/18 12:49:54  jns
 * o Initial commit. Needed for EMBOSS.
 *
 */
