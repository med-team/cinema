/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.sequence.types; // Package name inserted by JPack

import uk.ac.man.bioinf.sequence.SequenceType;
import uk.ac.man.bioinf.sequence.Element;

/**
 * AbstractSequenceType.java
 *
 *
 * Created: Thu Feb 17 13:02:28 2000
 *
 * @author Phillip Lord
 * @version $Id: AbstractSequenceType.java,v 1.5 2001/04/11 17:04:43 lord Exp $
 */

public abstract class AbstractSequenceType implements SequenceType
{
  private String name;
  protected Element[] elements;
  
  public AbstractSequenceType( Element[] elements, String name )
  {
    init( elements, name );
  }
  
  public AbstractSequenceType( Element[][] elements, String name )
  {
    int count = 0;
    for( int i = 0; i < elements.length; i++ ){
      count += elements[ i ].length;
    }
    Element[] allElements = new Element[ count ];
    
    int pos = 0;
    for( int i = 0; i < elements.length; i++ ){
      System.arraycopy( elements[ i ], 0, allElements, pos, elements[ i ].length );
      pos = elements[ i ].length;
    }
    init( allElements, name );
  }
  
  private void init( Element[] elements, String name )
  {
    this.elements = elements;
    this.name = name;
  }
  
  // PENDING Should  improve these implementations I think. Is a hash
  // table worth it? Depends on how many Elements I Guess...
  public boolean isElement( Element element )
  {
    for( int i = 0; i < elements.length; i ++ ){
      if( elements[ i ] == element ) return true;
    }
    return false;      
  }
  
  public boolean isElement( Element[] element )
  {
    for( int i = 0 ; i < element.length ; i++ ){
      if( !isElement( element[ i ] ) ) return false;
    } //end for( i < Element.length )
    return true;
  }
    
  public boolean isElement( char element )
  {
    for( int i = 0 ; i < elements.length ; i++ ){
      if( elements[ i ].toChar() == element ) return true;
    } //end for( i < elements.length )
    return false;
  }
  
  public boolean isElement( char[] element )
  {
    for( int i = 0 ; i < element.length ; i++ ){
      if( !isElement( element[ i ] )  ) return false;
    } //end for( i < element.length )
    return false;
  }
  
  public String getName()
  {
    if( name == null ) throw new IllegalStateException
      ( "The name of an AbstractSequenceType should be set during construction" );    
    return name;
  }
  
  public Element[] getElements()
  {
    if( elements == null ) throw new IllegalStateException
      ( "The Elements of an AbstractSequenceType should be set during construction" );
    return (Element[])elements.clone();
  }
  
  public int size()
  {
    return elements.length;
  }
} // AbstractSequenceType



/*
 * ChangeLog
 * $Log: AbstractSequenceType.java,v $
 * Revision 1.5  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.4  2000/08/01 12:47:05  jns
 * o removed references to BioInterface and BioObject.
 *
 * Revision 1.3  2000/06/27 16:12:52  lord
 * Loosened access on internal data structures
 *
 * Revision 1.2  2000/06/13 11:01:59  lord
 * Added size method
 *
 * Revision 1.1  2000/04/18 17:54:48  jns
 * o files previously in sequence package, moved here.
 *
 * Revision 1.3  2000/03/02 17:46:52  lord
 * Now declared abstract
 *
 * Revision 1.2  2000/03/01 20:17:41  lord
 * Updated for new interfaces
 *
 * Revision 1.1  2000/02/28 16:32:39  lord
 * Numerous changes
 *
 * Revision 1.2  2000/02/18 20:06:39  lord
 * Implements isElement methods.
 * Now extends BioObject rather than delegates.
 * Removed AbstractElement reference.
 * SequenceType name support added properly
 *
 * Revision 1.1  2000/02/18 18:39:57  lord
 * Initial checkin
 *
 */
