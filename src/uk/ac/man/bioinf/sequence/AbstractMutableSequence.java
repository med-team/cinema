/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.sequence; // Package name inserted by JPack
import uk.ac.man.bioinf.sequence.event.VetoableSequenceListener;
import uk.ac.man.bioinf.sequence.MutableSequence;
import uk.ac.man.bioinf.sequence.event.SequenceListener;
import uk.ac.man.bioinf.sequence.event.SequenceListenerSupport;
import uk.ac.man.bioinf.sequence.event.VetoableSequenceEvent;
import uk.ac.man.bioinf.sequence.event.SequenceVetoException;
import uk.ac.man.bioinf.sequence.event.SequenceEvent;


/**
 * AbstractMutableSequence.java
 *
 *
 * Created: Thu Mar  2 20:20:02 2000
 *
 * @author Phillip Lord
 * @version $Id: AbstractMutableSequence.java,v 1.4 2001/04/11 17:04:43 lord Exp $
 */

public abstract class AbstractMutableSequence extends AbstractSequence
  implements MutableSequence
{
  private SequenceListenerSupport supp = new SequenceListenerSupport();
  
  public AbstractMutableSequence( SequenceType type )
  {
    super( type );
  }
  
  protected void fireVetoableSequenceEvent( VetoableSequenceEvent event ) throws SequenceVetoException
  {
    supp.fireVetoableSequenceEvent( event );
  }
  
  protected void fireSequenceEvent( SequenceEvent event )
  {
    supp.fireSequenceEvent( event );
  }
  
  protected boolean hasListeners()
  {
    return supp.hasListeners();
  }
  
  // implementation of uk.ac.man.bioinf.sequence.MutableSequence interface

  public void addSequenceListener( SequenceListener listener )
  {
    supp.addSequenceListener( listener );
  }
  
  public void addVetoableSequenceListener( VetoableSequenceListener listener )
  {
    supp.addVetoableSequenceListener( listener );
  }
  
  public void removeSequenceListener( SequenceListener listener )
  {
    supp.removeSequenceListener( listener );
  }

  public void removeVetoableSequenceListener( VetoableSequenceListener listener )
  {
    supp.removeVetoableSequenceListener( listener );
  }
} // AbstractMutableSequence



/*
 * ChangeLog
 * $Log: AbstractMutableSequence.java,v $
 * Revision 1.4  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.3  2000/09/15 16:32:51  lord
 * Added hasListeners method
 *
 * Revision 1.2  2000/07/18 11:12:22  lord
 * Import rationalisation
 *
 * Revision 1.1  2000/03/08 17:26:35  lord
 * To many changes to document
 *
 */
