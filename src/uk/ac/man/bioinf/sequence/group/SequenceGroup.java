/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.sequence.group; // Package name inserted by JPack

import java.util.Iterator;
import uk.ac.man.bioinf.sequence.Sequence;

/**
 * SequenceGroup.java
 *
 * This interface defines a SequenceGroup that is capable of storing a
 * collection of sequences. It would usually be expected that the
 * sequences in the group would usually have something in common with
 * each other, being for instance some of the members of a
 * SequenceAlignment, although this is not required. 
 *
 * Created: Thu Jun  1 18:09:32 2000
 *
 * @author Phillip Lord
 * @version $Id: SequenceGroup.java,v 1.4 2001/04/11 17:04:43 lord Exp $ 
 */

public interface SequenceGroup 
{
  /**
   * Adds a sequence to this group
   * @param seq the sequence
   */
  public void addSequenceToGroup( Sequence seq );
  
  /**
   * Removes a sequence from this group
   * @param seq the sequence
   */
  public void removeSequenceFromGroup( Sequence seq );
  
  /**
   * Does this group contain the sequence 
   * @param seq the sequence
   * @return true if containined
   */
  public boolean containsSequence( Sequence seq );
  
  /**
   * Get all the sequences in this group. 
   * @return
   */
  public Sequence[] getSequences();
  
  /**
   * Empty the group of sequences
   */
  public void clearSequences();
  
  /**
   * Return the number of sequences in this group. 
   * @return the number 
   */
  public int getNumberSequences();
  
  /**
   * Returns an iterator of all the sequences. This should be fail
   * fast if the group is modified whilst the iterator is being used. 
   * @return
   */
  public Iterator iterator();
  
}// SequenceGroup


/*
 * ChangeLog
 * $Log: SequenceGroup.java,v $
 * Revision 1.4  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.3  2000/10/26 12:42:49  jns
 * o added editing facilities to SA - this includes insertion/deletion of gaps,
 * addition/removal of sequences from an alignment. It involved resolving some
 * conflicts with the group stuff.
 *
 * Revision 1.2  2000/08/01 12:47:05  jns
 * o removed references to BioInterface and BioObject.
 *
 * Revision 1.1  2000/06/05 14:46:26  lord
 * Initial checkin
 * 
 */

