/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.sequence.group; // Package name inserted by JPack

import uk.ac.man.bioinf.sequence.Sequence;
import uk.ac.man.bioinf.sequence.alignment.GappedSequence;


/**
 * AbstractGappedSequenceGroup.java
 *
 *
 * Created: Thu Jun  1 18:25:15 2000
 *
 * @author Phillip Lord
 * @version $Id: AbstractGappedSequenceGroup.java,v 1.4 2001/04/11 17:04:43 lord Exp $
 */

public abstract class AbstractGappedSequenceGroup implements GappedSequenceGroup
{
  public AbstractGappedSequenceGroup()
  {
  }

  public GappedSequence[] getGappedSequences()
  {
    Sequence[] retnSeqs = getSequences();
    GappedSequence[] retn = new GappedSequence[ retnSeqs.length ];
    System.arraycopy( retnSeqs, 0, retn, 0, retn.length );
    return retn;
  }
} // AbstractGappedSequenceGroup



/*
 * ChangeLog
 * $Log: AbstractGappedSequenceGroup.java,v $
 * Revision 1.4  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.3  2000/12/05 15:54:37  lord
 * Import rationalisation
 *
 * Revision 1.2  2000/08/01 12:47:05  jns
 * o removed references to BioInterface and BioObject.
 *
 * Revision 1.1  2000/06/05 14:46:25  lord
 * Initial checkin
 *
 */

