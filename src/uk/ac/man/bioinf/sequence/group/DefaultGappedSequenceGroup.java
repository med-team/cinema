/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.sequence.group; // Package name inserted by JPack

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import uk.ac.man.bioinf.sequence.Sequence;
import uk.ac.man.bioinf.sequence.alignment.GappedSequence;


/**
 * DefaultGappedSequenceGroup.java
 *
 *
 * Created: Thu Jun  1 18:33:14 2000
 *
 * @author Phillip Lord
 * @version $Id: DefaultGappedSequenceGroup.java,v 1.5 2001/04/11 17:04:43 lord Exp $
 */

public class DefaultGappedSequenceGroup extends AbstractGappedSequenceGroup
{
  private List sequences = new ArrayList();
  private GappedSequence[] cache;
  private boolean cacheOutOfDate = true;
  
  public DefaultGappedSequenceGroup()
  {
    super();
  }
  
  public void addSequenceToGroup( Sequence seq )
  {
    cacheOutOfDate = true;
    sequences.add( seq );
  }
  
  public void removeSequenceFromGroup( Sequence seq )
  {
    cacheOutOfDate = true;
    sequences.remove( seq );
  }
  
  public boolean containsSequence( Sequence seq )
  {
    return sequences.contains( seq );
  }

  public Sequence[] getSequences()
  {
    if( cacheOutOfDate ){
      // must have written this code a 1000 times before.
      Object[] retnObj = sequences.toArray();
      cache = new GappedSequence[ retnObj.length ];
      System.arraycopy( retnObj, 0, cache, 0, cache.length );
    }
    return cache;
  }
  
  public void clearSequences()
  {
    cacheOutOfDate = true;
    sequences.clear();
  }
  
  public int getNumberSequences()
  {
    return sequences.size();
  }
  
  
  public Sequence getSequenceAt( int index )
  {
    return (Sequence)sequences.get( index );
  }
  
  public GappedSequence getGappedSequenceAt( int index )
  {
    return (GappedSequence)sequences.get( index );
  }
  
  public Iterator iterator()
  {
    return sequences.iterator();
  }
} // DefaultGappedSequenceGroup



/*
 * ChangeLog
 * $Log: DefaultGappedSequenceGroup.java,v $
 * Revision 1.5  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.4  2000/12/05 15:54:37  lord
 * Import rationalisation
 *
 * Revision 1.3  2000/10/26 12:42:49  jns
 * o added editing facilities to SA - this includes insertion/deletion of gaps,
 * addition/removal of sequences from an alignment. It involved resolving some
 * conflicts with the group stuff.
 *
 * Revision 1.2  2000/08/01 12:47:05  jns
 * o removed references to BioInterface and BioObject.
 *
 * Revision 1.1  2000/06/05 14:46:25  lord
 * Initial checkin
 *
 */
