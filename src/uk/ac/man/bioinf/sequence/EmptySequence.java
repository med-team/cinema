/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.sequence; // Package name inserted by JPack
import uk.ac.man.bioinf.sequence.identifier.NoIdentifier;
import uk.ac.man.bioinf.sequence.types.EmptySequenceType;
import uk.ac.man.bioinf.sequence.identifier.Identifier;


/**
 * EmptySequence.java
 *
 *
 * Created: Mon Feb  5 17:53:37 2001
 *
 * @author Phillip Lord
 * @version $Id: EmptySequence.java,v 1.2 2001/04/11 17:04:43 lord Exp $
 */

public class EmptySequence implements Sequence
{

  private EmptySequence()
  {
  }
  
  private static Sequence inst = new EmptySequence();
  public static Sequence getInstance()
  {
    return inst;
  }
  
  private NoSuchSequenceElementException createException()
  {
    return new NoSuchSequenceElementException( "The empty sequence has no elements" );
  }
  
  // implementation of uk.ac.man.bioinf.sequence.Sequence interface
  public Sequence getSubSequence(int param1, int param2) {
    if( (param1 != 1) || (param2 != 1) ){
      throw createException();
    }
    return this;
  }

  public SequenceType getSequenceType() {
    return EmptySequenceType.getInstance();
  }

  private static Element[] noElements = new Element[ 0 ];
  public Element[] getSequenceAsElements() {
    return noElements;
  }
  
  private static char[] noChars = new char[ 0 ];
  public char[] getSequenceAsChars() {
    return noChars;
  }
  
  public int getLength() {
    return 0;
  }
  
  public char getElementAtAsChar(int param1) {
    throw createException();
  }
  
  public Element getElementAt(int param1) {
    throw createException();
  }
  
  // implementation of uk.ac.man.bioinf.sequence.identifier.Identifiable interface
  public Identifier getIdentifier() {
    return new NoIdentifier();
  }
} // EmptySequence



/*
 * ChangeLog
 * $Log: EmptySequence.java,v $
 * Revision 1.2  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.1  2001/02/19 17:49:38  lord
 * Initial checkin
 *
 */
