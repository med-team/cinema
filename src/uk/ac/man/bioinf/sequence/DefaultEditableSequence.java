/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.sequence; // Package name inserted by JPack
import java.util.ArrayList;
import java.util.Arrays;
import uk.ac.man.bioinf.sequence.identifier.NoIdentifier;
import uk.ac.man.bioinf.sequence.identifier.Identifier;



/**
 * DefaultEditableSequence.java
 *
 *
 * Created: Thu Mar  2 20:27:46 2000
 *
 * @author Phillip Lord
 * @version $Id: DefaultEditableSequence.java,v 1.6 2001/04/11 17:04:43 lord Exp $
 */

public class DefaultEditableSequence extends AbstractEditableSequence
  implements EditableSequence
{
  private ArrayList elements;
  private Identifier ident;
  
  
  public DefaultEditableSequence( Element[] elements, SequenceType type, Identifier identifier )
  {
    super( type );

    checkSequenceType( elements );
    this.ident = ident;
    
    // the ArrayList constructor involves a cloning step, so changes
    // will not percolate through to the underlying Element array. The
    // Arrays.asList function (although it also returns an Object of
    // underlying type ArrayList) does not do this on its own.
    this.elements = new ArrayList( Arrays.asList( elements ) );
  }

  public DefaultEditableSequence( Sequence sequence, SequenceType type )
  {
    this( sequence.getSequenceAsElements(), type, new NoIdentifier() );
  }
  
  public DefaultEditableSequence( Sequence sequence )
  {
    this( sequence, sequence.getSequenceType() );
  }
  
  public Identifier getIdentifier()
  {
    return ident;
  }
  
  public Element getElementAt( int index )
  {
    // -1!!! Dont you hate biologists!!!
    return (Element)elements.get( index - 1 );
  }
  
  public int getLength()
  {
    return elements.size();
  }
  
  public Element[] getSequenceAsElements()
  {
    // get the elements, and make the destination array
    Object[] elemObjs = elements.toArray();
    Element[] elems = new Element[ elemObjs.length ];
    
    // do the copy to array of appropriate type and return it
    System.arraycopy( elemObjs, 0, elems, 0, elemObjs.length );
    return elems;
  }
  
  protected Element setElementAtQuietly( Element elem, int index )
  {
    checkSequenceType( elem );
    Element retn = getElementAt( index );
    elements.set( index - 1, elem );
    return retn;
  }
  
  protected void insertElementAtQuietly( Element elem, int index )
  {
    checkSequenceType( elem );
    elements.add( index - 1, elem );
  }
  
  protected Element deleteElementAtQuietly( int index )
  {
    return (Element)elements.remove( index - 1 );
  }
} // DefaultEditableSequence



/*
 * ChangeLog
 * $Log: DefaultEditableSequence.java,v $
 * Revision 1.6  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.5  2000/11/27 18:18:56  lord
 * Fixed erroneous documentation
 *
 * Revision 1.4  2000/09/11 13:09:48  lord
 * SequenceIdentifier renamed to Identifier
 *
 * Revision 1.3  2000/07/18 11:12:44  lord
 * Import rationalisation
 * Changes due to BioInterface removal
 *
 * Revision 1.2  2000/05/30 16:04:18  lord
 * Have rationalised and sorted all the import statements
 *
 * Revision 1.1  2000/03/08 17:26:35  lord
 * To many changes to document
 *
 */
