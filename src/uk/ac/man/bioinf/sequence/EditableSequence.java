/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.sequence; // Package name inserted by JPack
import uk.ac.man.bioinf.sequence.event.SequenceVetoException;

/**
 * EditableSequence.java
 *
 *
 * Created: Wed Mar  1 19:39:40 2000
 *
 * @author Phillip Lord
 * @version $Id: EditableSequence.java,v 1.5 2001/04/11 17:04:43 lord Exp $ 
 */

public interface EditableSequence extends MutableSequence
{
  /**
   * Sets the value of the element at the specified index. 
   * @param element
   * @param index
   * @return the old value
   * @throws NoSuchSequenceElementException if the element does not
   * exist (index greater that length + 1 or less than 1)
   * @throws SequenceVetoException if some listener doesnt like this
   */
  public Element setElementAt( Element element, int index ) 
    throws NoSuchSequenceElementException, SequenceVetoException;
  
  public Element[] setElementAt( Element[] element, int index ) 
    throws NoSuchSequenceElementException, SequenceVetoException;
  
  /**
   * Insert the element at the specified index. After insertion
   * getElementAt( index ) will return element, and the sequence will
   * be longer by one
   * @param element the element to insert
   * @param index the index at which to insert
   */
  public void insertElementAt( Element element, int index ) 
    throws NoSuchSequenceElementException, SequenceVetoException;
  
  /**
   * Inserts the elements at this position. If the index is one longer
   * than the length of the sequence the sequence will be extended.
   * @param element the elements to insert
   * @param index the index at which to insert
   * @throws NoSuchSequenceElementException if the element does not
   * exist, which will be the case if index is less than 1 or greater
   * than sequence length + 1
   */
  public void insertElementAt( Element[] element, int index ) 
    throws NoSuchSequenceElementException, SequenceVetoException;
  
  /**
   * Delete the element at index. 
   * @param index the index to delete
   * @return the element which has just been deleted
   * @exception NoSuchSequenceElementException if index is less than 1
   * or greater than the length of the sequence
   */
  public Element deleteElementAt( int index ) 
    throws NoSuchSequenceElementException, SequenceVetoException;

  /**
   * Delete the elements starting at index for the specified length
   * @param index the index to start at
   * @param length the length to delete
   * @return the elements just deleted
   * @throws NoSuchSequenceElementException if the elements do not all
   * exist, so if index is less than 1, or index + length is greater
   * than the length of the sequence
   */
  public Element[] deleteElementAt( int index, int length ) 
    throws NoSuchSequenceElementException, SequenceVetoException;

}// EditableSequence


/*
 * ChangeLog
 * $Log: EditableSequence.java,v $
 * Revision 1.5  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.4  2000/11/27 18:18:35  lord
 * Cosmetic
 *
 * Revision 1.3  2000/05/30 16:04:18  lord
 * Have rationalised and sorted all the import statements
 *
 * Revision 1.2  2000/03/08 17:26:35  lord
 * To many changes to document
 *
 * Revision 1.1  2000/03/01 20:16:55  lord
 * Lots of stuff
 * 
 */

