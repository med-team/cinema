/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/*
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral
 * Research Fellow.
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate.
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file.
 */

package uk.ac.man.bioinf.sequence; // Package name inserted by JPack
import uk.ac.man.bioinf.sequence.DefaultSequence;
import uk.ac.man.bioinf.sequence.types.AminoAcid;
import uk.ac.man.bioinf.sequence.types.DNANucleotide;
import uk.ac.man.bioinf.sequence.types.DNASequenceType;
import uk.ac.man.bioinf.sequence.types.ProteinSequenceType;


/**
 * SequenceFactory provides a simple way into the Sequence API. It
 * offers factory methods to create sequences from Strings and
 * Characters. This, combined with the methods in the utility class
 * <code>Sequences</code> is probably all that most users will need.
 *
 *
 * Created: Tue Nov 14 18:06:37 2000
 *
 * @author Phillip Lord
 * @author Crispin Miller
 * @version $Id: SequenceFactory.java,v 1.3 2001/04/11 17:04:43 lord Exp $ */

public class SequenceFactory {

   protected static AminoAcid[] aa;

   static  {
      AminoAcid[] all = AminoAcid.getAll();
      aa = new AminoAcid[26];

      for (int i = 0; i < all.length; i++)
         aa[(int)all[i].toChar() - 65] = all[i];
   }

   /**
    * Create a Sequence representing a protein sequence.
    * @param proteinSequence a character array representing the residues in the sequence - see AminoAcid for more details
    * @throws InvalidSequenceTypeException if the input sequence contains an invalid character.
    */
   public static Sequence getProteinSequence( char[] proteinSequence ) throws InvalidSequenceTypeException  {
      Element[] elements = new Element[ proteinSequence.length ];

      for( int i = 0; i < proteinSequence.length; i++ ){
         try{
            elements[ i ] = aa[ Character.toUpperCase(proteinSequence[ i ]) - 65 ];
            if( elements[ i ] == null ){
               throw new InvalidSequenceTypeException( "No char " + proteinSequence[ i ] );
            }
         }
         catch( ArrayIndexOutOfBoundsException aiob ){
            System.out.println( (proteinSequence[ i ] - 65) );
            throw new InvalidSequenceTypeException( "No char " + proteinSequence[ i ] );
         }
      }
      return new DefaultSequence( elements, ProteinSequenceType.getInstance() );
   }

   /**
    * Create a Sequence representing a protein sequence.
    * @param proteinSequence  represents the residues in the sequence - see AminoAcid for more details
    * @throws InvalidSequenceTypeException if the input sequence contains an invalid character.
    */
   public static Sequence getProteinSequence( String proteinSequence ) throws InvalidSequenceTypeException  {
      Element[] elements = new Element[ proteinSequence.length() ];

      for( int i = 0; i < proteinSequence.length(); i++ ){
         try{
            elements[ i ] = aa[ Character.toUpperCase(proteinSequence.charAt(i)) - 65 ];
            if( elements[ i ] == null ){
               throw new InvalidSequenceTypeException( "No char " + proteinSequence.charAt(i) );
            }
         }
         catch( ArrayIndexOutOfBoundsException aiob ){
            System.out.println( (proteinSequence.charAt(i) - 65) );
            throw new InvalidSequenceTypeException( "No char " + proteinSequence.charAt(i) );
         }
      }
      return new DefaultSequence( elements, ProteinSequenceType.getInstance() );
   }

   /**
    * Create a Sequence representing a DNA sequence.
    * @param dnaSequence  represents the residues in the sequence - see Nucleotide for more details
    * @throws InvalidSequenceTypeException if the input sequence contains an invalid character.
    */
   public static Sequence getDNASequence(char[] dnaSequence ) throws InvalidSequenceTypeException  {
      Element[] elements = new Element[ dnaSequence.length ];
      
      for( int i = 0; i < dnaSequence.length; i++ ){
	
	switch( Character.toUpperCase(dnaSequence[i]) ){
	  
	case 'A':
	  elements[ i ] = DNANucleotide.ADENOSINE;
	  break;
	  
	case 'C':
	  elements[ i ] = DNANucleotide.CYTOSINE;
	  break;

	case 'G':
	  elements[ i ] = DNANucleotide.GUANINE;
	  break;
	  
	case 'T':
	  elements[ i ] = DNANucleotide.THIAMINE;
	  break;
	  
	default:
	  throw new InvalidSequenceTypeException( "Letter " + dnaSequence[ i ] 
						  + " is not a valid part of a DNA Sequence" );
	}
      }
      
      return new DefaultSequence( elements, DNASequenceType.getInstance() );
   }
  
   /**
    * Create a Sequence representing a DNA sequence.
    * @param dnaSequence  represents the residues in the sequence - see Nucleotide for more details
    * @throws InvalidSequenceTypeException if the input sequence contains an invalid character.
    */
   public static Sequence getDNASequence(String dnaSequence ) throws InvalidSequenceTypeException  {
     return getDNASequence( dnaSequence.toCharArray() );
   }
} // SequenceFactory



/*
 * ChangeLog
 * $Log: SequenceFactory.java,v $
 * Revision 1.3  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.2  2000/12/05 15:54:37  lord
 * Import rationalisation
 *
 * Revision 1.1  2000/11/22 18:58:25  lord
 * Initial checkin
 *
 */
