/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.gui.optionable; // Package name inserted by JPack
import javax.swing.JSplitPane;
import uk.ac.man.bioinf.apps.optionable.OptionHandler;
import uk.ac.man.bioinf.apps.optionable.OptionableStateException;
import uk.ac.man.bioinf.apps.optionable.NullOptionHandler;
import uk.ac.man.bioinf.apps.optionable.Optionable;


/**
 * OptionableJSplitPane.java
 *
 * Stores the divider location as an option
 *
 * Created: Fri Feb  2 17:55:18 2001
 *
 * @author Phillip Lord
 * @version $Id: OptionableJSplitPane.java,v 1.3 2001/04/11 17:04:42 lord Exp $
 */

public class OptionableJSplitPane extends JSplitPane implements Optionable
{
  private String optionableName;
  
  private OptionHandler optionHandler;
  private boolean locationSetAsOption = false;
  private boolean haveOptioned;
  
  public OptionableJSplitPane( String optionableName )
  {
    super();
    this.optionableName = optionableName;
    forceOptionable();
  }

  public OptionableJSplitPane( String optionableName, int orientation )
  {
    super( orientation );
    this.optionableName = optionableName;
    forceOptionable();
  }
  
  public void setDefaultDividerLocation( int location )
  {
    if( !locationSetAsOption ){
      super.setDividerLocation( location );
    }
  }
 
  public void setOptions() throws OptionableStateException
  {
    if( haveOptioned ){
      throw new OptionableStateException( "Have already set the optionable status of this frame" );
    }
    forceOptionable();
  }
    
  protected void forceOptionable()
  {
    if( haveOptioned ) 
      throw new RuntimeException( "Have already set the optionable status of this frame" );
    getOptionHandler().addOptionable( this );
    haveOptioned = true;
  }
  
  public Object getOptions()
  {
    return new Integer( getDividerLocation() );
  }
  
  public void setOptions( Object options )
  {
    try{
      if( options != null ){
        setDividerLocation( ((Integer)options).intValue() );
        locationSetAsOption = true;
      }
    }
    catch( ClassCastException cce ){
      // should only occur during development or version change
    }
  }

  public void setOptionHandler( OptionHandler optionHandler ) 
  {
    this.optionHandler = optionHandler;
  }

  public OptionHandler getOptionHandler() 
  {
    if( optionHandler == null ){
      return getDefaultOptionHandler();
    }
    else{
      return optionHandler;
    }
  }
  
  public String getOptionGroupName()
  {
    return optionableName;
  } 
  
  private static OptionHandler defaultOptionHandler;
  public static void setDefaultOptionHandler( OptionHandler optionHandler )
  {
    defaultOptionHandler = optionHandler;
  }
  
  public static OptionHandler getDefaultOptionHandler()
  {
    if( defaultOptionHandler == null ){
      return NullOptionHandler.getInstance();
    }
    return defaultOptionHandler;
  }
} // OptionableJSplitPane



/*
 * ChangeLog
 * $Log: OptionableJSplitPane.java,v $
 * Revision 1.3  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.2  2001/03/12 16:48:48  lord
 * New constructors added
 *
 * Revision 1.1  2001/02/19 17:42:53  lord
 * Initial checkin
 *
 */
