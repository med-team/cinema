/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package uk.ac.man.bioinf.gui.util;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.net.URL;
import java.io.IOException;


/**
 * Displays an URL with a single 'OK' button at the bottom
 */
public class PopUpHtmlDisplay {
   
   private PopUpHtmlDisplay() {
   }

   public static void show(Component frame, URL page){
      final JComponent pageC  = getPage(page);
      JOptionPane.showMessageDialog(frame,pageC);
   }
 

   /**
    * try to open the URL and get the page
    */ 
   public static final JComponent getPage(URL page) {
      JPanel contentPane = new JPanel();
      contentPane.setLayout(new BorderLayout());

      JEditorPane editorPane = new JEditorPane();
      editorPane.setEditable(false);
      editorPane.setPreferredSize(new Dimension(400, 400));
      editorPane.setMinimumSize(new Dimension(400, 400));

      JScrollPane editorScrollPane = new JScrollPane(editorPane);
      try {
         editorPane.setPage(page);
      } catch (IOException e) {
         System.err.println("Attempted to read a bad URL: " + page);
      }

      contentPane.add(editorPane, BorderLayout.CENTER);
      return contentPane;
   }
}
