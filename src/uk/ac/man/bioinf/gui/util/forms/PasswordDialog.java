/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package uk.ac.man.bioinf.gui.util.forms;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.net.URL;
import java.io.IOException;


/**
 * Pop up Dialog box to get a password off a user.
 * @author C.Miller
 * @version 1.0
 */
public class PasswordDialog {
   protected JFrame parent;
   protected JTextField userName;
   protected JPasswordField password;
   protected JCheckBox      remember;
   JLabel un;
   JLabel pw;

   public PasswordDialog(JFrame parent) {
      userName = new JTextField();
      password = new JPasswordField();
   }

   public PasswordDialog(JFrame parent, String userNameLabel, String passwordLabel) {
      this.parent = parent;
      userName = new JTextField();
      password = new JPasswordField();
      remember = new JCheckBox("Remember password?");
      un = new JLabel("Username");
      pw = new JLabel("Password");
   }

   public PasswordDialog(JFrame parent, String userNameLabel, String passwordLabel, String initialUserName, String initialPassword) {
      this.parent = parent;
      userName = new JTextField();
      password = new JPasswordField();
      remember = new JCheckBox("Remember password?");
      un = new JLabel(userNameLabel);
      pw = new JLabel(passwordLabel);
   }

   public PasswordDialog(JFrame parent, String userNameLabel, String passwordLabel, String rememberLabel,
                         String initialUserName, String initialPassword, boolean initialRemember) {
      this.parent = parent;
      userName = new JTextField(initialUserName);
      password = new JPasswordField(initialPassword);
      remember = new JCheckBox(rememberLabel);
      remember.setSelected(initialRemember);
      un = new JLabel(userNameLabel);
      pw = new JLabel(passwordLabel);
   }

   public void show() {
      JOptionPane.showMessageDialog(parent, getPasswordPane());
   }

   private void addRows(JComponent[] col1,
                        JComponent[] col2,
                        GridBagLayout gridbag,
                        Container container) {
      GridBagConstraints c = new GridBagConstraints();
      c.anchor = GridBagConstraints.EAST;
      int numCols = col1.length;
      
      for (int i = 0; i < numCols; i++) {
         c.gridwidth = GridBagConstraints.RELATIVE; //next-to-last
         c.fill = GridBagConstraints.NONE;      //reset to default
         c.weightx = 0.0;                       //reset to default
         gridbag.setConstraints(col1[i], c);
         container.add(col1[i]);
         
         c.gridwidth = GridBagConstraints.REMAINDER;     //end row
         c.fill = GridBagConstraints.HORIZONTAL;
         c.weightx = 1.0;
         gridbag.setConstraints(col2[i], c);
         container.add(col2[i]);
      }
   }
   
   public char[] getPassword() {
      return password.getPassword();
   }

   public String getUserName() {
      return userName.getText();
   }

   public void setPassword(String pw) {
      password.setText(pw);
   }

   public void setUserName(String un) {
      userName.setText(un);
   }

   public boolean isPasswordToBeRemembered() {
      return remember.isSelected();
   }

   public void setRememberPassword(boolean r) {
      remember.setSelected(r);
   }
   
   public JComponent getPasswordPane() {
      JPanel contentPane = new JPanel();
      GridBagLayout layout = new GridBagLayout();
      contentPane.setLayout(layout);
      JComponent[] col1 = {un,pw};
      JComponent[] col2 = {userName,password};
      addRows(col1,col2,layout,contentPane);
      GridBagConstraints c = new GridBagConstraints();
      c.anchor = GridBagConstraints.EAST;
      c.gridwidth = GridBagConstraints.REMAINDER;     //end row
      c.fill = GridBagConstraints.HORIZONTAL;
      c.weightx = 1.0;
      layout.setConstraints(remember, c);
      contentPane.add(remember);
      return contentPane;
   }
}
