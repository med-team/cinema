/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package uk.ac.man.bioinf.gui.util;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JScrollPane;
import javax.swing.JComponent;
import java.awt.BorderLayout;
import java.awt.Dimension;

/**
 * Triplet panel provides a GUI Component with three sub components.
 * There is a main window on the right handsied and two sub windows on the left.
 * The relative sacing of these windows is controlled by split panes.
 * Methods exist to add and remove Components from these windows.
 * @author C. Miller
 * @version $Id :
 */

public class TripletPanel extends JPanel {
   private JSplitPane leftRightSplit;
   private JSplitPane upDownSplit;
   private JPanel mainWindow;
   private JPanel topLeft;
   private JPanel bottomLeft;
   private JComponent tlt;
   private JComponent blt;
   private JComponent rht;
   private JScrollPane tlp;
   private JScrollPane blp;  
   private JScrollPane rhp;  



   public TripletPanel() {
      this(new JPanel(), new JPanel(), new JPanel());
   }

   public TripletPanel(JComponent topLeft, JComponent bottomLeft, JComponent mainWindow) {
      super();
      init();
      setMainWindow(mainWindow);
      setTopLeft(topLeft);
      setBottomLeft(bottomLeft);
   }

   private void init() {
      this.setLayout(new BorderLayout());
      topLeft    = new JPanel();
      bottomLeft = new JPanel();
      mainWindow = new JPanel();
      topLeft.setLayout(new BorderLayout());
      bottomLeft.setLayout(new BorderLayout());
      mainWindow.setLayout(new BorderLayout());

      Dimension minimumSize = new Dimension(100, 50);
      topLeft.setMinimumSize(minimumSize);
      bottomLeft.setMinimumSize(minimumSize);      
      mainWindow.setMinimumSize(minimumSize);

      Dimension preferredSize = new Dimension(100, 50);
      topLeft.setPreferredSize(preferredSize);
      bottomLeft.setPreferredSize(preferredSize);      
      preferredSize = new Dimension(400, 400);
      mainWindow.setPreferredSize(preferredSize);

      tlp = new JScrollPane(topLeft);
      blp = new JScrollPane(bottomLeft);
      rhp = new JScrollPane(mainWindow);

      upDownSplit    = new JSplitPane(JSplitPane.VERTICAL_SPLIT,tlp,blp);
      leftRightSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,upDownSplit,rhp);
      upDownSplit.setOneTouchExpandable(true);
      leftRightSplit.setOneTouchExpandable(true);
      leftRightSplit.setDividerLocation(150);
        //Provide minimum sizes for the two components in the split pane
      this.add(leftRightSplit,BorderLayout.CENTER);
   }

   public void setMainWindow(JComponent c) {
      if(rht != null) mainWindow.remove(rht);
      mainWindow.add(c,BorderLayout.CENTER);
      rht = c;
      mainWindow.revalidate();
      System.err.println("Set main window");
   }

   public JComponent getMainWindow() { return mainWindow; }

   public void setTopLeft(JComponent c) {
      if(tlt != null) topLeft.remove(tlt);
      topLeft.add(c,BorderLayout.CENTER);
      tlt = c;
      topLeft.revalidate();
   }

   public JComponent getTopLeft() { return topLeft; }

   public void setBottomLeft(JComponent c) {
      if(blt != null) bottomLeft.remove(blt);
      bottomLeft.add(c,BorderLayout.CENTER);
      blt = c;
      bottomLeft.revalidate();
   }

   public JComponent getBottomLeft() { return bottomLeft; }
}
