/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package uk.ac.man.bioinf.gui.viewer.test; // Package name inserted by JPack
// SliderExample.java
//
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import uk.ac.man.bioinf.gui.viewer.Install;
import uk.ac.man.bioinf.gui.viewer.JAlignmentRuler;

public class SliderExample extends JPanel {

  public SliderExample() {

    super(true);

    this.setLayout(new BorderLayout());
    JAlignmentRuler slider = new JAlignmentRuler();
    slider.setMinorTickSpacing(2);
    slider.setMajorTickSpacing(10);
    slider.setPaintTicks(true);
    slider.setPaintLabels(true);

    // Note that the following line is really unnecessary. By setting a
    // positive integer to the major tick spacing and setting the paintLabel
    // property to true, it's done for us!

    slider.setLabelTable(slider.createStandardLabels(10));
    add(slider, BorderLayout.CENTER);
  }

  public static void main(String s[]) {
    Class cla = Install.class;
    JFrame frame = new JFrame("Slider");
    frame.addWindowListener(new BasicWindowMonitor());
    frame.setContentPane(new SliderExample());
    frame.pack();
    frame.setVisible(true);
  }
}
