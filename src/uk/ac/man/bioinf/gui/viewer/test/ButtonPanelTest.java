/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.gui.viewer.test; // Package name inserted by JPack

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import uk.ac.man.bioinf.apps.cinema.core.test.CinemaTestLaunch;
import uk.ac.man.bioinf.gui.viewer.JAlignmentButtonPanel;


/**
 * ButtonPanelTest.java
 *
 *
 * Created: Fri May 19 17:22:31 2000
 *
 * @author Phillip Lord
 * @version $Id: ButtonPanelTest.java,v 1.3 2001/04/11 17:04:43 lord Exp $
 */

public class ButtonPanelTest implements ItemListener
{
  public static void main( String[] args ) throws Throwable
  {
    JFrame frame = new JFrame( "Test" );
    
    JAlignmentButtonPanel panel = new JAlignmentButtonPanel( CinemaTestLaunch.generateMsa() );
    JScrollPane scrollPane = new JScrollPane( panel );
    
    frame.getContentPane().add
      ( scrollPane );
    panel.addItemListener( new ButtonPanelTest() );
    frame.setSize( 300, 100 );
    frame.setVisible( true );
  } //end main method 
  
  public void itemStateChanged( ItemEvent event )
  {
    System.out.println( event.getItem() + " " + ((Sequence)event.getItem()).getIdentifier().getTitle() );
  }
  
} // ButtonPanelTest



/*
 * ChangeLog
 * $Log: ButtonPanelTest.java,v $
 * Revision 1.3  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.2  2000/07/18 11:10:00  lord
 * Import rationalisation.
 * Changes due to removal of biointerface
 *
 * Revision 1.1  2000/05/24 15:40:50  lord
 * Initial checkin
 *
 */
