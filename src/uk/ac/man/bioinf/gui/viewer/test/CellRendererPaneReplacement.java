/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.gui.viewer.test; // Package name inserted by JPack
import javax.swing.CellRendererPane;
import java.awt.Graphics;
import java.awt.Component;
import java.awt.Container;
import java.awt.Color;
import javax.swing.JComponent;
//import javax.swing.SwingGraphics;


/**
 * CellRendererPaneReplacement.java
 *
 *
 * Created: Fri Apr 14 17:02:05 2000
 *
 * @author Phillip Lord
 * @version $Id: CellRendererPaneReplacement.java,v 1.2 2001/04/11 17:04:43 lord Exp $
 */

public class CellRendererPaneReplacement extends CellRendererPane
{

     public void paintComponent(Graphics g, Component c, Container p,
				int x, int y, int w, int h, boolean shouldValidate) {
	if (c == null) {
	    if (p != null) {
		Color oldColor = g.getColor();
		g.setColor(p.getBackground());
		g.fillRect(x, y, w, h);
		g.setColor(oldColor);
	    }
	    return;
	}

	if (c.getParent() != this) {
	    this.add(c);
	}

	c.setBounds(x, y, w, h);

	if(shouldValidate) {
	    c.validate();
	}

	boolean wasDoubleBuffered = false;
	if ((c instanceof JComponent) && ((JComponent)c).isDoubleBuffered()) {
	    wasDoubleBuffered = true;
	    ((JComponent)c).setDoubleBuffered(false);
	}

	// move the co-ord space
	g.translate( x, y );
	c.paint(g);
	// and move it back
	g.translate( -x, - y);
	

	if ((c instanceof JComponent) && wasDoubleBuffered) {
	    ((JComponent)c).setDoubleBuffered(true);
	}

	// this bit is a buggy (4204449) duplication and can safely
	// be deleted
	
	//  if (c instanceof JComponent) {
//  	    JComponent jc = (JComponent)c;
//  	    jc.setDoubleBuffered(wasDoubleBuffered);
//  	}

	c.setBounds(-w, -h, 0, 0);
    }

} // CellRendererPaneReplacement



/*
 * ChangeLog
 * $Log: CellRendererPaneReplacement.java,v $
 * Revision 1.2  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.1  2000/04/18 17:45:24  lord
 * All files moved from uk.ac.man.bioinf.viewer package
 *
 */
