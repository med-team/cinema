/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.gui.viewer.test; // Package name inserted by JPack
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Dictionary;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import uk.ac.man.bioinf.gui.color.IndividualElementColorMap;
import uk.ac.man.bioinf.gui.viewer.Install;
import uk.ac.man.bioinf.gui.viewer.InvertedScrollPaneLayout;
import uk.ac.man.bioinf.gui.viewer.JAlignmentRuler;
import uk.ac.man.bioinf.gui.viewer.JAlignmentViewer;
import uk.ac.man.bioinf.gui.viewer.JChangedScrollPane;
import uk.ac.man.bioinf.gui.viewer.plaf.BasicAlignmentViewerUI;
import uk.ac.man.bioinf.sequence.Element;
import uk.ac.man.bioinf.sequence.Element;
import uk.ac.man.bioinf.sequence.alignment.DefaultSequenceAlignment;
import uk.ac.man.bioinf.sequence.types.AminoAcid;
import uk.ac.man.bioinf.sequence.types.AminoAcid;
import uk.ac.man.bioinf.sequence.types.ProteinSequenceType;
import uk.ac.man.bioinf.sequence.types.ProteinSequenceType;


/**
 * AlignmentViewerTest.java
 *
 *
 * Created: Wed Mar 15 20:11:02 2000
 *
 * @author Phillip Lord
 * @version $Id: AlignmentViewerTest.java,v 1.6 2001/04/11 17:04:43 lord Exp $
 */

public class AlignmentViewerTest extends JFrame
{
  
  JMenuItem blinkOn, blinkOff;
  JAlignmentViewer viewer;

  public AlignmentViewerTest() throws Throwable
  {
    super( "Test" );
    Class waste = Install.class;
    
    addWindowListener
      ( new WindowAdapter(){
	  public void windowClosing( WindowEvent event )
	  {
	    System.exit( 0 );
	  }
	});
   

    Element[][] elems = new Element[ 40 ][];
    elems[ 0 ] = AminoAcid.getAll();
    elems[ 1 ] = AminoAcid.getAll();
    elems[ 2 ] = AminoAcid.getAll();
    elems[ 3 ] = AminoAcid.getAll();
    elems[ 4 ] = AminoAcid.getAll();
    elems[ 5 ] = AminoAcid.getAll();
    elems[ 6 ] = AminoAcid.getAll();
    elems[ 7 ] = AminoAcid.getAll();
    elems[ 8 ] = AminoAcid.getAll();    
    elems[ 9 ] = AminoAcid.getAll();
    elems[ 10 ] = AminoAcid.getAll();
    elems[ 11 ] = AminoAcid.getAll();
    elems[ 12 ] = AminoAcid.getAll();
    elems[ 13 ] = AminoAcid.getAll();
    elems[ 14 ] = AminoAcid.getAll();
    elems[ 15 ] = AminoAcid.getAll();
    elems[ 16 ] = AminoAcid.getAll();
    elems[ 17 ] = AminoAcid.getAll();
    elems[ 18 ] = AminoAcid.getAll();    
    elems[ 19 ] = AminoAcid.getAll();
    elems[ 20 ] = AminoAcid.getAll();
    elems[ 21 ] = AminoAcid.getAll();
    elems[ 22 ] = AminoAcid.getAll();
    elems[ 23 ] = AminoAcid.getAll();
    elems[ 24 ] = AminoAcid.getAll();
    elems[ 25 ] = AminoAcid.getAll();
    elems[ 26 ] = AminoAcid.getAll();
    elems[ 27 ] = AminoAcid.getAll();
    elems[ 28 ] = AminoAcid.getAll();    
    elems[ 29 ] = AminoAcid.getAll();
    elems[ 30 ] = AminoAcid.getAll();
    elems[ 31 ] = AminoAcid.getAll();
    elems[ 32 ] = AminoAcid.getAll();
    elems[ 33 ] = AminoAcid.getAll();
    elems[ 34 ] = AminoAcid.getAll();
    elems[ 35 ] = AminoAcid.getAll();
    elems[ 36 ] = AminoAcid.getAll();
    elems[ 37 ] = AminoAcid.getAll();
    elems[ 38 ] = AminoAcid.getAll();    
    elems[ 39 ] = AminoAcid.getAll();

    DefaultSequenceAlignment msa = new DefaultSequenceAlignment( elems, ProteinSequenceType.getInstance() );
    msa.setInset( 3, 13 );
    msa.setInset( 1, 6 );
    msa.setInset( 4, 8 );
    msa.setInset( 2, 4 );
    msa.setInset( 5, 43 );
    msa.setInset( 6, 1 );
    msa.setInset( 7, 5 );
    msa.setInset( 8, 0 );
    msa.setInset( 9, 6 );
    msa.setInset( 10, 13 );
    msa.setInset( 13, 3 );
    msa.setInset( 11, 6 );
    msa.setInset( 14, 38 );
    msa.setInset( 12, 4 );
    msa.setInset( 15, 9 );
    msa.setInset( 16, 1 );
    msa.setInset( 17, 5 );
    msa.setInset( 18, 0 );
    msa.setInset( 19, 6 );
    msa.setInset( 20, 3 );
    msa.setInset( 23, 3 );
    msa.setInset( 21, 6 );
    msa.setInset( 24, 8 );
    msa.setInset( 22, 4 );
    msa.setInset( 25, 9 );
    msa.setInset( 26, 1 );
    msa.setInset( 27, 5 );
    msa.setInset( 28, 0 );
    msa.setInset( 29, 6 );
    msa.setInset( 30, 3 );
    msa.setInset( 33, 3 );
    msa.setInset( 31, 6 );
    msa.setInset( 34, 8 );
    msa.setInset( 32, 4 );
    msa.setInset( 35, 9 );
    msa.setInset( 36, 1 );
    msa.setInset( 37, 5 );
    msa.setInset( 38, 0 );
    msa.setInset( 39, 6 );
    msa.setInset( 40, 3 );

    // color mapping
    AminoAcid[] aa = AminoAcid.getAll();
    Color[] colors = new Color[aa.length];
    for (int i = 0; i < colors.length; i++) {
      if (aa[i] == AminoAcid.GLYCINE)
	colors[i] = new Color(255, 170, 136);
      else if (aa[i] == AminoAcid.ALANINE)
	colors[i] = new Color(255, 255, 255);
      else if (aa[i] == AminoAcid.VALINE)
	colors[i] = new Color(255, 255, 255);
      else if (aa[i] == AminoAcid.LEUCINE)
	colors[i] = new Color(255, 255, 255);
      else if (aa[i] == AminoAcid.ISOLEUCINE)
	colors[i] = new Color(255, 255, 255);
      else if (aa[i] == AminoAcid.SERINE)
	colors[i] = new Color(136, 255, 136);
      else if (aa[i] == AminoAcid.CYSTEINE)
	colors[i] = new Color(255, 255, 136);
      else if (aa[i] == AminoAcid.THREONINE)
	colors[i] = new Color(136, 255, 136);
      else if (aa[i] == AminoAcid.METHIONINE)
	colors[i] = new Color(255, 255, 255);
      else if (aa[i] == AminoAcid.PHENYLALANINE)
	colors[i] = new Color(255, 136, 255);
      else if (aa[i] == AminoAcid.TYROSINE)
	colors[i] = new Color(255, 136, 255);
      else if (aa[i] == AminoAcid.TRYPTOPHAN)
	colors[i] = new Color(255, 136, 255);
      else if (aa[i] == AminoAcid.PROLINE)
	colors[i] = new Color(255, 170, 136);
      else if (aa[i] == AminoAcid.HISTIDINE)
	colors[i] = new Color(136, 255, 255);
      else if (aa[i] == AminoAcid.LYSINE)
	colors[i] = new Color(136, 255, 255);
      else if (aa[i] == AminoAcid.ARGININE)
	colors[i] = new Color(136, 255, 255);
      else if (aa[i] == AminoAcid.ASPARTICACID)
	colors[i] = new Color(255, 136, 136);
      else if (aa[i] == AminoAcid.GLUTAMICACID)
	colors[i] = new Color(255, 136, 136);
      else if (aa[i] == AminoAcid.ASPARAGINE)
	colors[i] = new Color(136, 255, 136);
      else if (aa[i] == AminoAcid.GLUTAMINE)
	colors[i] = new Color(136, 255, 136);
      else
	colors[i] = new Color(102, 102, 102);
    }
    IndividualElementColorMap cm = new IndividualElementColorMap("test", elems[0], colors);

    System.out.println( "Amino acid " + ((Object[])AminoAcid.getAll()).length );
    
    System.out.println( "The length of the msa is " + msa.getLength() );
    //JLabel viewer = new JLabel( "Viewer Label " );
    
    viewer = new JAlignmentViewer( msa );
    viewer.setCellWidth( 40 );
    viewer.setCellHeight( 40 );
    viewer.setColorMap(cm);  // set the color map for the viewer
    
    JChangedScrollPane pane = new JChangedScrollPane
      ( new InvertedScrollPaneLayout(),
	viewer, 
       JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED );
    // performance speed ups
    System.out.println( "Backing store enabled" );
    pane.getViewport().setBackingStoreEnabled( true );
    System.out.println( "Window blitting" );
    pane.getViewport().putClientProperty
    ("EnableWindowBlit", Boolean.TRUE);
    
    // this prevents the scroll pane from resizing the JAlignmentRuler
    // in a manner I consider to be wrong!!!
    JPanel rulerPanel = new JPanel();
    rulerPanel.setLayout( new BorderLayout() );
    
    JAlignmentRuler ruler = new JAlignmentRuler( msa );

    ruler.setPreferredWidthPerBase( viewer.getCellWidth() );
    pane.setColumnHeaderView( rulerPanel );

    Dictionary dic = ruler.getLabelTable();
    JLabel oneLabel = (JLabel)dic.get( new Integer( 1 ) );
    oneLabel.setText( " 1" );
    ruler.setLabelTable( dic );
    
    
    rulerPanel.add( ruler, BorderLayout.WEST );

    JMenuBar bar;
    
    setJMenuBar( bar = new JMenuBar() );
    
    bar.add( blinkOn = new JMenuItem( "ON" ) );
    blinkOn.addActionListener
      ( new ActionListener(){
	  public void actionPerformed( ActionEvent event )
	  {
	    BasicAlignmentViewerUI ui =
	      (BasicAlignmentViewerUI)AlignmentViewerTest.this.viewer.getAlignmentUI();
	    ui.setCursorBlink();
	  }
	});
    bar.add( blinkOff = new JMenuItem( "OFF" ) );
    blinkOff.addActionListener
      ( new ActionListener() {
	  public void actionPerformed( ActionEvent event )
	  {
	    BasicAlignmentViewerUI ui =
	      (BasicAlignmentViewerUI)AlignmentViewerTest.this.viewer.getAlignmentUI();
	    ui.unsetCursorBlink();
	  }
	});
    

    pane.setSize( 400, 400 );
    getContentPane().add( pane );
    setSize( 400, 400 );
    viewer.setVisible( true );
    System.out.println( "Calling set visible ");
    setVisible( true );
    
    
  }
  
  public static void main( String[] args ) throws Throwable
  {
    AlignmentViewerTest test = new AlignmentViewerTest();
    
  } //end main method 
  
} // AlignmentViewerTest



/*
 * ChangeLog
 * $Log: AlignmentViewerTest.java,v $
 * Revision 1.6  2001/04/11 17:04:43  lord
 * Added License agreements to all code
 *
 * Revision 1.5  2000/05/30 16:04:18  lord
 * Have rationalised and sorted all the import statements
 *
 * Revision 1.4  2000/04/20 13:59:01  lord
 * Lots of tedious changes
 *
 * Revision 1.3  2000/04/18 18:07:49  jns
 * o modified to allow for Element, which remains in the sequence pacakge, not the types package.
 *
 * Revision 1.2  2000/04/18 18:00:14  lord
 * Modified to reflect changed package names
 *
 * Revision 1.1  2000/04/18 17:45:24  lord
 * All files moved from uk.ac.man.bioinf.viewer package
 *
 * Revision 1.8  2000/04/18 17:33:26  lord
 * Various blitting methods tried out
 *
 * Revision 1.7  2000/04/12 13:43:00  jns
 * o added in color mapping code
 *
 * Revision 1.6  2000/04/03 13:55:12  lord
 * Lots of tedious changes
 *
 * Revision 1.5  2000/03/31 16:27:39  lord
 * Made a series of changes to cope with the movement of the
 * JAlignmentRuler out of the JAlignmentViewer class
 *
 * Revision 1.4  2000/03/29 14:59:16  lord
 * Various unexciting changes
 *
 * Revision 1.3  2000/03/21 18:52:07  lord
 * More tedious changes
 *
 * Revision 1.2  2000/03/21 13:42:31  lord
 * Changes to boring to document
 *
 * Revision 1.1  2000/03/16 16:19:20  lord
 * Initial checkin
 *
 */


