/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Julian Selley (j.selley@man.ac.uk)
 * whilst at the University of Manchester as a Research Assistant. 
 *
 * The initial code base is copyright by the University of Manchester. 
 * Modifications to the initial code base are copyright of their 
 * respective authors, or their employers as appropriate. Authorship 
 * of the modifications may be determined from the ChangeLog placed 
 * at the end of this file
 */

package uk.ac.man.bioinf.gui.viewer; // Package name inserted by JPack

import java.awt.Color;
import java.awt.Graphics;
import uk.ac.man.bioinf.sequence.Element;
import uk.ac.man.bioinf.sequence.alignment.Gap;
import uk.ac.man.bioinf.sequence.geom.SequenceAlignmentPoint;


/**
 * ColorFastAlignmentViewerCellRenderer.java
 *
 * One of three classes designed to render cells in the
 * JAlignmentViewer. This one deals specifically with the color of the
 * cell (background).
 * <p>
 * By default, this will drawn first and other things placed on top of
 * it. However, all this will be determined by the RendererManager.
 *
 * Created: Fri Aug 25 14:40:58 2000
 *
 * @author Julian Selley
 * @version $Id: ColorFastAlignmentViewerCellRenderer.java,v 1.5 2001/04/11 17:04:42 lord Exp $
 */

public class ColorFastAlignmentViewerCellRenderer 
  implements FastAlignmentViewerCellRenderer
{
  public ColorFastAlignmentViewerCellRenderer() {}

  public void renderAlignmentViewerCell
    (Graphics g, int x, int y, int width, int height,
     JAlignmentViewer viewer, Element element,
     SequenceAlignmentPoint location, Color color,
     boolean isSelected, boolean hasFocus, boolean isAtPoint) 
  {
    // draw the background color
    if ( color != null && element != Gap.GAP ) {
      g.setColor(color);
      g.fillRect(x, y, width, height);
    }
  }
} // ColorFastAlignmentViewerCellRenderer



/*
 * ChangeLog
 * $Log: ColorFastAlignmentViewerCellRenderer.java,v $
 * Revision 1.5  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.4  2000/10/03 17:13:20  lord
 * Now cope with Gap elements correctly
 *
 * Revision 1.3  2000/09/21 18:38:49  jns
 * o fixing a bug - removing the border that it was leaving
 *
 * Revision 1.2  2000/09/19 18:08:36  jns
 * o removal of archiec function that was removed in the interface, and I forgot
 * to remove it here until now
 *
 * Revision 1.1  2000/09/18 17:54:54  jns
 * o created a new multiplexer cell renderer to take account of cell size
 * o initial code
 *
 */
