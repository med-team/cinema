/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.gui.viewer; // Package name inserted by JPack
import javax.swing.JScrollPane;
import java.awt.Component;
import javax.swing.ScrollPaneLayout;


/**
 * JChangedScrollPane.java
 *
 *
 * Created: Fri Mar 31 14:56:45 2000
 *
 * @author Phillip Lord
 * @version $Id: JChangedScrollPane.java,v 1.2 2001/04/11 17:04:42 lord Exp $
 */

public class JChangedScrollPane extends JScrollPane
{

  public JChangedScrollPane( Component view, int vsbPolicy, int hsbPolicy )
  {
    this( new ScrollPaneLayout.UIResource(), view, vsbPolicy, hsbPolicy );
  }
  
  public JChangedScrollPane( ScrollPaneLayout layout, Component view, int vsbPolicy, int hsbPolicy )
  {
    setLayout( layout );
    setVerticalScrollBarPolicy(vsbPolicy);
    setHorizontalScrollBarPolicy(hsbPolicy);
    setViewport(createViewport());
    setVerticalScrollBar(createVerticalScrollBar());
    setHorizontalScrollBar(createHorizontalScrollBar());
    if ( view != null) {
      setViewportView(view);
    }
    updateUI();
  }
  
} // JChangedScrollPane



/*
 * ChangeLog
 * $Log: JChangedScrollPane.java,v $
 * Revision 1.2  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.1  2000/04/18 17:43:56  lord
 * All files moved from uk.ac.man.bioinf.viewer package
 *
 * Revision 1.1  2000/03/31 16:21:01  lord
 * Initial checkin
 *
 */
