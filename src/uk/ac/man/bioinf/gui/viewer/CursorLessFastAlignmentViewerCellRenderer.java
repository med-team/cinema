/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.gui.viewer; // Package name inserted by JPack
import java.awt.Color;
import java.awt.Graphics;
import uk.ac.man.bioinf.gui.viewer.JAlignmentViewer;
import uk.ac.man.bioinf.sequence.Element;
import uk.ac.man.bioinf.sequence.geom.SequenceAlignmentPoint;


/**
 * CursorLessFastAlignmentViewerCellRenderer.java
 *
 *
 * Created: Thu Jul 13 17:59:36 2000
 *
 * @author Phillip Lord
 * @version $Id: CursorLessFastAlignmentViewerCellRenderer.java,v 1.4 2001/04/11 17:04:42 lord Exp $
 */

public class CursorLessFastAlignmentViewerCellRenderer 
  extends MultiplexerFastAlignmentViewerCellRenderer
{

  public CursorLessFastAlignmentViewerCellRenderer(JAlignmentViewer viewer) 
  {
    super(viewer);
  }
  
  public void renderAlignmentViewerCell
    ( Graphics g, int x, int y, int width, int height, 
      JAlignmentViewer viewer, Element element, 
      SequenceAlignmentPoint location, Color bgColor, 
      boolean isSelected, boolean hasFocus, boolean isAtPoint )
  {
    super.renderAlignmentViewerCell
      ( g, x, y, width, height, viewer, element, location, bgColor, isSelected, hasFocus, false );
  }
} // CursorLessFastAlignmentViewerCellRenderer



/*
 * ChangeLog
 * $Log: CursorLessFastAlignmentViewerCellRenderer.java,v $
 * Revision 1.4  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.3  2000/12/12 13:04:45  jns
 * o changing renderer to extend the multiplexer with the idea that this
 * will make the consensus sequence (which uses this renderer) keep pace
 * with the alignment viewer (which uses the multiplexer) - serving to
 * keep things more tidy. I have been meaning to do this for ages.
 *
 * Revision 1.2  2000/09/18 17:55:42  jns
 * o change of function name to make more sense
 *
 * Revision 1.1  2000/07/18 11:09:01  lord
 * Import rationalisation.
 *
 */
