/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.gui.viewer; // Package name inserted by JPack
import java.awt.AWTEventMulticaster;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.ItemSelectable;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;
import javax.swing.JButton;
import javax.swing.JPanel;
import uk.ac.man.bioinf.sequence.alignment.GappedSequence;
import uk.ac.man.bioinf.sequence.alignment.SequenceAlignment;
import uk.ac.man.bioinf.sequence.alignment.event.AlignmentEvent;
import uk.ac.man.bioinf.sequence.alignment.event.AlignmentEventType;
import uk.ac.man.bioinf.sequence.alignment.event.AlignmentListener;


/**
 * JAlignmentButtonPanel.java
 *
 * This component provides a rank of Buttons relating to a
 * SequenceAlignment. At the moment it only supports Vertical
 * alignment. The labels of the buttons are what ever is specified by
 * the individual Sequence resourceString. These labels define the
 * width of the overall panel, whilst the height is defined by the
 * specified height of the individual cells. 
 *
 * Created: Fri May 19 16:24:31 2000
 *
 * @author Phillip Lord
 * @version $Id: JAlignmentButtonPanel.java,v 1.8 2001/04/11 17:04:42 lord Exp $
 */

public class JAlignmentButtonPanel extends JPanel implements ItemSelectable, AlignmentListener
{
  private ItemListener itemListener;
  private int prototypicalCellHeight = 15;
  private Map buttonMap = new WeakHashMap();
  private Map colorMap  = new WeakHashMap();
  private SequenceAlignment msa;
  
  public JAlignmentButtonPanel( )
  {
    setLayout( new GridLayout( 0, 1 ) );
  }
  
  public JAlignmentButtonPanel( SequenceAlignment msa )
  {
    this();
    setSequenceAlignment( msa );
  }
  
  public void setFixedCellHeight( int height )
  {
    this.prototypicalCellHeight = height;
  }
  
  private Dimension preferredSize = new Dimension();
  public Dimension getPreferredSize()
  {
    // (PENDING:- PL) calculate this properly
    preferredSize. setSize( 70, getComponentCount() * prototypicalCellHeight );
    return preferredSize;
  }

  public Point getPointForSequence( GappedSequence seq )
  {
    return ((JButton)buttonMap.get( seq )).getLocation();
  }
  
  public void setSequenceColor( GappedSequence seq, Color color )
  {
    syncToAlignment();
    colorMap.put( seq, color );
    ((JButton)buttonMap.get( seq )).setBackground( color );
    repaint();
  }
  
  public Color getSequenceColor( GappedSequence seq )
  {
    return (Color)colorMap.get( seq );
  }
  
  public void clearSequenceColor( GappedSequence seq )
  {
    setSequenceColor( seq, getBackground() );
  }
  
  public void clearAllSequenceColors()
  {
    colorMap.clear();
    syncToAlignment();
  }

  public void setSequenceAlignment( SequenceAlignment msa )
  {
    this.msa = msa;
    msa.addAlignmentListener( this );
    syncToAlignment();
  }
  
  private void syncToAlignment()
  {
    buttonMap.clear();
    removeAll();
    // iterate through the msa, and buttons for them all
    int length = msa.getNumberSequences();
    
    // set up all the new buttons adding new ones as necessary. 
    for( int i = 0; i < length; i++ ){
      if( i >= getComponentCount() ){
        add( new JButton() );
      }
      
      GappedSequence seq = msa.getSequenceAt( i + 1);
      setButton( seq, (JButton)getComponent( i ) );
    }
    
    // delete any unnecessary buttons
    for( int i = getComponentCount(); i > length; i-- ){
      remove( i );
    }
    
    invalidate();
    repaint();
  }
  
  public void setButton( GappedSequence seq, JButton button )
  {
    // (PENDING:- PL) Button fonts should inherit straight from the
    // main component. 
    button.setFont( button.getFont().deriveFont( 6f ) );
    Color col = (Color)colorMap.get( seq );
    
    if( col != null ){
      button.setBackground( col );
    }
    
    button.setLabel( seq.getIdentifier().getTitle() );
    button.addActionListener( new SequenceButtonListener( seq ) );
    buttonMap.put( seq, button );
  }
  
  public void changeOccurred( AlignmentEvent event )
  {
    // (PENDING:- PL) This could be made a lot more restrictive
    if( event.getType() == AlignmentEventType.DELETE ||
        event.getType() == AlignmentEventType.INSERT ){
      syncToAlignment();
    }
  }
  
  public void addItemListener( ItemListener listener )
  {
    if( listener == null ) return;
    
    itemListener = AWTEventMulticaster.add( itemListener, listener );
  }
  
  public void removeItemListener( ItemListener listener )
  {
    if( listener == null ) return;
    
    itemListener = AWTEventMulticaster.remove( itemListener, listener );
  }
  
  public Object[] getSelectedObjects()
  {
    return null;
  }
  
  protected void processItemEvent( ItemEvent event )
  {
    if( itemListener != null ){
      itemListener.itemStateChanged( event );
    }
  }

  class SequenceButtonListener implements ActionListener
  {
    private GappedSequence seq;
    
    public SequenceButtonListener( GappedSequence seq )
    {
      this.seq = seq;
    }
    
    public GappedSequence getSequence()
    {
      return seq;
    }
    
    public void actionPerformed( ActionEvent event )
    {
      JAlignmentButtonPanel.this.processItemEvent
	( new ItemEvent( JAlignmentButtonPanel.this, ItemEvent.ITEM_STATE_CHANGED, seq, ItemEvent.SELECTED ) );
    }
  }
} // JAlignmentButtonPanel



/*
 * ChangeLog
 * $Log: JAlignmentButtonPanel.java,v $
 * Revision 1.8  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.7  2001/03/12 16:49:29  lord
 * Moved some code to a new method
 *
 * Revision 1.6  2001/03/12 16:36:17  lord
 * Restricted synchronisation to alignment to when its necessary.
 *
 * Revision 1.5  2000/07/18 11:09:14  lord
 * Changes due to removal of biointerface
 *
 * Revision 1.4  2000/06/27 16:00:23  lord
 * Added getPointForSequence information which makes PopupMenu placement
 * a lot easier.
 * Now listens to the alignment and responds when a new sequence is added
 *
 * Revision 1.3  2000/06/05 14:23:14  lord
 * Fixed bug. The last sequence in the alignment was not getting shown.
 *
 * Revision 1.2  2000/05/30 16:22:55  lord
 * Changed some method names
 *
 * Revision 1.1  2000/05/24 15:39:05  lord
 * Initial checkin
 *
 */


