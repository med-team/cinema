/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.gui.viewer; // Package name inserted by JPack
import java.awt.Point;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.JLabel;
import javax.swing.JPopupMenu;
import uk.ac.man.bioinf.sequence.alignment.GappedSequence;


/**
 * ButtonViewerPopupMenu.java
 *
 * This class implements a pop up menu for the
 * JAlignmentButtonPanel. The top menu item gives the title. This
 * serves as both a "tooltip" and a place to put buttons. 
 *
 * Created: Tue Oct 17 13:48:53 2000
 *
 * @author Phillip Lord
 * @version $Id: ButtonViewerPopupMenu.java,v 1.2 2001/04/11 17:04:42 lord Exp $
 */

public class ButtonViewerPopupMenu extends JPopupMenu implements ItemListener
{
  private JLabel titleLabel;
  private GappedSequence currentSequence;
  
  public ButtonViewerPopupMenu( JAlignmentButtonPanel panel )
  {
    add( titleLabel = new JLabel() );
    addSeparator();
    
    panel.addItemListener( this );
  }
  
  public GappedSequence getSelectedSequence()
  {
    return currentSequence;
  }
  
  public void itemStateChanged( ItemEvent event )
  {
    JAlignmentButtonPanel panel = (JAlignmentButtonPanel)event.getSource();
    currentSequence = (GappedSequence)event.getItem();
    
    // (PENDING:- PL) This can be done a lot more simply
    titleLabel.setText( ((GappedSequence)event.getItem()).getIdentifier().getTitle() );
    
    // where is the button doing the invoking
    Point pos =  panel.getPointForSequence( currentSequence );
    // offset it to the edge of the button
    pos.x += panel.getWidth();
    
    show( panel, pos.x, pos.y );
  }
  
  
} // ButtonViewerPopupMenu



/*
 * ChangeLog
 * $Log: ButtonViewerPopupMenu.java,v $
 * Revision 1.2  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.1  2000/10/19 17:53:18  lord
 * Initial checkin. Most of the code has come from CinemaSequenceMenu
 *
 */
