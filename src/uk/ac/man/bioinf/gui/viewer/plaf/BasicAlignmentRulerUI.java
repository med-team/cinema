/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.gui.viewer.plaf; // Package name inserted by JPack
import javax.swing.plaf.basic.BasicSliderUI;
import javax.swing.JSlider;
import java.awt.Graphics;
import javax.swing.plaf.ComponentUI;
import javax.swing.JComponent;
import java.awt.Dimension;
import uk.ac.man.bioinf.gui.viewer.JAlignmentRuler;


/**
 * BasicAlignmentRulerUI.java
 *
 *
 * Created: Thu Mar 23 17:10:31 2000
 *
 * @author Phillip Lord
 * @version $Id: BasicAlignmentRulerUI.java,v 1.3 2001/04/11 17:04:42 lord Exp $
 */

public class BasicAlignmentRulerUI extends BasicSliderUI
{
  
  private JAlignmentRuler ruler;
  
  public BasicAlignmentRulerUI( JComponent comp )
  {
    super( (JSlider)comp );
    this.ruler = (JAlignmentRuler)comp;
  }
  
  
  private Dimension prefSize = new Dimension();
  public Dimension getPreferredSize( JComponent comp )
  {
    prefSize.setSize
      ( ruler.getPreferredWidthPerBase() * ruler.getSequenceAlignment().getLength(), 
	ruler.getPreferredHeight() );
    return prefSize;
  }
  
  
  protected void recalculateIfInsetsChanged()
  {
    // This method is overriden because we automatically recalculate
    // every time anyway so there is no point doing it twice. 
  }
  
  public void paint( Graphics g, JComponent c )
  {
    // force this every time. If we do not there are a lot of problems
    // with using this component as a renderer. 
    calculateGeometry();
    super.paint( g, c );
  }
  
  
  public void paintThumb( Graphics g )
  {
    // get rid of this as we dont want it for the ruler
  }

  public void paintTrack( Graphics g )
  {
    // get rid of this as we dont want if for the ruler
  }

  
  int oldWidth, oldHeight = 0;
  protected void calculateGeometry()
  {
    // the CellRendererPane resizes the comp to 0, 0 after it renders.
    // The comp then recalcs its geometries all wrong, for the next
    // rendering, and all the contents dissappear. This prevents all
    // that. 
    int height = slider.getHeight();
    int width = slider.getWidth();
    if( width == 0 || height == 0 )
      return;
    
    // only do the calculation when we actually need to.
    if( height == oldHeight && width == oldWidth ) 
      return;
    
    oldHeight = height;
    oldWidth = width;
    
    super.calculateGeometry();
  }
  
  // this is irrelevant so why calculate it?
  protected void calculateTrackBuffer()
  {
  }
  
  protected void calculateTrackRect()
  {
    // mostly straight from super class
    trackRect.x = contentRect.x;
    trackRect.y = contentRect.y;
    trackRect.width = contentRect.width - (trackBuffer * 2);
    // Ive set this to four or the ticks start right at the top which
    // is a little bit ugly..
    trackRect.height = 4;
  }
  
  protected void calculateThumbLocation()
  {
    thumbRect.x = thumbRect.y = thumbRect.height = thumbRect.width = 0;
  }

  
  public static ComponentUI createUI( JComponent comp )
  {
    return new BasicAlignmentRulerUI( comp );
  }
  
} // BasicAlignmentRulerUI



/*
 * ChangeLog
 * $Log: BasicAlignmentRulerUI.java,v $
 * Revision 1.3  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.2  2000/05/30 16:04:18  lord
 * Have rationalised and sorted all the import statements
 *
 * Revision 1.1  2000/04/18 17:45:24  lord
 * All files moved from uk.ac.man.bioinf.viewer package
 *
 * Revision 1.4  2000/04/06 15:46:25  lord
 * Changes to support a cursor, with proper cursor movement
 *
 * Revision 1.3  2000/03/31 16:25:24  lord
 * Changed the way in which geometry is calculated to ensure that this
 * class be used as a cell renderer. Subsequently Ive stopped using this
 * class as a cell renderer so possible this is now obsolete, but I think
 * it makes sense to leave this stuff in regardless in case I choose to
 * do this again.
 *
 * Revision 1.2  2000/03/29 14:58:03  lord
 * Added preferred size calculations, and changed the calculation of the
 * track rectangle so that it doesnt take up room I dont want it too.
 *
 * Revision 1.1  2000/03/27 19:00:36  lord
 * Initial checkin
 *
 */


