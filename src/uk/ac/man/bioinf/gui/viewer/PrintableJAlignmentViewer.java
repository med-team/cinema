/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.gui.viewer; // Package name inserted by JPack
import java.awt.print.PrinterException;
import java.awt.Graphics;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import uk.ac.man.bioinf.sequence.alignment.SequenceAlignment;
import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.Font;


/**
 * PrintableJAlignmentViewer.java
 *
 * A JAlignmentViewer which can be used to print
 *
 * Created: Fri Mar  9 17:21:44 2001
 *
 * @author Phillip Lord
 * @version $Id: PrintableJAlignmentViewer.java,v 1.2 2001/04/11 17:04:42 lord Exp $
 */

public class PrintableJAlignmentViewer extends JAlignmentViewer
  implements Printable
{

  public PrintableJAlignmentViewer( SequenceAlignment msa )
  {
    super( msa );   
  }
  
  // (PENDING:- PL) Want to implement these methods!
  public void setJAlignmentViewer()
  {
  }
  
  /**
   * Set the number of cells which should be drawn across each page. This
   * method is probably a lot more useful than cell width which is a
   * bit blank.
   *
   * @param cells the number of cells per page
   */
  public void setXCellsPerPage( int cells )
  {
  }
  
  /**
   * Set the number of cells which should be drawn down each page. If
   * the total size of the alignment is too big to fit, then multiple
   * pages will have to be used. <p>
   * 
   * If the method <code>getCellsSquare</code> returns true then this
   * property will be ignored in preference to XCellsPerPage.
   
   *
   * @param cells an <code>int</code> value
   */
  public void setYCellsPerPage( int cells )
  {
  }
  
  public void setCellsSquare( boolean square )
  {
  }
  
  public boolean getCellsSquare()
  {
    return false;
  }
  
  public int print( Graphics g, PageFormat pf, int pi )
    throws PrinterException
  {
    if( pi >= 1 ){
      return Printable.NO_SUCH_PAGE;
    }
    
    Graphics2D g2 = (Graphics2D)g;
    double height = pf.getImageableHeight();
    double width  = pf.getImageableWidth ();
    
    g2.translate( pf.getImageableX(), 
                  pf.getImageableY() );
    
    paint( g2 );
    return Printable.PAGE_EXISTS;
  }
} // PrintableJAlignmentViewer



/*
 * ChangeLog
 * $Log: PrintableJAlignmentViewer.java,v $
 * Revision 1.2  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.1  2001/03/12 16:37:07  lord
 * Initial checkin
 *
 */
