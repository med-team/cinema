/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Julian Selley (j.selley@man.ac.uk)
 * whilst at the University of Manchester as a Research Assistant. 
 *
 * The initial code base is copyright by the University of Manchester. 
 * Modifications to the initial code base are copyright of their 
 * respective authors, or their employers as appropriate. Authorship 
 * of the modifications may be determined from the ChangeLog placed 
 * at the end of this file
 */

package uk.ac.man.bioinf.gui.viewer; // Package name inserted by JPack

import java.awt.Color;
import java.awt.Graphics;
import uk.ac.man.bioinf.sequence.Element;
import uk.ac.man.bioinf.sequence.alignment.Gap;
import uk.ac.man.bioinf.sequence.geom.SequenceAlignmentPoint;


/**
 * BorderFastAlignmentViewerCellRenderer.java
 *
 * One of three classes designed to render cells in the
 * JAlignmentViewer. This one deals specifically with the border to
 * the cell.
 * <p>
 * By default, it will probably be drawn second (after the color), but
 * all of this is determined by the RendererManager.
 *
 * Created: Fri Aug 25 14:06:17 2000
 *
 * @author Julian Selley
 * @version $Id: BorderFastAlignmentViewerCellRenderer.java,v 1.4 2001/04/11 17:04:42 lord Exp $
 */

public class BorderFastAlignmentViewerCellRenderer 
  implements FastAlignmentViewerCellRenderer
{
  public BorderFastAlignmentViewerCellRenderer() {}
  
  public void renderAlignmentViewerCell
    (Graphics g, int x, int y, int width, int height,
     JAlignmentViewer viewer, Element element,
     SequenceAlignmentPoint location, Color color,
     boolean isSelected, boolean hasFocus, boolean isAtPoint) 
  {
    /* decide on the color of the border based on the supplied
     * booleans
     */
    if ( element == null || element == Gap.GAP )
      g.setColor(viewer.getBackground());
    else
      g.setColor(Color.black);
    if (isSelected) 
      g.setColor(Color.red);
    if (isAtPoint) 
      g.setColor(Color.green);
    
    /* (PENDING: JNS 25.8.00) currently this only draws a border of
       * width 1 (or whatever the graphics drawRect is set to do). It
       * would be nice to later on specify the width of the border.
       */
    // draw the border
    g.drawRect(x, y, width - 1, height - 1);
  }
} // BorderFastAlignmentViewerCellRenderer



/*
 * ChangeLog
 * $Log: BorderFastAlignmentViewerCellRenderer.java,v $
 * Revision 1.4  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.3  2000/10/03 17:13:20  lord
 * Now cope with Gap elements correctly
 *
 * Revision 1.2  2000/09/19 14:57:33  jns
 * o making the borders draw properly
 *
 * Revision 1.1  2000/09/18 17:54:54  jns
 * o created a new multiplexer cell renderer to take account of cell size
 * o initial code
 *
 */
