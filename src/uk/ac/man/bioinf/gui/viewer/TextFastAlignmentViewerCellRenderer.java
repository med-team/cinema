/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Julian Selley (j.selley@man.ac.uk)
 * whilst at the University of Manchester as a Research Assistant. 
 *
 * The initial code base is copyright by the University of Manchester. 
 * Modifications to the initial code base are copyright of their 
 * respective authors, or their employers as appropriate. Authorship 
 * of the modifications may be determined from the ChangeLog placed 
 * at the end of this file
 */

package uk.ac.man.bioinf.gui.viewer; // Package name inserted by JPack

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import uk.ac.man.bioinf.sequence.Element;
import uk.ac.man.bioinf.sequence.geom.SequenceAlignmentPoint;


/**
 * TextFastAlignmentViewerCellRenderer.java
 *
 * One of three classes designed to render cells in the
 * JAlignmentViewer. This one deals specifically with drawing the
 * character of the element in the cell.
 * <p>
 * By default, this will probably be drawn last, although this is
 * determined by the RendererManager.
 *
 * Created: Fri Aug 25 15:11:04 2000
 *
 * @author Julian Selley
 * @version $Id: TextFastAlignmentViewerCellRenderer.java,v 1.2 2001/04/11 17:04:42 lord Exp $
 */

public class TextFastAlignmentViewerCellRenderer 
  implements FastAlignmentViewerCellRenderer
{
  private FontMetrics mets;
  private char[] charArray = new char[1];
  private int xOff, yOff, charWidth;
  
  public TextFastAlignmentViewerCellRenderer() {}
  
  public void renderAlignmentViewerCell
    (Graphics g, int x, int y, int width, int height,
     JAlignmentViewer viewer, Element element,
     SequenceAlignmentPoint location, Color color,
     boolean isSelected, boolean hasFocus, boolean isAtPoint) 
  {
    // check the font metrics are the same as previously encountered
    if (g.getFontMetrics() != mets) {
      mets = g.getFontMetrics();
    }
    
    if (element != null) {
      // draw black text
      g.setColor(Color.black);

      // set the character width      
      charWidth = mets.charWidth(element.toChar());
      
      // calculate the offsets
      xOff = (width - charWidth) / 2;
      yOff = ((height + mets.getHeight()) / 2)  - ((mets.getAscent() - mets.getDescent()) / 2);
      
      // draw the character
      charArray[0] = element.toChar();
      g.drawChars(charArray, 0, 1, x + xOff, y + yOff);
    }
  }
} // TextFastAlignmentViewerCellRenderer



/*
 * ChangeLog
 * $Log: TextFastAlignmentViewerCellRenderer.java,v $
 * Revision 1.2  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.1  2000/09/18 17:54:54  jns
 * o created a new multiplexer cell renderer to take account of cell size
 * o initial code
 *
 */
