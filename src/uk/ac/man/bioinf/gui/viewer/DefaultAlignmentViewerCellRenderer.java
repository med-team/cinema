/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.gui.viewer; // Package name inserted by JPack
import javax.swing.JLabel;
import java.awt.Component;
import uk.ac.man.bioinf.sequence.Element;
import javax.swing.border.LineBorder;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import uk.ac.man.bioinf.sequence.geom.SequenceAlignmentPoint;
import java.awt.Color;


/**
 * DefaultAlignmentViewerCellRenderer.java
 *
 *
 * Created: Mon Mar 20 20:23:12 2000
 *
 * @author Phillip Lord
 * @version $Id: DefaultAlignmentViewerCellRenderer.java,v 1.2 2001/04/11 17:04:42 lord Exp $
 */

public class DefaultAlignmentViewerCellRenderer extends JLabel implements AlignmentViewerCellRenderer
{
  public DefaultAlignmentViewerCellRenderer()
  {
    
  }
  Border green = new LineBorder( Color.green );
  Border red   = new LineBorder( Color.red );
  Border black = LineBorder.createBlackLineBorder();
  Border empty = new EmptyBorder( 1, 1, 1, 1 );
  
  public Component getAlignmentViewerCellRendererComponent
    ( JAlignmentViewer viewer, 
      Element element, 
      SequenceAlignmentPoint location,
      Color bgColor,
      boolean isSelected,
      boolean cellHasFocus,
      boolean isAtPoint )

  {
    if( element !=  null ){
      super.setText( "" + element.toChar() );
      setBorder( black );
    }
    else{
      super.setText( "" );
      setBorder( empty );
    }
	
    if( isSelected ){
      setBorder( red );
    }	

    if( isAtPoint ){
      setBorder( green );
    }
   
    // set the background color
    if (bgColor != null) {
      setOpaque(true);
      setBackground(bgColor);
    } else {
      setOpaque(false);
    }
    
    return this;
  }
} // DefaultAlignmentViewerCellRenderer



/*
 * ChangeLog
 * $Log: DefaultAlignmentViewerCellRenderer.java,v $
 * Revision 1.2  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.1  2000/04/18 17:43:55  lord
 * All files moved from uk.ac.man.bioinf.viewer package
 *
 * Revision 1.8  2000/04/13 15:37:39  lord
 * Cosmetic changes
 *
 * Revision 1.7  2000/04/13 15:27:10  lord
 * Added support for selection colouration
 *
 * Revision 1.6  2000/04/12 13:41:28  jns
 * o added in color mapping code
 *
 * Revision 1.5  2000/04/06 15:46:25  lord
 * Changes to support a cursor, with proper cursor movement
 *
 * Revision 1.4  2000/03/31 16:21:51  lord
 * Cosmetic changes
 *
 * Revision 1.3  2000/03/29 15:50:47  lord
 * Updated to use new sequence.geom package
 *
 * Revision 1.2  2000/03/21 18:54:21  lord
 * Now has borders and can cope with null elements
 *
 * Revision 1.1  2000/03/21 14:42:08  lord
 * Initial checkin
 *
 */
