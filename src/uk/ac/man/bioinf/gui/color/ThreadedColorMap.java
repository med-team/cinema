/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.gui.color; // Package name inserted by JPack
import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import uk.ac.man.bioinf.apps.invoker.AnonInvoker;
import uk.ac.man.bioinf.apps.invoker.InvokerInternalQueue;
import uk.ac.man.bioinf.gui.viewer.JAlignmentViewer;
import uk.ac.man.bioinf.sequence.Element;
import uk.ac.man.bioinf.sequence.alignment.SequenceAlignment;
import uk.ac.man.bioinf.sequence.alignment.event.AlignmentEvent;
import uk.ac.man.bioinf.sequence.alignment.event.AlignmentListener;
import uk.ac.man.bioinf.sequence.event.SequenceEvent;
import uk.ac.man.bioinf.sequence.event.SequenceListener;
import uk.ac.man.bioinf.sequence.geom.SequenceAlignmentPoint;


/**
 * ThreadedColorMap.java
 *
 * This class implements the threaded functionality for ColorMap. You
 * can use this class to wrap up any other ThreadableColorMap and it
 * will take care of the out thread calculation. 
 *
 * This class it should be noted is immutable in the sense that the
 * SequenceAlignment it set in the constructor, and you can not change
 * this, so you will need to create a new object. 
 * 
 *
 * Created: Fri Dec  1 14:58:48 2000
 *
 * @author Phillip Lord
 * @version $Id: ThreadedColorMap.java,v 1.4 2001/04/11 17:04:42 lord Exp $
 */

public class ThreadedColorMap implements ColorMap, SequenceListener, AlignmentListener, PropertyChangeListener
{
  public static final String NAME = "Threaded Color Map";
  
  private InvokerInternalQueue queue;
  private SequenceAlignment alignment;
  private ThreadableColorMap map;
  private JAlignmentViewer viewer;
  
  public ThreadedColorMap( JAlignmentViewer viewer, SequenceAlignment alignment, ThreadableColorMap map )
  {
    this( viewer, alignment, map, new InvokerInternalQueue() );
  }
  
  public ThreadedColorMap( JAlignmentViewer viewer, SequenceAlignment alignment, 
                           ThreadableColorMap map, InvokerInternalQueue queue )
  {
    this.alignment = alignment;
    this.queue = queue;
    this.map = map;
    this.viewer = viewer;
    
    viewer.addPropertyChangeListener( this );
    
    alignment.addAlignmentListener( this );
    alignment.addSequenceListener( this );
  }
  
  public Color getColorAt( SequenceAlignment alignment, Element element,
			   SequenceAlignmentPoint point )
  {
    return map.getColorAt( alignment, element, point );
  }
  
  protected void validate()
  {
    
    // we want to do the calculation out of the event thread, and then
    // fire change events back in it...
    queue.makeEmpty();
    queue.enqueue
        ( new AnonInvoker(){
	    long start;

	    public Object slow()
	    {	      
	      ThreadedColorMap.this.map.calculate();
	      return null;
	    }
	    
	    public void doRun()
	    {	
	      ThreadedColorMap.this.viewer.repaint();
	    }
	  });
  }
  
  public void propertyChange( PropertyChangeEvent event )
  {
    if( event.getPropertyName().equals( "colorMap" )
	&& event.getSource() == viewer
	&& event.getOldValue() == this ){
      destroy();
    }
  }
  
  public void changeOccurred( SequenceEvent event )
  {
    validate();
  }
  
  public void changeOccurred( AlignmentEvent event )
  {
    validate();
  }
  
  public String getName()
  {
    return ThreadedColorMap.NAME;
  }
  
  public String getInternalThreadedColorMap()
  {
    return map.getName();
  }

  public void destroy()
  {
    queue.destroy();
    queue = null;
    alignment.removeSequenceListener( this );
    alignment.removeAlignmentListener( this );
  }
} // ThreadedColorMap



/*
 * ChangeLog
 * $Log: ThreadedColorMap.java,v $
 * Revision 1.4  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.3  2000/12/13 16:32:10  lord
 * Cosmetic and debug statement removal
 *
 * Revision 1.2  2000/12/05 15:54:37  lord
 * Import rationalisation
 *
 * Revision 1.1  2000/12/05 15:20:59  lord
 * Initial checkin
 *
 */
