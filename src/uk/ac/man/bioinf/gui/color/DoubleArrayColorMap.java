/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */


package uk.ac.man.bioinf.gui.color; // Package name inserted by Jde-Package
import java.awt.Color;
import uk.ac.man.bioinf.sequence.Element;
import uk.ac.man.bioinf.sequence.alignment.Gap;
import uk.ac.man.bioinf.sequence.alignment.SequenceAlignment;
import uk.ac.man.bioinf.sequence.geom.SequenceAlignmentPoint;


/**
 * DoubleArrayColorMap.java
 *
 * This class takes a series of doubles, a list of colours and a
 * sequence alignment and turns them into a colour map. 
 *
 * The list of doubles is a colour for every element in every
 * sequence. Numbering starts at 0, and DOES NOT include gaps. Hence
 * element 0,0 of the array is the first element of the first sequence
 * where ever it starts. 
 *
 * Created: Thu Jun 21 16:42:36 2001
 *
 * @author Phillip Lord
 * @version $Id: DoubleArrayColorMap.java,v 1.1 2002/03/08 14:53:36 lord Exp $
 */

public class DoubleArrayColorMap implements ColorMap
{
  private int[][] colourAtPosition;
  private double[][] array;
  private ColorList colors;
  
  public DoubleArrayColorMap( double[][] array, ColorList colors )                              
  {
    this.array = array;
    this.colors = colors;
    
  //  colourAtPosition = new int[ alignment.getNumberOfSequences() ][];
        
//      for( int i = 0; i < colourAtPostion.length;i++ ){
//        colorAtPosition[ i ] = new int [ alignment.getLength() ];
//        for( int j = 0; j < colorAtPosition[ i ].length; j++ ){
        
//        }
//      }
  }

  public Color getColorAt( SequenceAlignment sa, Element elem, SequenceAlignmentPoint point )
  {
    try{
      
      if( elem == Gap.gap ) return Color.white;
      if( elem == null ) return Color.white;
      
      int yPosition = point.getY() - 1;
      if( (point.getX() - sa.getInset( yPosition + 1 )) < 1 ){
        return Color.white;
      }
      
      int xPosition = sa.getSequenceAt( yPosition + 1 ).getUngappedPositionOf
        ( point.getX() - sa.getInset( yPosition + 1 ) ) - 1;
      
      if( xPosition < 0 ){
        return Color.white;
      }
      
      //System.out.println( "Requesting colour for " + yPosition + " " + xPosition );
      return colors.getColorAt( (int)(array[ yPosition ][ xPosition ] * (colors.length() - 1 ) ) );
    }
    
    catch( ArrayIndexOutOfBoundsException aoofbe ){
      System.out.println(aoofbe.getMessage());
      return Color.green;
    }
    
  }
  
  
  public String getName()
  {
    return "DoubleArrayColorMap";
  }
  
} // DoubleArrayColorMap



/*
 * ChangeLog
 * $Log: DoubleArrayColorMap.java,v $
 * Revision 1.1  2002/03/08 14:53:36  lord
 * Initial checkin
 *
 */
