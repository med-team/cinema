/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.gui.color; // Package name inserted by JPack
import java.awt.Color;


/**
 * ColorList.java
 *
 * This class is similar to ColorGenerator, except that instead of
 * cycling through the colours it presents them as a list
 *
 * Created: Thu Jun  8 17:37:23 2000
 *
 * @author Phillip Lord
 * @version $Id: ColorList.java,v 1.3 2002/03/08 14:52:36 lord Exp $
 */

public class ColorList 
{
  public Color[] colors;
  
  public ColorList()
  {
    this( ColorStore.allColor );
  }
  
  public ColorList( Color[] colors )
  {
    this.colors = colors;
  }
  
  public Color getColorAt( int index )
  {
    return colors[ index ];
  }
  
  public int length()
  {
    return colors.length;
  }
  
  private static ColorList allColList;
  public static ColorList getAllColorList()
  {
    if( allColList == null )
      new ColorList( ColorStore.allColor );
    return allColList;
  }
  
  private static ColorList lightColList;
  public static ColorList getLightColorList()
  {
    if( lightColList == null )
      lightColList = new ColorList( ColorStore.lightColor );
    return lightColList;
  }
  
  private static ColorList coldHotList;
  public static ColorList getColdToHotColorList()
  {
    if( coldHotList == null )
      coldHotList = new ColorList( ColorStore.coldToHot );
    return coldHotList;
  }
  
  private static ColorList hotColdList;
  public static ColorList getHotToColdColorList()
  {
    if( hotColdList == null )
      hotColdList = new ColorList( ColorStore.hotToCold );
    return hotColdList;
  }

  private static ColorList gray50;
  public static ColorList getGray50List()
  {
    if( gray50 == null ){
      gray50 = new ColorList( ColorStore.grayScale50 );
    }
    return gray50;
  }
  
} // ColorList



/*
 * ChangeLog
 * $Log: ColorList.java,v $
 * Revision 1.3  2002/03/08 14:52:36  lord
 * Added another list
 *
 * Revision 1.2  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.1  2000/06/13 11:13:34  lord
 * Initial checkin
 *
 */
