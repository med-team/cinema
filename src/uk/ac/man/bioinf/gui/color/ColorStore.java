/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.gui.color; // Package name inserted by JPack
import java.awt.Color;


/**
 * ColorStore.java
 *
 * This is a static place holder of various lists of colours. 
 *
 * Created: Thu Jun  8 17:39:09 2000
 *
 * @author Phillip Lord
 * @version $Id: ColorStore.java,v 1.4 2002/03/08 15:04:45 lord Exp $
 */

public class ColorStore 
{
  private ColorStore()
  {
    // no instances
  }
  
  static final Color[] allColor = 
  {
    Color.white     ,
    Color.lightGray ,
    Color.gray      ,
    Color.darkGray  ,
    Color.black     ,
    Color.red       ,
    Color.pink      ,
    Color.orange    ,
    Color.yellow    ,
    Color.green     ,
    Color.magenta   ,
    Color.cyan      ,
    Color.blue      
  };
  
  
  static final Color[] lightColor =
  {
    Color.white     ,
    Color.red       ,
    Color.pink      ,
    Color.orange    ,
    Color.yellow    ,
    Color.green     ,
    Color.magenta   ,
    Color.cyan      ,
    Color.blue      
  };
  
  static final Color[] coldToHot  =
  {
    Color.blue      ,
    Color.cyan      ,
    Color.green     ,
    Color.yellow    ,
    Color.orange    ,
    Color.pink      ,
    Color.red       
  };
  
  static final Color[] hotToCold   =
  {
    Color.red       ,
    Color.pink      ,
    Color.orange   ,
    Color.yellow    ,
    Color.green     ,
    Color.cyan      ,
    Color.blue    
  };

  static final Color[] grayScale5;
  static final Color[] grayScale10;
  static final Color[] grayScale50;
  
  private static Color[] getGrayScale( int size )
  {
    Color[] array = new Color[ size ];
    
    for( int i = 0; i < size; i++ ){
      float j =  1.0f * i / (size - 1);
      array[ i ] = new Color( j, j, j );
    }

    return array;
  }
  
  static
  {
    grayScale5 = getGrayScale( 5 );
    grayScale10 = getGrayScale( 10 );
    grayScale50 = getGrayScale( 50 );
  }
  
} // ColorStore



/*
 * ChangeLog
 * $Log: ColorStore.java,v $
 * Revision 1.4  2002/03/08 15:04:45  lord
 * Removed debug statements
 *
 * Revision 1.3  2002/03/08 14:52:51  lord
 * Added another list
 *
 * Revision 1.2  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.1  2000/06/13 11:13:34  lord
 * Initial checkin
 *
 */
