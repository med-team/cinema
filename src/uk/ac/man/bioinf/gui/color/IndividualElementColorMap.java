/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Julian Selley (j.selley@man.ac.uk)
 * whilst at the University of Manchester as a Research Assistant. 
 *
 * The initial code base is copyright by the University of Manchester. 
 * Modifications to the initial code base are copyright of their 
 * respective authors, or their employers as appropriate. Authorship 
 * of the modifications may be determined from the ChangeLog placed 
 * at the end of this file
 */

package uk.ac.man.bioinf.gui.color; // Package name inserted by JPack

import java.awt.Color;
import java.util.HashMap;
import uk.ac.man.bioinf.sequence.Element;
import uk.ac.man.bioinf.sequence.alignment.SequenceAlignment;
import uk.ac.man.bioinf.sequence.geom.SequenceAlignmentPoint;


/**
 * IndividualElementColorMap.java
 *
 * Allows a direct mapping between an element and a color; thereby
 * every element in a sequence type will have an associated color.
 * NB: This class is <b>NOT</b> synchronized.
 *
 * Created: Thu Apr  6 13:13:24 2000
 *
 * @author J Selley
 * @version $Id: IndividualElementColorMap.java,v 1.8 2001/07/06 11:52:51 lord Exp $
 */

/*
 * Please excuse the spelling of color - it is so as not to confuse
 * our counterparts on the other side of the atlantic, and to keep
 * with the spelling chosen for use in the Java API.
 */

public class IndividualElementColorMap implements ColorMap
{
  private String name;
  private HashMap mapping = new HashMap();  // the mapping of elements to colors

  public IndividualElementColorMap(String name, 
				   Element[] elements, 
				   Color[] colors) 
  {
    this.name = name;
    if (elements.length != colors.length) 
      throw new ColorMapException("The number of elements and colors supplied " + 
				  "are not the same");
    for (int i = 0; i < elements.length; i++) {
      mapping.put(elements[i], colors[i]);
    }
  }
  
  /**
   * Returns the color for an element at a specific position in the
   * alignment. [from ColorMap] NB: In this case, this simply
   * identifies the element and returns the color for that element. 
   *
   * @param sa the sequence alignment
   * @param element the element from the position in the SA
   * @param point the position in the SA
   * @return the color of the element
   */
  public Color getColorAt
    (SequenceAlignment sa, Element element, SequenceAlignmentPoint point) 
  {
    if( element == null ) return null;

    /*    
	  int trueElementPosition = point.getX() - sa.getInset(point.getY());
	  if ((trueElementPosition > 0) && 
	  (trueElementPosition < ((DefaultGappedSequence)
	  sa.getSequenceAt(point.getY())).getLength()) && 
	  (element != ((DefaultGappedSequence)sa.getSequenceAt(point.getY())).
	  getGappedElementAt(trueElementPosition)))
	  throw new ColorMapException("The supplied element and the element at " +
	  "the given point do not match");
    */
    return (Color)mapping.get(element);
  }
  
  /**
   * Returns the name of this color map.
   *
   * @return the color map name
   */
  public String getName() 
  {
    return this.name;
  }
} // IndividualElementColorMap



/*
 * ChangeLog
 * $Log: IndividualElementColorMap.java,v $
 * Revision 1.8  2001/07/06 11:52:51  lord
 * Changed error message.
 *
 * Revision 1.7  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.6  2000/12/05 15:54:37  lord
 * Import rationalisation
 *
 * Revision 1.5  2000/08/01 12:47:05  jns
 * o removed references to BioInterface and BioObject.
 *
 * Revision 1.4  2000/05/18 17:07:43  lord
 * Removed error checking as its probably not necessary.
 *
 * Revision 1.3  2000/05/08 16:29:08  lord
 * Checks for null element which saves unnecessary hash lookup
 *
 * Revision 1.2  2000/04/12 13:37:15  jns
 * o added in checking for insets and end of sequence to prevent
 * the throwing of the exception.
 *
 * Revision 1.1  2000/04/11 18:30:09  jns
 * o initial coding for color mapping.
 *
 */
