/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.gui.color; // Package name inserted by JPack

import java.awt.Color;
import uk.ac.man.bioinf.analysis.misc.PercentageIDCalculator;
import uk.ac.man.bioinf.sequence.Element;
import uk.ac.man.bioinf.sequence.alignment.Gap;
import uk.ac.man.bioinf.sequence.alignment.SequenceAlignment;
import uk.ac.man.bioinf.sequence.geom.SequenceAlignmentPoint;


/**
 * PercentIDVarianceColorMap.java
 *
 * Calculates a colour scheme 
 *
 * Created: Fri Jun  9 16:03:46 2000
 *
 * @author Phillip Lord
 * @version $Id: PercentIDVarianceColorMap.java,v 1.10 2001/04/11 17:04:42 lord Exp $
 */

public class PercentIDVarianceColorMap implements ThreadableColorMap
{
  public static final String NAME = "Percentage ID variance Color Map";
  private ColorList colors;
  private PercentageIDCalculator calculator;
  private int[] colorAtColumn;
  
  public PercentIDVarianceColorMap( PercentageIDCalculator calculator )
  {
    this( calculator, ColorList.getHotToColdColorList() );
  }
  
  public PercentIDVarianceColorMap( PercentageIDCalculator calculator, ColorList lowToHighColours )
  {
    this.calculator = calculator;
    this.colors = lowToHighColours;
      
    calculate();
  }

  public Color getColorAt(SequenceAlignment sa, Element elem, SequenceAlignmentPoint point)
  {
    if( elem == null || elem == Gap.GAP ) return null;
    try{
      return colors.getColorAt( colorAtColumn[ point.getX() - 1 ] );
    }
    catch( ArrayIndexOutOfBoundsException iob ){
      System.out.println( "Iob exception at col " + point.getX() );
    }
    return null;
  }
  
  public String getName()
  {
    return NAME;
  }
  
  public void calculate()
  {
    double[] variance = calculator.getVariance();
    
    int[] colorAtColumn = new int[ variance.length ];
    
    for( int i = 0; i < colorAtColumn.length; i++ ){
      colorAtColumn[ i ] = (int)(variance[ i ] * (colors.length() - 1 ) );
    }

    this.colorAtColumn = colorAtColumn;
  }
} // PercentIDVarianceColorMap



/*
 * ChangeLog
 * $Log: PercentIDVarianceColorMap.java,v $
 * Revision 1.10  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.9  2000/12/05 15:54:37  lord
 * Import rationalisation
 *
 * Revision 1.8  2000/12/05 15:20:41  lord
 * Changes to ensure that the class is in a consistent state even whilst
 * the calculate method is being called.
 *
 * Revision 1.7  2000/09/25 16:36:44  lord
 * Inserted null element check
 *
 * Revision 1.6  2000/08/01 12:47:05  jns
 * o removed references to BioInterface and BioObject.
 *
 * Revision 1.5  2000/07/26 13:36:59  lord
 * Several changes. Mostly though pass in the PercentageIDCalculator in
 * the command line
 *
 * Revision 1.4  2000/07/18 11:08:25  lord
 * Import rationalisation
 *
 * Revision 1.3  2000/06/27 15:59:15  lord
 * Now uses PercentageIDCalculator class so that I can rationalise the
 * code in here with the PercentageIDConsensus class
 *
 * Revision 1.2  2000/06/19 14:31:23  lord
 * Have changed implementation to use integers instead
 *
 * Revision 1.1  2000/06/13 11:13:34  lord
 * Initial checkin
 *
 */






