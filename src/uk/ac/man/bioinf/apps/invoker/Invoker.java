/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Middlesex working as a post-doctoral
 * research fellow. 
 *
 * The initial code base is copyright by Middlesex Univeristy or the
 * Birth Defects Foundation. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.apps.invoker; // Package name inserted by JPack
import uk.ac.man.bioinf.util.ExceptionHandler;


/**
 * Invoker.java
 *
 * This class another attempt at making the sytem event queue easier
 * in Swing. This class is meant to be used an a base implementation of 
 * runnable with a few alternative constructors, and utility methods. Its
 * meant to be extended probably as an anonymous class to actually provide
 * any degree of functionality.
 *
 * This class was originally called the "SlowInvoker". The reasons for
 * the "Slow" are entirely historical, and relatively inappropriate.
 *
 * Created: Mon Dec 07 13:17:00 1998
 *
 * @author Phillip Lord
 * @version $Id: Invoker.java,v 1.3 2001/04/11 17:04:42 lord Exp $
 */

public abstract class Invoker implements Runnable 
{
  
  private Object source;
  private Object param;
  private ExceptionHandler handle;
  
  /**
   * Pointless constructor provided for the hell of it. Might as well
   * extend Runnable directly if you use this. 
   */
  public Invoker()
  {
    this( null, null );
  }
  
  /** 
   * Less Pointless constructor.
   * Takes a parameter which is meant to be the source on the 
   * anonymous class although you could clearly use it for 
   * whatever you choose
   * @param source the source or whatever you choose
   */
  public Invoker( Object source )
  {
    this( source, null );
  }
  
  /**
   * Even more less pointless contstructor
   * Takes a param to be used later as a parameter 
   * for the method which  will be called on what is proably
   * going to be the source. If you want to pass more than
   * one parameter then do it thru here as an array. Type checking
   * checking hell!
   * @param source the source or whatever you chose
   * @param param the param or whatever you choose
   */
  public Invoker( Object source, Object param )
  {
    this.source = source;
    this.param = param;
  }
  
  public void run()
  {
    try{
      doRun();
    }
    catch( Throwable t ){
      //If no exception handler has been installed then as default
      //print the stack and rethrow the exception
      if( handle == null ){
	t.printStackTrace();
      }
      //else handle the exception with the handler!
      handle.handleException( t );
    }
  }
  
      
  public abstract void doRun();
  
  /**
   * Get the source
   * @return the source object
   */
  public Object getSource()
  {
    return source;
  }
  
  /**
   * Get the parameter
   * @return the parameter object
   */
  public Object getParameter()
  {
    return param;
  }
  
  /**
   * Get the parameter specified by the given 
   * index. Note this requires a cast to 
   * an array, so if this doesnt work you'll get 
   * a runtime exception
   * @param index the index of the parameter to be returned
   */
  public Object getParameter( int index )
  {
    return ((Object[])param)[ index ];
  }
  
  public void setExceptionHandler( ExceptionHandler handle )
  {
    this.handle = handle;
  }
} // Invoker



/*
 * ChangeLog
 * $Log: Invoker.java,v $
 * Revision 1.3  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.2  2001/01/31 17:52:54  lord
 * Exception handler moved to utils package
 *
 * Revision 1.1  2000/05/08 16:22:10  lord
 * Initial checkin
 *
 * Revision 1.2  1999-05-14 15:10:48+01  phillip2
 * Updated exception handling of Invoker. This necessitated
 * a changed of method name from run to doRun. Previously an exception
 * thrown in the run method when invoked by SwingUtilities.invokeAndWait() was
 * getting silently swallowed. Now it either dumps stack or is passed to a Exception
 * Handler.
 *
 * Revision 1.1  1999-02-02 16:04:03+00  phillip2
 * Initial revision
 *
 */
