/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Middlesex working as a post-doctoral
 * research fellow. 
 *
 * The initial code base is copyright by Middlesex Univeristy or the
 * Birth Defects Foundation. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.apps.invoker; // Package name inserted by JPack


/**
 * AnonInvoker.java
 *
 * This class is used with the InvokerInternalQueue. It is meant to be
 * used as a Anonymous class hence the name. The idea is that you over
 * ride the slow method with the program logic which takes a long
 * time, and is done in the Queue's thread. The doRun() method on the
 * other hand is run in the Systems main EventThread. 
 * 
 * Parameters can be passed between the slow and fast method by the
 * "getSlowReturn" method. It is also possible to pass in a param
 * Object when OuterClass references are not appropriate. 
 *
 * <P> 
 * Created: Mon Dec 07 13:43:59 1998
 * <P> 
 * Compliant: 1.1
 * @author Phillip Lord
 * @version $Id: AnonInvoker.java,v 1.3 2001/04/11 17:04:42 lord Exp $
 */

public abstract class AnonInvoker extends Invoker 
{
  private Object slowReturn;
  
  public AnonInvoker() 
  {
    super( null );
  }
  
  public AnonInvoker( Object source )
  {
    super( source, null );
  }
  
  public AnonInvoker( Object source, Object param )
  {
    super( source, param );
  }
  
  public abstract Object slow();
  
  public void slowImpl()
  { 
    slowReturn = slow();
  }
  
  public Object getSlowReturn()
  {
    return slowReturn;
  }
} // AnonInvoker






/*
 * ChangeLog
 * $Log: AnonInvoker.java,v $
 * Revision 1.3  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.2  2000/05/15 16:23:35  lord
 * Moved pointless import
 *
 * Revision 1.1  2000/05/08 16:22:10  lord
 * Initial checkin
 *
 * Revision 1.1  1999-02-02 16:04:10+00  phillip2
 * Initial revision
 *
 */
