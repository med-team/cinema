/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.apps.xml; // Package name inserted by JPack
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import uk.ac.man.bioinf.module.ModuleIdentifier;


/**
 * ConfigNode.java
 *
 *
 * Created: Tue May  9 21:45:13 2000
 *
 * @author Phillip Lord
 * @version $Id: ConfigNode.java,v 1.4 2001/04/11 17:04:42 lord Exp $
 */

public class ConfigNode 
{
  private List nodes = new LinkedList();
  private ConfigNode[] nodeCache;
  
  private boolean isModule;
  private String stringData;
  private ModuleIdentifier moduleData;
  
  public void addChildNode( ConfigNode node )
  {
    nodes.add( node );
  }
  
  public ConfigNode[] getChildNodes()
  {
    if( nodeCache == null || nodes.size() != nodeCache.length ){
      nodeCache = new ConfigNode[ nodes.size() ];
      Iterator iter = nodes.listIterator();
      for( int i = 0; i < nodeCache.length; i++ ){
	nodeCache[ i ] = (ConfigNode)iter.next();
      }
    }
    return nodeCache;
  }
  
  /**
   * Set the data for this config node. 
   * This data is of the form ("MOD:"|"VAL:")data where data is the
   * data which will be returned, and the first bit shows whether this
   * is a module or not.
   * (PENDING:- PL) This is really crap, and I think that there is no
   * necessity for all this daft string matching. Need to remove it. 
   * @param data
   */
  public void setData( String data )
  {
    this.stringData = data;
    isModule = false;
  }
  
  public void setData( ModuleIdentifier moduleIdentifer )
  {
    this.moduleData = moduleIdentifer;
    isModule = true;
  }
  
  public boolean isModuleIdentifier()
  {
    return isModule;
  }
  
  public String getStringData()
  {
    return stringData;
  }
  
  public ModuleIdentifier getModuleData()
  {
    return moduleData;
  }
} // ConfigNode



/*
 * ChangeLog
 * $Log: ConfigNode.java,v $
 * Revision 1.4  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.3  2000/05/30 16:19:17  lord
 * Made this class a little bit more type safe.
 * Sorted imports
 *
 * Revision 1.2  2000/05/18 17:08:58  lord
 * Major replumping, and a few new methods
 *
 * Revision 1.1  2000/05/15 16:23:53  lord
 * Initial checkin
 *
 */




