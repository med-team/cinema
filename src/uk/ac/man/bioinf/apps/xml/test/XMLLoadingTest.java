/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.apps.xml.test; // Package name inserted by JPack
import uk.ac.man.bioinf.module.NullModuleFactory;
import java.util.Properties;
import uk.ac.man.bioinf.module.ModuleIdentifier;
import uk.ac.man.bioinf.apps.xml.ConfigNode;
import uk.ac.man.bioinf.debug.Debug;
import uk.ac.man.bioinf.debug.NullDebug;


/**
 * XMLLoadingTest.java
 *
 *
 * Created: Tue May 16 12:35:51 2000
 *
 * @author Phillip Lord
 * @version $Id: XMLLoadingTest.java,v 1.4 2001/04/11 17:04:42 lord Exp $
 */

public class XMLLoadingTest extends NullModuleFactory
{

  public void addConfig( ModuleIdentifier ident, Object config )
  {
    //pull open the config information
    Object[] confAr = (Object[])config;
    Properties props =  (Properties)confAr[ 0 ];
    ConfigNode node = (ConfigNode)confAr[ 1 ];
    
    System.out.println( "Properties config is :-" );
    props.list( System.out );
    
    System.out.println( "Config nodes are:- " );
    System.out.println( node );
    
    printNode( node );
  }
  
  public static void printNode( ConfigNode node )
  {
    printNode( node, 0 );
  }
  
  public static void printNode( ConfigNode node, int step )
  {
    System.out.print( step + "; " + node + " " );
    if( node.isModuleIdentifier() ){
      System.out.println( "Mod data: " +  node.getModuleData() );
    }
    else{
      System.out.println( "Strng data: " + node.getStringData() );
    }

    ConfigNode[] child = node.getChildNodes();
    for( int i = 0; i < child.length; i++ ){
      printNode( child[ i ], step + 1 );
    }
  }
    
  public static void main( String[] args )
  {
    // the class is now really knackered
    Debug.setInstance( new NullDebug() );
    //XMLBootModule mod = new XMLBootModule();
    //mod.load();
  } //end main method 
  
  
} // XMLLoadingTest



/*
 * ChangeLog
 * $Log: XMLLoadingTest.java,v $
 * Revision 1.4  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.3  2000/06/05 14:20:57  lord
 * Boring changes
 *
 * Revision 1.2  2000/05/30 16:22:33  lord
 * Boring changes
 *
 * Revision 1.1  2000/05/18 17:15:18  lord
 * Initial checkin
 *
 */
