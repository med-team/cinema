/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.apps.xml.test; // Package name inserted by JPack
import uk.ac.man.bioinf.apps.xml.XMLBootIdentifier;
import uk.ac.man.bioinf.module.Module;
import uk.ac.man.bioinf.module.ModuleFactoryInstance;

/**
 * XMLStartTest.java
 *
 *
 * Created: Thu May 25 12:05:07 2000
 *
 * @author Phillip Lord
 * @version $Id: XMLStartTest.java,v 1.2 2001/04/11 17:04:42 lord Exp $
 */

public class XMLStartTest 
{
  public static void main( String[] args ) throws Throwable
  {
    ModuleFactoryInstance inst =  new uk.ac.man.bioinf.module.DefaultModuleFactoryInstance();
    inst.addIdentifier( XMLBootIdentifier.XML_BOOT );
    inst.load( XMLBootIdentifier.XML_BOOT );
  } //end main method 


} // XMLStartTest



/*
 * ChangeLog
 * $Log: XMLStartTest.java,v $
 * Revision 1.2  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.1  2000/07/18 11:07:57  lord
 * Initial checkin
 *
 */
