/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.apps.xml; // Package name inserted by JPack
import uk.ac.man.bioinf.module.Module;
import java.util.Properties;


/**
 * XMLModule.java
 *
 * This class provides configuration information specific to the
 * XMLModules. Currently there are two forms of configuration
 * information. The first is a properties file, whilst the second is
 * tree structure. These are independent and either or neither of
 * these can be present.
 * 
 * Created: Mon May 15 22:06:23 2000
 *
 * @author Phillip Lord
 * @version $Id: XMLModule.java,v 1.3 2001/04/11 17:04:42 lord Exp $
 */

public abstract class XMLModule extends Module
{
  private boolean haveGotConfig = false;
  private Properties props;
  private ConfigNode rootNode;
  
  private void breakOpenConfig()
  {
    Object[] configObj = (Object[])getContext().getConfig();
    
    props = (configObj[ 0 ] != null ) ? (Properties)configObj[ 0 ] : null;
    rootNode = (configObj[ 1 ] != null ) ? (ConfigNode)configObj[ 1 ] : null;
    haveGotConfig = true;
  }
  
  public Properties getConfigProperties()
  {
    if( !haveGotConfig ) breakOpenConfig();
    
    return props;
  }
  
  public ConfigNode getConfigTree()
  {
    if( !haveGotConfig ) breakOpenConfig();
    
    return rootNode;
  }
  
} // XMLModule



/*
 * ChangeLog
 * $Log: XMLModule.java,v $
 * Revision 1.3  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.2  2000/06/05 14:20:44  lord
 * Now abstract, and no longer implements getVersion method
 *
 * Revision 1.1  2000/05/18 17:10:48  lord
 * Initial checkin
 *
 */
