/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.apps.xml; // Package name inserted by JPack

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.ErrorHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import uk.ac.man.bioinf.debug.Debug;

/**
 * XMLBootParserListener.java
 *
 *
 * Created: Tue May  9 22:10:53 2000
 *
 * @author Phillip Lord
 * @version $Id: XMLBootParserListener.java,v 1.13 2001/05/08 17:46:50 lord Exp $
 */

public class XMLBootParserListener implements ErrorHandler, ContentHandler
{
  private static final boolean localDebug = false;
  private XMLBootModule mod;
  
  public XMLBootParserListener( XMLBootModule mod )
  {
    this.mod = mod;
  }
  
  public void setDocumentLocator( Locator locator )
  {
    if( Debug.debug || localDebug  )
      Debug.message( this, "XML Parse: Have recieved document locator" );
  }
  
  public void startDocument()
  {
    if( Debug.debug || localDebug  )
      Debug.message( this, "XML Parse: Starting document" );
  }
  
  public void endDocument()
  {
    if( Debug.debug || localDebug  )
      Debug.message( this, "XML Parse: Ending document" );
  }

  public void startPrefixMapping( String prefix, String uri )
  {
  }
  
  public void endPrefixMapping( String prefix )
  {
  }
  
  public void startElement( String nameSpaceUri, String localName, String rawName, Attributes attrib )
  {
    if( Debug.debug || localDebug  )
      Debug.message( this, "XML Parse: Start element. NSURI: " + nameSpaceUri + " Local: " + localName 
		     + "  Raw: " + rawName );
    String elem = rawName;
    
    if( elem.equals( "properties" ) ){
      mod.propertiesStart();
    }
    
    if( elem.equals( "identifier" ) ){
      mod.identifierStart( attrib );
    }
    
    if( elem.equals( "node" ) ){
      mod.nodeStart();
    }
    
    if( elem.equals( "tree" ) ){
      mod.treeStart();
    }
    
    if( elem.equals( "config" ) ){
      mod.configStart( attrib );
    }

    if( elem.equals( "read" ) ){
      mod.readStart( attrib );
    }
  }
  
  public void endElement( String nameSpaceUri, String localName, String rawName ) throws SAXException
  {
    if( Debug.debug || localDebug  )
      Debug.message( this, "XML Parse: End element " + nameSpaceUri + " " + localName 
		     + " " + rawName );
    
    String elem = localName;
    
    if( elem.equals( "class" ) ){
      mod.classEnd();
    }
    
    if( elem.equals( "name" ) ){
      mod.nameEnd();
    }
    
    if( elem.equals( "enumeration" ) ){
      mod.enumerationEnd();
    }
    
    if( elem.equals( "generic" ) ){
      mod.genericEnd();
    }
    
    if( elem.equals( "start" ) ){
      mod.startEnd();
    }
   
    if( elem.equals( "load" ) ){
      mod.loadEnd();
    }
    
    if( elem.equals( "value" ) ){
      mod.valueEnd();
    }
    
    if( elem.equals( "paramname" ) ){
      mod.paramnameEnd();
    }
    
    if( elem.equals( "param" ) ){
      mod.paramEnd();
    }
    
    if( elem.equals( "properties" ) ){
      mod.propertiesEnd();
    }

    if( elem.equals( "module" ) ){
      mod.moduleEnd();
    }
    
    if( elem.equals( "read" ) ){
      mod.readEnd();
    }
    
    if( elem.equals( "node" ) ){
      mod.nodeEnd();
    }
  
    if( elem.equals( "tree" ) ){
      mod.treeEnd();
    }

    if( elem.equals( "provide" ) ){
      mod.provideEnd();
    }
  }

  public void characters( char[] ch, int start, int length )
  {
    if( Debug.debug || localDebug  ){
      Debug.message( this, "XML Parse: Characters " + new String( ch, start, length ) );
    }
    mod.characters( new String( ch, start, length ) );
  }

  public void ignorableWhitespace( char[] cha, int start, int length )
  {
  }
  
  public void processingInstruction( String target, String data )
  {
  }
  
  public void skippedEntity( String name )
  {
  }
  
  // implementation of org.xml.sax.ErrorHandler interface
  public void error(SAXParseException param1) throws SAXException 
  {
    if( Debug.debug || localDebug  )                      
      Debug.both( this, "XML Parse Error", param1 );
    Debug.message( this, "SaxParseException at " + param1.getLineNumber() + ":" + param1.getColumnNumber() );
    throw param1;
  }
  
  public void fatalError(SAXParseException param1) throws SAXException {
    if( Debug.debug || localDebug  )                      
      Debug.both( this, "XML Parse Error", param1 );
    Debug.message( this, "SaxParseException at " + param1.getLineNumber() + ":" + param1.getColumnNumber() );
    throw param1;
  }
  
  public void warning(SAXParseException param1) throws SAXException {
    if( Debug.debug || localDebug  )                      
      Debug.both( this, "XML Parse Error", param1 );
    Debug.message( this, "SaxParseException at " + param1.getLineNumber() + ":" + param1.getColumnNumber() );
    throw param1;
  }
} // XMLBootParserListener



/*
 * ChangeLog
 * $Log: XMLBootParserListener.java,v $
 * Revision 1.13  2001/05/08 17:46:50  lord
 * Have local debug option as I seem to need this so much.
 *
 * Revision 1.12  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.11  2001/01/31 17:54:06  lord
 * Sorted imports
 *
 * Revision 1.10  2001/01/26 17:12:05  lord
 * config element now takes attributes
 *
 * Revision 1.9  2001/01/15 18:43:48  lord
 * Improved exception handling.
 * Support for read required attribute
 *
 * Revision 1.8  2000/09/15 17:24:21  lord
 * More useful debug output
 *
 * Revision 1.7  2000/08/03 16:38:24  lord
 * Modifications to enable Cinema to run from jar file
 *
 * Revision 1.6  2000/07/26 13:36:24  lord
 * Bug fix. Properties start now called correctly
 *
 * Revision 1.5  2000/06/05 14:20:21  lord
 * Need to catch tree start now, to allow me to neaten up the
 * XMLBootModule code
 *
 * Revision 1.4  2000/05/30 16:55:22  lord
 * Horrible mess up in last checkin
 *
 * Revision 1.3  2000/05/30 16:22:20  lord
 * Support for more elements
 *
 * Revision 1.2  2000/05/18 17:09:38  lord
 * Support for lots more tags
 *
 * Revision 1.1  2000/05/15 16:23:53  lord
 * Initial checkin
 *
 */
