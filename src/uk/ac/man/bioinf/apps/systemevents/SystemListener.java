/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Middlesex working as a post-doctoral
 * research fellow. 
 *
 * The initial code base is copyright by Middlesex Univeristy or the
 * Birth Defects Foundation. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.apps.systemevents; // Package name inserted by JPack

import java.util.EventListener;

/**
 * SystemListener.java
 *
 * Listeners to system events. At the moment this consists of ShutDown events
 *
 * <P> 
 * Created: Thu Jan 14 17:48:54 1999
 * <P> 
 * Compliant: 
 * @author Phillip Lord
 * @version
 */

public interface SystemListener extends EventListener 
{
  /**
   * This is an event listener for system events. All listeners
   * should be signalled before the SystemEvent occurs. They should
   * not return from this method until they are ready for that event
   * to occur. An exception is thrown if the listener wishes to 
   * veto this system event. g
   * @param e the system event occuring
   * @exception SystemVetoException if the listener does not want the event to happen
   */
  public void systemEventOccured( SystemEvent e ) throws SystemVetoException;

  /**
   * Some events (particularly shut downs) have to happen in a 
   * very specific order. This method provides a priority to 
   * allow this to happen. Those with the lowest priority are 
   * informed first. Any valid int value is acceptable.
   * This interface does not provide the ability to set different priorities
   * for different events. The same effect can be achieved by delegating the 
   * to a small class. This way a single object can register more than one
   * listener object, and thus have different priorities for different events
   * A listener should not change its priority. There is not guarentee when 
   * this value will be used.
   */
  public int systemListenerPriority();
  
  /*
   * A rough guide to levels. Any value can be used
   */
  /**
   * This value should be used when the System shutdown can
   * not be aborted unless there is some error in the shutdown 
   * procedure
   */
  public int VETO_DUE_TO_ERROR = 0;
  /** 
   * This value should be used when the user has full control 
   * over aborting the shutdown.
   */
  public int VETO_DUE_TO_USER_REQUEST = -20;
  /**
   * This value should be used when the system is going to 
   * shutdown regardless of errors
   */
  public int NO_VETO = 20;
  
  
} // SystemListener


/*
 * ChangeLog
 * $Log: SystemListener.java,v $
 * Revision 1.2  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.1  2000/05/08 16:22:10  lord
 * Initial checkin
 *
 * Revision 1.5  1999/10/14 14:01:02  phillip2
 * Changed packages and a few other bits and bobs
 *
 * Revision 1.5  1999-10-12 14:37:12+01  phillip2
 * Outdated (renamed to 'D:\Home\rcs\D\Home\java\sources\photofit\systemevent\SystemListener.java,v').
 *
 * Revision 1.4  1999-09-30 15:01:41+01  phillip2
 * Updated package statement
 *
 * Revision 1.4  1999-09-30 14:58:28+01  phillip2
 * Outdated (renamed to 'D:\Home\rcs\D\Home\java\sources\photofit\systemevent\SystemListener.java,v').
 *
 * Revision 1.3  1999-04-25 16:48:39+01  phillip2
 * Added a priority to this interface
 *
 * Revision 1.2  1999-04-15 17:23:00+01  phillip2
 * SystemEvent Occured can now throw an exception if the listener isnt
 * happy. The new systemListenerPriority allows control over the order in which
 * listeners are informed of events
 *
 * Revision 1.1  1999-02-02 16:04:07+00  phillip2
 * Initial revision
 *
 */



