/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Middlesex working as a post-doctoral
 * research fellow. 
 *
 * The initial code base is copyright by Middlesex Univeristy or the
 * Birth Defects Foundation. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.apps.systemevents; // Package name inserted by JPack


/**
 * SystemEventProducer.java
 *
 * Interface defining the SystemEventProducer
 *
 * Created: Fri Oct 01 14:55:42 1999
 *
 * @author Phillip Lord
 * @version $Id: SystemEventProducer.java,v 1.2 2001/04/11 17:04:42 lord Exp $
 */

public interface SystemEventProducer 
{
  public void addSystemEventListener( SystemListener listener );
  
  public void removeSystemEventListener( SystemListener listener );
  
} // SystemEventProducer



/*
 * ChangeLog
 * $Log: SystemEventProducer.java,v $
 * Revision 1.2  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.1  2000/05/08 16:22:10  lord
 * Initial checkin
 *
 * Revision 1.1  1999/10/21 11:31:38  phillip2
 * Initial revision
 *
 */
