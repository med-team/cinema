/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Middlesex working as a post-doctoral
 * research fellow. 
 *
 * The initial code base is copyright by Middlesex Univeristy or the
 * Birth Defects Foundation. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.apps.systemevents; // Package name inserted by JPack

import java.util.EventObject;

/**
 * SystemEvent.java
 *
 *
 * <P> 
 * Created: Thu Jan 14 17:51:04 1999
 * <P> 
 * Compliant: 
 * @author Phillip Lord
 * @version
 */

public class SystemEvent extends EventObject 
{
  private SystemEventOption option;
  
  public SystemEvent( Object source, SystemEventOption option ) 
  {
    super( source );
    this.option = option;
  }
  
  public SystemEventOption getOption()
  {
    return option;
  }
  
} // SystemEvent



/*
 * ChangeLog
 * $Log: SystemEvent.java,v $
 * Revision 1.2  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.1  2000/05/08 16:22:10  lord
 * Initial checkin
 *
 * Revision 1.3  1999/10/14 14:01:01  phillip2
 * Changed packages and a few other bits and bobs
 *
 * Revision 1.3  1999-10-12 14:38:01+01  phillip2
 * Outdated (renamed to 'D:\Home\rcs\D\Home\java\sources\photofit\systemevent\SystemEvent.java,v').
 *
 * Revision 1.2  1999-09-30 15:01:42+01  phillip2
 * Updated package statement
 *
 * Revision 1.2  1999-09-30 14:57:37+01  phillip2
 * Outdated (renamed to 'D:\Home\rcs\D\Home\java\sources\photofit\systemevent\SystemEvent.java,v').
 *
 * Revision 1.1  1999-02-02 16:04:04+00  phillip2
 * Initial revision
 *
 */
