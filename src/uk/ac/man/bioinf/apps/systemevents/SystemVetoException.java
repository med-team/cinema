/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software is copyright by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 */

package uk.ac.man.bioinf.apps.systemevents; // Package name inserted by JPack


/**
 * SystemVetoException.java
 *
 *
 * Created: Thu Apr 15 17:23:20 1999
 *
 * @author Phillip Lord
 * @version $Id: SystemVetoException.java,v 1.2 2001/04/11 17:04:42 lord Exp $
 */

public class SystemVetoException extends Exception
{

  private SystemEvent event;
  public SystemVetoException( String message, SystemEvent event )
  {
    super( message );
    this.event = event;
  }

  public SystemEvent getSystemEvent()
  {
    return event;
  }
} // SystemVetoException



/*
 * ChangeLog
 * $Log: SystemVetoException.java,v $
 * Revision 1.2  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.1  2000/05/08 16:22:10  lord
 * Initial checkin
 *
 * Revision 1.3  1999/10/14 14:01:02  phillip2
 * Changed packages and a few other bits and bobs
 *
 * Revision 1.3  1999-10-12 14:37:41+01  phillip2
 * Outdated (renamed to 'D:\Home\rcs\D\Home\java\sources\photofit\systemevent\SystemVetoException.java,v').
 *
 * Revision 1.2  1999-09-30 15:01:41+01  phillip2
 * Updated package statement
 *
 * Revision 1.2  1999-09-30 14:58:43+01  phillip2
 * Outdated (renamed to 'D:\Home\rcs\D\Home\java\sources\photofit\systemevent\SystemVetoException.java,v').
 *
 * Revision 1.1  1999-04-15 18:25:15+01  phillip2
 * Initial revision
 *
 */
