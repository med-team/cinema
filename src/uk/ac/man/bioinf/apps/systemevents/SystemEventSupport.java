/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Middlesex working as a post-doctoral
 * research fellow. 
 *
 * The initial code base is copyright by Middlesex Univeristy or the
 * Birth Defects Foundation. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */


package uk.ac.man.bioinf.apps.systemevents; // Package name inserted by JPack

import java.util.*;

/**
 * SystemEventSupport.java
 *
 * Provides a basic queue for holding all the listeners. Im using 
 * a priority queue here.
 *
 * Created: Thu Apr 15 17:44:57 1999
 *
 * @author Phillip Lord
 * @version $Id: SystemEventSupport.java,v 1.3 2001/04/11 17:04:42 lord Exp $
 */

public class SystemEventSupport implements SystemEventProducer
{
  /**
   * Internal storage
   */
  private ArrayList list = new ArrayList();
  /** 
   * The value of the last listener retrieved from the 
   * list.
   */
  private int lastCalledPriority = Integer.MIN_VALUE;

  public void addSystemEventListener( SystemListener listener )
  {
    list.add( new ListenerToComparable( listener ) );
  }
  
  public void removeSystemEventListener( SystemListener listener )
  {
    list.remove( new ListenerToComparable( listener ) );
  }
  
  public void fireSystemEvent( SystemEventOption option ) throws SystemVetoException
  {
    lastCalledPriority = Integer.MIN_VALUE;
    //Sort the array list that we have into priority order
    Collections.sort( list );
    Iterator iter = list.iterator();
    
    while( iter.hasNext() ){
      SystemListener listener = ((ListenerToComparable)iter.next()).getSystemListener();
      lastCalledPriority = listener.systemListenerPriority();
      listener.systemEventOccured( new SystemEvent( this, option ) );
    }
  }
  
  /**
   * The priority of the last listener which this class attempted to
   * inform of a system event. If the runSystemEvent throws an exception
   * this method will therefore be the priority of the listener throwing
   * that exception
   */
  public int getLastPriorityRun()
  {
    return lastCalledPriority;
  }
  
  private class ListenerToComparable implements Comparable
  {
    private SystemListener listener;
    
    public ListenerToComparable( SystemListener listener )
    {
      this.listener = listener;
    }
    
    public int compareTo( Object rhs )
    {
      //Start with a cast!
      ListenerToComparable rhsListener = (ListenerToComparable)rhs;
      return listener.systemListenerPriority() < rhsListener.systemListenerPriority() ? - 1:
	listener.systemListenerPriority() == rhsListener.systemListenerPriority() ? 0 : 1;
    }
    
    public int systemListenerPriority()
    {
      return listener.systemListenerPriority();
    }
    
    public SystemListener getSystemListener()
    {
      return listener;
    }
    
    public boolean equals( Object obj )
    {
      if( obj instanceof ListenerToComparable ){
	return this.listener.equals( ((ListenerToComparable)obj).listener );
      }
      return false;
    }
    
    public int hashCode()
    {
      return listener.hashCode();
    }
  }
} // SystemEventSupport



/*
 * ChangeLog
 * $Log: SystemEventSupport.java,v $
 * Revision 1.3  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.2  2000/12/05 14:56:44  lord
 * Fixed nasty bug. The inner class which wrapped the listener had not
 * had its equals method defined properly, thereby making it impossible
 * to remove a listener.
 *
 * Revision 1.1  2000/05/08 16:22:10  lord
 * Initial checkin
 *
 * Revision 1.5  1999/10/14 14:01:01  phillip2
 * Changed packages and a few other bits and bobs
 *
 * Revision 1.5  1999-10-12 14:38:12+01  phillip2
 * Outdated (renamed to 'D:\Home\rcs\D\Home\java\sources\photofit\systemevent\SystemEventSupport.java,v').
 *
 * Revision 1.4  1999-10-01 16:38:22+01  phillip2
 * now implements SystemEventProducer, new interface
 *
 * Revision 1.3  1999-09-30 15:01:42+01  phillip2
 * Updated package statement
 *
 * Revision 1.3  1999-09-30 14:58:20+01  phillip2
 * Outdated (renamed to 'D:\Home\rcs\D\Home\java\sources\photofit\systemevent\SystemEventSupport.java,v').
 *
 * Revision 1.2  1999-05-06 14:05:13+01  phillip2
 * Removed debug statements
 *
 * Revision 1.1  1999-04-20 19:18:02+01  phillip2
 * Initial revision
 *
 */
