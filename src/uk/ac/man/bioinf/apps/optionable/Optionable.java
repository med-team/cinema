/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Middlesex, and modified whilst at the
 * University of Manchester.
 *
 * The initial code base is copyright by the respective
 * employers. Modifications to the initial code base are copyright of
 * their respective authors, or their employers as appropriate.
 * Authorship of the modifications may be determined from the
 * ChangeLog placed at the end of this file
 */

package uk.ac.man.bioinf.apps.optionable; // Package name inserted by JPack

/**
 * Optionable.java
 * 
 * An object implementing this interface may be in several
 * different states, that is provide options. These options 
 * can then be stored or in other ways treated at some 
 * time point, and may also need to be set at some point. Essentially
 * this interface was written to provide an easy way of serialising 
 * user defined options.
 * 
 * The options should be provided in the form of a hashtable. 
 * Every Optionable object must provide a name by which it wants
 * to group its options, so that instances of an object do not
 * (necessarily) conflict with each others options
 * Created: Mon Feb 01 16:11:12 1999
 *
 * @author Phillip Lord
 * @version $Id: Optionable.java,v 1.6 2001/04/11 17:04:42 lord Exp $
 */

public interface Optionable 
{

  /**
   * This string is used as an identifier for the groups
   * of options. It will probably be used on a per-instance
   * basis, but could also be shared by all members of a class
   * or by an entire application. It should probably however 
   * remain constant over time for any given Optionable object
   * If more than one optionable object is registered with
   * the same option handler and have the same group name the
   * options retrieved from one object will over ride all of the
   * others.
   * @return the string by the group of options is known
   */
  public String getOptionGroupName();
  
  /**
   * This method provides the current state of object as a map
   * of objects
   * @return the options
   */
  public Object getOptions();
  
  /**
   * Sets the options for this object
   * @param map
   */
  public void setOptions( Object options );
  

  /**
   * Get the value of OptionHandler.
   * @return Value of OptionHandler.
   */
  public OptionHandler getOptionHandler();

  /**
   * Set the value of OptionHandler.
   * @param OptionHandler the value to assign to OptionHandler
   */
  public void setOptionHandler( OptionHandler OptionHandler );
  
} // Optionable.



/* 
 *  ChangeLog
 *  $Log: %
 */
