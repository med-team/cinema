/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Middlesex, and modified whilst at the
 * University of Manchester.
 *
 * The initial code base is copyright by the respective
 * employers. Modifications to the initial code base are copyright of
 * their respective authors, or their employers as appropriate.
 * Authorship of the modifications may be determined from the
 * ChangeLog placed at the end of this file
 */

package uk.ac.man.bioinf.apps.optionable; // Package name inserted by JPack


/**
 * OptionHandler.java
 *
 * Objects implementing this interface should record all of
 * Optionable object, storing and restoring their options 
 * as necessary
 *
 * Created: Mon Feb 01 18:10:52 1999
 *
 * @author Phillip Lord
 * @version $Id: OptionHandler.java,v 1.7 2001/04/11 17:04:42 lord Exp $
 */

public interface OptionHandler  
{
  /**
   * Add an optionable object. On adding the Optionhandler
   * should set itself as the OptionHandler for the optionable
   * object, and call requestOptionUpdate also. 
   * @param Optionable the value to assign to Optionable
   */
  public void addOptionable( Optionable optionable );
  
  /**
   * Remove the following optionable object from 
   * the list
   * @param optionable
   */
  public void removeOptionable( Optionable optionable );

  /**
   * Request that the optionable object set the the options
   * of the optionable object via its setOptions method. This could
   * be used for a variety of reasons, although the most obvious 
   * is a "revert to saved" option.
   * @param optionable the optionable object
   * @see Optionable#setOptions
   */
  public void requestOptionUpdate( Optionable optionable );
  

} // OptionHandler


/*
 * ChangeLog
 * $Log: OptionHandler.java,v $
 * Revision 1.7  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.6  2001/01/30 14:02:49  lord
 * Repackaged from photofit. Initial checkin.
 *
 * Revision 1.5  1999/10/14 14:00:40  phillip2
 * Changed packages and a few other bits and bobs
 *
 * Revision 1.5  1999-10-12 15:10:09+01  phillip2
 * Outdated (renamed to 'D:\Home\rcs\D\Home\java\sources\photofit\options\OptionHandler.java,v').
 *
 * Revision 1.4  1999-09-30 15:11:06+01  phillip2
 * Updated package statement
 *
 * Revision 1.4  1999-09-30 15:04:41+01  phillip2
 * Outdated (renamed to 'D:\Home\rcs\D\Home\java\sources\photofit\options\OptionHandler.java,v').
 *
 * Revision 1.3  1999-02-10 18:56:27+00  phillip2
 * Updated documentation of addOptionable
 *
 * Revision 1.2  1999-02-02 16:16:36+00  phillip2
 * Removed getOptionable( int index) method. Theres not really any call for it.
 *
 * Revision 1.1  1999-02-02 16:04:12+00  phillip2
 * Initial revision
 *
 */

   


