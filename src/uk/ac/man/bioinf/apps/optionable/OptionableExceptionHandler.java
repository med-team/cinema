/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.apps.optionable; // Package name inserted by Jde-Package

import java.io.IOException;

/**
 * OptionableExceptionHandler.java
 *
 *
 * Created: Mon May 14 14:04:22 2001
 *
 * @author Phillip Lord
 * @version $Id: OptionableExceptionHandler.java,v 1.1 2001/05/14 17:08:18 lord Exp $
 */

public interface OptionableExceptionHandler 
{
  /**
   * Handle an exception occurring during the loading of options. 
   * @param exp
   * @return true if the an attempt should be made to load the options
   * again. 
   */
  public boolean handleLoadException( Exception exp );
  
  /**
   * ClassNotFoundException can occur when reading data from the
   * stream for a data type that does not exist. It won't normally
   * occur. 
   * @param exp
   * @return a <code>boolean</code> value
   */
  public boolean handleLoadException( ClassNotFoundException exp );
  
  /**
   * Signalled if some unknown IOException occurs
   * @param iop
   * @return a <code>boolean</code> value
   */
  public boolean handleLoadException( IOException iop );

  /**
   * If the optionable load file can not be found this error is signalled. 
   * @param osnfe
   * @return a <code>boolean</code> value
   */
  public boolean handleLoadException( OptionableSourceNotFoundException osnfe );
  
  /**
   * If something nasty occurs whilst gather the optionable
   * information occurs this is error is signalled. 
   * @param exp
   * @return a <code>boolean</code> value
   */
  public boolean handleSaveException( Exception exp );
  
  /**
   * If the save encounters IO problems!
   * @param io
   * @return a <code>boolean</code> value
   */
  public boolean handleSaveException( IOException io );
} // OptionableExceptionHandler



/*
 * ChangeLog
 * $Log: OptionableExceptionHandler.java,v $
 * Revision 1.1  2001/05/14 17:08:18  lord
 * Added proper error handling to save and restore.
 *
 */



