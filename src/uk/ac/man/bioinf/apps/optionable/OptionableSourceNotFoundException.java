/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.apps.optionable; // Package name inserted by Jde-Package

import java.io.IOException;
/**
 * OptionableSourceNotFoundException.java
 *
 *
 * Created: Mon May 14 14:13:40 2001
 *
 * @author Phillip Lord
 * @version $Id: OptionableSourceNotFoundException.java,v 1.1 2001/05/14 17:08:18 lord Exp $
 */

public class OptionableSourceNotFoundException extends IOException
{

  public OptionableSourceNotFoundException()
  {
    super();
  }
  
  public OptionableSourceNotFoundException( String message )
  {
    super( message );
  }
} // OptionableSourceNotFoundException



/*
 * ChangeLog
 * $Log: OptionableSourceNotFoundException.java,v $
 * Revision 1.1  2001/05/14 17:08:18  lord
 * Added proper error handling to save and restore.
 *
 */
