/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Middlesex, and modified whilst at the
 * University of Manchester.
 *
 * The initial code base is copyright by the respective
 * employers. Modifications to the initial code base are copyright of
 * their respective authors, or their employers as appropriate.
 * Authorship of the modifications may be determined from the
 * ChangeLog placed at the end of this file
 */

package uk.ac.man.bioinf.apps.optionable; // Package name inserted by JPack

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import uk.ac.man.bioinf.apps.systemevents.SystemEventProducer;
import uk.ac.man.bioinf.util.ExceptionHandler;




/**
 * FileSaveableOptions.java
 *
 * Provides Saveable Option support with files.
 *
 * Created: Tue Feb 02 16:40:27 1999
 *
 * @author Phillip Lord
 * @version $Id: FileSaveableOptions.java,v 1.16 2001/05/14 17:08:18 lord Exp $
 */

public class FileSaveableOptions extends SaveableOptions
{
  private File configFile;
  
  public FileSaveableOptions( File configFile, SystemEventProducer producer, 
                              OptionableExceptionHandler exHandler )
  {
    super( producer, exHandler );
    this.configFile = configFile;
    loadOptions();
  }
  
  /**
   * These were originally going to be abstract but you cant make
   * abstract static methods. I guess its no big thing as thing is
   * pretty much a one off class anyway.
   */
  public OutputStream getSaveStream() throws IOException
  {
    return new FileOutputStream( configFile );
  }
  
  public InputStream getLoadStream() throws IOException
  {
    try{
      return new FileInputStream( configFile );
    }
    catch( FileNotFoundException fnfe ){
      // convert the File not found in to the standard exception
      // indicating that the optionable source (which might not be a
      // file in other circumstances!) is not available. 
      throw new OptionableSourceNotFoundException( "No such file" );
    }
  }

} // FileSaveableOptions



/*
 * ChangeLog
 * $Log: FileSaveableOptions.java,v $
 * Revision 1.16  2001/05/14 17:08:18  lord
 * Added proper error handling to save and restore.
 *
 * Revision 1.15  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.14  2001/02/19 17:25:48  lord
 * Small bug fix
 *
 * Revision 1.13  2001/01/30 15:30:15  lord
 * This class now just provides the File support part.
 *
 * Revision 1.12  2001/01/30 13:59:49  lord
 * File renamed from SaveableOptions, and repacked from the
 * photofit directories. I have done as much as I need to get
 * this class recompiling.
 *
 * Revision 1.11  1999/10/14 14:00:41  phillip2
 * Changed packages and a few other bits and bobs
 *
 * Revision 1.11  1999-10-12 15:09:44+01  phillip2
 * Outdated (renamed to 'D:\Home\rcs\D\Home\java\sources\photofit\options\SaveableOptions.java,v').
 *
 * Revision 1.10  1999-10-01 16:53:18+01  phillip2
 * Added some imports due to changed package structure
 *
 * Revision 1.9  1999-09-30 15:11:05+01  phillip2
 * Updated package statement
 *
 * Revision 1.9  1999-09-30 15:05:02+01  phillip2
 * Outdated (renamed to 'D:\Home\rcs\D\Home\java\sources\photofit\options\SaveableOptions.java,v').
 *
 * Revision 1.8  1999-08-04 15:56:47+01  phillip2
 * Added more documentation
 *
 * Revision 1.7  1999-06-11 11:29:17+01  phillip2
 * Removed wildcards from imports
 * Removed reference to PhotoFitStatic
 *
 * Revision 1.6  1999-05-07 17:28:22+01  phillip2
 * Changed System.out to Debug.message
 *
 * Revision 1.5  1999-05-06 14:05:36+01  phillip2
 * Added some minimal exception handling
 *
 * Revision 1.4  1999-04-25 16:47:46+01  phillip2
 * Updated to changed SystemListener interface
 *
 * Revision 1.3  1999-02-15 18:04:24+00  phillip2
 * Removed reference to PhotoFitInstall directory which doesnt exist anymore
 *
 * Revision 1.2  1999-02-10 18:43:32+00  phillip2
 * The handler now forces an update of options and sets the options
 * handler for the optionable options during addOptionable
 *
 * Revision 1.1  1999-02-09 19:08:14+00  phillip2
 * Initial revision
 *
 */
