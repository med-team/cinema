/* 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
*/
 
/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.apps.cinema.core; // Package name inserted by JPack
import java.net.URL;


/**
 * VersionInformation.java
 *
 * This provides standard information about this version of Cinema.
 *
 * Created: Wed Apr 19 17:03:28 2000
 * 
 * @author Phillip Lord
 * @version $Id: VersionInformation.java,v 1.21 2002/04/20 15:03:42 lord Exp $
 */
public class VersionInformation 
{
  public static String getVersionNumber()
  {
    return "3.0.23";
  }
  
  public static URL getProjectURL()
  {
    return null;
  }
  
  public static String getAboutText()
  {
    return "Cinema";
  }
  
  public static URL[] getAuthorURLs()
  {
    return null;
  }

  public static String getAuthorString()
  {
    String[] authString =
    {
      "Phillip Lord",
      "Julian Selley",
    };
    
    String retn = "";
    
    java.util.LinkedList list = new java.util.LinkedList
      ( java.util.Arrays.asList( authString ) );
    java.util.Random rand = new java.util.Random();
    
    while( list.size() > 1 ){
      int ind = rand.nextInt( list.size() );
      retn = retn + list.get( ind );
      if( list.size() != 2 ){
	retn = retn + ", ";
      }
      list.remove( ind );
    }
    
    retn = retn + " and " + list.get( 0 );
    return retn;
  }
  
  public static URL getMaintainerURL()
  {
    return null;
  }
} // VersionInformation



/*
 * ChangeLog
 * $Log: VersionInformation.java,v $
 * Revision 1.21  2002/04/20 15:03:42  lord
 * release update
 *
 * Revision 1.20  2002/03/19 17:09:48  lord
 * 3.0.22
 *
 * Revision 1.19  2002/03/11 17:35:47  lord
 * 3.0.21
 *
 * Revision 1.18  2002/03/08 14:49:53  lord
 * Update for 3_0_20
 *
 * Revision 1.17  2001/07/10 14:20:01  lord
 * 3.0.19
 *
 * Revision 1.16  2001/07/06 12:39:22  lord
 * 3.0.18
 *
 * Revision 1.15  2001/05/22 15:53:23  lord
 * Updated to 3.0.17
 *
 * Revision 1.14  2001/05/14 17:23:14  lord
 * Updated for 3.0.16
 *
 * Revision 1.13  2001/05/08 17:40:13  lord
 * Changed for 3.0.15
 *
 * Revision 1.12  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.11  2001/04/11 16:30:55  lord
 * 3.0.14
 *
 * Revision 1.10  2001/03/26 15:32:49  lord
 * Updated for 3.0.12
 *
 * Revision 1.9  2001/02/21 15:28:03  lord
 * Release 3.0.10
 *
 * Revision 1.8  2001/02/21 15:15:57  lord
 * version 3.0.9
 *
 * Revision 1.7  2001/01/27 17:13:21  lord
 * Really updated for 3.0.8
 *
 * Revision 1.6  2001/01/27 17:13:00  lord
 * Update for 3.0.8
 *
 * Revision 1.5  2001/01/23 18:03:30  lord
 * 3.0.7
 *
 * Revision 1.4  2001/01/22 13:39:53  lord
 * Update for 3.0.5
 *
 * Revision 1.3  2000/12/05 16:06:17  lord
 * New version with lots of threading added.
 *
 * Revision 1.2  2000/10/12 11:43:13  lord
 * New version
 *
 * Revision 1.1  2000/04/20 16:23:48  lord
 * Initial checkin
 *
 */
