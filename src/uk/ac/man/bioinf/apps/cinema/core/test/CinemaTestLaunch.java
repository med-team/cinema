/* 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
*/
 
/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.apps.cinema.core.test; // Package name inserted by JPack

import java.awt.Color;
import java.io.File;
import java.io.FileReader;
import uk.ac.man.bioinf.apps.cinema.io.ParserFactory;
import uk.ac.man.bioinf.gui.color.ColorMap;
import uk.ac.man.bioinf.gui.color.IndividualElementColorMap;
import uk.ac.man.bioinf.io.ParserException;
import uk.ac.man.bioinf.io.ParserExceptionHandler;
import uk.ac.man.bioinf.sequence.Element;
import uk.ac.man.bioinf.sequence.alignment.DefaultSequenceAlignment;
import uk.ac.man.bioinf.sequence.alignment.Gap;
import uk.ac.man.bioinf.sequence.alignment.SequenceAlignment;
import uk.ac.man.bioinf.sequence.identifier.SimpleIdentifier;
import uk.ac.man.bioinf.sequence.types.AminoAcid;
import uk.ac.man.bioinf.sequence.types.ProteinSequenceType;


/**
 * CinemaTestLaunch.java
 *
 *
 * Created: Wed Apr 19 18:29:17 2000
 *
 * @author Phillip Lord
 * @version $Id: CinemaTestLaunch.java,v 1.11 2001/04/11 17:04:42 lord Exp $
 */

public class CinemaTestLaunch 
{

  private static SequenceAlignment msa;
  
  public static void main( String[] args ) throws Throwable
  {
    try{
      
      /*
	CinemaDebug debug = new CinemaDebug();
	debug.load();
	
	CinemaSystemEvents event = new CinemaSystemEvents();
	
	CinemaCoreGui gui = new CinemaCoreGui();
	gui.load();
	
	CinemaCoreView view = new CinemaCoreView();
	view.load();
	view.start();
	
	CinemaMenuSystem menu = new CinemaMenuSystem();
	menu.setCoreGui( gui );
	menu.load();
	menu.start();
	
	CinemaSequenceMenu seqMenu = new CinemaSequenceMenu();
	seqMenu.setCoreGui( gui );
	seqMenu.setCoreView( view );
	seqMenu.load();
	seqMenu.start();
	
	CinemaGroupModule grpMod = new CinemaGroupModule();
	grpMod.setCoreGui( gui );
	grpMod.setMenuSystem( seqMenu );
	grpMod.load();
	grpMod.start();
	
	
	gui.start();
	uk.ac.man.bioinf.debug.Debug.throwable( null, new Throwable() );
	*/

      System.out.println( "This class has been throughly nobbled by the introduction of the module architecture" );
      System.out.println( "I have only failed to remove it because it contains some utility functions which " );
      System.out.println( "Im still using for the moment" );
      
    }
    catch( Throwable thrw ){
      thrw.printStackTrace();
    }
    

    
  } //end main method 
  
  public static SequenceAlignment generateMsa() throws Throwable
  {
    if( true ){
      Element[][] elems = new Element[ 6 ][];
      elems[ 0 ] = AminoAcid.getAll();
      elems[ 1 ] = AminoAcid.getAll();
      elems[ 2 ] = AminoAcid.getAll();
      elems[ 3 ] = AminoAcid.getAll();
      elems[ 4 ] = AminoAcid.getAll();
      elems[ 5 ] = AminoAcid.getAll();
      //  elems[ 6 ] = AminoAcid.getAll();
//        elems[ 7 ] = AminoAcid.getAll();
//        elems[ 8 ] = AminoAcid.getAll();    
//        elems[ 9 ] = AminoAcid.getAll();
//        elems[ 10 ] = AminoAcid.getAll();
//        elems[ 11 ] = AminoAcid.getAll();
//        elems[ 12 ] = AminoAcid.getAll();
//        elems[ 13 ] = AminoAcid.getAll();
//        elems[ 14 ] = AminoAcid.getAll();
//        elems[ 15 ] = AminoAcid.getAll();
//        elems[ 16 ] = AminoAcid.getAll();
//        elems[ 17 ] = AminoAcid.getAll();
//        elems[ 18 ] = AminoAcid.getAll();    
//        elems[ 19 ] = AminoAcid.getAll();
//        elems[ 20 ] = AminoAcid.getAll();
//        elems[ 21 ] = AminoAcid.getAll();
//        elems[ 22 ] = AminoAcid.getAll();
//        elems[ 23 ] = AminoAcid.getAll();
//        elems[ 24 ] = AminoAcid.getAll();
//        elems[ 25 ] = AminoAcid.getAll();
//        elems[ 26 ] = AminoAcid.getAll();
//        elems[ 27 ] = AminoAcid.getAll();
//        elems[ 28 ] = AminoAcid.getAll();    
//        elems[ 29 ] = AminoAcid.getAll();
//        elems[ 30 ] = AminoAcid.getAll();
//        elems[ 31 ] = AminoAcid.getAll();
//        elems[ 32 ] = AminoAcid.getAll();
//        elems[ 33 ] = AminoAcid.getAll();
//        elems[ 34 ] = AminoAcid.getAll();
//        elems[ 35 ] = AminoAcid.getAll();
//        elems[ 36 ] = AminoAcid.getAll();
//        elems[ 37 ] = AminoAcid.getAll();
//        elems[ 38 ] = AminoAcid.getAll();    
//        elems[ 39 ] = AminoAcid.getAll();


      DefaultSequenceAlignment msa = new DefaultSequenceAlignment( elems, ProteinSequenceType.getInstance() );
      msa.setInset( 3, 13 );
      msa.setInset( 1, 6 );
      msa.setInset( 4, 8 );
      msa.setInset( 2, 4 );
      msa.setInset( 5, 43 );
      //  msa.setInset( 6, 1 );
//        msa.setInset( 7, 5 );
//        msa.setInset( 8, 0 );
//        msa.setInset( 9, 6 );
//        msa.setInset( 10, 13 );
//        msa.setInset( 13, 3 );
//        msa.setInset( 11, 6 );
//        msa.setInset( 14, 38 );
//        msa.setInset( 12, 4 );
//        msa.setInset( 15, 9 );
//        msa.setInset( 16, 1 );
//        msa.setInset( 17, 5 );
//        msa.setInset( 18, 0 );
//        msa.setInset( 19, 6 );
//        msa.setInset( 20, 3 );
//        msa.setInset( 23, 3 );
//        msa.setInset( 21, 6 );
//        msa.setInset( 24, 8 );
//        msa.setInset( 22, 4 );
//        msa.setInset( 25, 9 );
//        msa.setInset( 26, 1 );
//        msa.setInset( 27, 5 );
//        msa.setInset( 28, 0 );
//        msa.setInset( 29, 6 );
//        msa.setInset( 30, 3 );
//        msa.setInset( 33, 3 );
//        msa.setInset( 31, 6 );
//        msa.setInset( 34, 8 );
//        msa.setInset( 32, 4 );
//        msa.setInset( 35, 9 );
//        msa.setInset( 36, 1 );
//        msa.setInset( 37, 5 );
//        msa.setInset( 38, 0 );
//        msa.setInset( 39, 6 );
//        msa.setInset( 40, 3 );
      
      return msa;
    }
    else{
      
      /* (PENDING: JNS) 28/07/00 add in ability to get different
       * parsers (i.e., not just PIR - getAvailableParsers) -
       * rel. FileSequenceInput
       */
      SequenceAlignment seq = (new ParserFactory()).
	getInputParser("PIR").parse ( new SimpleIdentifier( "GPCRRHODOPSN" ),
				      new FileReader(new File
					(System.getProperty("user.home") + 
					 "/seq/gpcrrhodopsn.seqs" )), 
				      new ParserExceptionHandler(){
					  public void handleException( ParserException e )
					  {
					  }
					});
      
      return seq;
    }
  }
  
  public static ColorMap generateColorMap()
  {
        // color mapping
    AminoAcid[] aa = AminoAcid.getAll();
    Color[] colors = new Color[aa.length + 1];
    for (int i = 0; i < aa.length; i++) {
      if (aa[i] == AminoAcid.GLYCINE)
	colors[i] = new Color(255, 170, 136);
      else if (aa[i] == AminoAcid.ALANINE)
	colors[i] = new Color(255, 255, 255);
      else if (aa[i] == AminoAcid.VALINE)
	colors[i] = new Color(255, 255, 255);
      else if (aa[i] == AminoAcid.LEUCINE)
	colors[i] = new Color(255, 255, 255);
      else if (aa[i] == AminoAcid.ISOLEUCINE)
	colors[i] = new Color(255, 255, 255);
      else if (aa[i] == AminoAcid.SERINE)
	colors[i] = new Color(136, 255, 136);
      else if (aa[i] == AminoAcid.CYSTEINE)
	colors[i] = new Color(255, 255, 136);
      else if (aa[i] == AminoAcid.THREONINE)
	colors[i] = new Color(136, 255, 136);
      else if (aa[i] == AminoAcid.METHIONINE)
	colors[i] = new Color(255, 255, 255);
      else if (aa[i] == AminoAcid.PHENYLALANINE)
	colors[i] = new Color(255, 136, 255);
      else if (aa[i] == AminoAcid.TYROSINE)
	colors[i] = new Color(255, 136, 255);
      else if (aa[i] == AminoAcid.TRYPTOPHAN)
	colors[i] = new Color(255, 136, 255);
      else if (aa[i] == AminoAcid.PROLINE)
	colors[i] = new Color(255, 170, 136);
      else if (aa[i] == AminoAcid.HISTIDINE)
	colors[i] = new Color(136, 255, 255);
      else if (aa[i] == AminoAcid.LYSINE)
	colors[i] = new Color(136, 255, 255);
      else if (aa[i] == AminoAcid.ARGININE)
	colors[i] = new Color(136, 255, 255);
      else if (aa[i] == AminoAcid.ASPARTICACID)
	colors[i] = new Color(255, 136, 136);
      else if (aa[i] == AminoAcid.GLUTAMICACID)
	colors[i] = new Color(255, 136, 136);
      else if (aa[i] == AminoAcid.ASPARAGINE)
	colors[i] = new Color(136, 255, 136);
      else if (aa[i] == AminoAcid.GLUTAMINE)
	colors[i] = new Color(136, 255, 136);
      else
	colors[i] = new Color(102, 102, 102);
    }
    colors[ aa.length ] = Color.white;
    
    
    Element[] elem = new Element[ aa.length + 1 ];
    System.arraycopy( aa, 0, elem, 0, aa.length );
    elem[ aa.length ] = Gap.gap;
    
    IndividualElementColorMap cm = new IndividualElementColorMap("test", elem, colors);
    return cm;
  }
} // CinemaTestLaunch



/*
 * ChangeLog
 * $Log: CinemaTestLaunch.java,v $
 * Revision 1.11  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.10  2000/12/13 16:31:41  lord
 * Various
 *
 * Revision 1.9  2000/08/21 17:20:13  jns
 * o had to split to input parsers and output parsers - as a result the getParser
 * changed to getInputParser
 *
 * Revision 1.8  2000/08/01 13:28:26  jns
 * o added in stuff to use parsers rather than original SequenceParser
 *
 * Revision 1.7  2000/06/13 11:14:25  lord
 * Boring changes
 *
 * Revision 1.6  2000/05/30 16:13:43  lord
 * More boring changes
 *
 * Revision 1.5  2000/05/24 15:37:07  lord
 * Added more modules
 *
 * Revision 1.4  2000/05/18 17:11:40  lord
 * To boring to document
 *
 * Revision 1.3  2000/05/15 16:22:46  lord
 * nothing interesting
 *
 * Revision 1.1  2000/04/20 16:24:06  lord
 * Initial checkin
 *
 */





