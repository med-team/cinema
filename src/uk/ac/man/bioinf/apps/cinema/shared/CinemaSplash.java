/* 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
*/
 

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.apps.cinema.shared; // Package name inserted by JPack
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.MediaTracker;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import javax.swing.ImageIcon;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JWindow;
import uk.ac.man.bioinf.apps.cinema.CinemaModule;
import uk.ac.man.bioinf.apps.cinema.resources.CinemaResources;
import uk.ac.man.bioinf.module.ModuleException;


/**
 * CinemaSplash.java
 *
 *
 * Created: Tue Dec 12 17:42:27 2000
 *
 * @author Phillip Lord
 * @version $Id: CinemaSplash.java,v 1.4 2001/04/11 17:04:42 lord Exp $
 */

public class CinemaSplash extends CinemaModule
{
  private JWindow splash;
  
  public void load() throws ModuleException
  {
    splash = new JWindow();
    
    
    Container contentPane = splash.getContentPane();
    
    contentPane.setLayout(new BorderLayout());
    
    
    //JEditorPane editorPane = new JEditorPane();
    //editorPane.setEditable(false);
    //editorPane.setPreferredSize(new Dimension(300, 240));
    
    //try {
    // editorPane.setPage
    //   ( CinemaResources.getResource( "cinema.html" ) );
    //} catch (IOException e) {  
    // throw new ModuleException( e );
    //}
    
    //contentPane.add( editorPane, BorderLayout.CENTER );

    JLabel label = new JLabel();
    ImageIcon icon = new ImageIcon( CinemaResources.getResource( "cinema.jpg" ) );
    label.setIcon( icon );
    
    Dimension screensize = Toolkit.getDefaultToolkit().getScreenSize();
    contentPane.add( label, BorderLayout.CENTER );
    
    int height = icon.getIconHeight();
    int width  = icon.getIconWidth();
    
    int x = (screensize.width - width) / 2;
    int y = (screensize.height - height) / 2;
    
    splash.setBounds( x, y, width, height );
    splash.toFront();
    
    splash.show();
  }

  public void killSplash()
  {
    if( splash != null ){
      splash.setVisible( false );
      splash.dispose();
      splash = null;
    }
  }
  
  public String getVersion()
  {
    return "$Id: CinemaSplash.java,v 1.4 2001/04/11 17:04:42 lord Exp $";
  }
} // CinemaSplash



/*
 * ChangeLog
 * $Log: CinemaSplash.java,v $
 * Revision 1.4  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.3  2001/01/15 18:49:36  lord
 * Removed debug output
 *
 * Revision 1.2  2000/12/13 16:37:38  lord
 * Removed extraneous splash
 *
 * Revision 1.1  2000/12/13 16:31:24  lord
 * Initial checkin
 *
 */
