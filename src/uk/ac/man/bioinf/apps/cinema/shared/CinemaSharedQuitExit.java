/* 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
*/
 
/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.apps.cinema.shared; // Package name inserted by JPack
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import javax.swing.AbstractAction;
import javax.swing.Action;
import uk.ac.man.bioinf.apps.cinema.core.CinemaCoreGui;
import uk.ac.man.bioinf.module.Module;


/**
 * CinemaSharedQuitExit.java
 *
 *
 * Created: Fri Sep 15 13:51:24 2000
 *
 * @author Phillip Lord
 * @version $Id: CinemaSharedQuitExit.java,v 1.4 2001/04/11 17:04:42 lord Exp $
 */

public class CinemaSharedQuitExit extends Module
{
  private int numberOfCinemaInstances = 0;
  private ArrayList manList = new ArrayList();
  
  public Action[] getActions( CinemaCoreGui gui )
  {
    // every time this is got we need to add a new module to the
    // number of instances. 
    numberOfCinemaInstances++;
    QuitExitManager instance = new QuitExitManager( gui );
    Action[] retn = instance.getActions();
    if( manList.size() == 0 ){
      instance.disableQuit();
    }
    
    if( manList.size() == 1 ){
      ((QuitExitManager)manList.get( 0 )).enableQuit();
    }
    
    manList.add( instance );
    
    return retn;
  }
  
  private void removeQuitExitManager( QuitExitManager man )
  {
    manList.remove( man );
    
    if( manList.size() == 1 ){
      ((QuitExitManager)manList.get( 0 )).disableQuit();
    }
  }

  class QuitExitManager
  {
    private CinemaCoreGui gui;
    private Action quit;

    QuitExitManager( CinemaCoreGui gui )
    {
      this.gui = gui;
    }
    
    public void disableQuit()
    {
      quit.setEnabled( false );
    }
    
    public void enableQuit()
    {
      quit.setEnabled( true );
    }
    
    public Action[] getActions()
    {  
      Action[] retn = new Action[ 2 ];
      quit = retn[ 0 ] = new AbstractAction( "Close" )
	{
	  public void actionPerformed( ActionEvent event )
	  {
	    // (PENDING:- PL) Clearly some user checking here would be a
	    // good thing. 
	    QuitExitManager.this.gui.getContext().getModuleFactory().destroy();
	    removeQuitExitManager( QuitExitManager.this );
	  }
	};
      
      retn[ 1 ] = new AbstractAction( "Exit" )
	{
	  public void actionPerformed( ActionEvent event )
	  {
	    QuitExitManager.this.gui.attemptSystemExit();
	  }
	};
      return retn;
    }
  }
  
  public String getVersion()
  {
    return "$Id: CinemaSharedQuitExit.java,v 1.4 2001/04/11 17:04:42 lord Exp $";
  }
} // CinemaSharedQuitExit



/*
 * ChangeLog
 * $Log: CinemaSharedQuitExit.java,v $
 * Revision 1.4  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.3  2000/12/13 16:31:10  lord
 * Removed debug
 *
 * Revision 1.2  2000/10/19 17:44:28  lord
 * Import rationalisation
 *
 * Revision 1.1  2000/09/15 17:31:08  lord
 * Initial checking.
 *
 */
