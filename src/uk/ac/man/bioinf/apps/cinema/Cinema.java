/* 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
*/
 
/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.apps.cinema; // Package name inserted by JPack
import java.io.File;
import java.io.IOException;
import uk.ac.man.bioinf.apps.cinema.core.CinemaModuleCoreIdentifier;
import uk.ac.man.bioinf.apps.cinema.resources.CinemaResources;
import uk.ac.man.bioinf.debug.Debug;
import uk.ac.man.bioinf.module.AbstractEnumeratedModuleIdentifier;
import uk.ac.man.bioinf.module.Module;
import uk.ac.man.bioinf.module.ModuleException;
import uk.ac.man.bioinf.module.ModuleFactoryInstance;
import uk.ac.man.bioinf.module.ModuleIdentifier;


/**
 * Cinema.java
 *
 *
 * Created: Fri May 26 22:04:20 2000
 *
 * @author Phillip Lord
 * @version $Id: Cinema.java,v 1.15 2001/05/04 12:30:32 lord Exp $
 */

public class Cinema 
{
  private static ModuleFactoryInstance moduleFactory;
  private static String[] cmdLnArgs;  // stores the command line
                                      // arguments supplied to Cinema
  
  public static void main( String[] args )
  {
    try{
      cmdLnArgs = args;  // store the arguments
      moduleFactory = createModuleFactoryInstance();
      moduleFactory.load( CinemaBootIdentifier.CINEMA_SHARED );

      
      startCinemaInstance();
    }
    catch( Throwable t ){
      if( Debug.debug )
	Debug.both( Cinema.class, "Cinema Panic!", t );
      System.out.println( "Cinema Panic! Error in loading Core Cinema Modules!" );
      System.out.println( "Giving Up! Sorry!" );
      t.printStackTrace();      
      System.exit( -1 );
    }
  } //end main method

  /**
   * Returns the arguments supplied to Cinema on the command line, and
   * stored in the 'main' function.
   * @return the arguments
   */
  public static String[] getCommandLineArguments() 
  {
    return cmdLnArgs;
  }

  public static Module getSharedModuleInstance( ModuleIdentifier identifier )
    throws ModuleException
  {
    return moduleFactory.getModule( identifier );
  }
  
  public static ModuleFactoryInstance startCinemaInstance() throws ModuleException
  {
    // Set up the factory
    return startCinemaInstance( createModuleFactoryInstance() );
  }
  
  private static ModuleFactoryInstance createModuleFactoryInstance()
  {
    ModuleFactoryInstance moduleFactoryInstance = new CinemaModuleFactoryInstance();
    moduleFactoryInstance.addIdentifier
      ( AbstractEnumeratedModuleIdentifier.getAllIdentifiers( CinemaBootIdentifier.class ) );
    return moduleFactoryInstance;
  }
  
  private static ModuleFactoryInstance startCinemaInstance
    ( ModuleFactoryInstance moduleFactory ) throws ModuleException
  {
    // load the core identifiers
    moduleFactory.
      addIdentifier( AbstractEnumeratedModuleIdentifier.getAllIdentifiers
		     ( CinemaModuleCoreIdentifier.class ) );
    
    // load the XMLBoot Module which will load everything else
    moduleFactory.load( CinemaBootIdentifier.CINEMA_BOOT );
    return moduleFactory;
  }
} // Cinema



/*
 * ChangeLog
 * $Log: Cinema.java,v $
 * Revision 1.15  2001/05/04 12:30:32  lord
 * Cosmetic changes
 *
 * Revision 1.14  2001/04/11 17:04:41  lord
 * Added License agreements to all code
 *
 * Revision 1.13  2001/02/19 16:49:01  lord
 * Removed all the classpath stuff again
 *
 * Revision 1.12  2001/02/15 18:59:12  lord
 * Actually calls construct class path now.
 *
 * Revision 1.11  2001/02/15 18:50:42  lord
 * bug fix
 *
 * Revision 1.10  2001/02/15 18:20:48  lord
 * Support for automatic classpath insertion
 *
 * Revision 1.9  2001/01/31 17:40:27  lord
 * Exception handling change
 *
 * Revision 1.8  2001/01/04 14:50:50  lord
 * startCinemaInstance now returns the module factory. This allows the
 * class which is starting a new CinemaInstance to interact with the new
 * instance. The motivation for this was to allow setting the
 * SequenceAlignment of the new instance.
 *
 * Revision 1.7  2001/01/04 12:57:30  jns
 * o CINEMA command line processor. Code will be improved to look at an
 * XML file and from there load relevent code. Currently the code simply
 * loads a file from the command line, and has the option for a parser to
 * be specified. -i <filename> or <filename> to simply load a file. The
 * PIR parser is currently used by default (there are no other parsers as
 * yet).
 *
 * Revision 1.6  2000/12/13 19:35:03  lord
 * Cinema now shuts down if a Cinema Panic occurs
 *
 * Revision 1.5  2000/09/15 17:26:26  lord
 * Changed boot sequence to include support for multiple cinema instances
 *
 * Revision 1.4  2000/08/02 14:54:20  lord
 * Removed test status bar message
 *
 * Revision 1.3  2000/07/26 13:27:58  lord
 * Changed due to a spelling mistake in the super class name
 *
 * Revision 1.2  2000/06/27 13:36:37  lord
 * Added stack trace to exception handling
 *
 * Revision 1.1  2000/05/30 16:05:54  lord
 * Initial checkin
 *
 */
