;; This is just a little hack script that I wrote to generate Colour
;; map meta data classes based on a standard scheme.



;; does hex convert
(require 'hexl)

;; does the string replace
(defun color-convert-match( one-letter )
  (cond ((string= one-letter "A") 
         "AminoAcid.ALANINE"     )
        ((string= one-letter "R")
         "AminoAcid.ARGININE"    )
        ((string= one-letter "N")  
         "AminoAcid.ASPARAGINE"  )
        ((string= one-letter "D")  
         "AminoAcid.ASPARTICACID"   )
        ((string= one-letter "C")  
         "AminoAcid.CYSTEINE"     )
        ((string= one-letter "Q")  
         "AminoAcid.GLUTAMINE"   )
        ((string= one-letter "E")  
         "AminoAcid.GLUTAMICACID"   )
        ((string= one-letter "G")  
         "AminoAcid.GLYCINE"     )
        ((string= one-letter "H")  
         "AminoAcid.HISTIDINE"   )
        ((string= one-letter "I")  
         "AminoAcid.ISOLEUCINE"  )
        ((string= one-letter "L")  
         "AminoAcid.LEUCINE"     )
        ((string= one-letter "K")  
         "AminoAcid.LYSINE"      )
        ((string= one-letter "M")  
         "AminoAcid.METHIONINE"  )
        ((string= one-letter "F")  
         "AminoAcid.PHENYLALANINE"  )
        ((string= one-letter "P")  
         "AminoAcid.PROLINE"  )
        ((string= one-letter "S")  
         "AminoAcid.SERINE"  )
        ((string= one-letter "T")  
         "AminoAcid.THREONINE"  )
        ((string= one-letter "W")  
         "AminoAcid.TRYPTOPHAN"  )
        ((string= one-letter "Y")  
         "AminoAcid.TYROSINE"  )
        ((string= one-letter "V")
         "AminoAcid.VALINE"  )
        ((string= one-letter "B")
         "AminoAcid.ASNORASP" )
        ((string= one-letter "Z")
         "AminoAcid.GLUORGLN" )
        ((string= one-letter "X" )
         "AminoAcid.ANY" )
        (t 
         (error "Unknown letter %s" one-letter )))) 
       
        

(defun color-convert()
  "Create a new buffer based on the current buffer, which has the 
Java class for doing colours"
  (interactive)
  (let* ((from-buffer (current-buffer))
         (scheme-name
          (read-from-minibuffer
           "Colour Scheme Name: "
           (capitalize 
            (file-name-nondirectory
             (file-name-sans-extension 
              (buffer-file-name from-buffer))))))
         (buffer-name "*color-convert*")
         (exist-buffer (get-buffer buffer-name))
         (to-buffer
          (progn
            (if exist-buffer
                (kill-buffer exist-buffer))
            (get-buffer-create buffer-name))))
    (switch-to-buffer to-buffer)
    ;; do the conversion
    (color-convert-insert-preamble to-buffer scheme-name)
    (color-convert-convert-buffer to-buffer from-buffer)
    (color-convert-insert-postamble to-buffer)
    ;; suppress normal mode selection. Other wise I
    ;; get silly folding that I dont want.
    (let((change-major-mode-with-file-name 'nil))
      (set-visited-file-name
       (concat scheme-name "ColorMapMetaData.java"))
      (java-mode))
    ;; reindent
    (goto-char (point-min))
    (indent-according-to-mode)
    (while (= 0 (forward-line))
      (indent-according-to-mode))
    (normal-mode)
    (goto-char (point-min))
    ;; this is a good point to be. 
    (search-forward "class")))

(defun color-convert-insert-preamble( buffer name )
  (set-buffer buffer)
  (insert-string
"/* 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
*/
 
/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.apps.cinema.color;
import java.awt.Color;
import uk.ac.man.bioinf.analysis.consensus.ConsensusSequence;
import uk.ac.man.bioinf.apps.cinema.CinemaModule;
import uk.ac.man.bioinf.gui.color.ColorMap;
import uk.ac.man.bioinf.gui.color.IndividualElementColorMap;
import uk.ac.man.bioinf.gui.viewer.JAlignmentViewer;
import uk.ac.man.bioinf.sequence.Element;
import uk.ac.man.bioinf.sequence.alignment.Gap;
import uk.ac.man.bioinf.sequence.alignment.SequenceAlignment;
import uk.ac.man.bioinf.sequence.types.AminoAcid;


/**
 * THIS FILE WAS AUTOMATICALLY GENERATED USING color-convert.el
 * 
 * Created: Wed Jul 19 14:31:39 2000
 *
 * @author color-convert.el
 * @version $Id: color-convert.el,v 1.1 2001/07/06 12:34:37 lord Exp $
 */

public class ")
  (insert-string name)
  (insert-string 
"ColorMapMetaData implements ColorMapMetaData
{
  // we only need one!
  private static final IndividualElementColorMap map;
  public static final String NAME = \"")
  (insert-string name)
  (insert-string " Color Scheme\";
  
  static
  {
    // color mapping
    Element[] aa = AminoAcid.getAll();
    Color[] colors = new Color[aa.length + 1];
    for (int i = 0; i < aa.length; i++) {
    if( false ){
    }
"))


(defun color-convert-insert-postamble(buffer)
  (set-buffer buffer)
  (insert-string
   (concat
    "}
    colors[ aa.length ] = new Color( "
    color-convert-default-color " );"))    
  (insert-string "

    
       
    Element[] elem = new Element[ aa.length + 1 ];
    System.arraycopy( aa, 0, elem, 0, aa.length );
    elem[ aa.length ] = Gap.gap;

    map = new IndividualElementColorMap( NAME, elem, colors);

  }
   
  public void setModule( CinemaModule module )
  {
    // nothing required
  }
  
  public String getColorMapName()
  {
    return NAME;
  }
    
  public ColorMap getInstance( SequenceAlignment alignment, JAlignmentViewer viewer )
  {
    return map;
  }
  
  public ColorMap getConsensusInstance( ConsensusSequence sequence, JAlignmentViewer viewer )
  {
    return map;
  }
}"))

(defun color-convert-convert-buffer(to-buffer from-buffer)
  (set-buffer from-buffer)
  (goto-char (point-min))
  (color-convert-convert-line to-buffer from-buffer)
  ;;now insert the default colour
  (set-buffer to-buffer)
  (insert-string 
   (concat "else
colors[ i ] = new Color( " color-convert-default-color " );
")))
   

(defvar color-convert-gap-color nil)
(defvar color-convert-default-color nil)

(defun color-convert-convert-line(to-buffer from-buffer)
  "Convert a line at a time"
  (let* ((point (point))
         (amino-acid
          (buffer-substring-no-properties
           point
           (+ 1 point)))
         (colour
          (buffer-substring-no-properties
           (+ 2 point)
           (+ 8 point))))
    (cond 
     ;; check for terminiation
     ((string= "<" amino-acid)
      nil)
     ((string= " " amino-acid)
      (progn
        (setq color-convert-default-color
              (color-convert-hex-to-int-string colour))
        (forward-line)
        (color-convert-convert-line to-buffer from-buffer)))
     ((string= "-" amino-acid)
      (progn
        (setq color-convert-gap-color
              (color-convert-hex-to-int-string colour))
        (forward-line)
        (color-convert-convert-line to-buffer from-buffer)))
     (t
      (progn
        (save-excursion
          (set-buffer to-buffer)
          (insert-string
           "else if ( aa[ i ] == ")
          (insert-string (color-convert-match amino-acid))
          (insert-string 
           ")
colors[ i ] = new Color( ")
          (insert-string (color-convert-hex-to-int-string colour))
          (insert-string " );
"))
        (forward-line)
        (color-convert-convert-line to-buffer from-buffer))))))

(defun color-convert-hex-to-int-string( number )
  (mapconcat
   'hexl-hex-string-to-integer
   (list 
    (substring colour 0 2)
    (substring colour 2 4)
    (substring colour 4 6))
   " , "))
