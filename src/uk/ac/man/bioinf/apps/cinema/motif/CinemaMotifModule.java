/* 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
*/
 
/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.apps.cinema.motif; // Package name inserted by JPack
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import uk.ac.man.bioinf.apps.cinema.CinemaModule;
import uk.ac.man.bioinf.apps.cinema.core.CinemaActionProvider;
import uk.ac.man.bioinf.module.ModuleIdentifierList;
import uk.ac.man.bioinf.apps.cinema.io.SequenceOutputModuleIdentifier;


/**
 * CinemaMotifModule.java
 *
 * This module is designed to handle motif selection and viewing
 * within Cinema.
 *
 * Created: Wed Dec 27 14:57:33 2000
 *
 * @author Phillip Lord
 * @version $Id: CinemaMotifModule.java,v 1.5 2001/05/29 13:06:07 lord Exp $
 */

public class CinemaMotifModule extends CinemaModule implements CinemaActionProvider
{
  private CinemaMotifFrame motifDialog;
  private NamedAlignmentSelectionModel model;
  
  public void start()
  {
    setAlignmentSelectionModel
     ( model = new NamedAlignmentSelectionModel() );
  }
  
  public Action[] getActions()
  {
    Action[] retn = new Action[ 1 ];
    
    retn[ 0 ] = new AbstractAction
      ( "Motif Manager" ){
        public void actionPerformed( ActionEvent event )
        {
          if( motifDialog == null ){
            motifDialog = new CinemaMotifFrame( model, CinemaMotifModule.this );
          }
          motifDialog.setVisible( true );
        }
      };
    
    return retn;
  }
  
  public String getVersion()
  {
    return "$Id: CinemaMotifModule.java,v 1.5 2001/05/29 13:06:07 lord Exp $";
  }

  public ModuleIdentifierList getRequiredIdentifiers()
  {
    ModuleIdentifierList list = super.getRequiredIdentifiers();
    list.add( SequenceOutputModuleIdentifier.FILE_OUTPUT );
    return list;
  }
} // CinemaMotifModule



/*
 * ChangeLog
 * $Log: CinemaMotifModule.java,v $
 * Revision 1.5  2001/05/29 13:06:07  lord
 * Removed ParserFactory. Name change for Input module
 *
 * Revision 1.4  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.3  2001/01/31 17:44:42  lord
 * Changes due to removal of InterfaceIdentifier
 *
 * Revision 1.2  2001/01/23 18:01:24  lord
 * Requires file output module
 *
 * Revision 1.1  2001/01/19 15:52:08  lord
 * Initial Checkin
 *
 */
