/* 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
*/
 
/* 
 * This software was written by Julian Selley (j.selley@man.ac.uk)
 * whilst at the University of Manchester as a Research Assistant. 
 *
 * The initial code base is copyright by the University of Manchester. 
 * Modifications to the initial code base are copyright of their 
 * respective authors, or their employers as appropriate. Authorship 
 * of the modifications may be determined from the ChangeLog placed 
 * at the end of this file
 */

package uk.ac.man.bioinf.apps.cinema.io; // Package name inserted by JPack

import uk.ac.man.bioinf.module.AbstractEnumeratedModuleIdentifier;


/**
 * The identifier for the CinemaCommandLineParser module.
 *
 *
 * Created: Thu Dec 21 12:34:24 2000
 *
 * @author Julian Selley
 * @version $Id: CinemaCommandLineParserIdentifier.java,v 1.2 2001/04/11 17:04:42 lord Exp $
 */

public class CinemaCommandLineParserIdentifier 
  extends AbstractEnumeratedModuleIdentifier
{
  private CinemaCommandLineParserIdentifier(String className, String toString)
  {
    super(className, toString);
  }

  public static final CinemaCommandLineParserIdentifier COMMAND_LINE_PARSER = 
    new CinemaCommandLineParserIdentifier
    ("uk.ac.man.bioinf.apps.cinema.io.CinemaCommandLineParser",
     "Provides the command line parsing module");
} // CinemaCommandLineParserIdentifier



/*
 * ChangeLog
 * $Log: CinemaCommandLineParserIdentifier.java,v $
 * Revision 1.2  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.1  2001/01/04 12:57:30  jns
 * o CINEMA command line processor. Code will be improved to look at an
 * XML file and from there load relevent code. Currently the code simply
 * loads a file from the command line, and has the option for a parser to
 * be specified. -i <filename> or <filename> to simply load a file. The
 * PIR parser is currently used by default (there are no other parsers as
 * yet).
 *
 */
