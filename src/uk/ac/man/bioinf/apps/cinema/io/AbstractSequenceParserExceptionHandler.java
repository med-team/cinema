/* 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
*/
 
/* 
 * This software was written by Julian Selley (j.selley@man.ac.uk)
 * whilst at the University of Manchester as a Research Assistant. 
 *
 * The initial code base is copyright by the University of Manchester. 
 * Modifications to the initial code base are copyright of their 
 * respective authors, or their employers as appropriate. Authorship 
 * of the modifications may be determined from the ChangeLog placed 
 * at the end of this file
 */

package uk.ac.man.bioinf.apps.cinema.io; // Package name inserted by JPack

import java.util.ArrayList;
import uk.ac.man.bioinf.io.ParserException;
import uk.ac.man.bioinf.io.ParserExceptionHandler;


/**
 * AbstractSequenceInputParserExceptionHandler provides methods so
 * that the exceptions can be stored and at a later date shown all
 * together. This is the intended methodology to be used for Cinema.
 *
 *
 * Created: Fri Jun 16 14:06:51 2000
 *
 * @author Julian Selley
 * @version $Id: AbstractSequenceParserExceptionHandler.java,v 1.2 2001/04/11 17:04:42 lord Exp $
 */

public abstract class AbstractSequenceParserExceptionHandler 
  implements ParserExceptionHandler
{
  private ArrayList parserExceptions = new ArrayList();
  
  /**
   * Stores the exception, so that later a call can be made to
   * getParserExceptions() in order to display all the exceptions
   * together. The normal exception handler will just store the
   * exception and allow a gap to be inserted in the sequence.
   *
   * @param e the parser exception
   */
  public void storeException(ParserException e) 
  {
    this.parserExceptions.add(e);
  }
  
  /**
   * Return the stored exceptions, so that they can be displayed all
   * together.
   *
   * @return the exceptions
   */
  public ParserException[] getParserExceptions() 
  {
    Object[] exceptions = this.parserExceptions.toArray();
    ParserException[] rtn = new ParserException[exceptions.length];
    
    System.arraycopy(exceptions, 0, rtn, 0, exceptions.length);
    return rtn;
  }

  /**
   * Clears all exceptions that have been stored.
   */
  public void clearExceptions() 
  {
    this.parserExceptions.clear();
  }
} // AbstractSequenceParserExceptionHandler



/*
 * ChangeLog
 * $Log: AbstractSequenceParserExceptionHandler.java,v $
 * Revision 1.2  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.1  2000/08/21 17:21:23  jns
 * o added output parser stuff to package
 *
 * Revision 1.2  2000/08/01 14:54:10  jns
 * o change of name from ParseException to ParserException - more logical
 *
 * Revision 1.1  2000/06/30 09:36:42  jns
 * o added so that exceptions may be stored for printing at a later stage
 *
 */
