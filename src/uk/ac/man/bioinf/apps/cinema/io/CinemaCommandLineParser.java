/* 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
*/
 
/* 
 * This software was written by Julian Selley (j.selley@man.ac.uk)
 * whilst at the University of Manchester as a Research Assistant. 
 *
 * The initial code base is copyright by the University of Manchester. 
 * Modifications to the initial code base are copyright of their 
 * respective authors, or their employers as appropriate. Authorship 
 * of the modifications may be determined from the ChangeLog placed 
 * at the end of this file
 */

package uk.ac.man.bioinf.apps.cinema.io; // Package name inserted by JPack

import gnu.getopt.Getopt;
import gnu.getopt.LongOpt;
import java.io.File;
import java.io.IOException;
import uk.ac.man.bioinf.apps.cinema.Cinema;
import uk.ac.man.bioinf.apps.cinema.CinemaModule;
import uk.ac.man.bioinf.apps.cinema.io.FileSequenceInput;
import uk.ac.man.bioinf.debug.Debug;
import uk.ac.man.bioinf.module.ModuleException;
import uk.ac.man.bioinf.sequence.identifier.FileSource;
import uk.ac.man.bioinf.sequence.identifier.SimpleIdentifier;


/**
 * CinemaCommandLineParser.java
 *
 * This module is responsible for parsing the command line to Cinema
 * and invoking what ever actions are necessary.
 * <p>
 * <b>(PENDING: JNS)</b> 28-11-00 currently this is all hard coded and will
 * need to be altered and recompiled each time the command line
 * options are changed.
 * <p>
 *
 * Created: Mon Nov 27 17:36:39 2000
 *
 * @author Julian Selley
 * @version $Id: CinemaCommandLineParser.java,v 1.7 2001/05/24 15:39:13 lord Exp $
 */

public class CinemaCommandLineParser extends CinemaModule
{
  private String[] cmdLn;  // the command line as supplied to cinema
  private Getopt cmdLnOpts;  // the getopt object for the command line
  
  public CinemaCommandLineParser() {}

  public void load() throws ModuleException
  {
    // store the command line for use later
    cmdLn = Cinema.getCommandLineArguments();

    /* get the command line arguments from and create the Getopt
     * object
     */
    cmdLnOpts = new Getopt("cinema", // program name
			   cmdLn, // command line args
			   // the getopt syntax for what to expect
			   /* -i: in file
			    * -p: parser
			    */
			   "-:i:p:L:U:C:V:D:",
			   // a set of longopts
			   new LongOpt[] {
			     /* --in: in file - rel. to -i
			      */
			     new LongOpt("in", LongOpt.REQUIRED_ARGUMENT,
					 null, 'i'),
			     /* --parser: parser to use
			      */
			     new LongOpt("parser", LongOpt.REQUIRED_ARGUMENT,
					 null, 'p')
			   });
    
  }
  
  public void start() 
  {
    int c;
    boolean filenameSet = false;
    String filename = null;
    String parser = "PIR";
    
    // while there are options
    while ((c = cmdLnOpts.getopt()) != -1)
      switch (c) {
        // in file option
      case 'i':
        filename = cmdLnOpts.getOptarg();
        filenameSet = true;
        break;
      case 'p':
        parser = cmdLnOpts.getOptarg();
        break;
      }
    
    // if no option tag was supplied
    if ( !filenameSet && (cmdLnOpts.getOptind() <= cmdLn.length) && ( cmdLn.length > 0 ))
      filename = cmdLn[cmdLnOpts.getOptind() - 1];
    
    // if the filename has been declared, load the file
    if (filename != null)
      openFile(filename, parser);
  }

  /**
   * Opens a declared file in Cinema.
   *
   * @param filename the filename
   * @param parser the parser to use
   */
  public void openFile(String filename, String parser) 
  {
    try {
      // create a file object using the filename
      File file = new File(filename);
      // determine the header to use for the SimpleIdentifier
        // there is logic behind this - I want just the name, ex. the
        // pathname
      String fileNameNoExtension = file.getName();
      int i = fileNameNoExtension.lastIndexOf('.');
      if (i > 0 && i < fileNameNoExtension.length() - 1)
	fileNameNoExtension = fileNameNoExtension.substring(0, i);
      fileNameNoExtension = fileNameNoExtension.toUpperCase();
      
      // get file module
      FileSequenceInput fsim = (FileSequenceInput)getContext().getModule
	(SequenceInputModuleIdentifier.FILE_INPUT);
      
      // open the file and set the alignment
      setSequenceAlignment
	(fsim.openFile
	 (new File(filename), 
	  fsim.getParser(parser), 
	  new SimpleIdentifier(fileNameNoExtension, new FileSource(file))));
    } catch (ModuleException e) {
      /* (PENDING: JNS) 20.12.00 May need to deal with this
       * properly. Needs more thought than a simple debug statement.
       */
      Debug.both(this, "File Sequence Input Module not found!", e);
    } catch (IOException e) {
      Debug.both(this, "File was not found!", e);
    }
  }

  public String getVersion() 
  {
    return "$Id: CinemaCommandLineParser.java,v 1.7 2001/05/24 15:39:13 lord Exp $";
  }
} // CinemaCommandLineParser



/*
 * ChangeLog
 * $Log: CinemaCommandLineParser.java,v $
 * Revision 1.7  2001/05/24 15:39:13  lord
 * Modified due to changes in Input module
 *
 * Revision 1.6  2001/05/04 12:30:33  lord
 * Cosmetic changes
 *
 * Revision 1.5  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.4  2001/03/12 16:45:18  lord
 * Added some more options
 *
 * Revision 1.3  2001/01/31 17:42:07  lord
 * Removed interface identifier reference
 *
 * Revision 1.2  2001/01/15 18:52:19  lord
 * Cosmetic changes.
 * Improved exception handling.
 * Bug fix, in case of no command line options
 *
 * Revision 1.1  2001/01/04 12:57:30  jns
 * o CINEMA command line processor. Code will be improved to look at an
 * XML file and from there load relevent code. Currently the code simply
 * loads a file from the command line, and has the option for a parser to
 * be specified. -i <filename> or <filename> to simply load a file. The
 * PIR parser is currently used by default (there are no other parsers as
 * yet).
 *
 */
