/* 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
*/
 
/* 
 * This software was written by Julian Selley (j.selley@man.ac.uk)
 * whilst at the University of Manchester as a Research Assistant. 
 *
 * The initial code base is copyright by the University of Manchester. 
 * Modifications to the initial code base are copyright of their 
 * respective authors, or their employers as appropriate. Authorship 
 * of the modifications may be determined from the ChangeLog placed 
 * at the end of this file
 */

package uk.ac.man.bioinf.apps.cinema.io; // Package name inserted by JPack

import java.awt.event.ActionEvent;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import javax.swing.AbstractAction;
import javax.swing.Action;
import uk.ac.man.bioinf.apps.cinema.CinemaGuiModule;
import uk.ac.man.bioinf.apps.cinema.core.CinemaActionProvider;
import uk.ac.man.bioinf.debug.Debug;
import uk.ac.man.bioinf.io.AlignmentOutputParser;
import uk.ac.man.bioinf.io.ParserExceptionHandler;
import uk.ac.man.bioinf.module.ModuleException;
import uk.ac.man.bioinf.module.ModuleIdentifierList;
import uk.ac.man.bioinf.sequence.alignment.SequenceAlignment;


/**
 * This class is abstract and will be extended by any sequence output
 * modules. It merely provides the action stuff for the menu system,
 * and describes an abstract method run by the actionPerformed(...)
 * and implemented by the children of this class.
 *
 *
 * Created: Mon Jul 31 22:22:33 2000
 *
 * @author Julian Selley
 * @version $Id: AbstractSequenceOutput.java,v 1.5 2001/07/06 11:48:08 lord Exp $
 */

public abstract class AbstractSequenceOutput extends CinemaGuiModule
  implements CinemaActionProvider
{
  private static final Map parserMap = new HashMap();
  protected AbstractSequenceParserExceptionHandler eh;

  public final String[] getAvailableParsers()
  {
    Object[] keys = ((Set)parserMap.keySet()).toArray();
    String[] retn = new String[ keys.length ];
    
    System.arraycopy( keys, 0, retn, 0, keys.length );
    return retn;
  }

  public final AlignmentOutputParser getParser( String keyOfParser )
  {
    AlignmentOutputParser retn = null;
    
    try{
      retn = (AlignmentOutputParser)
        (((Class)parserMap.get( keyOfParser )).newInstance());
    }
    catch( InstantiationException ie ){
      if( Debug.debug )
        Debug.both( this, "Input Parser Error", ie );
    }
    catch( IllegalAccessException iae ){
      if( Debug.debug )
        Debug.both( this, "Input Parser Error", iae );
    }

    return retn;
  }
   
  public void load() throws ModuleException
  {
    Properties configProps = getConfigProperties();
    
    for ( Enumeration e = configProps.propertyNames() ; e.hasMoreElements() ;){
      try{
        String parserKey = (String)e.nextElement();
        if( !parserKey.equals( "default" ) ){
          String parserClassName = configProps.getProperty( parserKey );
          parserClassName = parserClassName.substring( 4, parserClassName.length() );
          Class parserClass = getClass().getClassLoader().
            loadClass( parserClassName );
          
          parserMap.put( parserKey, parserClass );
        }
      }
      catch( ClassNotFoundException cnfe ){
        if( Debug.debug )
          cnfe.printStackTrace();
        throw new ModuleException( cnfe );
      }
    }
  }
   
  public final String getParserDescription( String keyOfParser )
  {
    return getParser( keyOfParser ).getDescription();
  }
  
  
  public Action[] getActions() 
  {
    Action[] rtn = new Action[ 2 ];
    
    rtn[0] = new AbstractAction("Save...") {
	public void actionPerformed(ActionEvent event)
	{
	  saveAlignment(getSequenceAlignment());
	}
      };
    
    rtn[ 1 ] = new AbstractAction( "Save as..." ){
        public void actionPerformed( ActionEvent event )
        {
          saveAlignmentAs( getSequenceAlignment() );
        }
      };
    return rtn;
  }

  public abstract void saveAlignment(SequenceAlignment sa);

  public abstract void saveAlignmentAs( SequenceAlignment sa );
  
  
  public void setExceptionHandler
    (AbstractSequenceParserExceptionHandler exceptionHandler) 
  {
    eh = exceptionHandler;
  }
  
  public ParserExceptionHandler getExceptionHandler() 
  {
    return eh;
  }
  
  public ModuleIdentifierList getRequiredIdentifiers() 
  {
    return super.getRequiredIdentifiers();
  }
} // AbstractSequenceOutput



/*
 * ChangeLog
 * $Log: AbstractSequenceOutput.java,v $
 * Revision 1.5  2001/07/06 11:48:08  lord
 * Removed debug
 *
 * Revision 1.4  2001/05/29 13:01:31  lord
 * Removed ParserFactory
 *
 * Revision 1.3  2001/05/24 15:38:57  lord
 * Parsers are now configurable. Added save as option.
 *
 * Revision 1.2  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.1  2000/08/21 17:21:23  jns
 * o added output parser stuff to package
 *
 */
