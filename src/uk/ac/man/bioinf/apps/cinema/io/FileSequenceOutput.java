/* 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
*/
 
/* 
 * This software was written by Julian Selley (j.selley@man.ac.uk)
 * whilst at the University of Manchester as a Research Assistant. 
 *
 * The initial code base is copyright by the University of Manchester. 
 * Modifications to the initial code base are copyright of their 
 * respective authors, or their employers as appropriate. Authorship 
 * of the modifications may be determined from the ChangeLog placed 
 * at the end of this file
 */

package uk.ac.man.bioinf.apps.cinema.io; // Package name inserted by JPack

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import uk.ac.man.bioinf.debug.Debug;
import uk.ac.man.bioinf.sequence.alignment.SequenceAlignment;
import uk.ac.man.bioinf.sequence.identifier.FileSource;
import uk.ac.man.bioinf.sequence.identifier.Source;


/**
 * FileSequenceOutput is a module that covers sequence output to a
 * file from Cinema. It extends the abstract sequence output module
 * that provides the basics of any sequence output from Cinema. 
 *
 *
 * Created: Mon Jul 31 22:18:18 2000
 *
 * @author Julian Selley
 * @version $Id: FileSequenceOutput.java,v 1.7 2001/05/29 13:01:31 lord Exp $
 */

public class FileSequenceOutput extends AbstractSequenceOutput 
{
  JFileChooser chooser = new JFileChooser();
  
  public FileSequenceOutput()
  {
    super.setExceptionHandler(new FileSequenceParserExceptionHandler());
  }
  
  public void saveAlignment( SequenceAlignment sa )
  {
    Source source = sa.getIdentifier().getSource();
    
    // check whether there is a file associated with this sequence
    // alignment. If no then we need to find out what the file should
    // be from the user. 
    if( !( source instanceof FileSource ) ){
      saveAlignmentAs( sa );
      return;
    }
    
    
    FileSource fileSource = (FileSource)source;
    
    try {
      FileWriter writer = new FileWriter( fileSource.getFile() );
      
      super.getParser("PIR").write
        (sa, writer, getExceptionHandler());
      
      // flush the writer and close it
      writer.flush();
      writer.close();
    } catch (IOException e) {
      JOptionPane.showMessageDialog
        ( null, "<html>There was a problem with saving the results: <p>" +
          e.getMessage(), "Warning", JOptionPane.ERROR_MESSAGE  );
      
      if (Debug.debug)
        Debug.both(this, "File Sequence Output: An IOException was found",
                   e);
    }
  }
  
  public void saveAlignmentAs(SequenceAlignment sa) 
  {
    String[] availableParsers = super.
      getAvailableParsers();
    
    // (PENDING: JNS 01/08/00) set the file types from the parsers
    //for (int i = 0; i < availableParsers.length; i++) {
    //  chooser.addChoosableFileFilter();
    //}

    // open the file chooser
    int selected = chooser.showSaveDialog(getFrame());
    // if save a file then pass it to the parse factory
    if (selected == JFileChooser.APPROVE_OPTION) {
      File selectedFile;
      if ( (selectedFile = chooser.getSelectedFile()) != null) {
	
        if( selectedFile.exists() ){
          Object[] options = { "OK", "CANCEL" };
          int retn = JOptionPane.showOptionDialog
            (null, "<html>This file already exists.<p> " 
             + "Are you sure you want to overwrite it. <p>", "Warning", 
             JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE,
             null, options, options[0]);
          if( retn == 1 ) return;
        }
        
        FileSource fileSource = new FileSource
          ( chooser.getSelectedFile() );
        
        sa.getIdentifier().setSource( fileSource );
        saveAlignment( sa );
      }
    }
  }
  

  public String getVersion() 
  {
    return "$Id: FileSequenceOutput.java,v 1.7 2001/05/29 13:01:31 lord Exp $";
  }
} // FileSequenceOutput



/*
 * ChangeLog
 * $Log: FileSequenceOutput.java,v $
 * Revision 1.7  2001/05/29 13:01:31  lord
 * Removed ParserFactory
 *
 * Revision 1.6  2001/05/24 16:01:47  lord
 * Now put in error handling that actually works.
 *
 * Revision 1.5  2001/05/24 15:41:28  lord
 * Improved error handling.
 *
 * Revision 1.4  2001/05/24 15:39:29  lord
 * Modified due to changes in parser factory
 *
 * Revision 1.3  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.2  2000/12/20 15:58:45  jns
 * o bug-fix: writer needed flushing and closing in order for all the
 * information to be written to file.
 *
 * Revision 1.1  2000/08/21 17:21:23  jns
 * o added output parser stuff to package
 *
 */
