/* 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
*/
 
/* 
 * This software was written by Julian Selley (j.selley@man.ac.uk)
 * whilst at the University of Manchester as a Research Assistant. 
 *
 * The initial code base is copyright by the University of Manchester. 
 * Modifications to the initial code base are copyright of their 
 * respective authors, or their employers as appropriate. Authorship 
 * of the modifications may be determined from the ChangeLog placed 
 * at the end of this file
 */

package uk.ac.man.bioinf.apps.cinema.io; // Package name inserted by JPack

import uk.ac.man.bioinf.module.AbstractEnumeratedModuleIdentifier;


/**
 * SequenceOutputModuleIdentifier.java
 *
 *
 * Created: Fri Aug 11 15:06:07 2000
 *
 * @author Julian Selley
 * @version $Id: SequenceOutputModuleIdentifier.java,v 1.3 2001/05/29 13:01:31 lord Exp $
 */

public class SequenceOutputModuleIdentifier
  extends AbstractEnumeratedModuleIdentifier
{
  private SequenceOutputModuleIdentifier(String className, String toString, boolean isInterface)
  {
    super(className, toString, isInterface);
  }
  
  public static final SequenceOutputModuleIdentifier SEQ_OUTPUT = 
    new SequenceOutputModuleIdentifier("uk.ac.man.bioinf.apps.cinema.io.AbstractSequenceOutput",
						"Provides output from cinema into file",
						true);
  public static final SequenceOutputModuleIdentifier FILE_OUTPUT = 
    new SequenceOutputModuleIdentifier("uk.ac.man.bioinf.apps.cinema.io.FileSequenceOutput", 
						"Provides output from cinema into file",
						false);
} // SequenceOutputModuleIdentifier



/*
 * ChangeLog
 * $Log: SequenceOutputModuleIdentifier.java,v $
 * Revision 1.3  2001/05/29 13:01:31  lord
 * Removed ParserFactory
 *
 * Revision 1.2  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.1  2001/01/31 17:43:59  lord
 * Created after removing InterfaceIdentifier
 *
 * Revision 1.1  2000/08/21 17:21:23  jns
 * o added output parser stuff to package
 *
 */
