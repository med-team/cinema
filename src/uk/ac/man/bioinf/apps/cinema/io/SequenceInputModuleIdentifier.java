/* 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
*/
 
/* 
 * This software was written by Julian Selley (j.selley@man.ac.uk)
 * whilst at the University of Manchester as a Research Assistant. 
 *
 * The initial code base is copyright by the University of Manchester. 
 * Modifications to the initial code base are copyright of their 
 * respective authors, or their employers as appropriate. Authorship 
 * of the modifications may be determined from the ChangeLog placed 
 * at the end of this file
 */

package uk.ac.man.bioinf.apps.cinema.io; // Package name inserted by JPack

import uk.ac.man.bioinf.module.AbstractEnumeratedModuleIdentifier;


/**
 * A module interface identifier for sequence input. It will deligate
 * to the different protocols available (e.g: file input, http
 * input).
 *
 *
 * Created: Tue Jun 13 12:43:25 2000
 *
 * @author Julian Selley
 * @version $Id: SequenceInputModuleIdentifier.java,v 1.2 2001/04/11 17:04:42 lord Exp $
 */

public class SequenceInputModuleIdentifier 
  extends AbstractEnumeratedModuleIdentifier
{
  private SequenceInputModuleIdentifier( String className, String toString, boolean isInterface )
  {
    super( className, toString, isInterface );
  }
  
  public static final SequenceInputModuleIdentifier SEQ_INPUT =
    new SequenceInputModuleIdentifier( "uk.ac.man.bioinf.apps.cinema.io.AbstractSequenceInput",
						"Provides the interface for Sequence input into cinema",
						true );
  public static final SequenceInputModuleIdentifier FILE_INPUT = 
    new SequenceInputModuleIdentifier( "uk.ac.man.bioinf.apps.cinema.io.FileSequenceInput",
						"Provides input from a file into cinema",
						false );
} // SequenceInputModuleIdentifier



/*
 * ChangeLog
 * $Log: SequenceInputModuleIdentifier.java,v $
 * Revision 1.2  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.1  2001/01/31 17:43:59  lord
 * Created after removing InterfaceIdentifier
 *
 * Revision 1.3  2000/12/05 15:54:37  lord
 * Import rationalisation
 *
 * Revision 1.2  2000/08/03 16:41:29  lord
 * Now have this working correctly as an interface module
 *
 * Revision 1.1  2000/06/16 09:41:55  jns
 * o initial code for a sequence input modules, inc. for the moment just the
 * file sequence input module.
 *
 */
