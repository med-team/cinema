/* 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
*/
 
/* 
 * This software was written by Julian Selley (j.selley@man.ac.uk)
 * whilst at the University of Manchester as a Research Assistant. 
 *
 * The initial code base is copyright by the University of Manchester. 
 * Modifications to the initial code base are copyright of their 
 * respective authors, or their employers as appropriate. Authorship 
 * of the modifications may be determined from the ChangeLog placed 
 * at the end of this file
 */

package uk.ac.man.bioinf.apps.cinema.io; // Package name inserted by JPack

import uk.ac.man.bioinf.io.ParserException;


/**
 * FileSequenceInputParserExceptionHandler handles parser exceptions
 * to do with parsing files, and will display a dialog box.
 *
 *
 * Created: Thu Jun 15 23:39:37 2000
 *
 * @author Julian Selley
 * @version $Id: FileSequenceInputParserExceptionHandler.java,v 1.5 2001/04/11 17:04:42 lord Exp $
 */

public class FileSequenceInputParserExceptionHandler 
  extends AbstractSequenceInputParserExceptionHandler 
{
  public FileSequenceInputParserExceptionHandler() {}
  
  public void handleException(ParserException e) 
  {
    storeException(e);
  }
} // FileSequenceInputParserExceptionHandler



/*
 * ChangeLog
 * $Log: FileSequenceInputParserExceptionHandler.java,v $
 * Revision 1.5  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.4  2000/12/05 15:54:37  lord
 * Import rationalisation
 *
 * Revision 1.3  2000/08/01 14:56:01  jns
 * o ParseException to ParserException to be more logical and avoid confusion
 *
 * Revision 1.2  2000/06/30 09:33:24  jns
 * o now stores exception for printing at a later stage
 *
 * Revision 1.1  2000/06/16 09:41:55  jns
 * o initial code for a sequence input modules, inc. for the moment just the
 * file sequence input module.
 *
 */
