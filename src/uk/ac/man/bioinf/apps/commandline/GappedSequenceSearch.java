/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.apps.commandline; // Package name inserted by JPack
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import org.apache.regexp.RE;
import uk.ac.man.bioinf.analysis.regexp.GappedSequenceCharacterIterator;
import uk.ac.man.bioinf.io.ParserException;
import uk.ac.man.bioinf.io.ParserExceptionHandler;
import uk.ac.man.bioinf.io.parsers.PIRProteinAlignmentParser;
import uk.ac.man.bioinf.sequence.alignment.GappedSequence;
import uk.ac.man.bioinf.sequence.alignment.SequenceAlignment;
import uk.ac.man.bioinf.sequence.Sequences;


/**
 * SequenceSearch.java
 *
 * Searches through a SequenceAlignment specified on the command line for
 * all occurrences of the given regexp.
 *
 * Created: Mon Feb  5 14:48:30 2001
 *
 * @author Phillip Lord
 * @version $Id: GappedSequenceSearch.java,v 1.2 2001/02/20 17:55:12 lord Exp $
 */

public class GappedSequenceSearch 
{

  public static void main( String[] args ) throws Throwable
  {
    File inputFile = new File( args[ 0 ] );
    
    PIRProteinAlignmentParser parser = new PIRProteinAlignmentParser();
    
    ParserExceptionHandler exp = new ParserExceptionHandler(){
        public void handleException( ParserException e )
        {
          //e.printStackTrace();
        }
      };

    SequenceAlignment alignment = parser.parse
      ( new BufferedReader( new FileReader( inputFile ) ), exp );
    
    Sequences.printSequence( alignment.getSequenceAt( 1 ) );
    
    RE regexp = new RE( args[ 1 ] );
    
    for( int i = 1; i < (alignment.getNumberSequences() + 1); i++ ){
      scanSequence( alignment.getSequenceAt( i ), regexp );
    }
  } //end main method 
  
  private static void scanSequence( GappedSequence seq, RE regexp )
  {
    GappedSequenceCharacterIterator iter = new GappedSequenceCharacterIterator( seq );

    int index = 0;
    
    while( regexp.match( iter, index ) ){
      System.out.println();
      System.out.println( "Seq: " + seq.getIdentifier().getTitle() 
                          + " matches at " + regexp.getParenStart( 0 ) + " to " 
                          + regexp.getParenEnd( 0 ) + " with " + 
                          iter.substring
                          ( regexp.getParenStart( 0 ), 
                            (regexp.getParenEnd( 0 ) -
                             regexp.getParenStart( 0 ) ) ) );
      index = regexp.getParenEnd( 0 );
    }
    
    if( index == 0 ) System.out.println( "Seq: " + seq.getIdentifier().getTitle() + " does not match" );
  }
} // SequenceSearch



/*
 * ChangeLog
 * $Log: GappedSequenceSearch.java,v $
 * Revision 1.2  2001/02/20 17:55:12  lord
 * Removed extraneous import
 *
 * Revision 1.1  2001/02/19 17:23:49  lord
 * Initial checkin
 *
 */
