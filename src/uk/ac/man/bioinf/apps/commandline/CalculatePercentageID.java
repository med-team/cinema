/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.apps.commandline; // Package name inserted by JPack
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import uk.ac.man.bioinf.io.ParserException;
import uk.ac.man.bioinf.io.ParserExceptionHandler;
import uk.ac.man.bioinf.io.parsers.PIRProteinAlignmentParser;
import uk.ac.man.bioinf.sequence.Sequences;
import uk.ac.man.bioinf.sequence.alignment.SequenceAlignment;
import uk.ac.man.bioinf.analysis.misc.PercentageIDCalculator;


/**
 * CalculatePercentageID.java
 *
 *
 * Created: Tue Jan 30 17:42:44 2001
 *
 * @author Phillip Lord
 * @version $Id: CalculatePercentageID.java,v 1.1 2001/02/19 17:23:49 lord Exp $
 */

public class CalculatePercentageID 
{
  public static void main( String[] args ) throws Throwable
  {
    File inputFile = new File( args[ 0 ] );
    
    PIRProteinAlignmentParser parser = new PIRProteinAlignmentParser();
    
    ParserExceptionHandler exp = new ParserExceptionHandler(){
        public void handleException( ParserException e )
        {
          e.printStackTrace();
        }
      };

    SequenceAlignment alignment = parser.parse
      ( new BufferedReader( new FileReader( inputFile ) ), exp );
    
    
    PercentageIDCalculator calc = new PercentageIDCalculator( alignment );
    
    
    double[] variance = calc.getVariance();
    
    System.out.println( "Variance calculation:-" );
    
    for( int i = 0; i < variance.length; i ++ ){
      System.out.println( i + ": " + variance[ i ] );
    }
  } //end main method 
} // CalculatePercentageID



/*
 * ChangeLog
 * $Log: CalculatePercentageID.java,v $
 * Revision 1.1  2001/02/19 17:23:49  lord
 * Initial checkin
 *
 */
