/*
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf.database.fetch; // Package name inserted by JPack
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import uk.ac.man.bioinf.io.NarrowedInputStream;
import uk.ac.man.bioinf.io.ParserExceptionHandler;
import uk.ac.man.bioinf.io.SequenceInputParser;
import uk.ac.man.bioinf.io.parsers.FastaSequenceParser;
import uk.ac.man.bioinf.sequence.Sequence;


/**
 * ExpasySequenceRetriever.java
 *
 * Retrieves Swissprot sequence from Expasy. 
 *
 * For this class to work it makes an HTTP connection, and it
 * therefore works a lot better, if the HTTP proxy has been set
 * correctly. java -DproxySet=true -DproxyHost=proxyhost
 * [-DproxyPort=portNumber] is a good way to do this. 
 *
 *
 * It also makes explicit use of the URL format used at EXPASY, and I
 * have no idea how stable these are. 
 * 
 * I am retrieving 
 *
 * Created: Thu Feb  8 17:22:05 2001
 *
 * @author Phillip Lord
 * @version $Id: ExpasySequenceRetriever.java,v 1.2 2001/04/11 17:04:42 lord Exp $
 */

public class ExpasySequenceRetriever implements SequenceRetriever
{
  private String url;
  private SequenceInputParser parse;
  
  public ExpasySequenceRetriever() throws IOException
  {
    this( "http://ch.expasy.org" );
  }
  
  public ExpasySequenceRetriever( String baseURL ) throws IOException
  {
    url =  baseURL + "/cgi-bin/get-sprot-fasta?";
    this.parse = new FastaSequenceParser();
  }

  public Sequence retrieveSequence( String swissprot, ParserExceptionHandler exp ) throws IOException
  {
    return parse.parse
      ( new InputStreamReader( retrieveSequenceStream( swissprot ) ), exp );
  }
  
  public InputStream retrieveSequenceStream( String swissprot ) throws IOException
  {
    // the URL given is to a page with fasta format sequences. It has
    // other crap on it as well, but the sequence is the only thing in
    // a <PRE> tag so this should retrieve it. 
    return new BufferedInputStream 
      ( new NarrowedInputStream
        ( (new URL( url + swissprot ).openStream() ),
          "<PRE>".getBytes(), "</PRE>".getBytes() ) );
  }
  
  public void close()
  {
  }
  
  public void dispose()
  {
  }
  
}
/*
 * ChangeLog
 * $Log: ExpasySequenceRetriever.java,v $
 * Revision 1.2  2001/04/11 17:04:42  lord
 * Added License agreements to all code
 *
 * Revision 1.1  2001/02/19 17:39:49  lord
 * Initial checkin
 *
 */
