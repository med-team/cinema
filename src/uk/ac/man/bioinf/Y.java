/* 
 * This software was written by Phillip Lord (p.lord@hgmp.mrc.ac.uk)
 * whilst at the University of Manchester as a Pfizer post-doctoral 
 * Research Fellow. 
 *
 * The initial code base is copyright by Pfizer, or the University
 * of Manchester. Modifications to the initial code base are copyright
 * of their respective authors, or their employers as appropriate. 
 * Authorship of the modifications may be determined from the ChangeLog
 * placed at the end of this file
 */

package uk.ac.man.bioinf; // Package name inserted by JPack


/**
 * Y.java
 *
 * Class created entirely for the purpose of being tab
 * completable. Originally I used X.java, which is not buzz word
 * compliant, as I have forgotten about XML. Idiot!
 *
 * Created: Mon May  8 21:24:52 2000
 *
 * @author Phillip Lord
 * @version $Id: Y.java,v 1.1 2000/05/15 16:19:46 lord Exp $
 */

public class Y 
{

  public Y()
  {
   
  }
  
} // Y



/*
 * ChangeLog
 * $Log: Y.java,v $
 * Revision 1.1  2000/05/15 16:19:46  lord
 * Initial checkin
 *
 */
