

\documentclass[10pt, a4paper]{article}

\usepackage{times}
\usepackage{listings}
\usepackage{supertabular}
\usepackage{longtable}
\usepackage{alltt}
\usepackage{xspace}
\usepackage{url}
\usepackage[footnote]{acronym}

\newcommand{\Cinema}
{\ac{Cinema-MX}\xspace}
\newcommand{\MX}
{\ac{MX}\xspace}

\acrodef{Cinema-MX}{Colour INteractive Editor for Multiple Alignments}
\acrodef{MX}{Modular, eXtensible} 

\newcommand{\code}[1]{{\ttfamily\small#1}}


\title{The \acs{MX} architecture and its usage within 
  the \acs{Cinema-MX} application}
\lstset{language=Java, float, 
  captionpos=b, frame=tblr, basicstyle=\ttfamily\tiny}

\author{Phillip Lord} 

\begin{document}

\maketitle

\begin{abstract}
  This document describes the \MX architecture, and how it has been
  used in the \Cinema application. Its meant both as an overview of
  the architecture, and also a introduction for those who might wish
  to develop new modules for the application.
\end{abstract}


\section{Introduction}

The \MX architecture was designed to allow \Cinema (or other
applications) to be extended straightforwardly and simply. This is
achieved by splitting the application up into a series of small
modules. These modules can then be put together to form the end
application.

For the \Cinema application as well as allowing modularity we wanted
to gain extensibility, allowing the user to integrate new modules into
\Cinema, without requiring alteration of the main code base. To this
end, XML has been used to define which modules should be loaded, and
also to provide some configuration for these modules if required. 

In the following documentation I describe how the \MX architecture
works and what all the various bits do. If you can't be bothered to
read this, then there is a very quick ``step-by-step'' guide in
Section~\ref{sec:step-step}, which you might prefer.

\section{Requirements}

There were a number of requirements for the \MX architecture. 
\begin{itemize}
\item The architecture should be relatively \emph{light-weight}. That
  is it should be verbose, which would discourage programmers from
  making small modules. 
\item The basic architecture should be as customisable as possible.
\item The architecture should be \emph{fail fast}, that is it should
  break early, rather than run incorrectly. 
\item Dependency between modules should be explicit.
\item Loading should be as rapid as possible. 
\end{itemize}

\section{Identifiers}

The requirement that module dependency should be explicit creates a
problem. One module must be able to refer to another. However if that
class is referred to directly, then the class will be loaded
immediately, when we might want to defer it. 

To circumvent this problem, we use an identifier class, which can be
used to refer to the module, the interface for this is shown in
Listing \ref{lst:ident}.

\begin{lstlisting}[label=lst:ident, caption=The ModuleIdentifier Interface]{}
public interface ModuleIdentifier 
{
  public boolean isInterface();
  public String getClassName();
  public String getModuleName();
}// ModuleIdentifier
\end{lstlisting}

The two methods in this interface \code{public String getClassName()}
and \code{public String getModuleName()} define the link between a
module and a String which can be used to refer to it, which solves the
problem of referring to a Module (by a name) and a Class. 

One difficulty with this is that using a String (the module name) to
refer to a module is not type safe, and will result in errors when the
name is typed incorrectly. This difficulty is solved by extending the
class \code{AbstractEnumeratedModuleIdentifier}. This uses the a
variation of the theme of the Singleton design pattern to create an
identifier class which is relatively type safe. An example of its
usage from \Cinema is shown in Listing \ref{lst:enum}. This class uses
reflection to translate the variable name of its instances, into the
String that it uses for its Module name. 


\begin{lstlisting}[label=lst:enum, caption=The Abstract EnumeratedModuleIdentifier]{}
public class CinemaBootIdentifier extends AbstractEnumeratedModuleIdentifier
{
  private CinemaBootIdentifier( String className, String toString )
  {
    super( className, toString );
  }
  public static final CinemaBootIdentifier CINEMA_BOOT = 
    new CinemaBootIdentifier( "uk.ac.man.bioinf.apps.cinema.CinemaBoot",
                              "Main Cinema Boot Class" );
  public static final CinemaBootIdentifier CINEMA_SHARED =
    new CinemaBootIdentifier( "uk.ac.man.bioinf.apps.cinema.CinemaShared", 
                              "Shared Boot Class" );
} // CinemaBootIdentifier
\end{lstlisting}

The practical upshot of all of this is that to refer to for instance
the main Cinema Boot Module, the CINEMA\_BOOT instance can be used
directly. Its still possible to type this incorrectly of course, but
this will be detected at compile time. Its also possible to type the
class name incorrectly when writing the identifier, but at least this
needs to only be done once. 


\subsection{Abstract Identifiers}
\label{sec:abstract-identifiers}

There is a final method in the \code{ModuleIdentifier} class, called
\code{isInteface}. Normally this will return false, but it is possible
to define a module which acts like an abstract class, and delegates
its functionality to another module. This allows a degree of
polymorphism for modules. This feature is not used very widely within
\Cinema, as it turned out to be less needed than it appeared to be
during design. For most purposes its easier to use ``Optional
Modules'' which are described in Section \ref{sec:module-1}.

\section{The Module}
\label{sec:module-1}

The \code{ModuleIdentifier}interface refers to a Class name. This
Class should be a instance of the \code{Module} class. Its interface
is shown in Listing \ref{lst:module}. There are quite a few other
methods in this class, but most of them have been elided here for the
sake of simplicity. 

\begin{lstlisting}[label=lst:module,caption=The Module class]{}
public abstract class Module
{
  public void load() throws ModuleException {};
  public void start();
  public void destroy();
  public ModuleIdentifierList getRequiredIdentifiers();
  public Module getRequiredModule( ModuleIdentifier ident );
  public abstract String getVersion();
} // Module
\end{lstlisting}


We can divide the methods shown here into three groups.
\begin{itemize}
\item Those related to dependency with other modules.
\item Those directly to do with the function of the Module.
\item And the other method. 
\end{itemize}

Dealing with this in order. One of the requirements is for explicit
dependency between modules. This is provided by the
\code{public ModuleIdentifierList getRequiredIdentifiers()} method. In
this method any modules which this module depends on should be
identified. For example, see Listing \ref{lst:reqident}, which comes
from the \code{CinemaConsensusDisplay} module. This requires two other
modules, namely \code{CinemaConsensus}, which actually takes on the
task of calculating the consensus, and \code{CinemaSystemEvents}. The
consensus display is threaded, and needs to know when the application
is about to close, so that it can shut down cleanly. 

\begin{lstlisting}[label=lst:reqident, caption=An example of getRequiredIdentifiers]{}
public ModuleIdentifierList getRequiredIdentifiers()
{
  ModuleIdentifierList list = super.getRequiredIdentifiers();
  list.add( CinemaConsensusIdentifier.CINEMA_CONSENSUS );
  list.add( CinemaCoreIdentifier.CINEMA_SYSTEM_EVENTS );
  return list;
}
\end{lstlisting}

The second method \code{public Module
getRequiredModule(ModuleIdentifier ident)} actually allows access to
these modules. Listing \ref{lst:reqmod} comes again from the
\code{CinemaConsensusDisplay} class

\begin{lstlisting}[label=lst:reqmod, caption=An example of getRequiredModule]{}
if( queue == null ){
  queue = new InvokerInternalQueue
    ( (CinemaSystemEvents)getRequiredModule
        ( CinemaCoreIdentifier.CINEMA_SYSTEM_EVENTS ) );
}
\end{lstlisting}

The methods dealing with module functionality are hopefully largely
self-explanatory. When the module is initially loaded, unsurprising
the \code{public void load{}} method is called. During this time the
module should perform any initialisation that is required. The rule at
this time is that only initialisation that does not require other
modules should be performed, as this may well not be available yet. Or
in another way, while the \code{load()} method is running, there are
no guarantee's about what the \code{getRequiredModule()} method will
return (most likely it will return \code{null}).

Immediately after this time the \code{public void start()} method will
be called. At this time it is guaranteed that the
\code{getRequiredModule()} method will return any of the Modules
identified, and that further all of their \code{load()} methods will
have been called and have successfully completed. 

This is actually simpler than it sounds, but it's designed to cope
with a fairly complex dependency graph, and generally it just
works. No checking is performed to ensure that the graph is acyclic
The system will crash if you do this, but as per the design
requirement it will fail immediately. 

And finally the other method. This is meant to return a String
identifying the version of the Module. This is not widely used. No
specific semantics is required for this String, and generally the CVS
version keyword has been used. This might be removed at a later date. 


\subsection{Other methods}
\label{sec:other-methods}

There are a few other methods which are potentially of interest within
the \code{Module} interface. Firstly the \code{Module} provides access
to the \code{ModuleContext} class, which contains the \code{public
Module getModule(ModuleIdentifier ident)}. This enables access to
any other Modules in the system, beyond those named as required
modules. As they are not required they may be unavailable, so checking
the \code{ public boolean isModuleAvailable(ModuleIdentifier
identifier)} first is probably wise. 

And finally the \code{ModuleContext} class gives access to the
\code{public Object getConfig()} method. Of itself this is not that
useful. Its used internally to provide XML configuration though, which
is described in Section~\ref{sec:configuration}. 

\section{XML Loading and Configuration}
\label{sec:xml-load-conf}

The module system described so far provides a basic
architecture. However some mechanism needs to be available to define
which modules should be used. While it is possible to do this using
Java directly this would require the user to possess a Java compiler
to enable new modules, or reconfigure existing ones. By defining the
loading and configuration in XML, it's possible to do this using a
text editor. 


\subsection{Loading}
\label{sec:loading}

As described in Section \ref{sec:abstract-identifiers}, each module
referred to by a \code{ModuleIdentifier}. In order to load first the
ModuleIdentifier must be made available to the system. 
The \code{Identifier} directive can be used to this end, as shown in 
Listing \ref{lst:ident-dir}. This code assumes that the
\code{AbstractEnumeratedModuleIdentifier} has been used. For further
information see the \code{module.dtd} file in the source, which is
heavily documented. Its worth remembering that the
\code{AbstractEnumeratedModuleIdentifier} can contain identifiers for
many different modules, so relatively few of these statements are
needed. In \Cinema the modules are grouped into functional units. The
overhead of loading an \code{ModuleIdentifier} is very low (a few objects
for each additional one), so there is not really any problem in
loading these, even if the module is not used in the end. 

\begin{lstlisting}[language=HTML, caption=The Identifier Directive, label=lst:ident-dir]{}
<identifier>
  <enumeration>
    <class>uk.ac.man.bioinf.apps.cinema.utils.CinemaUtilityIdentifier</class>
  </enumeration>
</identifier>
\end{lstlisting}

Having made the \code{ModuleIdentifier} available, the module itself
can be loaded or started from within the XML, using the \code{load}
and \code{start} directive. The module is referred to by the name
returned by the identifier. For example, the code
in~\ref{lst:mod-load} shows loading and starting of the module that
provides the ``status bar'' in \Cinema. 

\begin{lstlisting}[language=HTML, caption=Loading a Module, label=lst:mod-load]{}
<load>
  <name>CINEMA_STATUS</name>
</load>
<start>
  <name>CINEMA_STATUS</name>
</start>
\end{lstlisting}


\subsection{Configuration}
\label{sec:configuration}

As well as loading modules its also possible to configure them. At the
current time, the configuration can come in one of two forms, which
are a properties list, or a tree structure. For example in
Listing~\ref{lst:props} the configuration which is used for input
module, is shown. It defines ``parsers'' which are used to output
sequence data. This configurability means that it's to possible to add
new ``parsers'' (perhaps inappropriately named as they are used for
both input and output of sequences) \Cinema, by altering the
configuration for this module. 

\begin{lstlisting}[label=lst:props, caption=Configuring Parsers]{}
<properties>
  <paramname>PIR</paramname>
    <value>uk.ac.man.bioinf.io.parsers.PIRProteinAlignmentParser</value>
  </param>
  <param>
    <paramname>MOT</paramname>
    <value>uk.ac.man.bioinf.io.parsers.MotProteinParser</value>
  </param>
</properties>
\end{lstlisting}

The second type of configuration is a simple tree structure, which
can be seen in Listing~\ref{lst:trees}. In this case the menu system
is being configured. In this case most of the configuration has been
elided. Generally speaking the properties configuration is to be
preferred because its much simpler, but the tree structure is much
more versatile, and means that tricks, such as providing additional
semantics to the keys of the properties lists are not necessary. 

\begin{lstlisting}[language=HTML, caption=Configuring the menu system, label=lst:trees]{}
<tree>
  <!-- The File Menu -->
  <node>
    <value>File</value>
    <node>
      <!-- Provides the open alignment -->
      <name>SEQ_INPUT</name>
    </node>
    <node>
      <!-- Provides the save alignment -->
      <name>SEQ_OUTPUT</name>
    </node>
    <node>
      <!-- Provides the exit menu -->
      <name>CINEMA_CORE_GUI</name>
    </node>
  </node>
</tree>
\end{lstlisting}

In order for the modules to access this configuration two methods (see
Listing~\ref{lst:config}) are provided by the \code{XMLModule} class
which all the \Cinema modules extend from. The standard Java
\code{Properties} class has been used here. Sadly Java does not
provide a standard Tree class, so a simple one has been provided. 

\begin{lstlisting}[label=lst:config, caption=The XML methods]{}
public Properties getConfigProperties();
public ConfigNode getConfigTree();
\end{lstlisting}

\section{\Cinema Modules}
\label{sec:cinema-modules}

Although the \MX architecture provides the ability to define modules,
and their interaction with each other, they do not provide any
specific Cinema functionality. It would be possible to provide all of
this functionality through the \MX system, by accessing specific
modules. However for convenience the \code{XMLModule} class has been
extended, to give access to a number of different method, through the
\code{CinemaModule} class.  
\footnote{As it happens, this has been implemented by accessing
  modules through the \MX architecture. All of the functionality
  provide by the \code{CinemaModule} is actually delegated to a
  \code{XMLModule} called \code{CinemaCoreView}, while all of the
  functionality provide by the more specific \code{CinemaGuiModule} is
  serviced by the \code{CinemaCoreGui} module.}
Additionally there is a more specific \code{CinemaCoreGui} class,
which gives access to more methods, which gives direct access to the
widgets used to build the basic \Cinema frame. Essentially the rule is
extend the \code{CinemaModule} unless you really need the
\code{CinemaGuiModule} as the latter is less likely to remain stable.


\begin{lstlisting}[caption=The Cinema Module, label=lst:cin-mod]{}
public abstract class CinemaModule extends XMLModule 
    implements AlignmentEventProvider
{
  public SequenceAlignment getSequenceAlignment()
  public void setSequenceAlignment( SequenceAlignment seq )
  public ColorMap getColorMap()
  public void setColorMap( ColorMap map )
  public AlignmentSelectionModel getAlignmentSelectionModel()
  public void setAlignmentSelectionModel( AlignmentSelectionModel model )
  public void setSequenceTitleColor( GappedSequence seq, Color colour )
  public void clearSequenceTitleColor( GappedSequence seq )
  public void sendStatusMessage( String message )
} // CinemaModule
\end{lstlisting}

The interface of the \code{CinemaModule} is shown in
Listing~\ref{lst:cin-mod}. Direct access is provided to the alignment
being shown, to the \code{ColorMap}.
\footnote{In the interests of international co-operation, it should be
  noted that the shorter spelling of the word ``Color'' was used. In
  the interests of flag waving jingoism, it should be noted that it
  hurt, it really hurt.}
The other methods give access to other information associated with the
view, including the Selection Model, the colour associated with the
sequence, and a status message which appears at the bottom of the
\Cinema frame.  


\subsection{\Cinema modules in use}
\label{sec:cinema-modules-use}

The \MX architecture works best if the individual \code{Module}'s are
relatively small. To give some idea of how this works in \Cinema, the
\code{Module}'s are described in Table~\ref{tab:modules}. Some of
these are very small (100 lines or so), and some quite a bit larger,
and use several other specialised classes. 

\begin{table}
  \begin{longtable}{|p{0.3\textwidth}|p{0.7\textwidth}|}
\hline\hline
CinemaModule & Function \\ \hline \hline
CinemaColorFactory & Generates \code{ColorMap} instances, and menu
items for their selection.\\ \hline
CinemaCommand\-LineParser & Parses the command line, and acts on it.\\ \hline
CinemaConsensus & Provides calculation of consensus sequences \\
\hline
CinemaGroupModule & Group sequences, for editing, viewing, and
analysis. \\ \hline
CinemaMenuBuilder & Generate menu items on the basis of the XML
configurations \\ \hline
CinemaMotifModule & The MotifManager dialog, and output \\\hline
CinemaMultiple\-ConsensusViewer & View consensus sequences of groups,
and there variance. \\\hline
CinemaPersist & Save information between instantiations of \Cinema
\\\hline
CinemaRegexp & Regular Expression searches down sequences \\\hline
CinemaResizeElements & Resize sequence cells \\\hline
CinemaSlaveViewerModule & Generate viewer frames for use by other
modules.\\\hline
CinemaSplash & Adds massive functionality in the form of a Splash
screen. \\\hline
FormGroupsByPrints & \textbf{Experimental!} Queries the PRINTS\_S
database to display the PRINTS motifs. \\\hline
PhylipInvoker & \textbf{Experimental!} Displays a phylogenetic
tree. \\
\hline\hline
CinemaGuiModule & \\ 
\hline\hline
AbstractSequence\-Input & Input sequence by some route. \\\hline
AbstractSequence\-Output & Output sequence by some route.\\\hline
CinemaColorSelector & Select \code{ColorMap}. \\\hline
CinemaConsensus\-Display & Displays the consensus sequences. \\\hline
CinemaCoreView & Support for CinemaModule class \\\hline
CinemaGo & Well everybody hacks some times \\\hline
CinemaHackMenu & More professionally displayed as ``in development''
in the menu system. Its quicker to add here than through XML. \\\hline
CineamMenuSystem & Uses CinemaMenuBuilder to build menu, then displays
it \\\hline
CinemaSequenceMenu & Right click on sequence button menu. \\\hline
CinemaStatusInformation & Prints ``cursor here'' information in status
bar. \\
\hline\hline
Others & \\
\hline\hline
CinemaCoreGui & Extends directly from the \code{Module} class, and
provides support for the CinemaGuiModule class. \\ \hline
XMLBootModule & Extends from directly from \code{Module}. Loads1 
parsers for reading XML configuration. \\ \hline
CinemaBoot & Extends from \code{XMLBootModule}. Additional support for
\Cinema XML loading. \\\hline
CinemaFile\-Persist & Extends from \code{CinemaPersist}. Save 
persistence data to file.\\\hline
FileSequenceInput & Extends from \code{AbstractSequenceInput}. 
Load from file. \\\hline
FileSequenceOutput & Extends from \code{AbstractSequenceOutput}. 
Save to file. \\\hline
  \end{longtable}
  \label{tab:modules}
  \caption{The modules in use within the \Cinema application. The
    modules are organized by their super class, either the
    \code{CinemaModule}, the \code{CinemaGuiModule}, or
    \code{Others}, which are extended from some other module. }
\end{table}


\section{Step by Step}
\label{sec:step-step}

\begin{enumerate}
\item Define a new identifier class. See for instance
  \code{\url{uk.ac.man.bioinf.apps.cinema.util.CinemaUtilityIdentifier}}.
  Make sure that you get the class name of your module correct. I tend
  to group my identifiers into logical blocks, as loading identifiers
  for modules that you don't actually use, has a very low overhead.
\item Define a new module class. This can be very small. Take for
  instance \code{
    \url{uk.ac.man.bioinf.apps.cinema.utils.CinemaSlaveViewerModule}}. This
  module is used only by other modules. The
  \code{\url{uk.ac.man.bioinf.apps.cinema.utils.CinemaStatusInformation}} is
  a ``user-facing'' module, that is also very simple. It provides the
  ``cursor information'' in the status bar.
\item Write, or add to a ``main.xml''. This file should be placed in
  ``./cinema/config''. This directory should already exist if you have
  run \Cinema. This is a simple XML file with a top level tag of
  ``configuration''. The DTD can be found in the \Cinema source.
\item Load your identifier code. Listing \ref{lst:ident-dir} shows how
  to do this. 
\item Actually load and start the module. Listing \ref{lst:mod-load}
  shows how to do this.
\end{enumerate}

And pretty much that should be it. 

\end{document}


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
