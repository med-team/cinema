# LaTeX2HTML 2K.1beta (1.47)
# Associate labels original text with physical files.


$key = q/sec:module-1/;
$external_labels{$key} = "$URL/" . q|node5.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:loading/;
$external_labels{$key} = "$URL/" . q|node8.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:step-step/;
$external_labels{$key} = "$URL/" . q|node12.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:cinema-modules-use/;
$external_labels{$key} = "$URL/" . q|node11.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:cinema-modules/;
$external_labels{$key} = "$URL/" . q|node10.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:configuration/;
$external_labels{$key} = "$URL/" . q|node9.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:xml-load-conf/;
$external_labels{$key} = "$URL/" . q|node7.html|; 
$noresave{$key} = "$nosave";

$key = q/tab:modules/;
$external_labels{$key} = "$URL/" . q|node11.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:abstract-identifiers/;
$external_labels{$key} = "$URL/" . q|node4.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:other-methods/;
$external_labels{$key} = "$URL/" . q|node6.html|; 
$noresave{$key} = "$nosave";

1;


# LaTeX2HTML 2K.1beta (1.47)
# labels from external_latex_labels array.


$key = q/sec:module-1/;
$external_latex_labels{$key} = q|4|; 
$noresave{$key} = "$nosave";

$key = q/lst:trees/;
$external_latex_labels{$key} = q|9|; 
$noresave{$key} = "$nosave";

$key = q/lst:props/;
$external_latex_labels{$key} = q|8|; 
$noresave{$key} = "$nosave";

$key = q/lst:mod-load/;
$external_latex_labels{$key} = q|7|; 
$noresave{$key} = "$nosave";

$key = q/lst:ident/;
$external_latex_labels{$key} = q|1|; 
$noresave{$key} = "$nosave";

$key = q/sec:cinema-modules/;
$external_latex_labels{$key} = q|6|; 
$noresave{$key} = "$nosave";

$key = q/tab:modules/;
$external_latex_labels{$key} = q|6.1|; 
$noresave{$key} = "$nosave";

$key = q/lst:reqmod/;
$external_latex_labels{$key} = q|5|; 
$noresave{$key} = "$nosave";

$key = q/sec:other-methods/;
$external_latex_labels{$key} = q|4.1|; 
$noresave{$key} = "$nosave";

$key = q/lst:enum/;
$external_latex_labels{$key} = q|2|; 
$noresave{$key} = "$nosave";

$key = q/lst:reqident/;
$external_latex_labels{$key} = q|4|; 
$noresave{$key} = "$nosave";

$key = q/sec:step-step/;
$external_latex_labels{$key} = q|7|; 
$noresave{$key} = "$nosave";

$key = q/sec:loading/;
$external_latex_labels{$key} = q|5.1|; 
$noresave{$key} = "$nosave";

$key = q/sec:cinema-modules-use/;
$external_latex_labels{$key} = q|6.1|; 
$noresave{$key} = "$nosave";

$key = q/lst:cin-mod/;
$external_latex_labels{$key} = q|11|; 
$noresave{$key} = "$nosave";

$key = q/sec:xml-load-conf/;
$external_latex_labels{$key} = q|5|; 
$noresave{$key} = "$nosave";

$key = q/lst:ident-dir/;
$external_latex_labels{$key} = q|6|; 
$noresave{$key} = "$nosave";

$key = q/sec:configuration/;
$external_latex_labels{$key} = q|5.2|; 
$noresave{$key} = "$nosave";

$key = q/lst:config/;
$external_latex_labels{$key} = q|10|; 
$noresave{$key} = "$nosave";

$key = q/sec:abstract-identifiers/;
$external_latex_labels{$key} = q|3.1|; 
$noresave{$key} = "$nosave";

$key = q/lst:module/;
$external_latex_labels{$key} = q|3|; 
$noresave{$key} = "$nosave";

1;

