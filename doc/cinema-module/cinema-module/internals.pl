# LaTeX2HTML 2K.1beta (1.47)
# Associate internals original text with physical files.


$key = q/sec:module-1/;
$ref_files{$key} = "$dir".q|node5.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:loading/;
$ref_files{$key} = "$dir".q|node8.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:step-step/;
$ref_files{$key} = "$dir".q|node12.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:cinema-modules-use/;
$ref_files{$key} = "$dir".q|node11.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:cinema-modules/;
$ref_files{$key} = "$dir".q|node10.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:configuration/;
$ref_files{$key} = "$dir".q|node9.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:xml-load-conf/;
$ref_files{$key} = "$dir".q|node7.html|; 
$noresave{$key} = "$nosave";

$key = q/tab:modules/;
$ref_files{$key} = "$dir".q|node11.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:abstract-identifiers/;
$ref_files{$key} = "$dir".q|node4.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:other-methods/;
$ref_files{$key} = "$dir".q|node6.html|; 
$noresave{$key} = "$nosave";

1;

