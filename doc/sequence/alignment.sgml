
<section>
  <title>Sequence Alignments</title>

  <section>
    <title>Design</title>
    
    <para>Most of the work for modelling a sequence alignment has
      already been done in the shape of the
      <link linkend="gappable.and.editable">Gapped Sequence</link>.
      From here it becomes relatively easy to model
      an alignment, as a simple list of GappedSequences, with the
      addition of an event model for describing those changes in the
      Sequences which are a part of the alignment. </para>

    <para>The SequenceAlignment interface therefore contains the
      following methods for adding and removing Sequences

      <programlisting>
    public void addSequence(GappedSequence seq, int inset) 
        throws AlignmentVetoException;
  
    public GappedSequence removeSequence(int seqIndex) 
        throws AlignmentVetoException;
      </programlisting>

      Insets have been provided so that all the sequences do not have
      to start at the same point. The alternative was to add Gaps at
      the beginning of the GappedSequences. This also means that we
      need methods to manipulate the insets separately. These are

      <programlisting>
    public void setInset(int seqIndex, int size) throws AlignmentVetoException;
    
    public int getInset(int seqIndex);
      </programlisting>
    </para>

    <para>Next we have a series of informational methods. As with
      Sequences, it seems appropriate to enforce "type-checking" on
      alignments as normally we will want to align sequencs of the
      same type. We use the same mechanism as with Sequences.

      <programlisting>
    public SequenceType getSequenceType();
      </programlisting>

      We also need mechanisms to access the individual sequences,
      their overall size, and number. 

      <programlisting>
    public GappedSequence getSequenceAt(int index) throws NoSuchSequenceException;
    
    public int getSequenceIndex(GappedSequence seq);
      
    public int getNumberSequences();
    
    public int getLength();
      </programlisting>
    </para>
  </section>

  <section>
    <title>Events</title>

    <para>The SequenceAlignment is also required to signal
      events. These have been classifed into two groups, namely the
      <classname>AlignmentEvents</classname>, which describe changes
      such as the addition or removal of Sequences. These are defined
      in the <classname>AlignmentEventProvider</classname>
      interface. </para>

    <para>The second type of event is the
      <classname>SequenceEvent</classname> which are the events
      signalled by the <classname>GappedSequence</classname>
      itself. In order to simply the process of listening to all its
      sequences an alignment acts as a proxy for all of its
      sequences. It implements the
      <classname>SequenceEventProvider</classname> interface, and
      any events which the individual
      <classname>GappedSequences</classname> signal will be passed
      onwards. This means that SequenceListeners do not have to listen
      for addition and removal of Sequences, and add and remove
      themselves as listeners when this happens. </para>
  </section>

  <section>
    <title>The default implementation</title>

    <para>As with the Sequence implementation the SequenceAlignment
      has uses the standard Java API's to store its data, in this case
      an <classname>ArrayList</classname>. This class by default
      listens directly to all of the sequences of which it is
      comprised and uses these to determine when length changes
      occur.
    </para>
  </section>

  <section>
    <title>Support classes</title>

    <para>Several support classes have been added to make dealing with
      <classname>SequenceAlignment</classname> simpler. The first of
      these is the <classname>AlignmentColumn</classname>. Because the
      individual <classname>GappedSequences</classname> have staggered
      start points dealing with an individual column requires
      calculating the inset of a sequence, and then checking the size
      to see whether the column is before or after the sequence. The
      <classname>AlignmentColumn</classname> does this work.</para>
    
    <![%draft;[
    <para>
      <emphasis>We really need to move the geom package into the
	alignment package. This is a bit of a pain in the ass, but its
	just not appropriate at the moment.</emphasis>
    </para>
    ]]>
    
    <para>Secondly there are a series of classes i4n the
      &quot;geom&quot; package which are designed to refer to regions
      of sequences. This package is modelled on the equivalent classes
      in the java.awt package, namely <classname>Point</classname>,
      <classname>Dimension</classname>, and
      <classname>Rectangle</classname>.</para>
  </section>
</section>
<!--
  We need to define local variables to enable psgml mode to work out
  what DTD we are using, and also where we are in the document.
   
  Local Variables: ***
  sgml-parent-document:("sequence.sgml" "article" "section" ) ***
  End: ***
  -->


